README
------

For general usage see the quick help. Each of the programs contain further
information in its doc directory. Here you will find a short description
of each program, how to create the advanced documentation from source code,
which are the compile options available and how to generate a profile for
each program.

PROGRAMS
--------

* **wseqs**: prints clustal weights from a fasta file to standard output.
* **paloma**: computes Partial Local Multiple Alignment (PLMA) from a set of sequences.
* **plma2dot**: generates a dot file for graphical output from a PLMA.
* **protobuild**: generates a Protomata model fromGenerates a protein family model from a fasta file. a PLMA.
* **protoscan**: scan a set of sequences for scores using a Protomata model.
* **protoparse**: parse a set of sequences for scores using a Protomata model.
* **protoalign**:	align a set of sequences using a Protomata model.
* **protomata-learner**: generates a protein family model from a fasta file.

COMPILATION
-----------

The oldest known version of GCC with which it compiled is 4.4.0.
For compiling all programs, type make in the src directory.
For testing, the UnitTest++ library should be installed first.

Available options are:

	make config			creates a Makefile.config from the template edit this file in order to fit your needs
	make				build
	make test			perform automated tests for detecting errors
	make install		automated install
	make clean			removes all created object files
	make uninstall		clean and removes all installed binaries
	make distclean		clean; uninstall and removes config files
	make dist			make distclean; make; make install
	make doc			create doxygen code documentation

PROFILING
---------

For gprof, compile with the -pg option. Then execute the program, it will 
generate an output file named gmon.out by default. This file will only be 
generated if the program ends by returning from main or by calling exit. After
this, execute

	gprof name_program gmon.out > output_file

which creates the flat profile.