#!/bin/bash

REQUIERED_ARGS=3
E_BADARGS=65

if [ $# -lt $REQUIERED_ARGS ]
then
	echo "Usage: `basename $0` bin_path_1 bin_path_2 fasta_file <output_path>"
	echo "Execute all protomata programs for each version and compare results."
	echo "Binary paths should contain all executable files."
	exit $E_BADARGS
fi

# program arguments
PATH_1=$1
PATH_2=$2
FASTA=$3
OUTPUT_PATH=$4

# by default output path is the current one
if [ ! -n "$OUTPUT_PATH" ]; then
	OUTPUT_PATH="."
else
	# create the directory if it doesn't exists
	if [ ! -d "$OUTPUT_PATH" ]; then
		mkdir $OUTPUT_PATH
	fi
fi

# parameters should be modified here
PALOMA_PARAMETERS="-t 5"
PROTOBUILD_PARAMETERS="-p 0.35 --pseudocounts Dirichlet --components byst-4.5-0-3.9comp"
PROTOSCAN_PARAMETERS=""
PROTOPARSE_PARAMETERS=""
PROTOALIGN_PARAMETERS=""
# these parameters are independent of the others, comparissons are just within protomata-learner
PROTOMATA_PARAMETERS="-t 5 -p 0.35 --pseudocounts Dirichlet --components byst-4.5-0-3.9comp"

echo -n "Checking paloma... "
$PATH_1/paloma -i $FASTA $PALOMA_PARAMETERS -o $OUTPUT_PATH/1.plma
$PATH_2/paloma -i $FASTA $PALOMA_PARAMETERS -o $OUTPUT_PATH/2.plma
if ! paloma/plma_diff/plmadiff $OUTPUT_PATH/1.plma $OUTPUT_PATH/2.plma; then
	echo "PLMA files produced by paloma are different."
else
	echo "ok"
fi

echo -n "Checking protobuild... "
$PATH_1/protobuild -i $OUTPUT_PATH/1.plma $PROTOBUILD_PARAMETERS -o $OUTPUT_PATH/1.proto
$PATH_2/protobuild -i $OUTPUT_PATH/2.plma $PROTOBUILD_PARAMETERS -o $OUTPUT_PATH/2.proto
if ! protobuild/proto_diff/protodiff $OUTPUT_PATH/1.proto $OUTPUT_PATH/2.proto; then
	echo "Proto files produced by protomata-learner are different."
else
	echo "ok"
fi

echo -n "Checking protoscan... "
$PATH_1/protoscan -i $OUTPUT_PATH/1.proto -s $FASTA $PROTOSCAN_PARAMETERS -o $OUTPUT_PATH/1.scan
$PATH_2/protoscan -i $OUTPUT_PATH/2.proto -s $FASTA $PROTOSCAN_PARAMETERS -o $OUTPUT_PATH/2.scan
if ! diff -q -I '^#' $OUTPUT_PATH/1.scan $OUTPUT_PATH/2.scan ; then
	echo "There are differences in scans. Run 'diff -I '^#' 1.scan 2.scan' to see them."
else
	echo "ok"
fi

echo -n "Checking protoparse... "
$PATH_1/protoparse -i $OUTPUT_PATH/1.proto -s $FASTA $PROTOPARSE_PARAMETERS -o $OUTPUT_PATH/1.parse
$PATH_2/protoparse -i $OUTPUT_PATH/2.proto -s $FASTA $PROTOPARSE_PARAMETERS -o $OUTPUT_PATH/2.parse
if ! diff -q -I '^#' $OUTPUT_PATH/1.parse $OUTPUT_PATH/2.parse ; then
	echo "There are differences in parses. Run 'diff -I '^#' 1.parse 2.parse' to see them."
else
	echo "ok"
fi

echo -n "Checking protoalign... "
$PATH_1/protoalign -i $OUTPUT_PATH/1.proto -s $FASTA $PROTOALIGN_PARAMETERS -o $OUTPUT_PATH/1.align
$PATH_2/protoalign -i $OUTPUT_PATH/2.proto -s $FASTA $PROTOALIGN_PARAMETERS -o $OUTPUT_PATH/2.align
if ! paloma/plma_diff/plmadiff $OUTPUT_PATH/1.align $OUTPUT_PATH/2.align; then
	echo "PLMA files produced by protoalign are different."
else
	echo "ok"
fi

echo -n "Checking protomata-learner... "
$PATH_1/protomata-learner -i $FASTA -s $FASTA $PROTOMATA_PARAMETERS -b PL --tag 1 --output-path $OUTPUT_PATH/ > /dev/null
$PATH_2/protomata-learner -i $FASTA -s $FASTA $PROTOMATA_PARAMETERS -b PL --tag 2 --output-path $OUTPUT_PATH/ > /dev/null
if ! paloma/plma_diff/plmadiff $OUTPUT_PATH/PL1_plma.xml $OUTPUT_PATH/PL2_plma.xml; then
	echo "PLMA files produced by protomata-learner are different."
else
	if ! protobuild/proto_diff/protodiff $OUTPUT_PATH/PL1_proto.xml $OUTPUT_PATH/PL2_proto.xml; then
		echo "Proto files produced by protomata-learner are different."
	else
		if ! diff -q -I '^#' $OUTPUT_PATH/PL1_scan.txt $OUTPUT_PATH/PL2_scan.txt ; then
			echo "There are differences in scans. Run 'diff -I '^#' 1.scan 2.scan' to see them."
		else
			echo "ok"
		fi
	fi
fi
