
#include "columns/AttributedColumn.h"
#include "ProtoParser.h"

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		std::cout << "usage: " << argv[0] << " proto1 proto2" << std::endl;
		exit(1);
	}

	typedef ProtoParser<double, AttributedColumn> ProtoParserType;
	typedef ProtoParserType::ProtoGraphType ProtoGraphType;

	ProtoParserType proto1(argv[1]);
	ProtoParserType proto2(argv[2]);

	// for checking both sequences and partition, the strategy is to check if sizes are the same
	// and then check if every sequence (partition element) of the first PLMA is found on the second

	// get each set of sequences used to build each PLMA
	const ProtoGraphType& pg1 = proto1.parse_zones_edges();
	const ProtoGraphType& pg2 = proto2.parse_zones_edges();

	// check that they have the same number of zones
	assert(pg1.zones() == pg2.zones());

	// check that they have the same number of transitions
	assert(pg1.transitions() == pg2.transitions());

	// check that they have the same number of zones
	assert(pg1.zones() == pg2.zones());

	ProtoGraphType::zone_const_iterator zit1 = pg1.zone_begin();
	ProtoGraphType::zone_const_iterator zit2 = pg2.zone_begin();
	while(zit1 != pg1.zone_end() && zit2 != pg2.zone_end())
	{
		const ProtoGraphType::ZoneType& zone1 = *zit1;
		const ProtoGraphType::ZoneType& zone2 = *zit2;

		// check that they have the same number of columns
		assert(zone1.size() == zone2.size());

		ProtoGraphType::ZoneType::column_const_iterator cit1 = zone1.columns_begin();
		ProtoGraphType::ZoneType::column_const_iterator cit2 = zone2.columns_begin();
		while(cit1 != zone1.columns_end() && cit2 != zone2.columns_end())
		{
			const ProtoGraphType::ColumnType& col1 = *cit1;
			const ProtoGraphType::ColumnType& col2 = *cit2;

			// check that they have the same number of amino acids
			assert(col1.aminoacids() == col2.aminoacids());

			ProtoGraphType::ColumnType::counts_const_iterator vit1 = col1.counts_begin();
			ProtoGraphType::ColumnType::counts_const_iterator vit2 = col2.counts_begin();
			while(vit1 != col1.counts_end() && vit2 != col2.counts_end())
			{
				const char aa1 = (*vit1).first;
				const char aa2 = (*vit2).first;

				// check amino acids
				assert(aa1 == aa2);

				const double w1 = (*vit1).second;
				const double w2 = (*vit2).second;

				// check weights
				assert(w1 == w2);

				vit1++; vit2++;
			}
			cit1++; cit2++;
		}
		zit1++; zit2++;
	}
}
