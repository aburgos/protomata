
#include <UnitTest++.h>
#include "ProtoGraphBuilder.h"
#include "generalized_sets/GlobalConsistentGA.h"

TEST(ProtoGraphBuilder)
{
	typedef ProtoGraphBuilder<Alignment_UFImp, double, SiteColumn> ProtoGraphBuilderType;
	typedef ProtoGraphBuilderType::ProtoGraphType ProtoGraphType;
	typedef ProtoGraphBuilderType::TransitionType::TransitionGraph TransitionGraph;

	const std::string test_path(TESTPATH);
	const std::string fasta_file(test_path+"/data/p450_homstrad.fasta");

	const MultiSequence ms(fasta_file);
	GlobalConsistentGA<Alignment_UFImp> ga(ms);

	// build a generalized alignment and check that after building the protograph, every expected relation is in it
	// these assures equality since we check that the number of built zones and transitions are the same
	// as the ones we defined

	//TEST(GeneralizedAlignmentConstruction)
	{
		// first site partition element
		Alignment_UFImp al1(ms);
		al1.addMatching(SitePairRelation(std::make_pair(Site(0, 5), Site(1, 5)), 0));
		al1.addMatching(SitePairRelation(std::make_pair(Site(0, 5), Site(2, 5)), 0));
		al1.addMatching(SitePairRelation(std::make_pair(Site(0, 5), Site(3, 5)), 0));
		al1.addMatching(SitePairRelation(std::make_pair(Site(0, 5), Site(4, 5)), 0));
		al1.addMatching(SitePairRelation(std::make_pair(Site(0, 5), Site(5, 5)), 0));
		CHECK(ga.addMatching(al1));

		// second site partition element
		Alignment_UFImp al2(ms);
		al2.addMatching(SitePairRelation(std::make_pair(Site(0, 10), Site(1, 6)), 0));
		al2.addMatching(SitePairRelation(std::make_pair(Site(0, 10), Site(3, 6)), 0));
		al2.addMatching(SitePairRelation(std::make_pair(Site(0, 10), Site(5, 6)), 0));
		CHECK(ga.addMatching(al2));

		// third site partition element
		Alignment_UFImp al3(ms);
		al3.addMatching(SitePairRelation(std::make_pair(Site(0, 7), Site(2, 6)), 0));
		al3.addMatching(SitePairRelation(std::make_pair(Site(0, 7), Site(4, 6)), 0));
		CHECK(ga.addMatching(al3));

		// fourth site partition element
		Alignment_UFImp al4(ms);
		al4.addMatching(SitePairRelation(std::make_pair(Site(0, 15), Site(2, 7)), 0));
		CHECK(ga.addMatching(al4));
	}

	ProtoGraphBuilderType builder(ms, ga, 0, 0);
	const ProtoGraphType& pg = builder.proto_graph();

	//TEST(Initialization)
	{
		CHECK_EQUAL(pg.zones(), (unsigned)6);
		CHECK_EQUAL(pg.transitions(), (unsigned)10);
	}

	// define the set of expected relations
	typedef std::pair<unsigned int, unsigned int> ZonePair;
	std::map<ZonePair, ProtoGraphBuilderType::TransitionType> expected_transitions;

	//TEST(ExpectedProtoGraphConstruction)
	{
		// first expected zone definition
		ProtoGraphBuilderType::BeginZoneType zone1(ms);

		// second expected zone definition
		ProtoGraphBuilderType::EndZoneType zone2(ms);

		// third expected zone definition
		ProtoGraphBuilderType::ZoneType zone3(3, ms);
		SitePartitionElement spe31;
		spe31.insert(Site(0, 5)); spe31.insert(Site(1, 5)); spe31.insert(Site(2, 5)); spe31.insert(Site(3, 5));
		spe31.insert(Site(4, 5)); spe31.insert(Site(5, 5));
		ProtoGraphBuilderType::ColumnType col31(1, ms, spe31);
		CHECK(zone3.add(col31));

		// fourth expected zone definition
		ProtoGraphBuilderType::ZoneType zone4(4, ms);
		SitePartitionElement spe41;
		spe41.insert(Site(0, 10)); spe41.insert(Site(1, 6)); spe41.insert(Site(3, 6)); spe41.insert(Site(5, 6));
		ProtoGraphBuilderType::ColumnType col41(1, ms, spe41);
		CHECK(zone4.add(col41));

		// fifth expected zone definition
		ProtoGraphBuilderType::ZoneType zone5(5, ms);
		SitePartitionElement spe51;
		spe51.insert(Site(0, 7)); spe51.insert(Site(2, 6)); spe51.insert(Site(4, 6));
		ProtoGraphBuilderType::ColumnType col51(1, ms, spe51);
		CHECK(zone5.add(col51));

		// sixth expected zone definition
		ProtoGraphBuilderType::ZoneType zone6(6, ms);
		SitePartitionElement spe61;
		spe61.insert(Site(0, 15)); spe61.insert(Site(2, 7));
		ProtoGraphBuilderType::ColumnType col61(1, ms, spe61);
		CHECK(zone6.add(col61));

		// define first expected transition (between zone1 and zone2)
		TransitionGraph tg1;
		for(unsigned int i = 6; i < 12; i++)
			tg1.addEdge(Segment(i, 0, 0), Segment(i, ms[i].length(), 0), Segment(i, 0, ms[i].length()));
		ProtoGraphBuilderType::TransitionType transition1(1, zone1.id(), zone2.id(), tg1, ms);
		// add first expected transition
		expected_transitions[std::make_pair(zone1.id(), zone2.id())] = transition1;

		// define second expected transition (between zone1 and zone3)
		TransitionGraph tg2;
		for(unsigned int i = 0; i < 6; i++)
			tg2.addEdge(Segment(i, 0, 0), Segment(i, 5, 1), Segment(i, 0, 5));
		ProtoGraphBuilderType::TransitionType transition2(2, zone1.id(), zone3.id(), tg2, ms);
		// add second expected transition
		expected_transitions[std::make_pair(zone1.id(), zone3.id())] = transition2;

		// define third expected transition (between zone3 and zone4)
		TransitionGraph tg3;
		tg3.addEdge(Segment(0, 5, 1), Segment(0, 7, 1), Segment(0, 6, 1));
		tg3.addEdge(Segment(2, 5, 1), Segment(2, 6, 1), Segment(2, 6, 0));
		tg3.addEdge(Segment(4, 5, 1), Segment(4, 6, 1), Segment(4, 6, 0));
		ProtoGraphBuilderType::TransitionType transition3(3, zone3.id(), zone4.id(), tg3, ms);
		// add third expected transition
		expected_transitions[std::make_pair(zone3.id(), zone4.id())] = transition3;

		// define fourth expected transition (between zone3 and zone5)
		TransitionGraph tg4;
		tg4.addEdge(Segment(1, 5, 1), Segment(1, 6, 1), Segment(1, 6, 0));
		tg4.addEdge(Segment(3, 5, 1), Segment(3, 6, 1), Segment(3, 6, 0));
		tg4.addEdge(Segment(5, 5, 1), Segment(5, 6, 1), Segment(5, 6, 0));
		ProtoGraphBuilderType::TransitionType transition4(4, zone3.id(), zone5.id(), tg4, ms);
		// add fourth expected transition
		expected_transitions[std::make_pair(zone3.id(), zone5.id())] = transition4;

		// define fifth expected transition (between zone4 and zone2)
		TransitionGraph tg5;
		tg5.addEdge(Segment(4, 6, 1), Segment(4, ms[4].length(), 0), Segment(4, 7, ms[4].length() - 6 - 1));
		ProtoGraphBuilderType::TransitionType transition5(5, zone4.id(), zone2.id(), tg5, ms);
		// add fifth expected transition
		expected_transitions[std::make_pair(zone4.id(), zone2.id())] = transition5;

		// define sixth expected transition (between zone4 and zone5)
		TransitionGraph tg6;
		tg6.addEdge(Segment(0, 7, 1), Segment(0, 10, 1), Segment(0, 8, 2));
		ProtoGraphBuilderType::TransitionType transition6(6, zone4.id(), zone5.id(), tg6, ms);
		// add sixth expected transition
		expected_transitions[std::make_pair(zone4.id(), zone5.id())] = transition6;

		// define seventh expected transition (between zone4 and zone6)
		TransitionGraph tg7;
		tg7.addEdge(Segment(2, 6, 1), Segment(2, 7, 1), Segment(2, 7, 0));
		ProtoGraphBuilderType::TransitionType transition7(7, zone4.id(), zone6.id(), tg7, ms);
		// add seventh expected transition
		expected_transitions[std::make_pair(zone4.id(), zone6.id())] = transition7;

		// define eighth expected transition (between zone5 and zone2)
		TransitionGraph tg8;
		tg8.addEdge(Segment(1, 6, 1), Segment(1, ms[1].length(), 0), Segment(1, 7, ms[1].length() - 6 - 1));
		tg8.addEdge(Segment(3, 6, 1), Segment(3, ms[3].length(), 0), Segment(3, 7, ms[3].length() - 6 - 1));
		tg8.addEdge(Segment(5, 6, 1), Segment(5, ms[5].length(), 0), Segment(5, 7, ms[5].length() - 6 - 1));
		ProtoGraphBuilderType::TransitionType transition8(8, zone5.id(), zone2.id(), tg8, ms);
		// add seventh expected transition
		expected_transitions[std::make_pair(zone5.id(), zone2.id())] = transition8;

		// define ninth expected transition (between zone5 and zone6)
		TransitionGraph tg9;
		tg9.addEdge(Segment(0, 10, 1), Segment(0, 15, 1), Segment(0, 11, 4));
		ProtoGraphBuilderType::TransitionType transition9(9, zone5.id(), zone6.id(), tg9, ms);
		// add ninth expected transition
		expected_transitions[std::make_pair(zone5.id(), zone6.id())] = transition9;

		// define tenth expected transition (between zone6 and zone2)
		TransitionGraph tg10;
		tg10.addEdge(Segment(0, 15, 1), Segment(0, ms[0].length(), 0), Segment(0, 16, ms[0].length() - 15 - 1));
		tg10.addEdge(Segment(2, 7, 1), Segment(2, ms[2].length(), 0), Segment(2, 8, ms[2].length() - 7 - 1));
		ProtoGraphBuilderType::TransitionType transition10(10, zone6.id(), zone2.id(), tg10, ms);
		// add tenth expected transition
		expected_transitions[std::make_pair(zone6.id(), zone2.id())] = transition10;
	}

	//TEST(ExpectedRelations)
	{
		// check that every relation of the protograph is in the expected relations set
		for(ProtoGraphType::relation_const_iterator rit = pg.relation_begin(); rit != pg.relation_end(); ++rit)
		{
			const ProtoGraphType::TransitionsGraph::relation& r = *rit;
			ZonePair zone_pair = std::make_pair(r.first(), r.second());
			// the protograph relation must exist in the defined expected relations
			CHECK(expected_transitions.find(zone_pair) != expected_transitions.end());

			TransitionGraph current_transition_graph = r.value().transitions();
			TransitionGraph expected_transition_graph = expected_transitions[zone_pair].transitions();
			ProtoGraphBuilderType::TransitionType::relation_const_iterator it;
			for(it = current_transition_graph.relation_begin(); it != current_transition_graph.relation_end(); ++it)
			{
				const TransitionGraph::relation& rt = *it;
				TransitionGraph::relation_iterator eit = expected_transition_graph.hasEdge(rt.first(), rt.second());
				// make sure the edge exists
				CHECK(eit != expected_transition_graph.relation_end());
				// make sure the value between vertices (a segment) are equal
				CHECK_EQUAL((*eit).value(), rt.value());
			}
		}
	}
}
