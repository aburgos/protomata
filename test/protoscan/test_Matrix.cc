
#include <UnitTest++.h>
#include "Matrix.h"

SUITE(Matrix)
{
	typedef Matrix<unsigned int, unsigned int, double> CommonMatrix;

	CommonMatrix m;

	TEST(Initialization)
	{
		CHECK_EQUAL(m.nrows(), (unsigned)0);
		CHECK_EQUAL(m.ncols(), (unsigned)0);
		CHECK(!m.exists(0, 0));
		CHECK(m.row_begin() == m.row_end());
		CHECK(m.col_begin() == m.col_end());
	}

	TEST(AddValue)
	{
		m(1, 1) = 1.4;
		CHECK_EQUAL(m(1, 1), 1.4);
		CHECK_EQUAL(m.nrows(), (unsigned)1);
		CHECK_EQUAL(m.ncols(), (unsigned)1);
		CHECK(m.exists(1, 1));
		CHECK(!m.exists(0, 0));
		CHECK(!m.exists(0, 1));
		CHECK(!m.exists(1, 0));
		CHECK(m.row_begin() != m.row_end());
		CHECK(m.col_begin() != m.col_end());
	}

	TEST(ExtendMatrix)
	{
		m(1, 2) = 0.8;
		m(2, 2) = 0.4;
		m(1, 3) = 1.3;
		CHECK_EQUAL(m(1, 2), 0.8);
		CHECK_EQUAL(m(2, 2), 0.4);
		CHECK_EQUAL(m(1, 3), 1.3);
		CHECK_EQUAL(m.nrows(), (unsigned)2);
		CHECK_EQUAL(m.ncols(), (unsigned)3);
		CHECK(m.exists(1, 1));
		CHECK(m.exists(1, 2));
		CHECK(m.exists(1, 3));
		CHECK(m.exists(2, 2));
		CHECK(!m.exists(0, 0));
		CHECK(!m.exists(0, 1));
		CHECK(!m.exists(1, 0));
		CHECK(!m.exists(1, 4));
		CHECK(!m.exists(2, 0));
		CHECK(m.exists(2, 1));
		CHECK(m.exists(2, 3));
		CHECK(!m.exists(2, 4));
		CHECK(m.row_begin() != m.row_end());
		CHECK(m.col_begin() != m.col_end());
	}

	TEST(RowIterator)
	{
		for(typename CommonMatrix::row_const_iterator rit = m.row_begin(); rit != m.row_end(); ++rit)
		{
			const std::pair<unsigned int, unsigned int>& p = *rit;
			CHECK(p.first == (unsigned)1 || p.first == (unsigned)2);
		}
	}

	TEST(ColIterator)
	{
		for(typename CommonMatrix::col_const_iterator rit = m.col_begin(); rit != m.col_end(); ++rit)
		{
			const std::pair<unsigned int, unsigned int>& p = *rit;
			CHECK(p.first == (unsigned)1 || p.first == (unsigned)2 || p.first == (unsigned)3 || p.first == (unsigned)4);
		}
	}

	TEST(CopyConstructor)
	{
		CommonMatrix copy(m);
		m(1, 1) = 1.4;
		m(1, 2) = 0.8;
		m(2, 2) = 0.4;
		m(1, 3) = 1.3;
		CHECK_EQUAL(m(1, 1), 1.4);
		CHECK_EQUAL(m(1, 2), 0.8);
		CHECK_EQUAL(m(2, 2), 0.4);
		CHECK_EQUAL(m(1, 3), 1.3);
		CHECK_EQUAL(m.nrows(), (unsigned)2);
		CHECK_EQUAL(m.ncols(), (unsigned)3);
		CHECK(m.exists(1, 1));
		CHECK(m.exists(1, 2));
		CHECK(m.exists(1, 3));
		CHECK(m.exists(2, 2));
		CHECK(!m.exists(0, 0));
		CHECK(!m.exists(0, 1));
		CHECK(!m.exists(1, 0));
		CHECK(!m.exists(1, 4));
		CHECK(!m.exists(2, 0));
		CHECK(m.exists(2, 1));
		CHECK(m.exists(2, 3));
		CHECK(!m.exists(2, 4));
		CHECK(m.row_begin() != m.row_end());
		CHECK(m.col_begin() != m.col_end());
	}

	TEST(AssignmentOperator)
	{
		CommonMatrix copy = m;
		m(1, 1) = 1.4;
		m(1, 2) = 0.8;
		m(2, 2) = 0.4;
		m(1, 3) = 1.3;
		CHECK_EQUAL(m(1, 1), 1.4);
		CHECK_EQUAL(m(1, 2), 0.8);
		CHECK_EQUAL(m(2, 2), 0.4);
		CHECK_EQUAL(m(1, 3), 1.3);
		CHECK_EQUAL(m.nrows(), (unsigned)2);
		CHECK_EQUAL(m.ncols(), (unsigned)3);
		CHECK(m.exists(1, 1));
		CHECK(m.exists(1, 2));
		CHECK(m.exists(1, 3));
		CHECK(m.exists(2, 2));
		CHECK(!m.exists(0, 0));
		CHECK(!m.exists(0, 1));
		CHECK(!m.exists(1, 0));
		CHECK(!m.exists(1, 4));
		CHECK(!m.exists(2, 0));
		CHECK(m.exists(2, 1));
		CHECK(m.exists(2, 3));
		CHECK(!m.exists(2, 4));
		CHECK(m.row_begin() != m.row_end());
		CHECK(m.col_begin() != m.col_end());
	}
}

