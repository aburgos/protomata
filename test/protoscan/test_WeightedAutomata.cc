
#include <UnitTest++.h>
#include "WeightedAutomata.h"

SUITE(WeightedAutomata)
{
	WeightedAutomata<double> wa;

	TEST(Initialization)
	{
		CHECK_EQUAL(wa.states_size(), (unsigned)0);
		CHECK(!wa.setStart(0));
		CHECK(!wa.setFinal(1));
		CHECK(wa.state_begin() == wa.state_end());
		CHECK(wa.start_state_begin() == wa.start_state_end());
		CHECK(wa.final_state_begin() == wa.final_state_end());
		CHECK(wa.transition_begin() == wa.transition_end());
	}

	TEST(AddTransition)
	{
		wa.addTransition(0, 1, std::make_pair('A', 0.1));
		wa.addTransition(0, 1, std::make_pair('B', 0.2));
		wa.addTransition(0, 2, std::make_pair('A', 1.1));
		wa.addTransition(1, 2, std::make_pair('B', 1.2));
		// check transitions between states
		std::map<char, double> fTransitions = wa.getTransitions(0, 1);
		CHECK_EQUAL(fTransitions['A'], 0.1);
		CHECK_EQUAL(fTransitions['B'], 0.2);
		CHECK(fTransitions.find('C') == fTransitions.end());
		std::map<char, double> sTransitions = wa.getTransitions(0, 2);
		CHECK_EQUAL(sTransitions['A'], 1.1);
		CHECK(sTransitions.find('B') == sTransitions.end());
		std::map<char, double> tTransitions = wa.getTransitions(1, 2);
		CHECK_EQUAL(tTransitions['B'], 1.2);
		CHECK(tTransitions.find('A') == tTransitions.end());
		std::map<char, double> uTransitions = wa.getTransitions(0, 3);
		CHECK_EQUAL(uTransitions.size(), (unsigned)0);
		// check transitions by char
		std::map<WeightedAutomata<double>::TranStates, double> ATransitions = wa.getTransitions('A');
		CHECK_EQUAL(ATransitions[std::make_pair(0, 1)], 0.1);
		CHECK_EQUAL(ATransitions[std::make_pair(0, 2)], 1.1);
		CHECK(ATransitions.find(std::make_pair(1, 2)) == ATransitions.end());
		std::map<WeightedAutomata<double>::TranStates, double> BTransitions = wa.getTransitions('B');
		CHECK_EQUAL(BTransitions[std::make_pair(0, 1)], 0.2);
		CHECK_EQUAL(BTransitions[std::make_pair(1, 2)], 1.2);
		CHECK(BTransitions.find(std::make_pair(0, 2)) == BTransitions.end());
	}

	TEST(SetStartFinalStates)
	{
		CHECK(wa.setStart(0));
		CHECK(wa.setFinal(1));
		CHECK(wa.setFinal(2));
		CHECK(!wa.setFinal(3));
		CHECK(wa.isStart(0));
		CHECK(!wa.isStart(1));
		CHECK(!wa.isStart(2));
		CHECK(!wa.isStart(3));
		CHECK(!wa.isFinal(0));
		CHECK(wa.isFinal(1));
		CHECK(wa.isFinal(2));
		CHECK(!wa.isFinal(3));
	}

	TEST(AddTransitions)
	{
		std::map<char, double> ftoAddTransitions;
		ftoAddTransitions['A'] = 0.4;
		ftoAddTransitions['B'] = 0.5;
		ftoAddTransitions['C'] = 0.6;
		wa.addTransitions(0, 1, ftoAddTransitions);
		// check transitions between states
		std::map<char, double> fTransitions = wa.getTransitions(0, 1);
		CHECK_EQUAL(fTransitions['A'], 0.1);
		CHECK_EQUAL(fTransitions['B'], 0.2);
		CHECK_EQUAL(fTransitions['C'], 0.6);
		CHECK(fTransitions.find('D') == fTransitions.end());

		std::map<char, double> stoAddTransitions;
		stoAddTransitions['A'] = 1.4;
		stoAddTransitions['B'] = 1.5;
		stoAddTransitions['C'] = 1.6;
		stoAddTransitions['D'] = 1.7;
		wa.addTransitions(3, 4, stoAddTransitions);
		// check transitions between states
		std::map<char, double> sTransitions = wa.getTransitions(3, 4);
		CHECK_EQUAL(sTransitions['A'], 1.4);
		CHECK_EQUAL(sTransitions['B'], 1.5);
		CHECK_EQUAL(sTransitions['C'], 1.6);
		CHECK_EQUAL(sTransitions['D'], 1.7);
		CHECK(sTransitions.find('E') == sTransitions.end());
		// check transitions by char
		std::map<WeightedAutomata<double>::TranStates, double> ATransitions = wa.getTransitions('A');
		CHECK_EQUAL(ATransitions[std::make_pair(0, 1)], 0.1);
		CHECK_EQUAL(ATransitions[std::make_pair(0, 2)], 1.1);
		CHECK_EQUAL(ATransitions[std::make_pair(3, 4)], 1.4);
		CHECK(ATransitions.find(std::make_pair(1, 2)) == ATransitions.end());
		std::map<WeightedAutomata<double>::TranStates, double> BTransitions = wa.getTransitions('B');
		CHECK_EQUAL(BTransitions[std::make_pair(0, 1)], 0.2);
		CHECK_EQUAL(BTransitions[std::make_pair(1, 2)], 1.2);
		CHECK_EQUAL(BTransitions[std::make_pair(3, 4)], 1.5);
		CHECK(BTransitions.find(std::make_pair(0, 2)) == BTransitions.end());
		std::map<WeightedAutomata<double>::TranStates, double> DTransitions = wa.getTransitions('D');
		CHECK_EQUAL(DTransitions[std::make_pair(3, 4)], 1.7);
		CHECK(DTransitions.find(std::make_pair(0, 1)) == DTransitions.end());
		CHECK(DTransitions.find(std::make_pair(0, 2)) == DTransitions.end());
		CHECK(DTransitions.find(std::make_pair(1, 2)) == DTransitions.end());
	}
}

