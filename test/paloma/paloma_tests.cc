
#include "test_MultiSequence.cc"
#include "test_UnionFind.cc"
#include "test_Alignment_UFImp.cc"
#include "test_Graph.cc"
#include "test_DiGraph.cc"
#include "test_Gabios.cc"

int main(int, char**)
{
	return UnitTest::RunAllTests();
}
