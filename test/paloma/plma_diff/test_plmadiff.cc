
#include "parsers/PlmaParser.h"

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		std::cout << "usage: " << argv[0] << " plma1 plma2" << std::endl;
		exit(1);
	}

	PlmaParser plma1(argv[1]);
	PlmaParser plma2(argv[2]);

	// for checking both sequences and partition, the strategy is to check if sizes are the same
	// and then check if every sequence (partition element) of the first PLMA is found on the second

	// get each set of sequences used to build each PLMA
	const MultiSequence& ms1 = plma1.multisequence();
	const MultiSequence& ms2 = plma2.multisequence();

	// check that they have the same number of sequences
	assert(ms1.size() == ms2.size());

	// check if sequences used were the same
	for(MultiSequence::const_iterator it = ms1.begin(); it != ms1.end(); ++it)
	{
		const Sequence& sequence1 = *it;
		const Sequence& sequence2 = ms2[sequence1.tag()];

		assert(sequence1.tag() == sequence2.tag());
		assert(sequence1.data() == sequence2.data());
	}

	// get produced generalized alignments
	const GeneralizedAlignment<LocalAlignment_UFImp>& ga1 = plma1.generalized_alignment();
	const GeneralizedAlignment<LocalAlignment_UFImp>& ga2 = plma2.generalized_alignment();

	// check that they have the same number of matchings
	assert(ga1.size() == ga2.size());

	// check that they have the same number of relations
	unsigned int relations1 = 0;
	unsigned int relations2 = 0;

	GeneralizedAlignment<LocalAlignment_UFImp>::matching_iterator it;
	for(it = ga1.matching_begin(); it != ga1.matching_end(); ++it)
		relations1 += (*it).relations_size();
	for(it = ga2.matching_begin(); it != ga2.matching_end(); ++it)
		relations2 += (*it).relations_size();

	assert(relations1 == relations2);

	// check that every relation in the first local alignment is also in the second
	site_visitor v1 = ga1.site_relations();
	while(v1.valid())
	{
		const SitePairRelation& r1 = v1.get();

		const Sequence& site1_sequence = ms1[r1.first().sequence()];
		const Sequence& site2_sequence = ms1[r1.second().sequence()];

		// get equivalent sites for ms2
		const Sequence& sequence21 = ms2[site1_sequence.tag()];
		const Sequence& sequence22 = ms2[site2_sequence.tag()];
		Site s21(sequence21.id(), r1.first().position());
		Site s22(sequence22.id(), r1.second().position());

		SitePairRelation r2(std::make_pair(s21, s22), r1.value());

		assert(ga2.hasSiteRelation(r2));

		v1.next();
	}

	return 0;
}
