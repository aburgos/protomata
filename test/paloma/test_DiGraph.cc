
#include <UnitTest++.h>
#include "lib/DiGraph.h"

SUITE(DiGraph)
{
	DiGraph<unsigned int, double> g;

	TEST(Initialization)
	{
		CHECK_EQUAL(g.vertices(), (unsigned)0);
		CHECK_EQUAL(g.edges(), (unsigned)0);
		CHECK(!(g.hasVertex(0) != g.vertex_end()));
		CHECK(!(g.hasEdge(0, 0) != g.relation_end()));
		CHECK(!g.deleteVertex(0));
		CHECK(!g.deleteEdge(0, 0));
		CHECK_EQUAL(g.degree(0), (unsigned)0);
	}

	TEST(AddVertex)
	{
		CHECK((g.addVertex(0)).second);
		CHECK(g.hasVertex(0) != g.vertex_end());
		CHECK_EQUAL(g.vertices(), (unsigned)1);
		CHECK_EQUAL(g.edges(), (unsigned)0);
		CHECK_EQUAL(g.degree(0), (unsigned)0);
	}

	TEST(AddReflexiveEdge)
	{
		CHECK(g.addEdge(0, 0, 0.1));
		CHECK(g.hasEdge(0, 0) != g.relation_end());
		CHECK_EQUAL(g.vertices(), (unsigned)1);
		CHECK_EQUAL(g.edges(), (unsigned)1);
		CHECK_EQUAL(g.degree(0), (unsigned)1);
	}

	TEST(AddEdgeWithNonExistingVertex)
	{
		CHECK(g.addEdge(0, 1, 0.15));
		CHECK(g.hasVertex(0) != g.vertex_end());
		CHECK(g.hasVertex(1) != g.vertex_end());
		CHECK(g.hasEdge(0, 1) != g.relation_end());
		CHECK(!(g.hasEdge(1, 0) != g.relation_end()));
		CHECK_EQUAL(g.vertices(), (unsigned)2);
		CHECK_EQUAL(g.edges(), (unsigned)2);
		CHECK_EQUAL(g.degree(0), (unsigned)2);
	}

	TEST(AddSymmetricEdges)
	{
		CHECK(g.addEdge(1, 0, 0.25));
		CHECK(g.hasVertex(0) != g.vertex_end());
		CHECK(g.hasVertex(1) != g.vertex_end());
		CHECK(g.hasEdge(0, 1) != g.relation_end());
		CHECK(g.hasEdge(1, 0) != g.relation_end());
		CHECK_EQUAL(g.vertices(), (unsigned)2);
		CHECK_EQUAL(g.edges(), (unsigned)3);
		CHECK_EQUAL(g.degree(0), (unsigned)2);
		CHECK_EQUAL(g.degree(1), (unsigned)1);
	}

	TEST(AddExistingVertex)
	{
		CHECK(!(g.addVertex(0)).second);
		CHECK(g.hasVertex(0) != g.vertex_end());
		CHECK_EQUAL(g.vertices(), (unsigned)2);
	}

	TEST(AddExistingEdge)
	{
		CHECK(!g.addEdge(0, 1, 0.2));
		CHECK(g.hasVertex(0) != g.vertex_end());
		CHECK(g.hasVertex(1) != g.vertex_end());
		CHECK(g.hasEdge(0, 1) != g.relation_end());
		// check if the weight remained the same
		DiGraph<unsigned int, double>::relation r = *g.hasEdge(0, 1);
		CHECK_EQUAL(r.value(), 0.15);
		CHECK(g.hasEdge(1, 0) != g.relation_end());
		CHECK_EQUAL(g.vertices(), (unsigned)2);
		CHECK_EQUAL(g.edges(), (unsigned)3);
		CHECK_EQUAL(g.degree(0), (unsigned)2);
		CHECK_EQUAL(g.degree(1), (unsigned)1);
	}

	TEST(ExtendGraph)
	{
		CHECK((g.addVertex(7)).second);
		CHECK(g.hasVertex(7) != g.vertex_end());
		CHECK(!(g.hasVertex(6) != g.vertex_end()));
		CHECK(!(g.hasEdge(7, 7) != g.relation_end()));
		CHECK_EQUAL(g.vertices(), (unsigned)3);
		CHECK_EQUAL(g.edges(), (unsigned)3);
		CHECK_EQUAL(g.degree(7), (unsigned)0);

		CHECK(g.addEdge(1, 7, 0.18));
		CHECK(g.hasEdge(1, 7) != g.relation_end());
		CHECK(!(g.hasEdge(7, 1) != g.relation_end()));
		CHECK(!(g.hasEdge(1, 1) != g.relation_end()));
		CHECK(!(g.hasEdge(7, 7) != g.relation_end()));
		CHECK_EQUAL(g.vertices(), (unsigned)3);
		CHECK_EQUAL(g.edges(), (unsigned)4);
		CHECK_EQUAL(g.degree(0), (unsigned)2);
		CHECK_EQUAL(g.degree(1), (unsigned)2);
		CHECK_EQUAL(g.degree(7), (unsigned)0);
	}

	TEST(ExistsPath)
	{
		CHECK(g.existsPath(0, 0));
		CHECK(g.existsPath(0, 1));
		CHECK(g.existsPath(1, 0));
		CHECK(g.existsPath(0, 7));
		CHECK(!g.existsPath(7, 0));
		CHECK(!g.existsPath(7, 1));
		CHECK(!g.existsPath(6, 6));
		// this are assumed by the library
		CHECK(g.existsPath(1, 1));
		CHECK(g.existsPath(7, 7));
	}

	TEST(AdjacentIterator)
	{
		unsigned int check_vertex = 1;
		std::set<unsigned int> adj_vertices;
		adj_vertices.insert(0); adj_vertices.insert(7);
		DiGraph<unsigned int, double>::vertex_iterator::adjacent_iterator ait;
		for(ait = (g.hasVertex(check_vertex)).adjacent_begin(); ait != (g.hasVertex(check_vertex)).adjacent_end(); ++ait)
		{
			unsigned int adj_vertex = *ait;
			CHECK(adj_vertices.find(adj_vertex) != adj_vertices.end());
		}
	}

	TEST(RelationIterator)
	{
		std::set<std::pair<unsigned int, unsigned int> > relations;
		relations.insert(std::make_pair(0, 0));
		relations.insert(std::make_pair(0, 1));
		relations.insert(std::make_pair(1, 0));
		relations.insert(std::make_pair(1, 7));
		DiGraph<unsigned int, double>::relation_iterator rit;
		for(rit = g.relation_begin(); rit != g.relation_end(); ++rit)
		{
			const DiGraph<unsigned int, double>::relation& r = *rit;
			CHECK(relations.find(std::make_pair(r.first(), r.second())) != relations.end());
		}
	}

	TEST(DeleteEdge)
	{
		CHECK(g.deleteEdge(0, 0));
		CHECK(!(g.hasEdge(0, 0) != g.relation_end()));
		CHECK_EQUAL(g.vertices(), (unsigned)3);
		CHECK_EQUAL(g.edges(), (unsigned)3);
		CHECK_EQUAL(g.degree(0), (unsigned)1);
	}

	TEST(DeleteVertex)
	{
		CHECK(g.deleteVertex(0));
		CHECK(!(g.hasEdge(0, 0) != g.relation_end()));
		CHECK(!(g.hasEdge(0, 1) != g.relation_end()));
		CHECK(!(g.hasEdge(1, 0) != g.relation_end()));
		CHECK_EQUAL(g.vertices(), (unsigned)2);
		CHECK_EQUAL(g.edges(), (unsigned)1);
		CHECK_EQUAL(g.degree(0), (unsigned)0);
	}
}
