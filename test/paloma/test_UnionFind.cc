
#include <UnitTest++.h>
#include "lib/UnionFind.h"

SUITE(UnionFind)
{
	UnionFind uf1(30);

	TEST(UndefinedRelations)
	{
		for(unsigned int i = 1; i <= 30; i++)
			CHECK(!uf1.hasRelation(i, i));
	}

	TEST(MakeSet)
	{
		uf1.makeSet(5);

		CHECK(uf1.hasRelation(5, 5));
		CHECK(!uf1.hasRelation(3, 3));
	}

	TEST(ExistingRelations)
	{
		// reflexive
		uf1.makeUnion(3, 3);

		CHECK(uf1.hasRelation(3, 3));
		CHECK(!uf1.hasRelation(1, 1));
		CHECK(!uf1.hasRelation(30, 30));
		CHECK(!uf1.hasRelation(35, 35));
	}

	TEST(IncreaseSize)
	{
		uf1.increase_size(50);

		CHECK(uf1.hasRelation(3, 3));
		CHECK(!uf1.hasRelation(1, 1));
		CHECK(!uf1.hasRelation(30, 30));
		CHECK(!uf1.hasRelation(35, 35));
		CHECK(!uf1.hasRelation(50, 50));
	}

	TEST(Symmetry)
	{
		// symmetric
		uf1.makeUnion(1, 6);

		CHECK(uf1.hasRelation(1, 6));
		CHECK(uf1.hasRelation(6, 1));
		CHECK(uf1.hasRelation(1, 1));
		CHECK(uf1.hasRelation(6, 6));
	}

	TEST(Transitivity)
	{
		// transitive
		uf1.makeUnion(10, 15);
		uf1.makeUnion(15, 20);

		CHECK(uf1.hasRelation(10, 15));
		CHECK(uf1.hasRelation(15, 20));
		CHECK(uf1.hasRelation(10, 20));
		CHECK(uf1.hasRelation(10, 10));
		CHECK(uf1.hasRelation(15, 15));
		CHECK(uf1.hasRelation(20, 20));
	}

	TEST(FindSet)
	{
		UnionFind::PartitionElement expected_set;
		expected_set.insert(10);
		expected_set.insert(15);
		expected_set.insert(20);
		UnionFind::PartitionElement retrieved_set = uf1.findSet(10);

		UnionFind::PartitionElement::const_iterator it1 = expected_set.begin();
		UnionFind::PartitionElement::const_iterator it2 = retrieved_set.begin();
		while(it1 != expected_set.end() && it2 != retrieved_set.end())
		{
			CHECK_EQUAL(*it1, *it2);

			it1++;
			it2++;
		}
	}

	TEST(Partition)
	{
		UnionFind::Partition expected_partition;
		UnionFind::PartitionElement set1;
		set1.insert(5);
		expected_partition.insert(set1);
		UnionFind::PartitionElement set2;
		set2.insert(3);
		expected_partition.insert(set2);
		UnionFind::PartitionElement set3;
		set3.insert(1);
		set3.insert(6);
		expected_partition.insert(set3);
		UnionFind::PartitionElement set4;
		set4.insert(10);
		set4.insert(15);
		set4.insert(20);
		expected_partition.insert(set4);

		UnionFind::Partition retrieved_partition = uf1.partition();

		UnionFind::Partition::const_iterator it1 = expected_partition.begin();
		UnionFind::Partition::const_iterator it2 = retrieved_partition.begin();
		while(it1 != expected_partition.end() && it2 != retrieved_partition.end())
		{
			const UnionFind::PartitionElement& expected_set = *it1;
			const UnionFind::PartitionElement& retrieved_set = *it2;

			UnionFind::PartitionElement::const_iterator eit1 = expected_set.begin();
			UnionFind::PartitionElement::const_iterator eit2 = retrieved_set.begin();
			while(eit1 != expected_set.end() && eit2 != retrieved_set.end())
			{
				CHECK_EQUAL(*eit1, *eit2);

				eit1++;
				eit2++;
			}

			it1++;
			it2++;
		}
	}

	TEST(CopyConstructor)
	{
		UnionFind uf2(uf1);

		CHECK(uf2.hasRelation(3, 3));
		CHECK(uf2.hasRelation(1, 6));
		CHECK(uf2.hasRelation(6, 1));
		CHECK(uf2.hasRelation(1, 1));
		CHECK(uf2.hasRelation(6, 6));
		CHECK(uf2.hasRelation(10, 15));
		CHECK(uf2.hasRelation(15, 20));
		CHECK(uf2.hasRelation(10, 20));
		CHECK(uf2.hasRelation(10, 10));
		CHECK(uf2.hasRelation(15, 15));
		CHECK(uf2.hasRelation(20, 20));
		CHECK(!uf2.hasRelation(2, 2));
		CHECK(!uf2.hasRelation(30, 30));
		CHECK(!uf2.hasRelation(35, 35));
		CHECK(!uf2.hasRelation(50, 50));
	}

	TEST(Assignment)
	{
		UnionFind uf3 = uf1;

		CHECK(uf3.hasRelation(3, 3));
		CHECK(uf3.hasRelation(1, 6));
		CHECK(uf3.hasRelation(6, 1));
		CHECK(uf3.hasRelation(1, 1));
		CHECK(uf3.hasRelation(6, 6));
		CHECK(uf3.hasRelation(10, 15));
		CHECK(uf3.hasRelation(15, 20));
		CHECK(uf3.hasRelation(10, 20));
		CHECK(uf3.hasRelation(10, 10));
		CHECK(uf3.hasRelation(15, 15));
		CHECK(uf3.hasRelation(20, 20));
		CHECK(!uf3.hasRelation(2, 2));
		CHECK(!uf3.hasRelation(30, 30));
		CHECK(!uf3.hasRelation(35, 35));
		CHECK(!uf3.hasRelation(50, 50));
	}
}

