
#include <UnitTest++.h>
#include "MultiSequence.h"

SUITE(MultiSequence)
{
	const std::string test_path(TESTPATH);
	const std::string fasta_file(test_path+"/data/4EIP-JM-all.fasta");
	const std::string weights_file(test_path+"/data/4EIP-JM-all.weights");

	SUITE(BuildingMode)
	{
		MultiSequence ms(fasta_file);

		TEST(Size)
		{
			CHECK_EQUAL(ms.size(), (unsigned)34);
		}

		TEST(AccessByPosition)
		{
			CHECK_EQUAL(ms[0].tag(), "Sg");
			CHECK_EQUAL(ms[3].tag(), "Hs4EBP3_O60516");
			CHECK_EQUAL(ms[6].tag(), "Ac4EBPEB300692.1A.californica");
		}

		TEST(AccessByTag)
		{
			CHECK_EQUAL(ms[0], ms["Sg"]);
			CHECK_EQUAL(ms[3], ms["Hs4EBP3_O60516"]);
			CHECK_EQUAL(ms[6], ms["Ac4EBPEB300692.1A.californica"]);
		}

		TEST(SequenceSegment)
		{
			const Segment seg(0, 6, 12);
			CHECK_EQUAL(ms[seg], "SRQLSAGRDIPA");
		}

		TEST(SequenceSite)
		{
			const Site site(0, 6);
			CHECK_EQUAL(ms[site], 'S');
		}

		TEST(Weights)
		{
			for(MultiSequence::const_iterator it = ms.begin(); it != ms.end(); ++it)
			{
				const Sequence& seq = *it;
				CHECK_EQUAL(seq.weight(), 1);
			}
		}

		TEST(ClustalWeights)
		{
			ms.weight_sequences();
			CHECK_CLOSE(ms["Sg"].weight(), 0.541667, 0.0001);
			CHECK_CLOSE(ms["Hs4EBP3_O60516"].weight(), 0.4375, 0.0001);
			CHECK_CLOSE(ms["Ac4EBPEB300692.1A.californica"].weight(), 0.645833, 0.0001);
		}

		TEST(UserWeights)
		{
			ms.weight_sequences(weights_file);
			CHECK_CLOSE(ms["Sg"].weight(), 0.74552, 0.0001);
			CHECK_CLOSE(ms["Hs4EBP3_O60516"].weight(), 0.8741, 0.0001);
			CHECK_CLOSE(ms["Ac4EBPEB300692.1A.californica"].weight(), 0.567, 0.0001);
		}

		TEST(NoX)
		{
			// we shouldn't find any X on the sequences
			for(MultiSequence::const_iterator it = ms.begin(); it != ms.end(); ++it)
			{
				const Sequence& seq = *it;
				CHECK_EQUAL(seq.data().find('X'), std::string::npos);
			}
		}
	}
}
