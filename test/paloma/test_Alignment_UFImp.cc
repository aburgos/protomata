
#include <UnitTest++.h>
#include "MultiSequence.h"
#include "matchings/Matching_SiGrImp.h"
#include "alignments/Alignment_UFImp.h"

SUITE(Alignment_UFImp)
{
	const std::string test_path(TESTPATH);
	const std::string fasta_file(test_path+"/data/p450_homstrad.fasta");

	// get the sequences
	const MultiSequence ms(fasta_file);

	// test an equivalent class but with method willBeAlignmentAfterAdding made public
	class TestAlignment_UFImp: public Alignment_UFImp
	{
	  public:
		TestAlignment_UFImp(const MultiSequence&): Alignment_UFImp(ms) {}
		bool TestwillBeAlignmentAfterAdding(const Matching& m) const { return willBeAlignmentAfterAdding(m); }
	};

	TestAlignment_UFImp al(ms);

	TEST(HasSiteRelation)
	{
		SitePairRelation spr(std::make_pair(Site(0,0), Site(1,0)), 0);
		CHECK(!al.hasSiteRelation(spr));
	}

	TEST(AddSitePairRelation)
	{
		CHECK_EQUAL(al.sites_size(), (unsigned)0);
		CHECK_EQUAL(al.relations_size(), (unsigned)0);

		SitePairRelation spr(std::make_pair(Site(0,0), Site(1,0)), 0);
		al.addMatching(spr);

		CHECK(al.hasSiteRelation(spr));
		CHECK_EQUAL(al.sites_size(), (unsigned)2);
		CHECK_EQUAL(al.relations_size(), (unsigned)1);
	}

	TEST(AlignmentCondition)
	{
		Matching_SiGrImp m(ms);
		SitePairRelation spr1(std::make_pair(Site(0,0), Site(2,0)), 0);
		m.addMatching(spr1);

		CHECK(!al.TestwillBeAlignmentAfterAdding(m));

		SitePairRelation spr2(std::make_pair(Site(1,0), Site(2,0)), 0);
		m.addMatching(spr2);

		CHECK(al.TestwillBeAlignmentAfterAdding(m));
	}

	TEST(AddMatching)
	{
		Matching_SiGrImp m(ms);
		SitePairRelation spr1(std::make_pair(Site(0,0), Site(2,0)), 0);
		m.addMatching(spr1);
		SitePairRelation spr2(std::make_pair(Site(1,0), Site(2,0)), 0);
		m.addMatching(spr2);

		al.addMatching(m);
		CHECK_EQUAL(al.sites_size(), (unsigned)3);
		CHECK_EQUAL(al.relations_size(), (unsigned)3);
	}

	TEST(Iterator)
	{
		unsigned int iterated_relations = 0;
		relation_visitor v = al.site_relations();
		while(v.valid())
		{
			SitePairRelation spr = v.get();
			CHECK(al.hasSiteRelation(spr));
			iterated_relations++;
			v.next();
		}

		CHECK_EQUAL(al.relations_size(), iterated_relations);
	}
}
