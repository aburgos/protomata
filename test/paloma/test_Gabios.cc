
#include <UnitTest++.h>
#include "lib/Gabios.h"

SUITE(Gabios)
{
	const std::string test_path(TESTPATH);
	const std::string fasta_file(test_path+"/data/p450_homstrad.fasta");

	// get the sequences
	const MultiSequence ms(fasta_file);

	Gabios g(ms);

	TEST(Initialization)
	{
		for(unsigned int k = 0; k < ms.size() - 1; ++k)
			for(unsigned int i = 0; i < ms[k].length(); ++i)
				for(unsigned int j = 0; j < ms[k + 1].length(); ++j)
				{
					const Site s1(k, i);
					const Site s2(k + 1, j);

					CHECK(g.areAlignable(s1, s2));
					CHECK(!g.areAligned(s1, s2));
				}
	}

	TEST(SitesCrossedAlignment)
	{
		const Site s1(0, 0);
		const Site s2(1, ms[1].length() - 1);

		const Site s3(0, 1);
		const Site s4(1, 1);

		CHECK(g.areAlignable(s1, s2));
		CHECK(g.addAlignment(s1, s2));
		CHECK(g.areAligned(s1, s2));

		CHECK(!g.areAligned(s3, s4));
		CHECK(!g.areAlignable(s3, s4));
	}	

	TEST(SitesRepeatedAlignment)
	{
		const Site s1(2, 0);
		const Site s2(3, 2);

		const Site s3(2, 5);
		const Site s4(3, 2);

		CHECK(g.areAlignable(s1, s2));
		CHECK(g.addAlignment(s1, s2));
		CHECK(g.areAligned(s1, s2));

		CHECK(!g.areAligned(s3, s4));
		CHECK(!g.areAlignable(s3, s4));
	}

	TEST(SegmentsCrossedAlignment)
	{
		const Segment s1(4, 0, 5);
		const Segment s2(5, ms[5].length() - 5, 5);

		const Segment s3(4, 10, 5);
		const Segment s4(5, 5, 5);

		CHECK(g.areAlignable(s1, s2));
		CHECK(g.addAlignment(s1, s2));
		CHECK(g.areAligned(s1, s2));

		CHECK(!g.areAligned(s3, s4));
		CHECK(!g.areAlignable(s3, s4));
	}	

	TEST(SegmentsRepeatedAlignment)
	{
		const Segment s1(6, 0, 5);
		const Segment s2(7, 5, 5);

		const Segment s3(6, 10, 5);
		const Segment s4(7, 5, 5);

		CHECK(g.areAlignable(s1, s2));
		CHECK(g.addAlignment(s1, s2));
		CHECK(g.areAligned(s1, s2));

		CHECK(!g.areAligned(s3, s4));
		CHECK(!g.areAlignable(s3, s4));
	}
}

