#! /usr/bin/env python2.6

import os, sys
from optparse import OptionParser

from Bio.Seq import Seq
from Bio import SeqIO

usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)
parser.add_option("-1", dest="fasta_filename_1", default=None, action="store", type="string", help="Fasta files to make union of")
parser.add_option("-2", dest="fasta_filename_2", default=None, action="store", type="string", help="Fasta files to make union of")
parser.add_option("-o", "--output", dest="output_filename", default=None, action="store", type="string", help="Output file name")

(options, args) = parser.parse_args()

sequences_1 = SeqIO.index(options.fasta_filename_1, "fasta")
sequences_2 = SeqIO.index(options.fasta_filename_2, "fasta")

keys = set()
seqs = list()
for key, record in sequences_1.iteritems():
	if key not in keys:
		keys.add(key)
		seqs.append(record)
for key, record in sequences_2.iteritems():
	if key not in keys:
		keys.add(key)
		seqs.append(record)

handle = open(options.output_filename, 'w')
SeqIO.write(seqs, handle, "fasta")
handle.close()
