#! /usr/bin/env python2.6

import os, sys
from optparse import OptionParser

from Bio.Seq import Seq
from Bio import SeqIO

usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)
parser.add_option("-f", "--fasta", dest="fasta_filename", default=None, action="store", type="string", help="Fasta file with sequences to partition")
parser.add_option("-i", "--index", dest="index_filename", default=None, action="store", type="string", help="Index file with the class of each sequence in the fasta file")
parser.add_option("-1", "--one-vs-all", dest="onevsall", default=False, action="store_true", help="Creates a fasta file for each class in the index file and a fasta file for the rest of the sequences in the fasta input")
parser.add_option("-o", "--output", dest="output_directory", default=".", action="store", type="string", help="Output directory")

(options, args) = parser.parse_args()

output_dir = options.output_directory
os.makedirs(output_dir)

handle = open(options.fasta_filename)
sequences = list(SeqIO.parse(handle, "fasta"))
handle.close()

index_file = open(options.index_filename, 'r')
sequence_class = dict([(str(((s[:-1]).split('\t'))[0]), str(((s[:-1]).split('\t'))[1]).replace("/", " ")) \
					for s in index_file.readlines()])

classes = set()
index_file.seek(0)
for s in index_file.readlines():
	classes.add(str(((s[:-1]).split('\t'))[1]).replace("/", " "))

basename, extension = os.path.splitext(os.path.basename(options.fasta_filename))

if options.onevsall:

	main_resume_filename = output_dir + "/" + basename + "_" + output_dir + ".resume"
	main_resume_file = open(main_resume_filename, 'w')
	main_resume_file.write(output_dir + " = " + str(len(classes)) + "\n\n")

	for c in classes:
		class_name = c.rstrip("\r").replace(" ", "-")
		os.makedirs(output_dir + "/" + class_name)
		filename_c = output_dir + "/" + class_name + "/" + basename + "_" + class_name + extension
		handle_c = open(filename_c, 'w')
		filename_r = output_dir + "/" + class_name + "/" + basename + "_" + class_name + "_negative" + extension
		handle_r = open(filename_r, 'w')

		class_seq = 0
		non_class_seq = 0

		for index, sequence in enumerate(sequences):
			if (sequence_class[sequence.name]).rstrip("\r").replace(" ", "-") == class_name:
				class_seq += 1
				SeqIO.write(sequences[index:index + 1], handle_c, "fasta")
			else:
				non_class_seq += 1
				SeqIO.write(sequences[index:index + 1], handle_r, "fasta")

		handle_c.close()
		handle_r.close()

		main_resume_file.write(class_name + " sequences = " + str(class_seq) + "\n")

		resume_filename = output_dir + "/" + class_name + "/" + basename + "_" + output_dir + "_" + class_name + ".resume"
		resume_file = open(resume_filename, 'w')
		resume_file.write(class_name + " sequences = " + str(class_seq) + "\n")
		resume_file.write(class_name + " negative sequences = " + str(non_class_seq))
		resume_file.close()

	main_resume_file.close()

else:

	handle = {}
	count = {}
	for c in classes:
		class_name = c.rstrip("\r").replace(" ", "-")
		filename = output_dir + "/" + basename + "_" + class_name + extension
		handle[class_name] = open(filename, 'w')
		count[class_name] = 0

	for index, sequence in enumerate(sequences):
		class_name = ((sequence_class[sequence.name]).rstrip("\r")).replace(" ", "-")
		count[class_name] += 1
		SeqIO.write(sequences[index:index + 1], handle[class_name], "fasta")

	for c in classes:
		class_name = c.rstrip("\r").replace(" ", "-")
		handle[class_name].close()

	resume_filename = output_dir + "/" + basename + "_" + output_dir + ".resume"
	resume_file = open(resume_filename, 'w')
	for c in classes:
		class_name = c.rstrip("\r").replace(" ", "-")
		resume_file.write(class_name + " sequences = " + str(count[class_name]) + "\n")
	resume_file.close()

