#!/bin/sh
# remove redundancy by calling cd-hit
# (with a shorter command line)

REDUNDANCY=90
if [ $# -eq 1 ]
then
    SAMPLE_FILENAME=$1
elif [ $# -eq 2 ]
then 
    SAMPLE_FILENAME=$1
    REDUNDANCY=$2
else
    echo "usage: $0 fastafile [redundacy]"
    echo "       redundacy default is 90"
    echo "example: '$0 toto.fasta 70' will produce toto_nr70.fasta" 
    exit
fi

SAMPLE_FILENAME_NOEXT=`echo ${SAMPLE_FILENAME} | sed "s/\.[^.]*//"`

REDUNDANCY_DIV100=`echo "scale=2; $REDUNDANCY / 100" | bc` 

if [ $REDUNDANCY -ge 80 ] 
then 
    WORD_LEN=5
elif [ $REDUNDANCY -ge 60 ]
then 
    WORD_LEN=4
elif [ $REDUNDANCY -ge 50 ]
then 
    WORD_LEN=3
elif [ $REDUNDANCY -ge 40 ]
then 
    WORD_LEN=2
else
    echo "warning low redundancy. does it work ?"
    WORD_LEN=2
fi  

RR_CMD="cd-hit -i $SAMPLE_FILENAME -o ${SAMPLE_FILENAME_NOEXT}_nr${REDUNDANCY}.fasta -c $REDUNDANCY_DIV100 -n $WORD_LEN"
echo $RR_CMD
$RR_CMD
