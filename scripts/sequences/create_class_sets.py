#! /usr/bin/env python2.6

import os, sys
from optparse import OptionParser

from Bio.Seq import Seq
from Bio import SeqIO

cdhit = "cdhit"

def divide_set(source_set, percentage):

	first_set = set()
	second_set = set()

	for seq in source_set:
		current_percentage = float(len(first_set)) / float(len(source_set))
		if current_percentage < percentage:
			first_set.add(seq)
		else:
			second_set.add(seq)

	return first_set, second_set

def representative_set(source_fasta, threshold, dest_1, dest_2, for_training = False):

	# get the sequences from the fasta file
	handle = open(source_fasta, 'r')
	sequences = SeqIO.to_dict(SeqIO.parse(handle, "fasta"))
	handle.close()

	# if there are no sequences, return two empty sets
	if len(sequences) == 0:
		handle = open(dest_1, 'w')
		handle.close()
		handle = open(dest_2, 'w')
		handle.close()
		return set(), set()

	# get a non-redundant set of sequences
	command = cdhit + " -i " + source_fasta + " -o " + dest_1 + " -c " + str(threshold)

	# add correct size of word (according to user's guide)
	if threshold < 0.7 and threshold >= 0.6:
		command += " -n 4"
	elif threshold < 0.6 and threshold >= 0.5:
		command += " -n 3"
	elif threshold < 0.5 and threshold >= 0.4:
		command += " -n 2"

	os.system(command)

	# delete files created by cd-hit
	os.system("rm " + dest_1 + ".clstr")
	os.system("rm " + dest_1 + ".bak.clstr")

	# get the sequences from destination 1
	handle = open(dest_1, 'r')
	sequences_1 = SeqIO.to_dict(SeqIO.parse(handle, "fasta"))
	if for_training:
		print source_fasta
		to_delete = set()
		for seq in sequences_1:
			seq_string = sequences_1[seq].seq
			if seq_string.find('X') != -1:
				to_delete.add(seq)
		for seq in to_delete:
			del sequences_1[seq]
		handle.close()
		handle = open(dest_1, 'w')
		SeqIO.write(sequences_1.values(), handle, "fasta")
	handle.close()

	# get the sequences that are not in the first set
	sequences_2 = set()
	for key, record in sequences.items():
		if key not in sequences_1:
			sequences_2.add(record)

	# write sequences to file
	handle = open(dest_2, 'w')
	SeqIO.write(sequences_2, handle, "fasta")
	handle.close()

	print("seqs_1 = %s, seqs_2 = %s, seqs = %s" % (len(sequences_1), len(sequences_2), len(sequences)))
	return set(sequences_1.values()), sequences_2

usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)
parser.add_option("-f", "--fasta", dest="fasta_filename", default=None, action="store", type="string", help="Fasta file with sequences to partition")
parser.add_option("-i", "--index", dest="index_filename", default=None, action="store", type="string", help="Index file with the class of each sequence in the fasta file")
parser.add_option("-r", "--NR-threshold", dest="NR_threshold", default=0.9, action="store", type="float", help="Cdhit's threshold to be applied to the positive set of sequences")
parser.add_option("-t", "--train-threshold", dest="train_threshold", default=0.7, action="store", type="float", help="Cdhit's threshold to be applied to the non-redundant positive set of sequences")
parser.add_option("-v", "--positive-validation-percentage", dest="pos_validation_percentage", default=0.5, action="store", type="float", help="Percentage of sequences from rest of NR positive set that will be used for validation")
parser.add_option("-V", "--negative-validation-percentage", dest="neg_validation_percentage", default=0.5, action="store", type="float", help="Percentage of sequences from negative set that will be used for validation")
parser.add_option("-o", "--output", dest="output_directory", default=".", action="store", type="string", help="Output directory")

(options, args) = parser.parse_args()

# create output directory
output_dir = options.output_directory
os.makedirs(output_dir)

# get the sequences from the fasta file
handle = open(options.fasta_filename)
sequences = set(SeqIO.parse(handle, "fasta"))
handle.close()

# get the set of classes present
# create a dictionary with sequences names as keys and classes as values
classes = set()
sequence_class = dict()
index_file = open(options.index_filename, 'r')
for s in index_file.readlines():
	sequence_name = str((s[:-1].split('\t'))[0])
	class_name = str(((s[:-1]).split('\t'))[1]).replace("/", "-").replace(" ", "-").translate(None, "'()\r\n")
	sequence_class[sequence_name] = class_name
	classes.add(class_name)

# get basename and extension of the fasta file
basename, extension = os.path.splitext(os.path.basename(options.fasta_filename))

# create a file for writing a resume of new fasta files
main_resume_filename = output_dir + "/" + basename + "_" + output_dir + ".resume"
main_resume_file = open(main_resume_filename, 'w')
main_resume_file.write("non-redundant threshold = " + str(options.NR_threshold) + "\n")
main_resume_file.write("train threshold = " + str(options.train_threshold) + "\n")
main_resume_file.write("positive validation percentage = " + str(options.pos_validation_percentage) + "\n")
main_resume_file.write("negative validation percentage = " + str(options.neg_validation_percentage) + "\n\n")
main_resume_file.write(output_dir + " = " + str(len(classes)) + "\n")
main_resume_file.write("total sequences = " + str(len(sequences)) + "\n\n")

# create a main dot file
main_dot_basename = output_dir + "/" + basename + "_" + output_dir
main_dot_file = open(main_dot_basename + ".dot", 'w')
main_dot_file.write("digraph G\n{\n")
main_dot_file.write("\tnode [shape = record];\n")
main_dot_file.write("\tn1 [label = \"{Sequences|{families: " + str(len(classes)) + "|total: " + str(len(sequences)) + "}}\", style = filled, fillcolor = powderblue];\n")

for class_name in classes:

	# create class directory
	current_dir = output_dir + "/" + class_name
	class_basename = "/" + basename + "_" + class_name
	os.makedirs(current_dir)

	positive_sequences = set()
	negative_sequences = set()

	# divide sequences according to its label
	for sequence in sequences:
		if sequence_class[sequence.name] == class_name:
			positive_sequences.add(sequence)
		else:
			negative_sequences.add(sequence)

	# write positive sequences to file
	filename_pos = current_dir + class_basename + "_positive" + extension
	handle_pos = open(filename_pos, 'w')
	SeqIO.write(positive_sequences, handle_pos, "fasta")
	handle_pos.close()

	# write negative sequences to file
	filename_neg = current_dir + class_basename + "_negative" + extension
	handle_neg = open(filename_neg, 'w')
	SeqIO.write(negative_sequences, handle_neg, "fasta")
	handle_neg.close()

	# write on the resume the number of sequences of this class
	main_resume_file.write(class_name + " sequences = " + str(len(positive_sequences)) + "\n")

	# compute train, validation and test sets
	filename_train = current_dir + class_basename + "_train" + extension
	filename_validation = current_dir + class_basename + "_validation" + extension
	filename_test = current_dir + class_basename + "_test" + extension

	# get a non-redundant set from positive sequences
	filename_discarded = output_dir + class_basename + "_discarded" + extension
	filename_NR = output_dir + class_basename + "_NR" + extension
	NR_pos_sequences, discarded_sequences = representative_set(filename_pos, options.NR_threshold, filename_NR, filename_discarded)

	# get a non-redundant set of sequences for training
	filename_pmt = output_dir + class_basename + "_non_train_nr_pos" + extension
	train_sequences, non_train_nr_pos_sequences = representative_set(filename_NR, options.train_threshold, filename_train, filename_pmt, True)

	# divide the remaining positive sequences in validation and testing
	pos_validation_sequences, pos_test_sequences = divide_set(non_train_nr_pos_sequences, options.pos_validation_percentage)

	# divide the negative set in validation and testing
	neg_validation_sequences, neg_test_sequences = divide_set(negative_sequences, options.neg_validation_percentage)

	# create sets and write them to files
	validation_sequences = pos_validation_sequences | neg_validation_sequences
	if len(validation_sequences) > 0:
		handle = open(filename_validation, 'w')
		SeqIO.write(validation_sequences, handle, "fasta")
		handle.close()

	test_sequences = pos_test_sequences | neg_test_sequences
	if len(test_sequences) > 0:
		handle = open(filename_test, 'w')
		SeqIO.write(test_sequences, handle, "fasta")
		handle.close()

	# clean up temporary fasta files
	os.system("rm " + filename_pos)
	os.system("rm " + filename_neg)
	os.system("rm " + filename_NR)
	os.system("rm " + filename_discarded)
	os.system("rm " + filename_pmt)
	#os.system("rm " + filename_pos_validation)
	#os.system("rm " + filename_pos_test)
	#os.system("rm " + filename_neg_test_1)
	#os.system("rm " + filename_neg_test_2)

	# write on this class resume the number of sequences of each sequence set
	resume_filename = current_dir + "/" + basename + "_" + output_dir + "_" + class_name + ".resume"
	resume_file = open(resume_filename, 'w')
	resume_file.write(class_name + " train sequences = " + str(len(train_sequences)) + "\n")
	resume_file.write(class_name + " validation sequences = " + str(len(validation_sequences)))
	resume_file.write(" (pos: " + str(len(pos_validation_sequences)))
	resume_file.write(", neg: " + str(len(neg_validation_sequences)) + ")\n")
	resume_file.write(class_name + " test sequences = " + str(len(test_sequences)))
	resume_file.write(" (pos: " + str(len(pos_test_sequences)))
	resume_file.write(", neg: " + str(len(neg_test_sequences)) + ")\n")
	resume_file.close()

	# write class info to main dot file
	main_dot_file.write("\t\"" + class_name + "\" [label = \"{" + class_name + " family|sequences: " + str(len(positive_sequences)) + "}\", style = filled, fillcolor = navajowhite];\n")
	main_dot_file.write("\tn1 -> \"" + class_name + "\";\n")

	# create the dot file for the class
	dot_file = open(current_dir + class_basename + ".dot", 'w')
	dot_file.write("digraph G\n{\n")
	dot_file.write("\tnode [shape = record];\n")
	dot_file.write("\tn1 [label = \"{Sequences|total: " + str(len(sequences)) + "}\", color = darkblue];\n")
	dot_file.write("\tn2 [label = \"{Positive Sequences|{family: " + class_name + "|total: " + str(len(positive_sequences)) + "}}\", color = green];\n")
	dot_file.write("\tn3 [label = \"{Negative Sequences|total: " + str(len(negative_sequences)) + "}\", color = red];\n")
	dot_file.write("\tn1 -> n2;\n")
	dot_file.write("\tn1 -> n3;\n")

	dot_file.write("\tn6 [label = \"{Train Sequences|{total: " + str(len(train_sequences)) + "|prop: identity \<= " + str(int(options.train_threshold * 100)) + "%}}\", fillcolor = green, style = filled];\n")
	dot_file.write("\tn7 [label = \"{Rest of non-redundant Positive Sequences|{total: " + str(len(non_train_nr_pos_sequences)) + "|prop: identity \<= " + str(int(options.NR_threshold * 100)) + "%}}\", color = forestgreen];\n")

	if options.NR_threshold != options.train_threshold and options.NR_threshold < 1.0:
		dot_file.write("\tn4 [label = \"{Non-redundant Positive Sequences|{total: " + str(len(NR_pos_sequences)) + "|prop: identity \<= " + str(int(options.NR_threshold * 100)) + "%}}\", color = green];\n")
		dot_file.write("\tn5 [label = \"{Discarded Sequences|total: " + str(len(discarded_sequences))  + "}\", color = saddlebrown];\n")
		dot_file.write("\tn2 -> n5;\n")
		dot_file.write("\tn2 -> n4;\n")
		dot_file.write("\tn4 -> n6;\n")
		dot_file.write("\tn4 -> n7;\n")
	else:
		dot_file.write("\tn2 -> n6;\n")
		dot_file.write("\tn2 -> n7;\n")

	if len(validation_sequences) > 0:
		dot_file.write("\tn8 [label = \"{Validation Sequences|total: " + str(len(validation_sequences)) + " (pos: " + str(len(pos_validation_sequences)) + ", neg: " + str(len(neg_validation_sequences)) + ")}\", fillcolor = gold, style = filled];\n")
		if len(pos_validation_sequences) > 0:
			dot_file.write("\tn7 -> n8;\n")
		if len(neg_validation_sequences) > 0:
			dot_file.write("\tn3 -> n8;\n")

	if len(test_sequences) > 0:
		dot_file.write("\tn9 [label = \"{Test Sequences|total: " + str(len(test_sequences)) + " (pos: " + str(len(pos_test_sequences)) + ", neg: " + str(len(neg_test_sequences)) + ")}\", fillcolor = crimson, style = filled];\n")
		if len(pos_test_sequences) > 0:
			dot_file.write("\tn7 -> n9;\n")
		if len(neg_test_sequences) > 0:
			dot_file.write("\tn3 -> n9;\n")

	dot_file.write("}")
	dot_file.close()

	os.system("dot -Tsvg " + current_dir + class_basename + ".dot -o " + current_dir + class_basename + ".svg")
	#os.system("rm " + current_dir + class_basename + ".dot")

main_resume_file.close()
main_dot_file.write("}")
main_dot_file.close()

os.system("dot -Tsvg " + main_dot_basename + ".dot -o " + main_dot_basename + ".svg")
#os.system("rm " + main_dot_basename + ".dot")
