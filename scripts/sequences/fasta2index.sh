#!/bin/sh

REQUIERED_ARGS=3
E_BADARGS=65

if [ $# -lt $REQUIERED_ARGS ]
then
	echo "Usage: `basename $0` <fasta> <class> <output>"
	echo ""
	exit $E_BADARGS
fi

FASTA=$1
CLASS=$2
OUTPUT=$3

grep '>' $FASTA | sed -e "s/>*$/&\t$CLASS/g" > $OUTPUT
grep '>' $OUTPUT | sed -e "s/>//g" > "temp"
mv "temp" $OUTPUT
