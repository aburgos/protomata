#! /usr/bin/env python2.6

import os, sys
from optparse import OptionParser

from Bio.Seq import Seq
from Bio import SeqIO

usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)
parser.add_option("-f", "--fasta", dest="fasta_filename", default=None, action="store", type="string", help="Fasta file")
parser.add_option("-d", "--discard", dest="discard_filename", default=None, action="store", type="string", help="Discards all sequences of this fasta file from the input fasta file")
parser.add_option("-o", "--output", dest="output_filename", default=None, action="store", type="string", help="Output file name")

(options, args) = parser.parse_args()

sequences = SeqIO.index(options.fasta_filename, "fasta")
discard = SeqIO.index(options.discard_filename, "fasta")

diff_seqs = set()
for key, record in sequences.iteritems():
	if key not in discard:
		diff_seqs.add(record)

handle = open(options.output_filename, 'w')
SeqIO.write(diff_seqs, handle, "fasta")
handle.close()
