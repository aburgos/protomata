
args = commandArgs();
input_data <- args[6]
y_min <- args[7]
y_max <- args[8]
title <- args[9]

# set output for image
#png(paste(input_data, ".png"), width=800, height=600)
pdf(paste(input_data, ".pdf", sep = ""))

# get data from file
data <- read.table(input_data, header=TRUE, sep="\t")

y_min = as.numeric(y_min)
y_max = as.numeric(y_max)

matplot(
	as.numeric(data[[2]]),
	as.numeric(data[[2]]),
	type = "n",
	main = title,
	cex.lab = 1.5,
	cex.axis = 1.5,
	xlab = 'K-fold step',
	ylab = 'Scores',
	xlim = c(1, nrow(data)),
	ylim = c(y_min, y_max)
)

num_seqs = ncol(data) - 2
formatted_data <- matrix(0, num_seqs * nrow(data), 2)

for(row in (1:nrow(data)))
{
	parameter = as.numeric(data[[1]][row])
	row_data = data[row,]

	points(
		parameter,
		row_data[2],
		pch = 3,
		col = "green"
	)

	for(i in (1:num_seqs))
	{
		formatted_data[i + (row - 1) * num_seqs, ] = c(row, data[row,i + 2])
	}
}

groups = factor(formatted_data[,1])

boxplot(
	formatted_data[,2] ~ groups,
	formatted_data,
	col = "tomato3",
	boxwex = 0.5,
	style = "tukey",
	outline = FALSE,
	axes = FALSE,
	ann = FALSE,
	add = TRUE
)

legend(
	"topright",
	bg = "grey80",
	c("Positive Validation", "Negative Validation"),
	pch = c(3,4),
	col = c("green", "red")
)
