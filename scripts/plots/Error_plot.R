
args = commandArgs();
input_data <- args[6]
title <- args[7]

# set output for image
#png(paste(input_data, ".png"), width=800, height=600)
pdf(paste(input_data, ".pdf", sep = ""))

# get data from file
data <- read.table(input_data, header=TRUE, sep="\t")

matplot(
	data[[1]],
	as.numeric(data[[2]]),
	type = "n",
	main = title,
	cex.lab = 1.5,
	cex.axis = 1.5,
	xlab = names(data)[1],
	ylab = "Error rate",
	xlim = c(0, nrow(data)),
	ylim = c(0, 1)
)

for(row in (1:nrow(data)))
{
	parameter = data[[1]][row]

    for(column in (2:ncol(data)))
	{
		points(
			parameter,
			data[[column]][row],
			pch = 20,
			col = column - 1
		)
	}

	segments(parameter, 0, parameter, 100, col = "grey30")
}

legend(
	"topright",
	bg = "grey80",
	names(data)[2:ncol(data)],
	pch = 20,
	col = (1:(nrow(data) - 1))
)
