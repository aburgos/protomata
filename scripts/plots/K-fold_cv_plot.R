
args = commandArgs();
input_data <- args[6]
y_min <- args[7]
y_max <- args[8]
title <- args[9]

# set output for image
#png(paste(input_data, ".png"), width=800, height=600)
pdf(paste(input_data, ".pdf", sep = ""))

# get data from file
data <- read.table(input_data, header=TRUE, sep="\t")

color <- NULL
color['pos_valid'] <- "green"
color['neg_valid'] <- "red"
shape <- NULL
shape['pos_valid'] <- 3
shape['neg_valid'] <- 4

finite_rows <- (1:length(data$mean))[is.finite(data$mean)]
y_min = as.numeric(y_min)
y_max = as.numeric(y_max)

matplot(
	as.numeric(data$class_label),
	as.numeric(data$mean),
	type = "n",
	main = title,
	cex.lab = 1.5,
	cex.axis = 1.5,
	xlab = 'K-fold step',
	ylab = 'Scores',
	xlim = c(1, nrow(data) / 2),
	ylim = c(y_min, y_max)
)

for(row in finite_rows)
{
	parameter = as.numeric(data[[1]][row])
	class_label = toString(data$class_label[[row]])
	mean = as.numeric(data$mean[[row]])
	std_dev = as.numeric(data$std_dev[[row]])

	# plot segment bars
	segments(parameter, mean, parameter, mean + std_dev, col = "grey30")
	segments(parameter, mean, parameter, mean - std_dev, col = "grey30")
	segments(parameter - 0.05, mean + std_dev, parameter + 0.05, mean + std_dev, col = "grey30")
	segments(parameter - 0.05, mean - std_dev, parameter + 0.05, mean - std_dev, col = "grey30")

	points(
		parameter,
		mean,
		pch = shape[class_label],
		col = color[class_label]
	)
}

legend(
	"topright",
	bg = "grey80",
	c("Positive Validation", "Negative Validation"),
	pch = shape,
	col = color
)
