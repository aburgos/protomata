
args = commandArgs();
input_data <- args[6]
title <- args[7]

# set output for image
#png(paste(input_data, ".png"), width=800, height=600)
pdf(paste(input_data, ".pdf", sep = ""))

# get data from file
data <- read.table(input_data, header=TRUE, sep="\t")

rows <- (1:length(data$parameter_number))
xlim_max = max(data$parameter_number) + 1
ylim_min = min(data$precision, data$recall, 0.5)
ylim_max = max(data$precision, data$recall)

matplot(
	0,
	0,
	type = "o",
	main = title,
	cex.lab = 1.5,
	cex.axis = 1.5,
	xlab = "Parameters",
	ylab = "",
	xlim = c(0,xlim_max),
	ylim = c(ylim_min,ylim_max),
	axes = FALSE,
	ann = TRUE,
)

axis(1, at=0:xlim_max)
axis(2, at=0.1*0:10)

for(i in 0.1*0:10)
{
	segments(0, i, xlim_max, i, col = "grey30")
}

for(row in rows)
{
	lines(
		data[[1]],
		data[[2]],
		col = 2,
		pch = 16,
		lwd = 2,
		type = "o"
	)
	lines(
		data[[1]],
		data[[3]],
		col = 4,
		pch = 16,
		lwd = 2,
		type = "o"
	)

	fmeasure <- (2 * data[[2]] * data[[3]]) / (data[[2]] + data[[3]])

	lines(
		data[[1]],
		fmeasure,
		col = 3,
		pch = 16,
		lwd = 2,
		type = "o"
	)
}

legend(
	"bottomright",
	bg = "grey80",
	c("Precision", "Recall", "F-measure"),
	col = c(2,4,3),
	pch = 16,
	lty = 1
)
