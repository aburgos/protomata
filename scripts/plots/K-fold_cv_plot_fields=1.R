
args = commandArgs();
input_data <- args[6]

# set output for image
#png(paste(input_data, ".png"), width=800, height=600)
pdf(paste(input_data, "_fields_1.pdf", sep = ""))

# get data from file
data <- read.table(input_data, header=TRUE, sep="\t")

categories <- levels(data$class_label)

color <- NULL
color['pos_train'] <- "seagreen"
color['pos_valid'] <- "green"
color['neg_valid'] <- "red"
color['pos_test'] <- "chartreuse"
color['neg_test'] <- "coral"
drift <- 0

matplot(
	data[[1]],
	as.numeric(data[[3]]),
	type = "n",
	main = "LOOCV Experiment",
	cex.lab = 1.5,
	cex.axis = 1.5,
	xlab = names(data)[2],
	ylab = names(data)[length(data) - 1],
	xlim = c(min(as.numeric(data[[2]])) - 1, max(as.numeric(data[[2]])) + 1),
	ylim = c(min(data$mean - data$std_dev) - 1, max(data$mean + data$std_dev) + 1),
)

for(category in categories)
{
	# drift added to the x coord of every point for plotting bars
	drift <- drift + 0.05

	index <- (1:length(data$mean))[data$class_label == category]

	category_data <- as.numeric(data[[3]])[index]
	mean_score <- data$mean[index]
	std_score <- data$std_dev[index]

	#if(category != 'positive' && category != 'negative')
	#{
	#	color[category] <- last.color
	#	last.color = last.color + 1
	#}

	points(
		category_data + drift,
		mean_score,
		pch = 20, # filled circle
		col = color[category],
	)

	# plot segment bars
	segments(category_data + drift, mean_score, category_data + drift, mean_score + std_score, col = "grey30")
	segments(category_data + drift, mean_score, category_data + drift, mean_score - std_score, col = "grey30")
	segments(category_data + drift - 0.03, mean_score + std_score, category_data + drift + 0.03, mean_score + std_score, col = "grey30")
	segments(category_data + drift - 0.03, mean_score - std_score, category_data + drift + 0.03, mean_score - std_score, col = "grey30")
}

legend(
	"topright",
	bg = "grey80",
	categories,
	pch = 20,
	col = color[categories]
)

if(!is.numeric(data[[3]]))
{
	legend(
		"topleft",
		bg = "grey90",
		paste(1:length(levels(data[[3]])), levels(data[[3]]))
	)
}
