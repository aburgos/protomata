
args = commandArgs();
input_data <- args[6]
title <- args[7]

# set output for image
#png(paste(input_data, ".png"), width=800, height=600)
pdf(paste(input_data, ".pdf", sep = ""))

# get data from file
data <- read.table(input_data, header=TRUE, sep="\t")

finite_rows <- (1:length(data$mean))[is.finite(data$mean)]
y_min = min(min(data$normalized_score[finite_rows]), min(data$mean_distance[finite_rows]), min(data$std_dev_distance[finite_rows]))
ylim_min = y_min - 1
y_max = max(max(data$normalized_score[finite_rows]), max(data$mean_distance[finite_rows]), max(data$std_dev_distance[finite_rows]))
ylim_max = y_max + 1

matplot(
	data[[1]],
	as.numeric(data$normalized_score),
	type = "n",
	main = title,
	cex.lab = 1.5,
	cex.axis = 1.5,
	xlab = names(data)[1],
	ylab = "Scores",
	xlim = c(0, nrow(data) + 1),
	ylim = c(ylim_min, ylim_max)
)

for(row in finite_rows)
{
	parameter = data[[1]][row]
	norm_score = data$normalized_score[[row]]
	mean_dist = data$mean_distance[[row]]
	std_dev_dist = data$std_dev_distance[[row]]

	points(
		parameter,
		norm_score,
		pch = 20,
		col = 1
	)
	points(
		parameter,
		mean_dist,
		pch = 20,
		col = 2
	)
	points(
		parameter,
		std_dev_dist,
		pch = 20,
		col = 3
	)

	segments(parameter, ylim_min, parameter, ylim_max, col = "grey30")
}

legend(
	"topright",
	bg = "grey80",
	c("Normalized score", "Mean distance", "Std_dev distance"),
	pch = 20,
	col = c(1,2,3)
)
