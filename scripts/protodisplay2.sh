#!/bin/sh

PROTO2LOGO_CMD='proto2logo.py'
PDFVIEWER_CMD='acroread'

if [ $# -ne 1 -a $# -ne 2 ]; then 
    echo "usage $0 protomata [title]"
    exit
fi

PROTOMATA=$1
PROTOMATA_BASENAME=`basename $PROTOMATA`
PROTOMATA_BASENAME_NO_EXT=${PROTOMATA_BASENAME%.*}
OUTPUT_DIR="logo_dir"

if [ $# -ge 2 ]; then
	TITLE=$2
else
	TITLE=$PROTOMATA
fi

if [ ! -f $PROTOMATA ]; then
    echo "ERROR: $PROTOMATA does not exists: stop"
    exit
fi

if [ -d $OUTPUT_DIR ]; then
    echo "WARNING: $OUTPUT_DIR already exists: stop"
    exit
fi

echo "unning $0..." 
echo "($0 uses a2ping which outputs errors but whose pdf output is handled more nicely in Latex than the classical way..."
echo "The classical error message are:"
echo "Name "main::opt_extra" used only once: possible typo at /usr/bin/a2ping line 546."
echo "Name "main::opt_help" used only once: possible typo at /usr/bin/a2ping line 534."
echo "GPL Ghostscript 8.63: Unrecoverable error, exit code 1)"



LOGOS_BASENAME=${PROTOMATA_BASENAME_NO_EXT}_logos

# Generating sequence logos view
if [ -z "$TITLE" ]; then
	$PROTO2LOGO_CMD -i $PROTOMATA -d $OUTPUT_DIR > /dev/null
else
	$PROTO2LOGO_CMD -i $PROTOMATA -d $OUTPUT_DIR -t $TITLE > /dev/null
fi

# classical way
## Generating ps
#dot -Tps2 $OUTPUT_DIR/$LOGOS_BASENAME.dot -o $LOGOS_BASENAME.ps2
## Generating pdf
#ps2pdf $LOGOS_BASENAME.ps2

# a2ping way
dot -Tps $OUTPUT_DIR/$LOGOS_BASENAME.dot -o $LOGOS_BASENAME.ps
a2ping $LOGOS_BASENAME.ps PDF: $LOGOS_BASENAME.pdf


# Generating svg 
#pstoedit -f plot-svg $LOGOS_BASENAME.ps $LOGOS_BASENAME.svg
# cleaning temp files
rm -r $OUTPUT_DIR
# display
$PDFVIEWER_CMD $LOGOS_BASENAME.pdf
