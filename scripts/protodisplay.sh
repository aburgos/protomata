#!/bin/sh

PROTO2LOGO_CMD='proto2logo.py'
PDFVIEWER_CMD='acroread'

if [ $# -ne 1 -a $# -ne 2 ]; then 
    echo "usage $0 protomata [title]"
    exit
fi

PROTOMATA=$1
PROTOMATA_BASENAME=`basename $PROTOMATA`
PROTOMATA_BASENAME_NO_EXT=${PROTOMATA_BASENAME%.*}
OUTPUT_DIR="logo_dir"

if [ $# -ge 2 ]; then
	TITLE=$2
else
	TITLE=$PROTOMATA
fi

if [ ! -f $PROTOMATA ]; then
    echo "ERROR: $PROTOMATA does not exists: stop"
    exit
fi

if [ -d $OUTPUT_DIR ]; then
    echo "WARNING: $OUTPUT_DIR already exists: stop"
    exit
fi

LOGOS_BASENAME=${PROTOMATA_BASENAME_NO_EXT}_logos

# Generating sequence logos view
if [ -z "$TITLE" ]; then
	$PROTO2LOGO_CMD -i $PROTOMATA -d $OUTPUT_DIR > /dev/null
else
	$PROTO2LOGO_CMD -i $PROTOMATA -d $OUTPUT_DIR -t $TITLE > /dev/null
fi
# Generating ps
dot -Tps2 $OUTPUT_DIR/$LOGOS_BASENAME.dot -o $LOGOS_BASENAME.ps
# Generating pdf
ps2pdf $LOGOS_BASENAME.ps
# Generating svg 
#pstoedit -f plot-svg $LOGOS_BASENAME.ps $LOGOS_BASENAME.svg
# cleaning temp files
rm -r $OUTPUT_DIR
# display
$PDFVIEWER_CMD $LOGOS_BASENAME.pdf
