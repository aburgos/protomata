#!/bin/bash

PROTO2LOGO_CMD='proto2logo.py'

if [ $# -eq 0 ]; then 
    PREFIX="sample"
elif [ $# -eq 1 ]; then 
    PREFIX=$1
else
    echo "usage $0 [prefix]"
    echo "assuming that basename.xml contains the protomata and the corresponding dot file is basename.dot" 
    exit
fi

# Generating sequence logos view
$PROTO2LOGO_CMD -i $1.xml -d output_logos
# Generating ps
dot -Tps output_logos/${1}_logos.dot -o ${1}_logos.ps
convert -density 300 ${1}_logos.ps ${1}_logos.jpg

rm -rf output_logos
rm ${1}_logos.ps
