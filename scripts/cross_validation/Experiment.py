#! /usr/bin/env python2.6

import os, sys
import logging

from xml.dom.minidom import Document
from xml.dom.minidom import parse, parseString
from xml.dom.minidom import getDOMImplementation

from Parsers import Options_Parser
from Sequences import Sequences

# when parsing an XML file this class expands every path to the absolute path
class Experiment:

  # not set until is written to db
  _id = None

  # required values in an experiment's XML file
  _name = None
  _train = None

  # default values if not specified in XML file
  _validation_fold = 1
  _output = "."

  # optional values
  _desc = None
  _validation = None
  _test = None
  _positive_class = None
  _labels = None

  def __init__(self, xml_filename, logger, experiment_id = None):

    self._logger = logger

    if experiment_id == None:

      # parse an XML file by name
      dom = parse(xml_filename)

      # set experiment main variables
      for experiment_node in dom.getElementsByTagName('experiment'):
        if experiment_node.hasAttribute('name'):
          self._name = experiment_node.getAttribute('name');
        else:
          self._logger.debug("No experiment name defined.")
          sys.exit(1)
        if experiment_node.hasAttribute('train'):
          self._train = os.path.abspath(experiment_node.getAttribute('train'));
          self._train_sequences = Sequences(self._train)
        else:
          self._logger.debug("No experiment train file defined.")
          sys.exit(1)
        if experiment_node.hasAttribute('desc'):
          self._desc = experiment_node.getAttribute('desc');
        if experiment_node.hasAttribute('validation'):
          self._validation = os.path.abspath(experiment_node.getAttribute('validation'));
          self._validation_sequences = Sequences(self._validation)
        else:
          self._validation_sequences = Sequences()
        if experiment_node.hasAttribute('test'):
          self._test = os.path.abspath(experiment_node.getAttribute('test'));
          self._test_sequences = Sequences(self._test)
        else:
          self._test_sequences = Sequences()
        if experiment_node.hasAttribute('labels'):
          self._labels = os.path.abspath(experiment_node.getAttribute('labels'));
        if experiment_node.hasAttribute('positive_label'):
          self._positive_class = experiment_node.getAttribute('positive_label');
        if experiment_node.hasAttribute('validation_fold'):
          self._validation_fold = experiment_node.getAttribute('validation_fold');
        if experiment_node.hasAttribute('output_dir'):
          self._output = os.path.abspath(experiment_node.getAttribute('output_dir'));

      # initialize each program in the experiment
      program_parameters = dict()
      for program_node in dom.getElementsByTagName('parameters'):
        prog_opt = Options_Parser(program_node, self._logger)
        program_parameters[prog_opt._name] = prog_opt.generate_parameter_combinations()

      # create all program parameters instances
      self._parameters = dict()
      for params in program_parameters['protomata-learner']:
        self._parameters[len(self._parameters)] = params

    else:

      self._id = experiment_id
      # in this case the xml_filename is a dictionary
      self._parameters = xml_filename

    self._logger.info("Experiment: %s parameter combinations" % (len(self._parameters)))

  def total_parameters(self):
    return len(self._parameters)

  def train_filename(self):
    return self._train

  def train_sequences(self, class_type = None):
    if class_type == None:
      return self._train_sequences
    else:
      return_seqs = Sequences()
      for seq in self._train_sequences:
        if class_type == self._class_label[seq.id]:
          return_seqs.add_sequence(seq)
    return return_seqs

  def validation_sequences(self, class_type = None):
    if class_type == None:
      return self._validation_sequences
    else:
      return_seqs = Sequences()
      for seq in self._validation_sequences:
        if class_type == self._class_label[seq.id]:
          return_seqs.add_sequence(seq)
    return return_seqs

  def test_sequences(self, class_type = None):
    if class_type == None:
      return self._test_sequences
    else:
      return_seqs = Sequences()
      for seq in self._test_sequences:
        if class_type == self._class_label[seq.id]:
          return_seqs.add_sequence(seq)
    return return_seqs

  def sequences_labels(self):
    self._class_label = dict()
    if self._labels != None:
      try:
        labels_file = open(self._labels, 'r')
      except IOError as (errno, strerror):
        print "I/O error({0}): {1}".format(errno, strerror)
        raise
      for s in labels_file.readlines():
        sequence_name = str((s[:-1].split('\t'))[0])
        class_name = str(((s[:-1]).split('\t'))[1]).replace("/", "-").replace(" ", "-").translate(None, "'()\r\n")
        if class_name == self._positive_class:
          self._class_label[sequence_name] = 'positive'
        else:
          self._class_label[sequence_name] = 'negative'
    #else:
    #  for sequence in self._train_sequences:
    #    class_label[sequence.name] = 'positive'

    return self._class_label

  def output_directory(self):
    return self._output

  def validation_fold(self):
    return int(self._validation_fold)

  def get_id(self):
    if self._id == None:
      print("Error: experiment not written to DB.")
      sys.exit(1)
    return self._id

  # gets the minimum integer value for all its runs
  def min_integer_value(self, flag):
    # TODO: check for flag type
    min_value = sys.maxint
    for params in self._parameters.values():
      if params.has_flag(flag):
        if int(params.get_value(flag)) < min_value:
          min_value = int(params.get_value(flag))
    return min_value

  # gets the maximum integer value for all its runs
  def max_integer_value(self, flag):
    # TODO: check for flag type
    max_value = -sys.maxint
    for params in self._parameters.values():
      if params.has_flag(flag):
        if int(params.get_value(flag)) > max_value:
          max_value = int(params.get_value(flag))
    return max_value

  def significativity(self):
    for params in self._parameters.values():
      if params.has_flag('significativity'):
        return True
    return False

  # iterates over all protomata learner parameters
  def __iter__(self):
    return self._parameters.__iter__()

  def items(self):
    return self._parameters.items()

  def parameters_items(self):
    return self._parameters.items()

  def rm_parameter(self, full_flag):
    program, short_flag = full_flag.split(".")
    for plp in self._parameters.values():
      plp.rm_parameter(program, short_flag)
    to_delete = set()
    for N_1, plp_1 in self._parameters.items():
      for i in range(N_1 + 1, len(self._parameters)):
        if plp_1._all_parameters == self._parameters[i]._all_parameters:
          to_delete.add(i)
    new_parameters = dict()
    for index in range(0, len(self._parameters)):
      if index not in to_delete:
        new_parameters[len(new_parameters)] = self._parameters[index]
    self._parameters = new_parameters

  # write to database experiment's data
  def write2db(self, db_connection):

    self._id = db_connection.write_protomata_experiment(self)

    # write experiment's data
    self._train_sequences.write2db(db_connection, self, "train")
    self._validation_sequences.write2db(db_connection, self, "validation")
    self._test_sequences.write2db(db_connection, self, "test")

    # write all parameters used in the experiment
    for number, parameters in self._parameters.items():
      parameters.experiment_id(self._id)
      parameters.write2db(db_connection)

    return self._id

