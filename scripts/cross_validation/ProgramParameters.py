#! /usr/bin/env python2.6

import os, sys

class Program_Parameters:

  def __init__(self, name, logger):

    self._id = None
    self._name = name
    self._parameters = dict()
    self._logger = logger

  def has_flag(self, flag):
    return self._parameters.has_key(flag)

  def add_flag(self, flag):
    self._parameters[flag] = ""

  def set_flag(self, flag, value):
    self._parameters[flag] = value

  def set_flags(self, parameters):
    self._parameters = parameters

  def rm_flag(self, flag):
    if flag in self._parameters:
      del self._parameters[flag]

  def get_value(self, flag):
    return self._parameters[flag]

  def get_id(self):
    if self._id == None:
      print("Error: " + self._name + " program run not written to DB.")
      sys.exit(1)
    return self._id

  def get_name(self):
    return self._name

  def get_command(self):
    command = self._name + " "
    for flag, value in self._parameters.items():
      if value != "":
        command += flag + " " + str(value) + " "
      else:
        command += flag + " "
    return command.rstrip(" ")

  def __iter__(self):
    return self._parameters.__iter__()

  def items(self):
    return self._parameters.items()

  def write2db(self, db_connection):
    self._id = db_connection.write_program_parameters(self)
    return self._id

