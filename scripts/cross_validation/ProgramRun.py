#! /usr/bin/env python2.6

import os, sys
import MySQLdb
import subprocess

from utilities import timestamp

class Program_Run:

  def __init__(self, name, command, logger):

    self._id = None
    self._name = name
    self._input = None
    self._output = None
    self._other_param = dict()
    self._command = command
    self._logger = logger

  def input_file(self, input_filename):
    self._input = input_filename

  def output_file(self, output_filename):
    self._output = output_filename

  def add_parameter(self, flag, value):
    self._other_param[flag] = value

  def get_id(self):
    if self._id == None:
      print("Error: " + self._name + " program run not written to DB.")
      sys.exit(1)
    return self._id

  def execute(self):
    command = self._command
    if self._input == None:
      print("Error: No input file selected for program " + self._name)
      sys.exit(1)
    command += " --input " + self._input
    #if self._output == None:
    #  print("Error: No output file selected for program " + self._name)
    #  sys.exit(1)
    if self._output != None:
      command += " --output " + self._output
    for flag, value in self._other_param.items():
      command += " " + flag + " " + value

    self._start_time = timestamp()
    os.system(command)
    #print(command)
    #try:
    #  self._logger.debug("Program_Run: Executing " % (command))
    #  run = subprocess.Popen(command)
    #except:
    #  raise RuntimeError("Error executing " + command)
    self._finish_time = timestamp()

  def write2db(self, db_connection):
    self._id = db_connection.write_program_run(self)
    return self._id

