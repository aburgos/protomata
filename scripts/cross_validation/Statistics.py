#! /usr/bin/env python2.6

import os, sys
import math
import itertools
import logging

from utilities import *
from optparse import OptionParser
from Sequences import Sequences
from Experiment import Experiment
from ProgramRun import Program_Run
from Parsers import *

# global constants
dialign_exe = "dialign"
plots_path = 

# parse CLI arguments
usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)
parser.add_option("-i", "--input", dest="exp_filename", default=None, action="store", type="string", help="Experiment XML file")
parser.add_option("-o", "--output", dest="output_directory", default=".", action="store", type="string", help="Output directory")
parser.add_option("--no-validation", dest="no_validation", default=False, action="store_true", help="Treat validation sequences as test sequences.")

(options, args) = parser.parse_args()

# log to std output
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-3s %(message)s',
                    datefmt='%d%b%Y %H:%M:%S',
                    filename=options.exp_filename + '.log',
                    filemode='a')

logging.info('Computing statistics from previously computed results')

# parse parameters from XML file
experiment = Experiment(options.exp_filename, logging)

# get sequences labels
sequences_labels = experiment.sequences_labels()

# get sets of sequences
test_sequences = dict()
validation_sequences = dict()
train_sequences_plus_valid = dict()
train_sequences = experiment.train_sequences()
validation_sequences['pos_valid'] = experiment.validation_sequences('positive')
validation_sequences['neg_valid'] = experiment.validation_sequences('negative')
train_sequences_plus_valid['pos_valid'] = Sequences()
train_sequences_plus_valid['pos_valid'].add_sequences(train_sequences)
train_sequences_plus_valid['pos_valid'].add_sequences(validation_sequences['pos_valid'])
train_sequences_plus_valid['neg_valid'] = validation_sequences['neg_valid']
test_sequences['pos_test'] = experiment.test_sequences('positive')
test_sequences['neg_test'] = experiment.test_sequences('negative')

if options.no_validation:
  test_sequences['pos_test'].add_sequences(validation_sequences['pos_valid'])
  test_sequences['neg_test'].add_sequences(validation_sequences['neg_valid'])
  validation_sequences['pos_valid'] = Sequences()
  validation_sequences['neg_valid'] = Sequences()

total_validation_sequences = len(validation_sequences['pos_valid']) + len (validation_sequences['neg_valid'])
total_test_sequences = len(test_sequences['pos_test']) + len (test_sequences['neg_test'])
total_train_valid = len(train_sequences_plus_valid['pos_valid']) + len(train_sequences_plus_valid['neg_valid'])

# remove parameters that you don't want to be taken into account in format 'program.field'
#experiment.rm_parameter('paloma2.threshold')
#experiment.rm_parameter('paloma2.quorum')

os.makedirs(options.output_directory)

output_name = experiment._name.replace(" ", "_")
path_basename = options.output_directory + "/" + output_name
experiment_directory = experiment.output_directory()

# create file for storing scores results for all parameters
experiment_score_filename = path_basename + "_all_parameters.scores"
experiment_score_file = open(experiment_score_filename, "w")
any_params = experiment._parameters[0]
parameters_list = get_formatted_parameters(any_params, 'parameters_number', 'class_label', 'mean', 'std_dev')
experiment_score_file.write(parameters_list)

# create file for storing scores margin for all parameters
experiment_margin_filename = path_basename + "_all_parameters.margins"
experiment_margin_file = open(experiment_margin_filename, "w")
experiment_margin_file.write("parameter_number\tpositive_normalized_score\tnormalized_score\n")

# create file for storing ftest scores for all parameters
experiment_fscores_filename = path_basename + "_all_parameters.fscores"
experiment_fscores_file = open(experiment_fscores_filename, "w")
experiment_fscores_file.write("parameter_number\tfscore\n")

# create file for storing error strategies for all parameters
experiment_error_stg_filename = path_basename + "_all_parameters.validation_error_strategies"
experiment_error_stg_file = open(experiment_error_stg_filename, "w")
experiment_error_stg_file.write("parameter_number\tmean_half\tmean_half_std_dev\tmean_half_min_max\n")

if total_test_sequences > 0:

  # create file for storing errors for all parameters
  experiment_error_filename = path_basename + "_all_parameters.errors"
  experiment_error_file = open(experiment_error_filename, "w")
  experiment_error_file.write("parameter_number\tvalidation_error\ttest_error\n")

  # create file for storing error strategies for all parameters
  experiment_test_error_stg_filename = path_basename + "_all_parameters.test_error_strategies"
  experiment_test_error_stg_file = open(experiment_test_error_stg_filename, "w")
  experiment_test_error_stg_file.write("parameter_number\tmean_half\tmean_half_std_dev\tmean_half_min_max\n")

  # create file for storing precision and recall for all parameters
  experiment_pr_filename = path_basename + "_all_parameters.pr"
  experiment_pr_file = open(experiment_pr_filename, "w")
  experiment_pr_file.write("parameter_number\tprecision\trecall\n")

basename, extension = os.path.splitext(os.path.basename(experiment.train_filename()))
basename = basename.rstrip("_train")
k = experiment.validation_fold()

y_min_scr = 0
y_max_scr = 0
y_min_valid_err = 0
y_max_valid_err = 0
y_min_test_err = 0
y_max_test_err = 0
y_min_mrg = 0
y_max_mrg = 0
y_min_fsc = 0
y_max_fsc = 0

# store all scores
scores = dict()
mean = dict()
std_dev = dict()

parameter_score_file = dict()
parameter_dtl_score_file = dict()
parameter_margin_file = dict()
parameter_roc_file = dict()

# generate k-fold combinations
kfold_combinations = kfold_combinations(experiment.train_sequences(), k)

for parameters_number in range(1, experiment.total_parameters() + 1):

  # store each k-fold scores of a parameters set
  scores[parameters_number] = dict()
  mean[parameters_number] = dict()
  std_dev[parameters_number] = dict()

  # create a directory for the parameter's fold results
  parameters_output = "parameters_" + str(parameters_number)
  parameters_directory = options.output_directory + "/" + parameters_output + "/"
  os.makedirs(parameters_directory)

  # create file for storing score results of each parameter
  parameter_score_filename = parameters_directory + output_name + "_" + parameters_output + ".scores"
  parameter_score_file[parameters_number] = open(parameter_score_filename, "w")
  parameter_score_file[parameters_number].write(str(k) + "-fold_step\tclass_label\tmean\tstd_dev\n")
  # create file for storing detailed score results of each parameter
  parameter_dtl_score_filename = parameters_directory + output_name + "_" + parameters_output + ".detailed"
  parameter_dtl_score_file[parameters_number] = open(parameter_dtl_score_filename, "w")
  header = str(k) + "-fold_step"
  # left out sequences
  total_left_out_sequences = len(train_sequences) - len(kfold_combinations[1])
  for seq_n in range(1, total_left_out_sequences + 1):
    header += "\tpos_seq_" + str(seq_n)
  # positive validation sequences
  for seq_n in range(1, len(validation_sequences['pos_valid']) + 1):
    header += "\tpos_seq_" + str(total_left_out_sequences + seq_n)
  # negative validation sequences
  for seq_n in range(1, len(validation_sequences['neg_valid']) + 1):
    header += "\tneg_seq_" + str(seq_n)
  header += "\n"
  parameter_dtl_score_file[parameters_number].write(header)
  # create file for storing margins of each parameter
  parameter_margin_filename = parameters_directory + output_name + "_" + parameters_output + ".margins"
  parameter_margin_file[parameters_number] = open(parameter_margin_filename, "w")
  parameter_margin_file[parameters_number].write(str(k) + "-fold_step\tnormalized_score\n")
  # create file for storing roc curves of each parameter
  parameter_roc_filename = parameters_directory + output_name + "_" + parameters_output + ".roc"
  parameter_roc_file[parameters_number] = open(parameter_roc_filename, "w")
  parameter_roc_file[parameters_number].write("parameter_number\ttrue_positive_rate\tfalse_positive_rate\n")

# loop on training set leaving k sequences out
for kfold_step, current_train_sequences in kfold_combinations.items():

  # loop on parameters
  logging.info('fold iteration: %d / %d' % (kfold_step, len(kfold_combinations)))
  kfold_directory = experiment_directory + "/%d-fold_%d/" % (k, kfold_step)

  parameters_number = 0
  to_scan_sequences_filename = kfold_directory + basename + "_rest_step_%d" % (kfold_step) + extension

  # iterate over the parameters of the experiment
  total_iterations = len(experiment.parameters_items())
  for iteration_number, parameters in experiment.parameters_items():

    logging.info('getting scores for parameters: %d / %d' % (iteration_number + 1, len(experiment.parameters_items())))
    output_scan_filename = to_scan_sequences_filename + "_scan_" + str(iteration_number + 1) + ".txt"

    significativity = parameters.has_flag("--significativity")
    scan_values = Protoscan_Parser(output_scan_filename, significativity)

    parameters_number += 1
    scores[parameters_number][kfold_step] = scan_values.score()

    parameter_dtl_score_file[parameters_number].write("%d" % kfold_step)
    for seq_n in train_sequences:
      if seq_n.id in scores[parameters_number][kfold_step].keys():
        y_max_scr = max(y_max_scr, scores[parameters_number][kfold_step][seq_n.id])
        parameter_dtl_score_file[parameters_number].write("\t%f" % scores[parameters_number][kfold_step][seq_n.id])
    for seq_n in validation_sequences['pos_valid']:
      if scores[parameters_number][kfold_step][seq_n.id] != float('-inf'):
        y_min_scr = min(y_min_scr, scores[parameters_number][kfold_step][seq_n.id])
      parameter_dtl_score_file[parameters_number].write("\t%f" % scores[parameters_number][kfold_step][seq_n.id])
    for seq_n in validation_sequences['neg_valid']:
      if scores[parameters_number][kfold_step][seq_n.id] != float('-inf'):
        y_min_scr = min(y_min_scr, scores[parameters_number][kfold_step][seq_n.id])
      parameter_dtl_score_file[parameters_number].write("\t%f" % scores[parameters_number][kfold_step][seq_n.id])
    parameter_dtl_score_file[parameters_number].write("\n")

    mean[parameters_number][kfold_step] = dict()
    std_dev[parameters_number][kfold_step] = dict()

    # for the left out training sequence set and the validation sequence set
    # get the mean and standard deviation of them
    for seqs_set, seqs in train_sequences_plus_valid.items():

      val1, val2 = get_mean_std(scan_values.score(), seqs)
      mean[parameters_number][kfold_step][seqs_set] = val1
      std_dev[parameters_number][kfold_step][seqs_set] = val2

      parameter_score_file[parameters_number].write("%d\t%s\t%f\t%f\n" \
                            % (kfold_step, seqs_set, val1, val2))

    norm_score = mean[parameters_number][kfold_step]['pos_valid'] - mean[parameters_number][kfold_step]['neg_valid']
    if std_dev[parameters_number][kfold_step]['neg_valid'] != 0:
      norm_score /= std_dev[parameters_number][kfold_step]['neg_valid']
    parameter_margin_file[parameters_number].write("%d\t%f\n" % (kfold_step, norm_score))

# general stats on parameters
for parameters_number, protomata_parameters in experiment.parameters_items():

  parameters_number += 1
  logging.info('computing averages for parameters: %d / %d' % (parameters_number, len(experiment.parameters_items())))

  score_mean = dict()
  score_stats = dict()
  sums_per_seq = dict()

  # computes scores
  for seqs_set, seqs in train_sequences_plus_valid.items():
    score_mean[seqs_set] = dict()
    for seq in seqs:
      score_mean[seqs_set][seq.id] = 0
      sums_per_seq[seq.id] = 0

  for seqs_set, seqs in train_sequences_plus_valid.items():
    for kfold_step in kfold_combinations:
      for seq in scores[parameters_number][kfold_step]:
        if seqs.has_sequence_accession(seq):
          score_mean[seqs_set][seq] += scores[parameters_number][kfold_step][seq]
          sums_per_seq[seq] += 1

    for seq in scores[parameters_number][kfold_step]:
      if seqs.has_sequence_accession(seq):
        score_mean[seqs_set][seq] /= sums_per_seq[seq]

    score_stats[seqs_set] = dict()
    score_stats[seqs_set]['mean'], score_stats[seqs_set]['std_dev'] = get_mean_std(score_mean[seqs_set], seqs)
    score_list = get_formatted_values(protomata_parameters, parameters_number, \
                                      seqs_set, score_stats[seqs_set]['mean'], score_stats[seqs_set]['std_dev'])
    experiment_score_file.write(score_list)

  # compute margins
  norm_scores = dict()
  for seqs_set, seqs in train_sequences_plus_valid.items():
    norm_scores[seqs_set] = dict()
    for kfold_step in kfold_combinations:
      norm_scores[seqs_set][kfold_step] = dict()
      for seq in scores[parameters_number][kfold_step]:
        if seqs.has_sequence_accession(seq):
          norm_scores[seqs_set][kfold_step][seq] = scores[parameters_number][kfold_step][seq]
          norm_scores[seqs_set][kfold_step][seq] -= mean[parameters_number][kfold_step]['neg_valid']
          if std_dev[parameters_number][kfold_step]['neg_valid'] != 0:
            norm_scores[seqs_set][kfold_step][seq] /= std_dev[parameters_number][kfold_step]['neg_valid']

  # eliminate seqs_set
  all_norm_scores = dict()
  for seqs_set, seqs in train_sequences_plus_valid.items():
    for seq in seqs:
      all_norm_scores[seq.id] = 0

  # eliminate kfold
  by_set_norm_scores = dict()
  for seqs_set, seqs in train_sequences_plus_valid.items():
    by_set_norm_scores[seqs_set] = dict()
    for kfold_step in kfold_combinations:
      for seq in norm_scores[seqs_set][kfold_step]:
        all_norm_scores[seq] += norm_scores[seqs_set][kfold_step][seq]
        by_set_norm_scores[seqs_set][str(kfold_step) + seq] = norm_scores[seqs_set][kfold_step][seq]

  # get averages (no need for train sequences since they are only evaluated once)
  for seq in validation_sequences['pos_valid']:
    all_norm_scores[seq.id] /= len(kfold_combinations)
  for seq in validation_sequences['neg_valid']:
    all_norm_scores[seq.id] /= len(kfold_combinations)

  mean_norm_score = dict()
  for seqs_set in train_sequences_plus_valid:
    mean_norm_score[seqs_set], norm_std_dev = get_mean_std_dev(by_set_norm_scores[seqs_set])

  all_sequences_norm_score = by_set_norm_scores['pos_valid']
  for k, v in by_set_norm_scores['neg_valid'].items():
    all_sequences_norm_score[k] = v
  discard, all_means_std_dev = get_mean_std_dev(all_sequences_norm_score)
  norm_score = mean_norm_score['pos_valid'] - mean_norm_score['neg_valid']
  if all_means_std_dev != 0:
    norm_score /= all_means_std_dev
  experiment_margin_file.write("%d\t%f\t%f\n" % (parameters_number, mean_norm_score['pos_valid'], norm_score))

  # get min and max margin values for plotting
  y_min_mrg = min(y_min_mrg, mean_norm_score['pos_valid'], norm_score)
  y_max_mrg = max(y_max_mrg, mean_norm_score['pos_valid'], norm_score)

  # compute fscores  
  overall_mean = (score_stats['pos_valid']['mean'] + score_stats['neg_valid']['mean']) / 2
  pos_pow = math.pow((score_stats['pos_valid']['mean'] - overall_mean), 2)
  neg_pow = math.pow((score_stats['neg_valid']['mean'] - overall_mean), 2)
  explained_var = len(score_stats['pos_valid']) * pos_pow + len(score_stats['neg_valid']) * neg_pow

  within_group_sum_squares = 0
  for seqs_set, seqs in score_mean.items():
    for seq in seqs:
      if score_mean[seqs_set][seq] != float('-inf'):
        within_group_sum_squares += math.pow((score_mean[seqs_set][seq] - score_stats[seqs_set]['mean']), 2)
  unexplained_var = within_group_sum_squares / (2 * len(score_mean['pos_valid']))

  if unexplained_var != 0:
    fscore = explained_var / unexplained_var
    experiment_fscores_file.write("%d\t%f\n" % (parameters_number, fscore))

    # update plot range for fscores
    if fscore < y_min_fsc:
      y_min_fsc = fscore
    if fscore > y_max_fsc:
      y_max_fsc = fscore

  else:
    experiment_fscores_file.write("%d\t%f\n" % (parameters_number, 0))

  # output parameters scores and margins
  title = get_parameters_values(protomata_parameters)

  # get directory path
  parameters_output = "parameters_" + str(parameters_number)
  parameters_directory = options.output_directory + "/" + parameters_output + "/"

  parameter_score_file[parameters_number].close()
  parameter_score_filename = parameters_directory + output_name + "_parameters_" + str(parameters_number) + ".scores"
  os.system("Rscript " + plots_path + "K-fold_cv_plot.R " + parameter_score_filename + " " + str(y_min_scr + 1) + " " + str(y_max_scr + 1) + " " + title)

  parameter_margin_file[parameters_number].close()
  parameter_margin_filename = parameters_directory + output_name + "_parameters_" + str(parameters_number) + ".margins"
  os.system("Rscript " + plots_path + "NormalizedScore.R " + parameter_margin_filename + " " + title)

  parameter_dtl_score_file[parameters_number].close()
  parameter_dtl_score_filename = parameters_directory + output_name + "_parameters_" + str(parameters_number) + ".detailed"
  os.system("Rscript " + plots_path + "box_plot.R " + parameter_dtl_score_filename + " " + str(y_min_scr + 1) + " " + str(y_max_scr + 1) + " " + title)

  # CLASSIFIER APPROACHES
  all_score_mean = dict()
  for seqs_set, seqs in train_sequences_plus_valid.items():
    for seq in seqs:
      all_score_mean[seq.id] = score_mean[seqs_set][seq.id]

  neg_validation_sequences_subset = get_higher_scores(all_score_mean, validation_sequences['neg_valid'], len(train_sequences_plus_valid['pos_valid']))

  if total_test_sequences > 0:

    # parse test scores
    test_output_path = experiment.output_directory() + "/test/"
    output_scan_filename = test_output_path + output_name + "_test_scan_" + str(parameters_number) + ".txt"

    test_values = Protoscan_Parser(output_scan_filename, significativity)
    test_scan_values = test_values.score()
    neg_test_sequences_subset = get_higher_scores(test_scan_values, test_sequences['neg_test'], len(test_sequences['pos_test']))

  thr_mean = dict()
  thr_std_dev = dict()
  for seqs_set, seqs in train_sequences_plus_valid.items():
    thr_mean[seqs_set], thr_std_dev[seqs_set] = get_mean_std(score_mean[seqs_set], seqs)

  # mean half point using means
  thr_1 = (thr_mean['pos_valid'] + thr_mean['neg_valid']) / 2
  ve_1, vp_1, vr_1 = error_precision_recall(all_score_mean, thr_1, train_sequences_plus_valid['pos_valid'], neg_validation_sequences_subset)
  if total_test_sequences > 0:
    te_1, tp_1, tr_1 = error_precision_recall(test_scan_values, thr_1, test_sequences['pos_test'], neg_test_sequences_subset)

  # mean half std dev point using means
  thr_2 = thr_mean['pos_valid'] - thr_std_dev['pos_valid']
  thr_2 += (thr_mean['neg_valid'] + thr_std_dev['neg_valid'])
  thr_2 /= 2
  ve_2, vp_2, vr_2 = error_precision_recall(all_score_mean, thr_2, train_sequences_plus_valid['pos_valid'], neg_validation_sequences_subset)
  if total_test_sequences > 0:
    te_2, tp_2, tr_2 = error_precision_recall(test_scan_values, thr_2, test_sequences['pos_test'], neg_test_sequences_subset)

  # half point between minimum positive and maximum negative
  thr_3 = min(score_mean['pos_valid'].values()) + max(score_mean['neg_valid'].values())
  thr_3 /= 2
  ve_3, vp_3, vr_3 = error_precision_recall(all_score_mean, thr_3, train_sequences_plus_valid['pos_valid'], neg_validation_sequences_subset)
  if total_test_sequences > 0:
    te_3, tp_3, tr_3 = error_precision_recall(test_scan_values, thr_3, test_sequences['pos_test'], neg_test_sequences_subset)

  if total_test_sequences > 0:
    experiment_error_file.write("%d\t%f\t%f\n" % (parameters_number, ve_1, te_1))
    experiment_pr_file.write("%d\t%f\t%f\n" % (parameters_number, tp_1, tr_1))
    experiment_test_error_stg_file.write("%d\t%f\t%f\t%f\n" % (parameters_number, te_1, te_2, te_3))
  experiment_error_stg_file.write("%d\t%f\t%f\t%f\n" % (parameters_number, ve_1, ve_2, ve_3))

  # update error values for plotting
  y_min_valid_err = min(y_min_valid_err, ve_1, ve_2, ve_3)
  y_max_valid_err = max(y_max_valid_err, ve_1, ve_2, ve_3)
  if total_test_sequences > 0:
    y_min_test_err = min(y_min_test_err, te_1, te_2, te_3)
    y_max_test_err = max(y_max_test_err, te_1, te_2, te_3)

  # ROC curve computation (threshold varies from negative mean to positive mean)
  for thr in frange(mean_norm_score['pos_valid'], mean_norm_score['neg_valid'], 100):
    tpr, fpr = classification_rates(all_norm_scores, thr, train_sequences_plus_valid['pos_valid'], neg_validation_sequences_subset)
    parameter_roc_file[parameters_number].write("%f\t%f\t%f\n" % (thr, tpr, fpr))

  parameter_roc_file[parameters_number].close()
  parameter_roc_filename = parameters_directory + output_name + "_" + parameters_output + ".roc"
  os.system("Rscript " + plots_path + "ROC_curve.R " + parameter_roc_filename + " " + str(parameters_number) + " 'ROC curve on averaged scores'")

experiment_score_file.close()
experiment_margin_file.close()
experiment_fscores_file.close()
experiment_error_stg_file.close()

os.system("Rscript " + plots_path + "K-fold_cv_plot.R " + experiment_score_filename + " " + str(y_min_scr + 1) + " " + str(y_max_scr + 1) + " '" + experiment._name + " - Mean and Std Dev Scores'")

margin_plot_cmd = "Rscript " + plots_path + "dot_plot.R " + experiment_margin_filename + " "
margin_plot_cmd += str(y_min_mrg - 0.1) + " " + str(y_max_mrg + 0.1) + " 'Normalized Score' '" + experiment._name + " - Margins for scores'"

fscore_plot_cmd = "Rscript " + plots_path + "dot_plot.R " + experiment_fscores_filename + " "
fscore_plot_cmd += str(y_min_fsc - 2) + " " + str(y_max_fsc + 2) + " 'F-score' '" + experiment._name + " - F-test over averaged scores'"

error_stg_plot_cmd = "Rscript " + plots_path + "dot_plot.R " + experiment_error_stg_filename + " "
error_stg_plot_cmd += str(0) + " " + str(y_max_valid_err + 0.1) + " 'Error rate' '" + experiment._name + " - Different strategies for validation'"

os.system(margin_plot_cmd)
os.system(fscore_plot_cmd)
os.system(error_stg_plot_cmd)

if total_test_sequences > 0:

  # error rate results
  experiment_error_file.close()
  max_error = max(y_max_valid_err, y_max_test_err)
  error_plot_cmd = "Rscript " + plots_path + "dot_plot.R " + experiment_error_filename + " "
  error_plot_cmd += str(0) + " " + str(max_error + 0.1) + " 'Error rate' '" + experiment._name + " - Validation vs. Test'"
  os.system(error_plot_cmd)

  # test error strategies results
  experiment_test_error_stg_file.close()
  test_error_stg_plot_cmd = "Rscript " + plots_path + "dot_plot.R " + experiment_test_error_stg_filename + " "
  test_error_stg_plot_cmd += str(0) + " " + str(y_max_test_err + 0.1) + " 'Error rate' '" + experiment._name + " - Different strategies for test'"
  os.system(test_error_stg_plot_cmd)

  # precision recall results
  experiment_pr_file.close()
  os.system("Rscript " + plots_path + "PrecisionRecall.R " + experiment_pr_filename + " '" + experiment._name + " for test set using mean half strategy'")
