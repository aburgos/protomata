
K-fold cross-validation
-----------------------

From wikipedia:

In K-fold cross-validation, the original sample is randomly partitioned into K 
subsamples. Of the K subsamples, a single subsample is retained as the 
validation data for testing the model, and the remaining K − 1 subsamples are 
used as training data. The cross-validation process is then repeated K times 
(the folds), with each of the K subsamples used exactly once as the validation 
data. [...] The advantage of this method over repeated random sub-sampling is 
that all observations are used for both training and validation, and each 
observation is used for validation exactly once. [...]

Script:

The main goal is to find a good set of parameters. We do this by training a
classifier and seeing how well it generalizes to unseen data.

We set an experiment XML file like the one in the examples directory, where we
specify a value or a set of values for the parameters we want to try. The
script will perform a K-fold cross-validation for each possible combination of
these parameters. Then for each parameter's combination we will compute the
scores of the sequences in the unused subsample of the fold step, and the
scores of the validation sequences.

The validation sequences should be close in similarity to the train ones, so a 
good separation in score values between positive and negative sequences is an 
indicator of good parameters. We can also include positive sequences in the
validation set, though it is not necessary.

For assuring the validation results, a test set of unseen sequences can be
used. A model is built from the training sequences and test sequences are
scored. We use our trained classifier to classify them and to see how well
it generalizes.

Options for running the script are set in an XML file. See experiment.xml for 
an example.

Experiment options:

name: the name given to the experiment will be used in the graphics of 
statistics
train: a fasta file containing the set of positive sequences
validation: a fasta file containing the set of validation sequences
test: a fasta file containing the set of test sequences
labels: a text file where each sequence tag (of train and validation) is 
assigned a label
positive_label: the label of train sequences
validation_fold: the value of K in K-fold cross validation
output_dir: where the computed files will be stored

Parameters are described by their flag and its value. It is possible to use
several values for a flag, in this case the values should be separated by a
comma with no white spaces. It also possible to toggle a flag, setting t to
true. See the example for better understanding.
