#! /usr/bin/env python2.6

import time
import math
from operator import itemgetter
from Sequences import Sequences

def timestamp():
    lt = time.localtime(time.time())
    return "%04d-%02d-%02d %02d:%02d:%02d" % (lt[0], lt[1], lt[2], lt[3], lt[4], lt[5])

def extract_parameters(string):
  try:
    param = {}
    splitted_string = string.split(' ')
    for first, second in zip(splitted_string[1:], splitted_string[2:]):
      if first.startswith('-') and not second.startswith('-'):
        param[first] = second
      elif first.startswith('-') and second.startswith('-'):
        param[first] = None
      elif not first.startswith('-') and not second.startswith('-'):
        raise "No support to 2 consecutive values on command."
    return param
  except ValError:
    print "Error parsing command line options (extract_parameters): " + ValError

def get_mean_std(values, sequences):
  if len(sequences) == 0:
    return float('nan'), float('nan')
  mean = 0
  not_none_seqs_len = len(sequences)
  for sequence in sequences:
    if not values.has_key(sequence.id):
      not_none_seqs_len -= 1
    elif values[sequence.id] == None or values[sequence.id] == float('-inf') and values[sequence.id] != float('nan'):
    #elif values[sequence.id] == None:
      not_none_seqs_len -= 1
    else:
      mean += values[sequence.id]
  std = 0
  sum_diff_mean = 0
  if not_none_seqs_len > 0:
    mean /= not_none_seqs_len
    for sequence in sequences:
      if values.has_key(sequence.id):
        if values[sequence.id] != None and values[sequence.id] != float('-inf') and values[sequence.id] != float('nan'):
        #if values[sequence.id] != None:
          sum_diff_mean += math.pow((values[sequence.id] - mean), 2)
    sum_diff_mean /= not_none_seqs_len
    std = math.sqrt(sum_diff_mean)
  return mean, std

def get_mean_std_dev(dictionary):
  mean = 0
  total_values = len(dictionary)
  if total_values == 0:
    return float('nan'), float('nan')
  for value in dictionary.values():
    if value == None or value == float('nan') or value == float('-inf'):
      total_values -= 1
    else:
      mean += value
  mean /= total_values
  std = 0
  sum_diff_mean = 0
  for value in dictionary.values():
    if value != None and value != float('nan') and value != float('-inf'):
      sum_diff_mean += math.pow((value - mean), 2)
  sum_diff_mean /= total_values
  std = math.sqrt(sum_diff_mean)
  return mean, std

def get_formatted_values(plp, parameters_number=None, category=None, mean="", std_dev=""):
  if mean == None or std_dev == None:
    return ""
  values_list = ""
  if parameters_number != None:
    values_list += str(parameters_number) + "\t"
  if category != None:
    values_list += category + "\t"
  for parameter, value in plp.items():
    if value == "":
      continue
    values_list += str(value) + "\t"
  values_list += str(mean) + "\t"
  values_list += str(std_dev) + "\n"
  return values_list

def get_formatted_parameters(protomata_learner_parameters, str1=None, str2=None, str3=None, str4=None):
  parameters_list = ""
  if str1 != None:
    parameters_list += str1 + "\t"
  if str2 != None:
    parameters_list += str2 + "\t"
  for parameter, value in protomata_learner_parameters.items():
    if value == "":
      continue
    parameter = parameter.lstrip("-")
    parameter = parameter.replace("-", "_")
    parameters_list += parameter + "\t"
  if str3 != None:
    parameters_list += str3 + "\t"
  if str4 != None:
    parameters_list += str4 + "\t"
  parameters_list = parameters_list.rstrip("\t")
  parameters_list += "\n"
  return parameters_list

def get_parameters_values(protomata_learner_parameters):
  title = "'"
  line_parameters = 0
  last_program = "paloma"
  for parameter, value in protomata_learner_parameters.items():
    if parameter == "--components" or parameter == "--display-dot" or parameter == "--plma-dot" or parameter == "--proto-dot":
      continue
    parameter = parameter.lstrip("-")
    parameter = parameter.replace("-", "_")
    if line_parameters == 3:
      line_parameters = 0
      title = title.rstrip(", ")
      title += "\n"
    if value == "":
      title += parameter + ", "
    else:
      title += parameter + " = " + str(value) + ", "
    line_parameters += 1
  title = title.rstrip(", ")
  title += "'"
  return title

def error_precision_recall(score, threshold, positive, negative):
  total_values = len(positive) + len(negative)
  false_positives = 0
  false_negatives = 0
  true_positives = 0
  true_negatives = 0
#  print "\n\ntotal values=",total_values
#  print "positives=", len(positive)
#  print "negatives=", len(negative)
#  print "threshold=", threshold
  if total_values == 0:
    return 0, 0, 0
  for seq in positive:
    if score[seq.id] < threshold:
      false_negatives += 1
#      print "\tfalse negative: ", seq.id, " = ", score[seq.id]
    else:
      true_positives += 1
  for seq in negative:
    if score[seq.id] >= threshold:
      false_positives += 1
#      print "\tfalse positive: ", seq.id, " = ", score[seq.id]
    else:
      true_negatives += 1
  if true_positives + false_positives == 0:
    precision = 0
  else:
    precision = float(true_positives) / float(true_positives + false_positives)
  if true_positives + false_negatives == 0:
    recall = 0
  else:
    recall = float(true_positives) / float(true_positives + false_negatives)
#  print "stats:\tfalse_pos\t",false_positives,"\tfalse_neg\t",false_negatives
  error_rate = float(false_positives + false_negatives) / float(total_values)
#  print "error_rate\t",error_rate
  return error_rate, precision, recall

def classification_rates(score, threshold, positive, negative):
  total_values = len(positive) + len(negative)
  false_positives = 0
  false_negatives = 0
  true_positives = 0
  true_negatives = 0
  if total_values == 0:
    return 0, 0
  for seq in positive:
    if score[seq.id] < threshold:
      false_negatives += 1
    else:
      true_positives += 1
  for seq in negative:
    if score[seq.id] >= threshold:
      false_positives += 1
    else:
      true_negatives += 1
  TPR = float(true_positives) / float(len(positive))
  FPR = float(false_positives) / float(len(negative))
  return TPR, FPR

def get_higher_scores(score, sequences, subset_size):
  if len(sequences) <= int(subset_size):
    return sequences
  sequences_subset = Sequences()
  for key, val in sorted(score.items(), key = itemgetter(1), reverse = True):
    if len(sequences_subset) == int(subset_size):
      return sequences_subset
    else:
      if sequences.has_sequence_accession(key):
        sequences_subset.add_sequence(sequences.get_sequence(key))

def kfold_combinations(sequences, K):

  kfold_sets = dict()

  if K < 1:
    return kfold_sets

  sequences_list = list()
  for seq in sequences:
    sequences_list.append(seq)

  last_sequence_number_added = 0
  kfolds = math.ceil(float(len(sequences)) / float(K))
  for kfold_iteration in range(0, int(kfolds)):
    exclude_from = K * kfold_iteration
    exclude_to = K * (kfold_iteration + 1)
    current_list = sequences_list[0:exclude_from]
    current_list.extend(sequences_list[exclude_to:len(sequences_list)])

    current_set = set()
    for seq in current_list:
      current_set.add(seq)
    kfold_sets[kfold_iteration + 1] = current_set

  return kfold_sets

def frange(start, stop, n):
  L = [0.0] * n
  nm1 = n - 1
  nm1inv = 1.0 / nm1
  for i in range(n):
    L[i] = nm1inv * (start*(nm1 - i) + stop*i)
  return L

def get_subset(dictionary, sequences):
  return_dict = dict()
  for sequence in sequences:
    if dictionary.has_key(sequence.id):
      return_dict[sequence.id] = dictionary[sequence.id]
  return return_dict
