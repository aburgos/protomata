#! /usr/bin/env python2.6

import os, sys
import MySQLdb

from Bio.Seq import Seq
from Bio import SeqIO

# wrapper for a sequence set
class Sequences:

  # can initialize from a fasta file or with an empty set of sequences
  def __init__(self, fasta_filename = None):
    if fasta_filename == None:
      self._sequences = set()
    else:
      try:
        handle = open(fasta_filename, 'r')
        self._sequences = set(SeqIO.parse(handle, "fasta"))
        handle.close()
      except IOError as (errno, strerror):
        print "I/O error({0}): {1}".format(errno, strerror)
        raise

  def add_sequence(self, sequence):
    self._sequences.add(sequence)

  # sequences can be added from a set or from a Sequences object
  def add_sequences(self, sequences):
    for sequence in sequences:
      self._sequences.add(sequence)

  def del_sequences(self, sequences):
    for sequence in sequences:
      self._sequences.remove(sequence)

  def clear(self):
    self._sequences.clear()

  def has_sequence_accession(self, sequence_accession):
    for sequence in self._sequences:
      if sequence.id == sequence_accession:
        return True
    return False

  def get_sequence(self, sequence_accession):
    for sequence in self._sequences:
      if sequence.id == sequence_accession:
        return sequence
    return False

  # iterates through sequences as if it was a set
  def __iter__(self):
    return self._sequences.__iter__()

  def __len__(self):
    return len(self._sequences)

  def items(self):
    return self._sequences.items()

  def write2fasta(self, fasta_filename):
    try:
      handle = open(fasta_filename, 'w')
      SeqIO.write(self._sequences, handle, "fasta")
      handle.close()
    except IOError as (errno, strerror):
      print "I/O error({0}): {1}".format(errno, strerror)
      raise

  # writes each sequence to table protomata_experiment_data
  def write2db(self, db_connection, experiment, sequences_class):
    sequences_labels = experiment.sequences_labels()
    for sequence in self._sequences:
      db_connection.write_protomata_experiment_data(experiment.get_id(), sequences_class, sequences_labels[sequence.name], sequence.name)

