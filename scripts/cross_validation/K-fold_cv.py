#! /usr/bin/env python2.6

import os, sys
import math
import itertools
import logging

from utilities import kfold_combinations
from optparse import OptionParser
from Sequences import Sequences
from Experiment import Experiment
from ProgramRun import Program_Run
from Parsers import Protoscan_Parser

# global constants
dialign_exe = "dialign"

# parse CLI arguments
usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)
parser.add_option("-i", "--input", dest="exp_filename", default=None, action="store", type="string", help="Experiment XML file")
parser.add_option("-t", "--test-only", dest="test_only", default=False, action="store_true", help="Compute test scores only")
parser.add_option("-v", "--verbose", dest="verbose", default=False, action="store_true", help="Verbose output")

(options, args) = parser.parse_args()

# logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-3s %(message)s',
                    datefmt='%d%b%Y %H:%M:%S',
                    filename=options.exp_filename + '.log',
                    filemode='w')

# parse parameters from XML file
experiment = Experiment(options.exp_filename, logging)

if not options.test_only:

  # create the output directory
  os.makedirs(experiment.output_directory())

  basename, extension = os.path.splitext(os.path.basename(experiment.train_filename()))
  basename = basename.rstrip("_train")
  min_threshold = experiment.min_integer_value('--threshold')
  max_size = experiment.max_integer_value('--max-size')
  k = experiment.validation_fold()

  # generate k-fold combinations
  kfold_combinations = kfold_combinations(experiment.train_sequences(), k)

  # loop on training set leaving k sequences out
  for kfold_step, current_train_sequences in kfold_combinations.items():

    # loop on parameters
    logging.info('main loop, fold iteration: %d / %d' % (kfold_step, len(kfold_combinations)))
    kfold_directory = experiment.output_directory() + "/%d-fold_%d/" % (k, kfold_step)
    os.makedirs(kfold_directory)

    # write current training sequences to a fasta file
    current_train_sequences_filename = kfold_directory + basename + "_train_step_%d" % (kfold_step) + extension
    current_train_sequences_obj = Sequences()
    current_train_sequences_obj.add_sequences(current_train_sequences)
    current_train_sequences_obj.write2fasta(current_train_sequences_filename)

    # get all sequences left out of train + validation
    to_scan_sequences_filename = kfold_directory + basename + "_rest_step_%d" % (kfold_step) + extension
    to_scan_sequences = Sequences()
    to_scan_sequences.add_sequences(experiment.train_sequences())
    to_scan_sequences.del_sequences(current_train_sequences)
    to_scan_sequences.add_sequences(experiment.validation_sequences())
    to_scan_sequences.write2fasta(to_scan_sequences_filename)

    # create dialign file to be used by the runs
    logging.info('executing dialign...')
    command = dialign_exe + " -nta -thr " + str(min_threshold) + " -lmax " + str(max_size);
    command += " -afc " + current_train_sequences_filename
    os.system(command)
    current_afc_file = kfold_directory + basename + "_train_step_%d" % (kfold_step) + ".afc"

    # iterate over the parameters of the experiment
    for iteration_number, parameters in experiment.parameters_items():

      logging.info('\tparameter iteration: %d / %d' % (iteration_number + 1, len(experiment.parameters_items())))

      # set changing parameters for protomata-learner
      parameters.set_flag("--use-afc", current_afc_file)
      parameters.set_flag("--sequences", to_scan_sequences_filename)
      parameters.set_flag("--output-path", kfold_directory)
      run = Program_Run("protomata-learner", parameters.get_command(), logging)
      run.input_file(current_train_sequences_filename)
      output_scan_filename = to_scan_sequences_filename + "_scan_" + str(iteration_number + 1) + ".txt"
      run.add_parameter("--protoscan-output", output_scan_filename)

      run.execute()

if len(experiment.test_sequences()) > 0:

  test_output_path = experiment.output_directory() + "/test/"
  os.makedirs(test_output_path)

  # compute test scores for each parameter
  for iteration_number, parameters in experiment.parameters_items():

    parameters.rm_flag("--use-afc")
    parameters.set_flag("--sequences", experiment._test)
    parameters.set_flag("--output-path", test_output_path)
    run = Program_Run("protomata-learner", parameters.get_command(), logging)
    run.input_file(experiment._train)
    output_name = experiment._name.replace(" ", "_")
    output_scan_filename = test_output_path + output_name + "_test_scan_" + str(iteration_number + 1) + ".txt"
    run.add_parameter("--protoscan-output", output_scan_filename)
    run.execute()

