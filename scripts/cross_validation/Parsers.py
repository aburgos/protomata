#! /usr/bin/env python2.6

import os, sys
import itertools
import logging

from xml.dom.minidom import Document
from xml.dom.minidom import parse, parseString
from xml.dom.minidom import getDOMImplementation

from ProgramParameters import Program_Parameters
from ProgramRun import Program_Run
from utilities import extract_parameters

class Options_Parser:

  # initialization from a DOM node object corresponding to a program
  def __init__(self, domNode, logger):

    # assign logging
    self._logger = logger

    self._parameters = dict()
    self._name = 'protomata-learner'
    for param_node in domNode.getElementsByTagName('param'):
      if param_node.hasAttribute('t'):
        if param_node.getAttribute('t') == "true":
          self._parameters[param_node.getAttribute('f')] = ["None", ""];
      elif param_node.hasAttribute('v'):
        flag_value = param_node.getAttribute('v')
        if flag_value.find(',') != -1:
          param_values = flag_value.split(',')
          self._parameters[param_node.getAttribute('f')] = param_values;
        elif param_node.hasAttribute('v'):
          self._parameters[param_node.getAttribute('f')] = [param_node.getAttribute('v')];
      else:
        self._parameters[param_node.getAttribute('f')] = [""]

  def generate_parameter_combinations(self):
    # expand parameters
    expanded_parameters = dict()
    temp_expanded_parameters = dict()
    for flag in self._parameters:
      temp_expanded_parameters[flag] = [str(flag) + " " + str(v) for v in self._parameters[flag]]
    # set parameters order
    for k, v in temp_expanded_parameters.items():
      if k == "--quorum":
        expanded_parameters[len(expanded_parameters)] = v
    for k, v in temp_expanded_parameters.items():
      if k == "--threshold":
        expanded_parameters[len(expanded_parameters)] = v
    for k, v in temp_expanded_parameters.items():
      if k != "--quorum" and k != "--threshold":
        expanded_parameters[len(expanded_parameters)] = v

    # create program runs
    program_run_commands = list()
    for option in itertools.product(*expanded_parameters.values()):
      program_run_commands.append(self._name + " " + str.join(" ", [opt for opt in option if opt.find("None") == -1]))

    # create program parameter instance and add parameters from command
    program_runs = list()
    for program_run in program_run_commands:
      run = Program_Parameters(self._name, self._logger)
      run.set_flags(extract_parameters(program_run))
      if not run.has_flag("--threshold"):
        run.set_flag("--threshold", 3)
      if not run.has_flag("--max-size"):
        run.set_flag("--max-size", 15)
      program_runs.append(run)

    self._logger.info("Options_Parser: %s runs generated for %s" % (len(program_runs), self._name))
    return program_runs

class Protoscan_Parser:

  def __init__(self, scan_filename, significativity_computed):
    try:
      scan_output_file = open(scan_filename, 'r')
    except IOError as (errno, strerror):
      print "I/O error({0}): {1}".format(errno, strerror)
      raise

    if significativity_computed:
      self._scan_values = dict([(str(((s[:-1]).split('\t'))[3]), (float(((s[:-1]).split('\t'))[0]), \
                           float(((s[:-1]).split('\t'))[1]))) \
                           for s in scan_output_file.readlines() if not s.startswith('#')])
    else:
      # which value should be used when no significativity was computed? NULL?
      self._scan_values = dict([(str(((s[:-1]).split('\t'))[3]), (float(((s[:-1]).split('\t'))[0]), None)) \
                           for s in scan_output_file.readlines() if not s.startswith('#')])

  def __iter__(self):
    return self._scan_values.__iter__()

  def items(self):
    return self._scan_values.items()

  def score(self):
    scores = dict()
    for k, v in self._scan_values.items():
      scores[k] = v[0]
    return scores

  def significativity(self):
    significativity = dict()
    for k, v in self._scan_values.items():
      significativity[k] = v[1]
    return significativity
