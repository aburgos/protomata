#! /usr/bin/env python2.6

import os, sys
import math
import itertools
import logging
import drmaa

from utilities import kfold_combinations
from optparse import OptionParser
from Sequences import Sequences
from Experiment import Experiment
from ProgramRun import Program_Run
from Parsers import Protoscan_Parser

# global constants
dialign_exe = "dialign"

# parse CLI arguments
usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)
parser.add_option("-i", "--input", dest="exp_filename", default=None, action="store", type="string", help="Experiment XML file")
parser.add_option("-v", "--verbose", dest="verbose", default=False, action="store_true", help="Verbose output")

(options, args) = parser.parse_args()

# logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-3s %(message)s',
                    datefmt='%d%b%Y %H:%M:%S',
                    filename=options.exp_filename + '.log',
                    filemode='w')

# parse parameters from XML file
experiment = Experiment(options.exp_filename, logging)

# create the output directory
os.makedirs(experiment.output_directory())

basename, extension = os.path.splitext(os.path.basename(experiment.train_filename()))
basename = basename.rstrip("_train")
min_threshold = experiment.min_integer_value('--threshold')
max_size = experiment.max_integer_value('--max-size')
k = experiment.validation_fold()

# generate k-fold combinations
kfold_combinations = kfold_combinations(experiment.train_sequences(), k)

# prepare job scheduling
s = drmaa.Session()
s.initialize()
workdir = os.getcwd()

# loop on training set leaving k sequences out
# first parallelization step: dialign launching
job = s.createJobTemplate()
for kfold_step, current_train_sequences in kfold_combinations.items():

  # loop on parameters
  logging.info('dialign loop, fold iteration: %d / %d' % (kfold_step, len(kfold_combinations)))
  kfold_directory = experiment.output_directory() + "/%d-fold_%d/" % (k, kfold_step)
  os.makedirs(kfold_directory)

  # write current training sequences to a fasta file
  current_train_sequences_filename = kfold_directory + basename + "_train_step_%d" % (kfold_step) + extension
  current_train_sequences_obj = Sequences()
  current_train_sequences_obj.add_sequences(current_train_sequences)
  current_train_sequences_obj.write2fasta(current_train_sequences_filename)

  # get all sequences left out of train + validation
  to_scan_sequences_filename = kfold_directory + basename + "_rest_step_%d" % (kfold_step) + extension
  to_scan_sequences = Sequences()
  to_scan_sequences.add_sequences(experiment.train_sequences())
  to_scan_sequences.del_sequences(current_train_sequences)
  to_scan_sequences.add_sequences(experiment.validation_sequences())
  to_scan_sequences.write2fasta(to_scan_sequences_filename)

  # create dialign file to be used by the runs
  logging.info('executing dialign...')
  job.remoteCommand = dialign_exe
  job.args = ["-nta", "-thr", str(min_threshold), "-lmax", str(max_size), "-afc", current_train_sequences_filename]
  job.jobEnvironment = {'DIALIGN2_DIR': os.environ["DIALIGN2_DIR"], 'PATH': os.environ["PATH"]}
  job.joinFiles = True
  job.workingDirectory = kfold_directory
  job.outputPath = ":" + "dialign.log"
  s.runJob(job)

# clean job scheduling
s.synchronize([drmaa.Session.JOB_IDS_SESSION_ALL], drmaa.Session.TIMEOUT_WAIT_FOREVER, True)
s.deleteJobTemplate(job)

# loop on training set leaving k sequences out
# first parallelization step: protomata-learner launching
delayed_learner_jobs = [] # list of arg lists + iteration numbers
job = s.createJobTemplate()
for kfold_step, current_train_sequences in kfold_combinations.items():

  # loop on parameters
  logging.info('first protomata-learner loop, fold iteration: %d / %d' % (kfold_step, len(kfold_combinations)))
  kfold_directory = experiment.output_directory() + "/%d-fold_%d/" % (k, kfold_step)
  
  current_train_sequences_filename = kfold_directory + basename + "_train_step_%d" % (kfold_step) + extension
  to_scan_sequences_filename = kfold_directory + basename + "_rest_step_%d" % (kfold_step) + extension
  current_afc_file = kfold_directory + basename + "_train_step_%d" % (kfold_step) + ".afc"

  # iterate over the parameters of the experiment
  generated_plma = []
  for iteration_number, parameters in experiment.parameters_items():

    # set changing parameters for protomata-learner
    output_scan_filename = to_scan_sequences_filename + "_scan_" + str(iteration_number + 1) + ".txt"
    job.remoteCommand = "protomata-learner"
    args = ["--use-afc", current_afc_file, "--sequences", to_scan_sequences_filename, "--output-path", kfold_directory, "--input", current_train_sequences_filename, "--protoscan-output", output_scan_filename]
    
    current_plma_params = ""
    
    for flag, value in parameters.items():
      args.append(flag)
      if value != "":
        args.append(str(value))
      if flag in ["--tag", "--use-afc", "--quorum-plma", "--threshold", "--min-size", "--max-size"]: # These are paloma flags used in plma filename
        current_plma_params += flag + str(value)

    # Launch the job, only if no other job with same paloma settings has already been launched.
    # If it's the case, we have to launch this job later to avoid several jobs writing and reading the same plma file.
    if current_plma_params not in generated_plma:
      generated_plma.append(current_plma_params)
    
      job.args = args
      job.jobEnvironment = {'PATH': os.environ["PATH"]}
      job.joinFiles = True
      job.workingDirectory = kfold_directory
      job.outputPath = ":" + "protomata-learner_" + str(iteration_number + 1) + ".log"
      s.runJob(job)
    else:
      delayed_learner_jobs.append({'args' : args, 'log' : ":" + kfold_directory + "protomata-learner_" + str(iteration_number + 1) + ".log"})

# clean job scheduling
s.synchronize([drmaa.Session.JOB_IDS_SESSION_ALL], drmaa.Session.TIMEOUT_WAIT_FOREVER, True)
s.deleteJobTemplate(job)

# loop on protomata-learner jobs that have been delayed
job = s.createJobTemplate()
for delayed_job in delayed_learner_jobs:

  logging.info('second protomata-learner loop (paloma finished), fold iteration: %d / %d' % (delayed_learner_jobs.index(delayed_job)+1, len(delayed_learner_jobs)))

  # Launch the job. The paloma file is alerady generated during the preceding loop.
  job.remoteCommand = "protomata-learner"
  job.args = delayed_job['args']
  job.jobEnvironment = {'PATH': os.environ["PATH"]}
  job.joinFiles = True
  job.outputPath = delayed_job['log']
  s.runJob(job)

# clean job scheduling
s.synchronize([drmaa.Session.JOB_IDS_SESSION_ALL], drmaa.Session.TIMEOUT_WAIT_FOREVER, True)
s.deleteJobTemplate(job)

if len(experiment.test_sequences()) > 0:

  test_output_path = experiment.output_directory() + "/test/"
  os.makedirs(test_output_path)
  # make a copy of train sequences in this directory
  train_sequences = test_output_path + "train.fasta"
  os.system("cp %s %s" % (experiment._train, train_sequences))

  job = s.createJobTemplate()
  # create dialign file to be used by the runs
  logging.info('executing dialign for test data...')
  job.remoteCommand = dialign_exe
  job.args = ["-nta", "-thr", str(min_threshold), "-lmax", str(max_size), "-afc", train_sequences]
  job.jobEnvironment = {'DIALIGN2_DIR': os.environ["DIALIGN2_DIR"], 'PATH': os.environ["PATH"]}
  job.joinFiles = True
  job.outputPath = ":" + test_output_path + "dialign.log"
  s.runJob(job)

  s.synchronize([drmaa.Session.JOB_IDS_SESSION_ALL], drmaa.Session.TIMEOUT_WAIT_FOREVER, True)
  s.deleteJobTemplate(job)

  job = s.createJobTemplate()
  # compute test scores for each parameter
  for iteration_number, parameters in experiment.parameters_items():

    logging.info('protomata-learner on test data, parameter number: %d / %d' % (iteration_number + 1, len(experiment.parameters_items())))
    job.remoteCommand = "protomata-learner"

    output_name = experiment._name.replace(" ", "_")
    output_scan_filename = test_output_path + output_name + "_test_scan_" + str(iteration_number + 1) + ".txt"

    parameters.set_flag("--input", train_sequences)
    parameters.set_flag("--sequences", experiment._test)
    parameters.set_flag("--use-afc", train_sequences.replace(".fasta", ".afc"))
    parameters.set_flag("--output-path", test_output_path)
    parameters.set_flag("--protoscan-output", output_scan_filename)

    args = []
    for flag, value in parameters.items():
      args.append(flag)
      if value != "":
        args.append(str(value))

    job.args = args
    job.jobEnvironment = {'PATH': os.environ["PATH"]}
    job.joinFiles = True
    job.outputPath = ":" + test_output_path + "protomata-learner_" + str(iteration_number + 1) + ".log"
    s.runJob(job)

    # clean job scheduling
    s.synchronize([drmaa.Session.JOB_IDS_SESSION_ALL], drmaa.Session.TIMEOUT_WAIT_FOREVER, True)
    s.deleteJobTemplate(job)

s.exit()
