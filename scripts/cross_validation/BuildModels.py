#! /usr/bin/env python2.6

import os, sys, shutil
import math
import itertools
import logging
import drmaa

from optparse import OptionParser
from Experiment import Experiment

def which(program):
    import os
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

# global constants
dialign_exe = "dialign"

# parse CLI arguments
usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)
parser.add_option("-i", "--input", dest="exp_filename", default=None, action="store", type="string", help="Experiment XML file")
parser.add_option("-o", "--output", dest="output_directory", default=".", action="store", type="string", help="Output directory")
parser.add_option("-v", "--verbose", dest="verbose", default=False, action="store_true", help="Verbose output")

(options, args) = parser.parse_args()

# logging
#
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-3s %(message)s',
                    datefmt='%d%b%Y %H:%M:%S',
                    filename=options.exp_filename + '.log',
                    filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
#
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)-11s:%(levelname)-3s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

# parse parameters from XML file
experiment = Experiment(options.exp_filename, logging)

# create the output directory
os.makedirs(options.output_directory)
output_dir = os.path.abspath(options.output_directory)+"/"

min_threshold = experiment.min_integer_value('--threshold')
max_size = experiment.max_integer_value('--max-size')

# prepare job scheduling
s = drmaa.Session()
s.initialize()
workdir = os.getcwd()

# dialign step
# create dialign file to be used by the runs
logging.info('\tExecuting dialign...')
job = s.createJobTemplate()
job.remoteCommand = dialign_exe
job.args = ["-nta", "-thr", str(min_threshold), "-lmax", str(max_size), "-afc", experiment.train_filename()]
job.jobEnvironment = {'DIALIGN2_DIR': os.path.dirname(which(dialign_exe)), 'PATH': os.environ["PATH"]}
job.joinFiles = True
job.workingDirectory = output_dir
job.outputPath = ":" + output_dir + "dialign.log"
s.runJob(job)

# clean job scheduling
s.synchronize([drmaa.Session.JOB_IDS_SESSION_ALL], drmaa.Session.TIMEOUT_WAIT_FOREVER, True)
s.deleteJobTemplate(job)

# move afc file to work dir
shutil.move(workdir + "/" + os.path.splitext(os.path.basename(experiment.train_filename()))[0] + '.afc', output_dir + "dialign.afc")

# iterate over the parameters of the experiment
job = s.createJobTemplate()
for iteration_number, parameters in experiment.parameters_items():
  logging.info('protomata-learner loop, fold iteration: %d / %d' % (iteration_number+1, len(experiment.parameters_items())))
  current_dir = output_dir + "params_" + str(iteration_number + 1)
  if parameters.has_flag("--threshold"):
    current_dir += "_t" + parameters.get_value("--threshold")
  if parameters.has_flag("--max-size"):
    current_dir += "_M" + parameters.get_value("--max-size")
  if parameters.has_flag("--quorum"):
    current_dir += "_q" + parameters.get_value("--quorum")
  if parameters.has_flag("--percentage-quorum"):
    current_dir += "_p" + parameters.get_value("--percentage-quorum")
  if parameters.has_flag("--components"):
    current_dir += "_comp:" + parameters.get_value("--components")
  current_dir += "/"
  os.makedirs(current_dir)

  # set changing parameters for protomata-learner
  job.remoteCommand = which("protomata-learner")
  args = ["--use-afc", output_dir + "dialign.afc", "--input", experiment.train_filename(), '--plma-dot', '--proto-dot' ]
  for flag, value in parameters.items():
    args.append(flag)
    if flag == '--plma-dot-attributes' and value != "":
      args.append(os.path.abspath(str(value)))
    else:
      if value != "":
        args.append(str(value))

  job.args = args
  job.jobEnvironment = {'PATH': os.environ["PATH"]}
  job.joinFiles = True
  job.workingDirectory = current_dir
  job.outputPath = ":" + current_dir + "protomata-learner.log"
  s.runJob(job)

# clean job scheduling
s.synchronize([drmaa.Session.JOB_IDS_SESSION_ALL], drmaa.Session.TIMEOUT_WAIT_FOREVER, True)
s.deleteJobTemplate(job)
s.exit()

# iterate over the parameters of the experiment
for iteration_number, parameters in experiment.parameters_items():
  logging.info('graphviz loop, fold iteration: %d / %d' % (iteration_number+1, len(experiment.parameters_items())))
  current_dir = output_dir + "params_" + str(iteration_number + 1)
  if parameters.has_flag("--threshold"):
    current_dir += "_t" + parameters.get_value("--threshold")
  if parameters.has_flag("--max-size"):
    current_dir += "_M" + parameters.get_value("--max-size")
  if parameters.has_flag("--quorum"):
    current_dir += "_q" + parameters.get_value("--quorum")
  if parameters.has_flag("--percentage-quorum"):
    current_dir += "_p" + parameters.get_value("--percentage-quorum")
  if parameters.has_flag("--components"):
    current_dir += "_comp:" + parameters.get_value("--components")
  current_dir += "/"

  os.system("dot -O -Tsvg " + current_dir + "*dot")
  os.chdir(current_dir)
  os.system("proto2logojpg.sh `basename *_proto.xml .xml`")
