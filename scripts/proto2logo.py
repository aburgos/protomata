#!/usr/bin/env python

import os, sys
from optparse import OptionParser

from numpy import array
from weblogolib import *

from xml.dom.minidom import Document
from xml.dom.minidom import parse, parseString
from xml.dom.minidom import getDOMImplementation

# parse CLI arguments
usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)
parser.add_option("-i", "--input", dest="proto_filename", default=None, action="store", type="string", help="Protomata file.")
parser.add_option("-d", "--output-dir", dest="output_directory", default="logo_dir", action="store", type="string", help="Output directory for images generated. The generated dot will be in this directory.")
parser.add_option("-t", "--title", dest="title", default=None, action="store", type="string", help="Set title of drawing")

(options, args) = parser.parse_args()

# create the output directory
os.makedirs(options.output_directory)

# parse an XML file by name
dom = parse(options.proto_filename)

# define start and end zones numbers (should be equal to those in classes Begin_Zone and End_Zone)
begin_zone_id = "1"
end_zone_id = "2"

zero_column = dict({'A': 0, 'C': 0, 'E': 0, 'D': 0, 'G': 0, 'F': 0, 'I': 0, 'H': 0, 'K': 0, 'M': 0, 'L': 0, 'N': 0, 'Q': 0, 'P': 0, 'S': 0, 'R': 0, 'T': 0, 'W': 0, 'V': 0, 'Y': 0})
zones_id = set()

# generate a logo image for each present zone in the protomata
for zone_node in dom.getElementsByTagName('zone'):

  zone_id = zone_node.getAttribute('id')
  # some zones are reserved for start and end zones
  if zone_id == begin_zone_id or zone_id == end_zone_id:
    continue

  zones_id.add(zone_id)
  counts_list = list()

  # get counts of each column
  for column_node in zone_node.getElementsByTagName('column'):
    column = dict()
    for k in zero_column:
      column[k] = 0
    for aa_node in column_node.getElementsByTagName('aminoacid'):
      letter = aa_node.getAttribute('letter')
      count = float(aa_node.getAttribute('count'))
      column[letter] = count

    counts_list.append(column.values())

  if len(counts_list) > 0:
    zone_counts = array(counts_list)
    data = LogoData.from_counts(column.keys(), zone_counts)

    logo_options = LogoOptions(color_scheme = colorscheme.taylor, show_yaxis = False, show_xaxis = False, show_fineprint = False, show_ends = False, show_boxes = False, stroke_width = True, scale_width = False)
    logo_options.title = "Zone" + str(zone_id)
    format = LogoFormat(data, logo_options)

    fout = open(options.output_directory + "/" + logo_options.title + '.eps', 'w') 
    eps_formatter(data, format, fout)

    data = None
    logo_options = None
    format = None

# create a new dot file with the nodes replaced by logo images
basename, extension = os.path.splitext(os.path.basename(options.proto_filename))
new_dot_filename = options.output_directory + "/" + basename + "_logos.dot"
new_dot_file = open(new_dot_filename, 'w')


new_dot_file.write("DiGraph G\n{\n\trankdir = LR;\n\tcenter = true;\n\tconcentrate = false;\n")
if options.title:
  new_dot_file.write("\tlabel=\""+options.title+"\";\n")
new_dot_file.write("\tnode [shape = record, color = brown, label = \"\"];\n")
new_dot_file.write("\tedge [color = green, style=\"setlinewidth(2)\", fontcolor = blue, count = 1, ")
new_dot_file.write("headport = w, tailport = e];\n")

new_dot_file.write("\t"+begin_zone_id+" [fontname = monospace, color = red, fontcolor = black, label = \"START\"];\n")
new_dot_file.write("\t"+end_zone_id+" [fontname = monospace, color = red, fontcolor = black, label = \"END\"];\n")

# create all nodes
for zone_id in zones_id:
  zone_filename = options.output_directory + "/" + "Zone" + str(zone_id) + ".eps"
  new_line = "\t" + str(zone_id) + " [shapefile = \"" + zone_filename + "\", imagescale = true];\n"
  new_dot_file.write(new_line)

# create edges on dot file
for edge_node in dom.getElementsByTagName('edge'):
  source = edge_node.getAttribute('source')
  target = edge_node.getAttribute('target')
  edge_type = edge_node.getAttribute('type')

  # compute gap or exception lengths
  lengths = set()
  for subseq_node in edge_node.getElementsByTagName('subsequence'):
    string = subseq_node.getAttribute('string')
    lengths.add(len(string))
  begin = min(lengths)
  end = max(lengths)

  if edge_type == "Gap":
    gap = "\t" + str(source) + " -> " + str(target)
    if begin == 0 and end == 0:
      gap += " [weight = 10];\n"
    else:
      label = "(" + str(begin)
      if end != begin:
        label += "," + str(end)
      label += ")"
      gap += " [color = green, weight = 5, fontcolor = blue, label = \"" + label + "\"];\n"
    new_dot_file.write(gap)
  else:
    if edge_type == "Exception":
      exception = "\t" + str(source) + " -> " + str(target)
      label = "(" + str(begin)
      if end != begin:
        label += "," + str(end)
      label += ")"
      exception += " [style = dotted, color = grey30, weight = 1, fontcolor = grey30, label = \"" + label + "\"];\n"
      new_dot_file.write(exception)
new_dot_file.write("}")

print "--- Now you can"
print "--- generate .ps"
dot_cmd = "dot -Tps2 " + new_dot_filename + " -o " + basename + "_logos.ps"
print dot_cmd
#os.system(dot_cmd)

print "--- and then convert .ps to .pdf"
ps2pdf_cmd = "ps2pdf "+ basename + "_logos.ps"
print ps2pdf_cmd

print "--- or convert .ps to svg"
ps2svg_cmd = "pstoedit -f plot-svg " + basename + "_logos.ps " + basename + "_logos.svg" 
print ps2svg_cmd
