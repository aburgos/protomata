
#include <iostream>
#include "tclap/CmdLine.h"
#include "parsers/ClustalWeightsParser.h"

int main(int argc, char **argv)
{
	TCLAP::CmdLine cmd("Prints clustal weights from a fasta file to standard output.", ' ', VERSION);
	TCLAP::ValueArg<std::string> input("i", "input", "Set fasta filename.", true, "", "fasta_filename");

	// add in reverse appereance order
	cmd.add(input);

	cmd.parse(argc, argv);

	// set output file name if it was not set
	std::string input_file = input.getValue();

	ClustalWeightsParser cwp(input_file);

	for(ClustalWeightsParser::const_iterator it = cwp.begin(); it != cwp.end(); ++it)
	{
		const std::string& tag = (*it).first;
		double weight = (*it).second;

		std::cout << tag << "\t\t" << weight << std::endl;
	}


	std::cout << std::endl;
	std::cout << "sum of weights\t\t" << cwp.total_weight() << std::endl;

	return 0;
}
