
#ifndef MATRIX_H
#define MATRIX_H

#include <map>
#include <cstring>
#include <iostream>

/*!
A rectangular matrix. The type of TVal must accept 0 as value, which is the assigned value to all non-specified by the user matrix values.
*/
template <class TRow, class TCol, class TVal>
class Matrix
{
  private:
	TVal* data;
	typedef std::map<TRow, unsigned int> RowIndex;
	typedef std::map<TCol, unsigned int> ColIndex;
	RowIndex rows;
	ColIndex cols;
	unsigned int num_rows;
	unsigned int num_cols;
	unsigned int rows_size;
	unsigned int cols_size;
	TVal* expand_col(unsigned int);
	TVal* expand_row(unsigned int);

  public:
	Matrix();
	/*! Reserves enough space for the number of columns set. */
	Matrix(unsigned int cols);
	/*! Reserves enough space for the number of rows and columns set. */
	Matrix(unsigned int cols, unsigned int rows);
	Matrix(const Matrix& m);
	Matrix& operator=(const Matrix& m);
	~Matrix();

	unsigned int nrows() const { return num_rows; }
	unsigned int ncols() const { return num_cols; }

	bool exists(const TRow&, const TCol&) const;

	std::map<TRow, TVal> col(const TCol&) const;
	std::map<TCol, TVal> row(const TRow&) const;
	TVal operator()(const TRow&, const TCol&) const;

	TVal& operator()(const TRow&, const TCol&);

	typedef typename RowIndex::iterator row_iterator;
	row_iterator row_begin() { return rows.begin(); }
	row_iterator row_end() { return rows.end(); }

	typedef typename RowIndex::const_iterator row_const_iterator;
	row_const_iterator row_begin() const { return rows.begin(); }
	row_const_iterator row_end() const { return rows.end(); }

	typedef typename ColIndex::iterator col_iterator;
	col_iterator col_begin() { return cols.begin(); }
	col_iterator col_end() { return cols.end(); }

	typedef typename ColIndex::const_iterator col_const_iterator;
	col_const_iterator col_begin() const { return cols.begin(); }
	col_const_iterator col_end() const { return cols.end(); }

	friend std::ostream& operator<<(std::ostream& aStream, const Matrix<TRow, TCol, TVal>& m)
	{
		aStream << "\033[1;35m" << "\t";
		typename ColIndex::const_iterator cit;
		for(cit = m.cols.begin(); cit != m.cols.end(); cit++)
			aStream << (*cit).first << "\t";
		aStream << std::endl << "\033[m";
		typename RowIndex::const_iterator rit;
		for(rit = m.rows.begin(); rit != m.rows.end(); rit++)
		{
			aStream << "\033[1;33m" << (*rit).first << "\033[m" << "\t";
			for(cit = m.cols.begin(); cit != m.cols.end(); cit++)
				aStream << m((*rit).first, (*cit).first) << "\t";
			aStream << std::endl;
		}

		return aStream;
	}
};

template <class TRow, class TCol, class TVal>
Matrix<TRow, TCol, TVal>::Matrix()
{
	num_rows = 0;
	num_cols = 0;
	rows_size = 2;
	cols_size = 2;
	data = new TVal[rows_size * cols_size];
	for(unsigned int i = 0; i < rows_size * cols_size; i++)
		data[i] = 0;
}

template <class TRow, class TCol, class TVal>
Matrix<TRow, TCol, TVal>::Matrix(unsigned int cols): num_cols(cols)
{
	num_rows = 0;
	rows_size = 2;
	cols_size = cols + 2;
	data = new TVal[rows_size * cols_size];
	for(unsigned int i = 0; i < rows_size * cols_size; i++)
		data[i] = 0;
}

template <class TRow, class TCol, class TVal>
Matrix<TRow, TCol, TVal>::Matrix(unsigned int cols, unsigned int rows): num_cols(cols), num_rows(rows)
{
	rows_size = rows + 2;
	cols_size = cols + 2;
	data = new TVal[rows_size * cols_size];
	for(unsigned int i = 0; i < rows_size * cols_size; i++)
		data[i] = 0;
}

template <class TRow, class TCol, class TVal>
Matrix<TRow, TCol, TVal>::Matrix(const Matrix& m)
{
	rows = m.rows;
	cols = m.cols;
	num_rows = m.num_rows;
	num_cols = m.num_cols;
	rows_size = m.rows_size;
	cols_size = m.cols_size;
	data = new TVal[rows_size * cols_size];
	memcpy(data, m.data, sizeof(TVal) * rows_size * cols_size);
}

template <class TRow, class TCol, class TVal>
Matrix<TRow, TCol, TVal>& Matrix<TRow, TCol, TVal>::operator=(const Matrix& m)
{
	// test for self-assignment
	if(this == &m) return *this;

	rows = m.rows;
	cols = m.cols;
	num_rows = m.num_rows;
	num_cols = m.num_cols;
	rows_size = m.rows_size;
	cols_size = m.cols_size;
	data = new TVal[rows_size * cols_size];
	memcpy(data, m.data, sizeof(TVal) * rows_size * cols_size);

	return *this;
}

template <class TRow, class TCol, class TVal>
Matrix<TRow, TCol, TVal>::~Matrix()
{
	delete [] data;
}

template <class TRow, class TCol, class TVal>
bool Matrix<TRow, TCol, TVal>::exists(const TRow& row, const TCol& col) const
{
	return (rows.find(row) != rows.end() && cols.find(col) != cols.end());
}

template <class TRow, class TCol, class TVal>
std::map<TRow, TVal> Matrix<TRow, TCol, TVal>::col(const TCol& _col) const
{
	std::map<TRow, TVal> ret;

	typename ColIndex::const_iterator cit;
	if((cit = cols.find(_col)) == cols.end()) return ret;
	unsigned int ncol = (*cit).second;

	typename RowIndex::const_iterator it;
	for(it = rows.begin(); it != rows.end(); it++)
		ret[(*it).first] = data[(*it).second * cols_size + ncol];

	return ret;
}

template <class TRow, class TCol, class TVal>
std::map<TCol, TVal> Matrix<TRow, TCol, TVal>::row(const TRow& _row) const
{
	std::map<TCol, TVal> ret;

	typename RowIndex::const_iterator rit;
	if((rit = rows.find(_row)) == rows.end()) return ret;
	unsigned int nrow = (*rit).second;

	typename ColIndex::const_iterator it;
	for(it = cols.begin(); it != cols.end(); it++)
		ret[(*it).first] = data[nrow * cols_size + (*it).second];

	return ret;
}

template <class TRow, class TCol, class TVal>
TVal Matrix<TRow, TCol, TVal>::operator()(const TRow& row, const TCol& col) const
{
	// doesn't check anything
	return data[(*(rows.find(row))).second * cols_size + (*(cols.find(col))).second];
}

template <class TRow, class TCol, class TVal>
TVal& Matrix<TRow, TCol, TVal>::operator()(const TRow& row, const TCol& col)
{
	typename RowIndex::const_iterator rit;
	if((rit = rows.find(row)) == rows.end())
	{
		if(num_rows + 2 == rows_size) data = expand_row(rows_size * 2);
		rows[row] = num_rows;
	}
	typename ColIndex::const_iterator cit;
	if((cit = cols.find(col)) == cols.end())
	{
		if(num_cols + 2 == cols_size) data = expand_col(cols_size * 2);
		cols[col] = num_cols;
	}

	if(rit == rows.end()) num_rows++;
	if(cit == cols.end()) num_cols++;

	return data[rows[row] * cols_size + cols[col]];
}

template <class TRow, class TCol, class TVal>
inline TVal* Matrix<TRow, TCol, TVal>::expand_col(unsigned int new_size)
{
	TVal* new_data = new TVal[rows_size * new_size];
	for(unsigned int i = 0; i < rows_size; i++)
	{
		memcpy(new_data + i * new_size, data + i * cols_size, sizeof(TVal) * cols_size);
		for(unsigned int j = cols_size; j < new_size; j++)
			new_data[i * new_size + j] = 0;
	}
	delete [] data;
	cols_size = new_size;
	return new_data;
}

template <class TRow, class TCol, class TVal>
inline TVal* Matrix<TRow, TCol, TVal>::expand_row(unsigned int new_size)
{
	TVal* new_data = new TVal[new_size * cols_size];
	memcpy(new_data, data, sizeof(TVal) * cols_size * rows_size);
	for(unsigned int i = rows_size * cols_size; i < new_size * cols_size; i++)
		new_data[i] = 0;
	delete [] data;
	rows_size = new_size;
	return new_data;
}

#endif
