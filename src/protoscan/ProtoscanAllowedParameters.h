
#ifndef PROTOSCANALLOWEDPARAMETERS_H
#define PROTOSCANALLOWEDPARAMETERS_H

#include "tclap/CmdLine.h"

class ProtoscanAllowedParameters
{
  private:
	std::vector<std::string> strategies_values;
	std::vector<std::string> scan_scores;

  public:
	ProtoscanAllowedParameters();

	TCLAP::ValuesConstraint<std::string>* strategies_values_constraints();
	TCLAP::ValuesConstraint<std::string>* scan_scores_constraints();
};

#endif
