
#ifndef WATRANSLATOR_H
#define WATRANSLATOR_H

#include "Translator.h"
#include "WAProperties.h"

template <typename CntT>
class WATranslator
{
  public:
	typedef WAProperties<CntT> WAType;

	WATranslator() {}

	WAType toDNA(const WAType&) const;
};

template <typename CntT>
WAProperties<CntT> WATranslator<CntT>::toDNA(const WAProperties<CntT>& wa_proteins) const
{
	WAType wa_dna;
	Translator translator;

	typedef std::map<char, CntT> Transitions;
	typedef std::map<char, typename WAType::State> InternalTransitions;

	typename WAType::State first_unused_state = wa_proteins.states_size() + 1;
	// iterate through all pair of states where there is a transition
	typename WAType::transition_const_iterator it;
	for(it = wa_proteins.transition_begin(); it != wa_proteins.transition_end(); ++it)
	{
		const typename WAType::TranStates& states = (*it).first;
		const typename WAType::State source = states.first;
		const typename WAType::State target = states.second;

		InternalTransitions first_transitions;
		InternalTransitions second_transitions;

		// get all transitions between the currnet source and target states
		const Transitions& transitions = wa_proteins.getTransitions(source, target);
		// for each protein of the transition, get its translation
		for(typename Transitions::const_iterator tit = transitions.begin(); tit != transitions.end(); ++tit)
		{
			const char protein = (*tit).first;
			const CntT count = (*tit).second;

			Translator::DNASet dnas = translator.protein2dna(protein);
			for(Translator::DNASet::const_iterator dit = dnas.begin(); dit != dnas.end(); ++dit)
			{
				const std::string dna = *dit;

				// only first state transition's will be reused
				if(first_transitions.find(dna[0]) == first_transitions.end())
				{
					first_transitions[dna[0]] = first_unused_state;
					// internal transition with count 0
					wa_dna.addTransition(source, first_unused_state, std::make_pair(dna[0], 0));
					first_unused_state++;
				}

				second_transitions[dna[1]] = first_unused_state;
				// internal transition with count 0
				wa_dna.addTransition(first_transitions[dna[0]], first_unused_state, std::make_pair(dna[1], 0));
				first_unused_state++;

				if(!wa_dna.addTransition(second_transitions[dna[1]], target, std::make_pair(dna[2], count)))
					{ std::cerr << "Could not add DNA transition " << std::endl; exit(1); }
			}
		}
	}

	for(typename WAType::state_iterator it = wa_proteins.start_state_begin(); it != wa_proteins.start_state_end(); ++it)
		wa_dna.setStart(*it);
	for(typename WAType::state_iterator it = wa_proteins.final_state_begin(); it != wa_proteins.final_state_end(); ++it)
		wa_dna.setFinal(*it);

	return wa_dna;
}

#endif
