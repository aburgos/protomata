
#include "Translator.h"

Translator::Translator()
{
	toprotein["AAA"] = 'K'; toprotein["AAC"] = 'N'; toprotein["AAG"] = 'K'; toprotein["AAT"] = 'N';
	toprotein["ACA"] = 'T'; toprotein["ACC"] = 'T'; toprotein["ACG"] = 'T'; toprotein["ACT"] = 'T';
	toprotein["AGA"] = 'R'; toprotein["AGC"] = 'S'; toprotein["AGG"] = 'R'; toprotein["AGT"] = 'S';
	toprotein["ATA"] = 'I'; toprotein["ATC"] = 'I'; toprotein["ATG"] = 'M'; toprotein["ATT"] = 'I';

	toprotein["CAA"] = 'Q'; toprotein["CAC"] = 'H'; toprotein["CAG"] = 'Q'; toprotein["CAT"] = 'H';
	toprotein["CCA"] = 'P'; toprotein["CCC"] = 'P'; toprotein["CCG"] = 'P'; toprotein["CCT"] = 'P';
	toprotein["CGA"] = 'R'; toprotein["CGC"] = 'R'; toprotein["CGG"] = 'R'; toprotein["CGT"] = 'R';
	toprotein["CTA"] = 'L'; toprotein["CTC"] = 'L'; toprotein["CTG"] = 'L'; toprotein["CTT"] = 'L';

	toprotein["GAA"] = 'E'; toprotein["GAC"] = 'D'; toprotein["GAG"] = 'E'; toprotein["GAT"] = 'D';
	toprotein["GCA"] = 'A'; toprotein["GCC"] = 'A'; toprotein["GCG"] = 'A'; toprotein["GCT"] = 'A';
	toprotein["GGA"] = 'G'; toprotein["GGC"] = 'G'; toprotein["GGG"] = 'G'; toprotein["GGT"] = 'G';
	toprotein["GTA"] = 'V'; toprotein["GTC"] = 'V'; toprotein["GTG"] = 'V'; toprotein["GTT"] = 'V';

	toprotein["TAA"] = '-'; toprotein["TAC"] = 'Y'; toprotein["TAG"] = '-'; toprotein["TAT"] = 'Y';
	toprotein["TCA"] = 'S'; toprotein["TCC"] = 'S'; toprotein["TCG"] = 'S'; toprotein["TCT"] = 'S';
	toprotein["TGA"] = '-'; toprotein["TGC"] = 'C'; toprotein["TGG"] = 'W'; toprotein["TGT"] = 'C';
	toprotein["TTA"] = 'L'; toprotein["TTC"] = 'F'; toprotein["TTG"] = 'L'; toprotein["TTT"] = 'F';
}

Translator::DNASet Translator::protein2dna(char protein) const
{
	DNASet ret;
	for(std::map<std::string, char>::const_iterator it = toprotein.begin(); it != toprotein.end(); ++it)
	{
		const std::string& dna = (*it).first;
		char current_protein = (*it).second;

		if(protein == current_protein) ret.insert(dna);
	}
	return ret;
}

char Translator::dna2protein(const std::string& dna) const
{
	std::map<std::string, char>::const_iterator it = toprotein.find(dna);
	if(it == toprotein.end()) { std::cerr << "DNA codon " << dna << " not found." << std::endl; exit(1); }
	return (*it).second;
}
