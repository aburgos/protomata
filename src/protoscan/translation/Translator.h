
#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <set>
#include <map>
#include <cstdlib>
#include <iostream>

class Translator
{
  public:
	typedef std::set<std::string> DNASet;

  private:
	std::map<std::string, char> toprotein;

  public:
	Translator();

	DNASet protein2dna(char) const;
	char dna2protein(const std::string&) const;
};

#endif
