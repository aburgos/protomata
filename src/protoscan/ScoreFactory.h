
#ifndef SCOREFACTORY_H
#define SCOREFACTORY_H

#include "scores/Viterbi_NoPath.h"
#include "scores/Forward.h"

template <typename CntT>
class ScoreFactory
{
  public:
	Viterbi_NoPath<CntT>* create(const WeightedAutomata<CntT>& wa, bool allow_partial_sequence_match, bool allow_partial_model_match, bool allow_x, bool forward, Strategy x_strategy)
	{
		if(forward) return new Forward<CntT>(wa, allow_partial_sequence_match);
		else return new Viterbi_NoPath<CntT>(wa, allow_partial_sequence_match, allow_partial_model_match, allow_x, x_strategy);
	}
};

#endif
