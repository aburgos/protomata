
#include <time.h>
#include <iostream>
#include "ProtoScan.h"
#include "tclap/CmdLine.h"

int main(int argc, char **argv)
{
	//double start, end;
	//start = (double)clock();

	// parse parameters
	ProtoscanParameters parameters;
	parameters.parse(argc, argv);

	// choose right instantiation for scanning sequences
	if(parameters.use_integers()) ProtoScan<int> protoscan(parameters);
	else ProtoScan<double> protoscan(parameters);

	//end = (double)clock();
	//double time_elapsed = double(end - start) / CLOCKS_PER_SEC;
	//std::cout << std::endl << "Time elapsed: " << time_elapsed << " secs" << std::endl;

	return 0;
}
