
#include "ProtoscanAllowedParameters.h"

ProtoscanAllowedParameters::ProtoscanAllowedParameters()
{
	strategies_values.push_back("minVal");
	strategies_values.push_back("maxVal");
	strategies_values.push_back("meanVal");

	scan_scores.push_back("probability");
	scan_scores.push_back("log");
	scan_scores.push_back("LR");
	scan_scores.push_back("logLR");
}

TCLAP::ValuesConstraint<std::string>* ProtoscanAllowedParameters::strategies_values_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(strategies_values);
	return &allowedVals;
}

TCLAP::ValuesConstraint<std::string>* ProtoscanAllowedParameters::scan_scores_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(scan_scores);
	return &allowedVals;
}
