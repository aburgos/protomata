
#ifndef XMLPATHAGGREGATOR_H
#define XMLPATHAGGREGATOR_H

#include "pugixml.hpp"
#include "paths/StatsPath.h"

class XmlPathAggregator
{
  public:
	typedef std::vector<StatsPath> StatsPathVector;
	XmlPathAggregator(const std::string&, const StatsPathVector&);
};

XmlPathAggregator::XmlPathAggregator(const std::string& input_file, const StatsPathVector& paths)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(input_file.c_str());
    if(!result) { std::cerr << "Error loading file " << input_file << std::endl; exit(1); }

	pugi::xml_node main_node = doc.child("weighted_protomata");
	if(!main_node) { std::cerr << "Error parsing file " << input_file << std::endl; exit(1); }

	pugi::xml_node resume_node = doc.child("weighted_protomata").child("resume");
	resume_node.append_attribute("paths") = (unsigned int)paths.size();

	pugi::xml_node paths_node = main_node.append_child("paths");
	for(StatsPathVector::const_iterator it = paths.begin(); it != paths.end(); ++it)
	{
		const StatsPath& path = *it;

		pugi::xml_node path_node = paths_node.append_child("path");
		path_node.append_attribute("mean") = path.mean();
		path_node.append_attribute("var") = path.var();
		path_node.append_attribute("zones") = path.zones();
		path_node.append_attribute("zones_length") = path.zones_length();
		path_node.append_attribute("transitions_cost") = path.transitions_cost();
	}

	doc.save_file(input_file.c_str());
}

#endif
