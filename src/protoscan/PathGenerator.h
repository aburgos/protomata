
#ifndef PATHGENERATOR_H
#define PATHGENERATOR_H

#include <iostream>
#include <cstdlib>
#include <vector>
#include <set>
#include <math.h>
#include "paths/Path.h"
#include "ProtoGraph.h"
#include "zones/EndZone.h"
#include "zones/BeginZone.h"
#include "columns/Column.h"

template <typename ProtoGraphType>
class PathGenerator
{
  public:
	typedef std::vector<Path> PathVector;
	typedef typename ProtoGraphType::CountType CountType;
	typedef typename ProtoGraphType::ColumnType ColumnType;
	typedef typename ProtoGraphType::ZoneType ZoneType;
	typedef typename ProtoGraphType::TransitionType TransitionType;

  private:
	PathVector final_paths;

  public:
	PathGenerator(const ProtoGraphType&, unsigned int, unsigned int);

	PathVector paths_copy() const { return final_paths; }
	const PathVector& paths() const { return final_paths; }
};

template <typename ProtoGraphType>
PathGenerator<ProtoGraphType>::PathGenerator(const ProtoGraphType& pg, unsigned int begin_zone_id, unsigned int end_zone_id)
{
	typedef unsigned int ZoneID;
	typedef std::set<ZoneID> ZoneIDSet;

	// find all paths between the begin node and the end node
	std::map<ZoneID, PathVector> paths_ending_in;

	// add the initial node
	Path p; p.add(begin_zone_id);
	PathVector t; t.push_back(p);
	paths_ending_in[begin_zone_id] = t;

	ZoneIDSet nodesToExpand;
	nodesToExpand.insert(begin_zone_id);

	// breadth-first search algorithm
	while(!nodesToExpand.empty())
	{
		ZoneIDSet nextNodesToExpand;
		std::map<ZoneID, PathVector> paths_to_update;

		for(ZoneIDSet::const_iterator sit = nodesToExpand.begin(); sit != nodesToExpand.end(); ++sit)
		{
			typename ProtoGraphType::TransitionsGraph::vertex_iterator vit = pg.m_zone_graph.hasVertex(*sit);
			typename ProtoGraphType::TransitionsGraph::vertex_iterator::adjacent_iterator ait;
			for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
			{
				unsigned int paths_before_loop = paths_to_update[*ait].size();
				for(PathVector::iterator pit = paths_ending_in[*sit].begin(); pit != paths_ending_in[*sit].end(); ++pit)
				{
					Path new_path(*pit);
					new_path.add(*ait);
					paths_to_update[*ait].push_back(new_path);
				}
				if(paths_to_update[*ait].size() != paths_before_loop) nextNodesToExpand.insert(*ait);
			}
		}

		// for efficiency only
		std::swap(paths_ending_in, paths_to_update);
		std::swap(nodesToExpand, nextNodesToExpand);
		nodesToExpand.erase(end_zone_id);

		// save found paths
		final_paths.insert(final_paths.end(), paths_ending_in[end_zone_id].begin(), paths_ending_in[end_zone_id].end());
		paths_ending_in.erase(end_zone_id); // maybe takes more time? how much memory does it saves?
	}
}

#endif
