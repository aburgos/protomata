
#ifndef WAPROPERTIES_H
#define WAPROPERTIES_H

#include <cstdlib>
#include "WeightedAutomata.h"

template <typename CntT>
class WAProperties: public WeightedAutomata<CntT>
{
  public:
	typedef unsigned int State;
	typedef std::pair<unsigned int, unsigned int> ZoneColumnPair;

  private:
	//! Correspondance of states with a proto zone and its column.
	std::map<State, ZoneColumnPair> m_zonecol;
	//! Correspondance of states with the transition type.
	std::map<State, std::string> m_trantype;

  public:
	WAProperties() {}

	bool is_zone(State s) const;
	bool has_transition_type(State s) const;

	void zone_column(State, const ZoneColumnPair&);
	ZoneColumnPair zone_column(State) const;

	void transition_type(State, const std::string&);
	std::string transition_type(State) const;
};

template <typename CntT>
bool WAProperties<CntT>::is_zone(State s) const
{
	return (m_zonecol.find(s) != m_zonecol.end());
}

template <typename CntT>
bool WAProperties<CntT>::has_transition_type(State s) const
{
	return (m_trantype.find(s) != m_trantype.end());
}

template <typename CntT>
void WAProperties<CntT>::zone_column(State state, const ZoneColumnPair& zc)
{
	m_zonecol[state] = zc;
}

template <typename CntT>
typename WAProperties<CntT>::ZoneColumnPair WAProperties<CntT>::zone_column(State state) const
{
	if(m_zonecol.find(state) == m_zonecol.end())
	{ std::cerr << "No (zone, column) assigned in Weighted Automata to state " << state << std::endl; exit(1); } 
	return (*(m_zonecol.find(state))).second;
}

template <typename CntT>
void WAProperties<CntT>::transition_type(State state, const std::string& type)
{
	m_trantype[state] = type;
}

template <typename CntT>
std::string WAProperties<CntT>::transition_type(State state) const
{
	return (*(m_trantype.find(state))).second;
}

#endif
