
#ifndef VITERBINOPATH_H
#define VITERBINOPATH_H

#include <cmath>
#include <limits>
#include "Scan.h"
#include "AminoAcids.h"
#include "TransitionStrategies.h"
#include "ProtoScanEnumTypes.h"

/*!
Computes the scores of sequences given a weighted automata.
*/
template <typename CntT>
class Viterbi_NoPath: public Scan<CntT>
{
  protected:
	typedef unsigned int State;
	typedef std::map<State, CntT> Column;
	typedef std::pair<State, State> TranStates;
	typedef std::map<TranStates, CntT> Transitions;
	typedef std::map<char, Transitions> AminoAcidTransitions;

	AminoAcidTransitions transitions;
	std::set<State> start_states;
	std::set<State> final_states;
	// Stores the score of each sequence.
	std::map<Sequence, CntT> m_scores;

	virtual void initialize_minus_inf(const WeightedAutomata<CntT>&);
	void add_X_transitions(const WeightedAutomata<CntT>&, Strategy);

  private:
	Column minus_inf;

  public:
	//! Gets all transitions from the weighted automata.
	Viterbi_NoPath(const WeightedAutomata<CntT>&, bool, bool, bool = true, Strategy = minVal);
	virtual ~Viterbi_NoPath() {}

	virtual CntT score(const Sequence&);
};

template <typename CntT>
Viterbi_NoPath<CntT>::Viterbi_NoPath(const WeightedAutomata<CntT>& wa, bool allow_partial_sequence_match, bool allow_partial_model_match, bool allow_X, Strategy X_strategy)
{
	// get all transitions
	const AminoAcids& aa = AminoAcids::instance();
	for(AminoAcids::const_iterator ait = aa.begin(); ait != aa.end(); ++ait)
		transitions[*ait] = wa.getTransitions(*ait);

	if(allow_X) add_X_transitions(wa, X_strategy);

	typedef typename WeightedAutomata<CntT>::state_iterator wa_state_iterator;

	if(allow_partial_model_match)
	{
		// set every state as a start and a final state
		for(typename WeightedAutomata<CntT>::state_iterator sit = wa.state_begin(); sit != wa.state_end(); ++sit)
		{
			start_states.insert(*sit);
			final_states.insert(*sit);
		}
	}
	if(allow_partial_sequence_match)
	{
		// add "loop" transitions to every start and end state
		typename WeightedAutomata<CntT>::state_iterator sit;
		for(sit = wa.start_state_begin(); sit != wa.start_state_end(); ++sit)
			for(AminoAcids::const_iterator ait = aa.begin(); ait != aa.end(); ++ait)
				transitions[*ait].insert(std::make_pair(std::make_pair(*sit, *sit), 0));
		for(sit = wa.final_state_begin(); sit != wa.final_state_end(); ++sit)
			for(AminoAcids::const_iterator ait = aa.begin(); ait != aa.end(); ++ait)
				transitions[*ait].insert(std::make_pair(std::make_pair(*sit, *sit), 0));
	}

	if(!allow_partial_model_match)
	{
		// get start and final states
		typename WeightedAutomata<CntT>::state_iterator sit;
		for(sit = wa.start_state_begin(); sit != wa.start_state_end(); ++sit)
			start_states.insert(*sit);
		for(sit = wa.final_state_begin(); sit != wa.final_state_end(); ++sit)
			final_states.insert(*sit);
	}

	this->initialize_minus_inf(wa);
}

template <typename CntT>
void Viterbi_NoPath<CntT>::initialize_minus_inf(const WeightedAutomata<CntT>& wa)
{
	// initiallize all states to -inf (or minimum)
	typename WeightedAutomata<CntT>::state_iterator sit;
	for(sit = wa.state_begin(); sit != wa.state_end(); ++sit)
		if(std::numeric_limits<CntT>::has_infinity) minus_inf[*sit] = -std::numeric_limits<CntT>::infinity();
		else minus_inf[*sit] = std::numeric_limits<CntT>::min();
}

template <typename CntT>
void Viterbi_NoPath<CntT>::add_X_transitions(const WeightedAutomata<CntT>& wa, Strategy X_strategy)
{
	TransitionStrategies<CntT> strategy(wa);
	// define special transition for X
	switch(X_strategy)
	{
		case minVal: transitions['X'] = strategy.min_value(); break;
		case maxVal: transitions['X'] = strategy.max_value(); break;
		case meanVal: transitions['X'] = strategy.mean_value(); break;
	}
}

template <typename CntT>
inline CntT Viterbi_NoPath<CntT>::score(const Sequence& sequence)
{
	typename std::map<Sequence, CntT>::const_iterator mit;
	if((mit = m_scores.find(sequence)) != m_scores.end())
		return (*mit).second;

	Column current = minus_inf;

	// iterate through the sequence's chars
	for(Sequence::const_iterator it = sequence.begin(); it != sequence.end(); ++it)
	{
		// set target to -inf
		Column target = minus_inf;

		// get the transitions of the current amino acid
		const Transitions& current_transitions = transitions[*it];

		if(std::numeric_limits<CntT>::has_infinity)
		{
			typename std::map<std::pair<State, State>, CntT>::const_iterator tit;
			for(tit = current_transitions.begin(); tit != current_transitions.end(); ++tit)
			{
				const std::pair<State, State>& tr = (*tit).first;
				const State source_state = tr.first;
				const State target_state = tr.second;
				const CntT weight = (*tit).second;

				if(current[source_state] == -std::numeric_limits<CntT>::infinity())
				{
					if((start_states.find(source_state) != start_states.end()))
					{
						if(target[target_state] < weight) target[target_state] = weight;
					}
				}
				else if(target[target_state] < current[source_state] + weight)
				{
					target[target_state] = current[source_state] + weight;
				}
			}
		}
		else
		{
			typename std::map<std::pair<State, State>, CntT>::const_iterator tit;
			for(tit = current_transitions.begin(); tit != current_transitions.end(); ++tit)
			{
				const std::pair<State, State>& tr = (*tit).first;
				const State source_state = tr.first;
				const State target_state = tr.second;
				const CntT weight = (*tit).second;

				if(current[source_state] == std::numeric_limits<CntT>::min())
				{
					if((start_states.find(source_state) != start_states.end()))
					{
						if(target[target_state] < weight) target[target_state] = weight;
					}
				}
				else if(target[target_state] < current[source_state] + weight)
				{
					target[target_state] = current[source_state] + weight;
				}
			}
		}

		std::swap(current, target);
	}

	// get the maximum score
	CntT m_score;
	if(std::numeric_limits<CntT>::has_infinity) m_score = -std::numeric_limits<CntT>::infinity();
	else m_score = std::numeric_limits<CntT>::min();

	for(std::set<State>::iterator it = final_states.begin(); it != final_states.end(); ++it)
		if(m_score < current[*it]) m_score = current[*it];

	m_scores[sequence] = m_score;
	return m_score;
}

#endif
