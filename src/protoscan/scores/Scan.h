
#ifndef SCAN_H
#define SCAN_H

#include "Sequence.h"

template <typename CntT>
class Scan
{
  public:
	Scan() {}
	virtual ~Scan() {}

	virtual CntT score(const Sequence&) = 0;
};

#endif
