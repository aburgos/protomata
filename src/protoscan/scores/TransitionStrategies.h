
#ifndef TRANSITIONSTRATEGIES_H
#define TRANSITIONSTRATEGIES_H

#include <limits>
#include "WeightedAutomata.h"

template <typename CntT>
class TransitionStrategies
{
  private:
	typedef unsigned int State;
	typedef std::pair<State, State> TranStates;
	typedef std::map<char, CntT> WATransitions;
	typedef std::map<TranStates, CntT> Transitions;
	typedef WeightedAutomata<CntT> WAType;

	const WAType& m_wa;

  public:
	TransitionStrategies(const WAType& wa): m_wa(wa) {}

	Transitions min_value() const;
	Transitions max_value() const;
	Transitions mean_value() const;
};

template <typename CntT>
typename TransitionStrategies<CntT>::Transitions TransitionStrategies<CntT>::min_value() const
{
	Transitions aa_transitions;

	for(typename WAType::transition_const_iterator it = m_wa.transition_begin(); it != m_wa.transition_end(); ++it)
	{
		const typename WAType::TranStates& state_transition = (*it).first;
		const WATransitions& transitions = m_wa.getTransitions(state_transition.first, state_transition.second);

		if(std::numeric_limits<CntT>::has_infinity)
		{
			CntT transition_min_value = std::numeric_limits<CntT>::infinity();
			for(typename WATransitions::const_iterator tit = transitions.begin(); tit != transitions.end(); ++tit)
			{
				const CntT current_value = (*tit).second;
				if(current_value < transition_min_value) transition_min_value = current_value;
			}

			aa_transitions[state_transition] = transition_min_value;
		}
		else
		{
			CntT transition_min_value = std::numeric_limits<CntT>::max();
			for(typename WATransitions::const_iterator tit = transitions.begin(); tit != transitions.end(); ++tit)
			{
				const CntT current_value = (*tit).second;
				if(current_value < transition_min_value) transition_min_value = current_value;
			}

			aa_transitions[state_transition] = transition_min_value;
		}
	}

	return aa_transitions;
}

template <typename CntT>
typename TransitionStrategies<CntT>::Transitions TransitionStrategies<CntT>::max_value() const
{
	Transitions aa_transitions;

	for(typename WAType::transition_const_iterator it = m_wa.transition_begin(); it != m_wa.transition_end(); ++it)
	{
		const typename WAType::TranStates& state_transition = (*it).first;
		const WATransitions& transitions = m_wa.getTransitions(state_transition.first, state_transition.second);

		if(std::numeric_limits<CntT>::has_infinity)
		{
			CntT transition_max_value = -std::numeric_limits<CntT>::infinity();
			for(typename WATransitions::const_iterator tit = transitions.begin(); tit != transitions.end(); ++tit)
			{
				const CntT current_value = (*tit).second;
				if(current_value > transition_max_value) transition_max_value = current_value;
			}

			aa_transitions[state_transition] = transition_max_value;
		}
		else
		{
			CntT transition_max_value = std::numeric_limits<CntT>::min();
			for(typename WATransitions::const_iterator tit = transitions.begin(); tit != transitions.end(); ++tit)
			{
				const CntT current_value = (*tit).second;
				if(current_value > transition_max_value) transition_max_value = current_value;
			}

			aa_transitions[state_transition] = transition_max_value;
		}
	}

	return aa_transitions;
}

template <typename CntT>
typename TransitionStrategies<CntT>::Transitions TransitionStrategies<CntT>::mean_value() const
{
	Transitions aa_transitions;

	for(typename WAType::transition_const_iterator it = m_wa.transition_begin(); it != m_wa.transition_end(); ++it)
	{
		const typename WAType::TranStates& state_transition = (*it).first;
		const WATransitions& transitions = m_wa.getTransitions(state_transition.first, state_transition.second);

		CntT sum_values = 0;
		for(typename WATransitions::const_iterator tit = transitions.begin(); tit != transitions.end(); ++tit)
		{
			const CntT current_value = (*tit).second;
			sum_values += current_value;
		}

		aa_transitions[state_transition] = (double)sum_values / (double)transitions.size();
	}

	return aa_transitions;
}

#endif
