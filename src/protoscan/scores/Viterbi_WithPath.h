
#ifndef VITERBIWITHPATH_H
#define VITERBIWITHPATH_H

#include "Viterbi_NoPath.h"
#include "WAProperties.h"
#include "Sequence.h"
#include "paths/AlignmentPath.h"

/*!
Computes the scores and the paths of sequences given a weighted automata.
*/
template <typename CntT>
class Viterbi_WithPath: Viterbi_NoPath<CntT>
{
  protected:
	typedef unsigned int State;
	typedef std::pair<State, State> TranStates;
	typedef std::map<TranStates, CntT> Transitions;
	typedef std::map<char, Transitions> AminoAcidTransitions;
	typedef std::map<State, std::pair<State, CntT> > ColumnWithLastState;

	const WAProperties<CntT>& m_wa;
	ColumnWithLastState minus_inf;
	std::map<Sequence, AlignmentPath> sequence_path;

	void initialize_minus_inf(const WeightedAutomata<CntT>&);

  public:
	//! Gets all transitions from the weighted automata.
	Viterbi_WithPath(const WAProperties<CntT>&, bool, bool, bool = true, Strategy = minVal);
	virtual ~Viterbi_WithPath() {}

	CntT score(const Sequence&);
	const AlignmentPath& path(const Sequence& seq) const { return (*(sequence_path.find(seq))).second; }
};

template <typename CntT>
Viterbi_WithPath<CntT>::Viterbi_WithPath(const WAProperties<CntT>& wa, bool allow_partial_sequence_match, bool allow_partial_model_match, bool allow_X, Strategy X_strategy): Viterbi_NoPath<CntT>(wa, allow_partial_sequence_match, allow_partial_model_match, allow_X, X_strategy), m_wa(wa)
{
	initialize_minus_inf(wa);
}

template <typename CntT>
void Viterbi_WithPath<CntT>::initialize_minus_inf(const WeightedAutomata<CntT>& wa)
{
	// initiallize all states to -inf (or minimum)
	typename WeightedAutomata<CntT>::state_iterator sit;
	for(sit = wa.state_begin(); sit != wa.state_end(); ++sit)
		if(std::numeric_limits<CntT>::has_infinity)
			minus_inf[*sit] = std::make_pair(*sit, -std::numeric_limits<CntT>::infinity());
		else minus_inf[*sit] = std::make_pair(*sit, std::numeric_limits<CntT>::min());
}

template <typename CntT>
inline CntT Viterbi_WithPath<CntT>::score(const Sequence& sequence)
{
	typename std::map<Sequence, CntT>::const_iterator mit;
	if((mit = this->m_scores.find(sequence)) != this->m_scores.end())
		return (*mit).second;

	std::vector<ColumnWithLastState> columns;
	ColumnWithLastState current = minus_inf;

	// iterate through the sequence's chars
	for(Sequence::const_iterator it = sequence.begin(); it != sequence.end(); ++it)
	{
		// set target to -inf
		ColumnWithLastState target = minus_inf;

		// get the transitions of the current amino acid
		const Transitions& current_transitions = this->transitions[*it];

		if(std::numeric_limits<CntT>::has_infinity)
		{
			typename std::map<std::pair<State, State>, CntT>::const_iterator tit;
			for(tit = current_transitions.begin(); tit != current_transitions.end(); ++tit)
			{
				const std::pair<State, State>& tr = (*tit).first;
				const State source_state = tr.first;
				const State target_state = tr.second;
				const CntT weight = (*tit).second;

				if(current[source_state].second == -std::numeric_limits<CntT>::infinity())
				{
					if((this->start_states.find(source_state) != this->start_states.end()))
					{
						if(target[target_state].second < weight)
							target[target_state] = std::make_pair(source_state, weight);
					}
				}
				else if(target[target_state].second < current[source_state].second + weight)
				{
					target[target_state] = std::make_pair(source_state, current[source_state].second + weight);
				}
			}
		}
		else
		{
			typename std::map<std::pair<State, State>, CntT>::const_iterator tit;
			for(tit = current_transitions.begin(); tit != current_transitions.end(); ++tit)
			{
				const std::pair<State, State>& tr = (*tit).first;
				const State source_state = tr.first;
				const State target_state = tr.second;
				const CntT weight = (*tit).second;

				if(current[source_state].second == std::numeric_limits<CntT>::min())
				{
					if((this->start_states.find(source_state) != this->start_states.end()))
					{
						if(target[target_state].second < weight)
							target[target_state] = std::make_pair(source_state, weight);
					}
				}
				else if(target[target_state].second < current[source_state].second + weight)
				{
					target[target_state] = std::make_pair(source_state, current[source_state].second + weight);
				}
			}
		}

		columns.push_back(current);
		std::swap(current, target);
	}

	// get the maximum score
	CntT m_score;
	if(std::numeric_limits<CntT>::has_infinity) m_score = -std::numeric_limits<CntT>::infinity();
	else m_score = std::numeric_limits<CntT>::min();

	// if there is at least a final state, get the path
	if(this->final_states.begin() != this->final_states.end())
	{
		// get the last state, the state with biggest score from all the final states
		State last_state = *this->final_states.begin();

		for(std::set<State>::iterator it = this->final_states.begin(); it != this->final_states.end(); ++it)
			if(m_score < current[*it].second)
			{
				last_state = current[*it].first;
				m_score = current[*it].second;
			}

		// get the alignment path
		AlignmentPath path;
		unsigned int position = sequence.length() - 1;
		typename std::vector<ColumnWithLastState>::const_reverse_iterator rit;
		for(rit = columns.rbegin(); rit != columns.rend(); ++rit)
		{
			const ColumnWithLastState& curr_col = *rit;
			const State from_state = (*(curr_col.find(last_state))).second.first;

			if(m_wa.is_zone(last_state))
			{
				const AlignmentPath::ZoneColumnPair& zc = m_wa.zone_column(last_state);
				if(m_wa.is_zone(zc.first)) path.add(position, zc);
			}

			last_state = from_state;
			position--;
		}
		sequence_path[sequence] = path;
	}
	// add an empty path
	else sequence_path[sequence] = AlignmentPath();

	this->m_scores[sequence] = m_score;
	return m_score;
}

#endif
