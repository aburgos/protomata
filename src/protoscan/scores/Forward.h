
#ifndef FORWARD_H
#define FORWARD_H

#include "Viterbi_NoPath.h"

template <typename CntT>
class Forward: public Viterbi_NoPath<CntT>
{
  public:
	typedef Viterbi_NoPath<CntT> ViterbiType;
	typedef typename ViterbiType::State State;

  private:
	typename ViterbiType::Column minus_inf;

  public:
	Forward(const WeightedAutomata<CntT>& wa, bool);
	virtual ~Forward() {}

	CntT score(const Sequence&);
};

template <typename CntT>
Forward<CntT>::Forward(const WeightedAutomata<CntT>& wa, bool allow_partial_sequence_match): Viterbi_NoPath<CntT>(wa, allow_partial_sequence_match, false, false) {}

template <typename CntT>
inline CntT Forward<CntT>::score(const Sequence& sequence)
{
	typename std::map<Sequence, CntT>::const_iterator mit;
	if((mit = this->m_scores.find(sequence)) != this->m_scores.end())
		return (*mit).second;

	// compute the null model score
	CntT null_score = 0;
	const AminoAcids& aa = AminoAcids::instance();

	typename ViterbiType::Column current = this->minus_inf;

	// iterate through the sequence's chars
	for(Sequence::const_iterator it = sequence.begin(); it != sequence.end(); ++it)
	{
		const char aminoacid = *it;

		// add background probability to the null score
		null_score += aa.back_prob(aminoacid);

		// set target to -inf
		typename ViterbiType::Column target = this->minus_inf;

		// get the transitions of the current amino acid
		const typename ViterbiType::Transitions& current_transitions = this->transitions[aminoacid];

		if(std::numeric_limits<CntT>::has_infinity)
		{
			typename std::map<std::pair<State, State>, CntT>::const_iterator tit;
			for(tit = current_transitions.begin(); tit != current_transitions.end(); ++tit)
			{
				const std::pair<State, State>& tr = (*tit).first;
				const State source_state = tr.first;
				const State target_state = tr.second;
				const CntT weight = (*tit).second;

				if(current[source_state] == -std::numeric_limits<CntT>::infinity())
				{
					if((this->start_states.find(source_state) != this->start_states.end()))
					{
						if(target[target_state] == -std::numeric_limits<CntT>::infinity())
							target[target_state] = weight;
						else
							target[target_state] += weight;
					}
				}
				else
				{
					if(target[target_state] == -std::numeric_limits<CntT>::infinity())
						target[target_state] = current[source_state] + weight;
					else
						target[target_state] += current[source_state] + weight;
				}
			}
		}
		else
		{
			typename std::map<std::pair<State, State>, CntT>::const_iterator tit;
			for(tit = current_transitions.begin(); tit != current_transitions.end(); ++tit)
			{
				const std::pair<State, State>& tr = (*tit).first;
				const State source_state = tr.first;
				const State target_state = tr.second;
				const CntT weight = (*tit).second;

				if(current[source_state] == std::numeric_limits<CntT>::min())
				{
					if((this->start_states.find(source_state) != this->start_states.end()))
					{
						if(target[target_state] == std::numeric_limits<CntT>::min())
							target[target_state] = weight;
						else
							target[target_state] += weight;
					}
				}
				else
				{
					if(target[target_state] == std::numeric_limits<CntT>::min())
						target[target_state] = current[source_state] + weight;
					else
						target[target_state] += current[source_state] + weight;
				}
			}
		}

		std::swap(current, target);
	}

	// get the maximum score
	CntT m_score;
	if(std::numeric_limits<CntT>::has_infinity) m_score = -std::numeric_limits<CntT>::infinity();
	else m_score = std::numeric_limits<CntT>::min();

	for(typename std::set<State>::iterator it = this->final_states.begin(); it != this->final_states.end(); ++it)
		if(m_score < current[*it]) m_score = current[*it];

	m_score /= null_score;

	this->m_scores[sequence] = m_score;
	return m_score;
}

#endif
