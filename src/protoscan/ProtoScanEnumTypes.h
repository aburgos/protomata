
#ifndef PROTOSCANENUMTYPES_H
#define PROTOSCANENUMTYPES_H

enum Strategy
{
	minVal,
	maxVal,
	meanVal
};

enum ScanScore
{
	probability,
	logT,
	LR,
	logLR
};

#endif
