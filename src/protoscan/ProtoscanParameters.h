
#ifndef PROTOSCANPARAMETERS_H
#define PROTOSCANPARAMETERS_H

#include <limits>
#include "Parameters.h"
#include "FileNameSearcher.h"
#include "ProtobuildAllowedParameters.h"
#include "ProtoscanAllowedParameters.h"
#include "ProtoScanEnumTypes.h"

class ProtoscanParameters: public virtual Parameters
{
  protected:
	ProtobuildAllowedParameters protobuild_allowed;
	ProtoscanAllowedParameters protoscan_allowed;

	std::string m_wa;
	TCLAP::ValueArg<std::string> m_protoscan_input;
	TCLAP::ValueArg<std::string> m_protoscan_output;
	TCLAP::ValueArg<std::string> m_protoscan_tag;
	TCLAP::ValueArg<std::string> m_sequences;
	TCLAP::ValueArg<std::string> m_sscr;
	TCLAP::ValueArg<double> m_score_threshold;
	TCLAP::ValueArg<unsigned int> m_lobound;
	TCLAP::ValueArg<unsigned int> m_upbound;
	TCLAP::ValueArg<std::string> m_pseudocounts;
	TCLAP::ValueArg<std::string> m_components;
	TCLAP::ValueArg<std::string> m_xstr;

	Strategy m_x_strategy;
	ScanScore m_scan_score;

	TCLAP::SwitchArg m_disallow_X;
	TCLAP::SwitchArg m_exceptions_as_gaps;
	TCLAP::SwitchArg m_disallow_partial_sequence_match;
	TCLAP::SwitchArg m_allow_partial_model_match;
	TCLAP::SwitchArg m_use_integers;
	TCLAP::SwitchArg m_significativity;
	TCLAP::SwitchArg m_output_wa;
	TCLAP::SwitchArg m_forward;
	TCLAP::SwitchArg m_scan_dna;

	virtual void sanity_checks();
	virtual void initialize_values();
	virtual void add_string_parameters();
	virtual std::string compute_output_file(const std::string&) const;

  public:
	ProtoscanParameters();
	virtual ~ProtoscanParameters() {}

	virtual void parse(int argc, char **argv);
	virtual void add_arguments(TCLAP::CmdLine&);

	virtual std::string input() const { return m_protoscan_input.getValue(); }
	virtual std::string output() const;
	virtual std::string tag() const { return m_protoscan_tag.getValue(); }
	virtual std::string sequences() const { return m_sequences.getValue(); }
	virtual std::string pseudocounts() const { return m_pseudocounts.getValue(); }
	virtual std::string components_file() const;
	std::string wa_file() const;

	bool allow_X() const { return !m_disallow_X.getValue(); }
	bool exceptions_as_gaps() const { return m_exceptions_as_gaps.getValue(); }
	bool allow_partial_sequence_match() const { return !m_disallow_partial_sequence_match.getValue(); }
	bool allow_partial_model_match() const { return m_allow_partial_model_match.getValue(); }
	bool use_integers() const { if(output_wa()) return true; else return m_use_integers.getValue(); }
	bool significativity() const { return m_significativity.getValue(); }
	bool output_wa() const { return m_output_wa.getValue(); }
	bool forward() const { return m_forward.getValue(); }
	bool scan_dna() const { return m_scan_dna.getValue(); }

	double score_threshold() const { return m_score_threshold.getValue(); }
	unsigned int lobound() const { return m_lobound.getValue(); }
	unsigned int upbound() const { return m_upbound.getValue(); }

	Strategy x_strategy() const { return m_x_strategy; }
	ScanScore scan_score() const { return m_scan_score; }
};

#endif
