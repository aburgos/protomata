
#ifndef WEIGHTEDAUTOMATA_H
#define WEIGHTEDAUTOMATA_H

#include <string>
#include <cmath>
#include <vector>
#include <set>
#include "Matrix.h"

/*!
Deterministic automata. It uses a matrix with pair of states as rows and amino acids as columns, and it keeps track of the set values with another matrix.
*/
template <typename CntT>
class WeightedAutomata
{
  public:
	typedef unsigned int State;
	typedef std::pair<State, State> TranStates;
	typedef std::set<State> StateSet;

  private:
	//! Store the current states.
	std::set<State> states;
	//! Store the current start states.
	std::set<State> start_states;
	//! Store the current final states.
	std::set<State> final_states;
	//! Structure to store the weighted automata.
	Matrix<TranStates, char, CntT> wam;
	//! Structure to store keep track of set values.
	Matrix<TranStates, char, bool> is_set;

  public:
	WeightedAutomata() {}
	bool setStart(State);
	bool setFinal(State);
	/*!
		Adds a transition between the states for a specific char.
		If there exists already such a transition, it is not added.
		\return True if the transition was added. False otherwise.
	*/
	bool addTransition(State, State, const std::pair<char, CntT>&);
	/*!
		For each char specified, adds a transition between the states.
		If there exists already such a transition, it is not added.
		\return True if at least one transition was added. False otherwise.
	*/
	bool addTransitions(State, State, const std::map<char, CntT>&);
	//! Gets all the transitions between two states.
	std::map<char, CntT> getTransitions(State, State) const;
	StateSet getTransitionsWithSource(State) const;
	StateSet getTransitionsWithTarget(State) const;
	//! Returns all the source and target states where a transition of the parameter exists.
	std::map<TranStates, CntT> getTransitions(char) const;
	//! Returns the number of current states.
	unsigned int states_size() const { return states.size(); }
	//! Check if a state is a start state.
	bool isStart(State state) const { return (start_states.find(state) != start_states.end()); }
	//! Check if a state is a final state.
	bool isFinal(State state) const { return (final_states.find(state) != final_states.end()); }
	//! Alias for state iterators.
	typedef std::set<State>::const_iterator state_iterator;
	//! Begin iterator for all states.
	state_iterator state_begin() const { return states.begin(); }
	//! End iterator for all states.
	state_iterator state_end() const { return states.end(); }
	//! Begin iterator for start states.
	state_iterator start_state_begin() const { return start_states.begin(); }
	//! End iterator for start states.
	state_iterator start_state_end() const { return start_states.end(); }
	//! Begin iterator for final states.
	state_iterator final_state_begin() const { return final_states.begin(); }
	//! End iterator for final states.
	state_iterator final_state_end() const { return final_states.end(); }
	//! Alias for transitions iterator
	typedef typename Matrix<TranStates, char, CntT>::row_const_iterator transition_const_iterator;
	transition_const_iterator transition_begin() const { return wam.row_begin(); }
	transition_const_iterator transition_end() const { return wam.row_end(); }
	//! Weighted automata output.
	template <typename T> friend std::ostream& operator<<(std::ostream& aStream, const WeightedAutomata<T>& wa);
};

template <typename CntT>
bool WeightedAutomata<CntT>::setStart(State state)
{
	if(states.find(state) != states.end())
	{
		start_states.insert(state);
		return true;
	}
	else return false;
}

template <typename CntT>
bool WeightedAutomata<CntT>::setFinal(State state)
{
	if(states.find(state) != states.end())
	{
		final_states.insert(state);
		return true;
	}
	else return false;
}

template <typename CntT>
bool WeightedAutomata<CntT>::addTransition(State source, State target, const std::pair<char, CntT>& aa_w)
{
	const TranStates& t = std::make_pair(source, target);
	if(wam.exists(t, aa_w.first))
		if(is_set(t, aa_w.first)) return false;
	is_set(t, aa_w.first) = true;
	wam(t, aa_w.first) = aa_w.second;
	states.insert(source);
	states.insert(target);

	return true;
}

template <typename CntT>
bool WeightedAutomata<CntT>::addTransitions(State source, State target, const std::map<char, CntT>& aa_ws)
{
	bool ret = true;
	const TranStates& t = std::make_pair(source, target);
	typename std::map<char, CntT>::const_iterator it;
	for(it = aa_ws.begin(); it != aa_ws.end(); ++it)
	{
		const char aa = (*it).first;
		const double weight = (*it).second;
		if(wam.exists(t, aa))
			if(is_set(t, aa)) { ret = false; continue; }
		is_set(t, aa) = true;
		wam(t, aa) = weight;
	}
	states.insert(source);
	states.insert(target);

	return ret;
}

template <typename CntT>
std::map<char, CntT> WeightedAutomata<CntT>::getTransitions(State source, State target) const
{
	const TranStates& t = std::make_pair(source, target);
	std::map<char, CntT> ret = wam.row(t);
	// clean the unset amino acids
	for(Matrix<TranStates, char, bool>::col_const_iterator it = is_set.col_begin(); it != is_set.col_end(); ++it)
	{
		const char aa = (*it).first;
		if(!is_set(t, aa)) ret.erase(aa);
	}

	return ret;
}

template <typename CntT>
typename WeightedAutomata<CntT>::StateSet WeightedAutomata<CntT>::getTransitionsWithSource(State source) const
{
	StateSet ret;
	for(transition_const_iterator it = transition_begin(); it != transition_end(); ++it)
	{
		const TranStates& t = (*it).first;
		if(t.first == source) ret.insert(t.second);
	}
	return ret;
}

template <typename CntT>
typename WeightedAutomata<CntT>::StateSet WeightedAutomata<CntT>::getTransitionsWithTarget(State target) const
{
	StateSet ret;
	for(transition_const_iterator it = transition_begin(); it != transition_end(); ++it)
	{
		const TranStates& t = (*it).first;
		if(t.second == target) ret.insert(t.first);
	}
	return ret;
}

template <typename CntT>
std::map<typename WeightedAutomata<CntT>::TranStates, CntT> WeightedAutomata<CntT>::getTransitions(char aa) const
{
	std::map<typename WeightedAutomata<CntT>::TranStates, CntT> ret = wam.col(aa);
	for(Matrix<TranStates, char, bool>::row_const_iterator it = is_set.row_begin(); it != is_set.row_end(); ++it)
	{
		const TranStates t = (*it).first;
		if(!is_set(t, aa)) ret.erase(t);
	}

	return ret;
}

template <typename CntT>
std::ostream& operator<<(std::ostream& aStream, const WeightedAutomata<CntT>& wa)
{
	aStream << "WA begin" << std::endl;
	// stats
	aStream << wa.wam.nrows() << " transitions" << std::endl;
	aStream << wa.states.size() << " states" << std::endl;
	std::set<unsigned int>::const_iterator it;
	aStream << wa.start_states.size() << " initial state(s) :";
	for(it = wa.start_states.begin(); it != wa.start_states.end(); it++)
		aStream << " " << (*it - 1);
	aStream << std::endl;
	aStream << wa.final_states.size() << " final state(s) :";
	for(it = wa.final_states.begin(); it != wa.final_states.end(); it++)
		aStream << " " << (*it - 1);
	aStream << std::endl;
	// unused options kept to avoid parser problems
	aStream << "default threshold 0" << std::endl;
	aStream << "optimisation : max" << std::endl;

	// define the order of amino acids for wascan (wascan assumes an amino acid order in the matrix)
	std::vector<char> aa; aa.resize(20);
	aa[0] = 'A'; aa[1] = 'R'; aa[2] = 'N'; aa[3] = 'D'; aa[4] = 'C'; aa[5] = 'Q'; aa[6] = 'E'; aa[7] = 'G';
	aa[8] = 'H'; aa[9] = 'I'; aa[10] = 'L'; aa[11] = 'K'; aa[12] = 'M'; aa[13] = 'F'; aa[14] = 'P'; aa[15] = 'S';
	aa[16] = 'T'; aa[17] = 'W'; aa[18] = 'Y'; aa[19] = 'V';

	// matrix
	aStream << std::endl << "   ->   ";
	for(unsigned int i = 0; i < 20; i++)
		aStream << "   " << aa[i];
	aStream << std::endl << std::endl;
	typename Matrix<typename WeightedAutomata<CntT>::TranStates, char, CntT>::row_const_iterator mit;
	for(mit = wa.wam.row_begin(); mit != wa.wam.row_end(); mit++)
	{
		typename WeightedAutomata<CntT>::TranStates t = (*mit).first;
		aStream << "   " << (t.first - 1) << "   " << (t.second - 1) << "  ";
		typename std::map<char, CntT>::iterator rit;
		std::map<char, CntT> cols = wa.wam.row(t);
		// loop through the amino acids in the order defined above
		aStream << "   " << cols['A'] << "   " << cols['R'] << "   " << cols['N'] << "   " << cols['D'];
		aStream << "   " << cols['C'] << "   " << cols['Q'] << "   " << cols['E'] << "   " << cols['G'];
		aStream << "   " << cols['H'] << "   " << cols['I'] << "   " << cols['L'] << "   " << cols['K'];
		aStream << "   " << cols['M'] << "   " << cols['F'] << "   " << cols['P'] << "   " << cols['S'];
		aStream << "   " << cols['T'] << "   " << cols['W'] << "   " << cols['Y'] << "   " << cols['V'];
		// added for wascan compatibility
		aStream << "  0   0";
		aStream << std::endl;
	}
	aStream << "WA end" << std::endl;
	return aStream;

/*
	saved for future use; will replace the one above
	// matrix
	aStream << endl << "->\t";
	Weights i = (*(wam.begin())).second;
	Weights::const_iterator wit;
	for(wit = i.begin(); wit != i.end(); wit++)
		aStream << "\t" << (*wit).first;
	aStream << endl;
	Matrix::const_iterator mit;
	for(mit = wam.begin(); mit != wam.end(); mit++)
	{
		Transition t = (*mit).first;
		aStream << t.first << "\t" << t.second;
		Weights w = (*mit).second;
		for(wit = w.begin(); wit != w.end(); wit++)
			aStream << "\t" << (*wit).second;
		aStream << endl;
	}
	aStream << "WA end" << endl;
	return aStream;
*/
}

#endif
