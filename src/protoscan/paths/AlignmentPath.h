
#ifndef ALIGNMENTPATH_H
#define ALIGNMENTPATH_H

#include <map>
#include "Sequence.h"

class AlignmentPath
{
  public:
	typedef unsigned int ZoneID;
	typedef unsigned int ColumnID;
	typedef std::pair<ZoneID, ColumnID> ZoneColumnPair;
	typedef std::map<unsigned int, ZoneColumnPair> PositionZoneColumnPath;

  private:
	PositionZoneColumnPath m_paths;

  public:
	AlignmentPath() {}

	bool empty() const { return m_paths.empty(); }
	void add(unsigned int position, const ZoneColumnPair& zc_pair) { m_paths[position] = zc_pair; }

	bool aligned(unsigned int position) const { return (m_paths.find(position) != m_paths.end()); }
	ZoneColumnPair operator[](unsigned int position) const { return (*(m_paths.find(position))).second; }

	typedef PositionZoneColumnPath::iterator iterator;
	iterator begin() { return m_paths.begin(); }
	iterator end() { return m_paths.end(); }

	typedef PositionZoneColumnPath::const_iterator const_iterator;
	const_iterator begin() const { return m_paths.begin(); }
	const_iterator end() const { return m_paths.end(); }

	typedef PositionZoneColumnPath::reverse_iterator reverse_iterator;
	reverse_iterator rbegin() { return m_paths.rbegin(); }
	reverse_iterator rend() { return m_paths.rend(); }

	typedef PositionZoneColumnPath::const_reverse_iterator const_reverse_iterator;
	const_reverse_iterator rbegin() const { return m_paths.rbegin(); }
	const_reverse_iterator rend() const { return m_paths.rend(); }

	friend std::ostream& operator<<(std::ostream& aStream, const AlignmentPath& path)
	{
		for(AlignmentPath::const_iterator it = path.begin(); it != path.end(); ++it)
		{
			const unsigned int position = (*it).first;
			const ZoneColumnPair& zc = (*it).second;
			aStream << position << "=(" << zc.first << ", " << zc.second << ") ";
		}
		return aStream;
	}

	friend std::ostream& operator<<(std::ostream& aStream, const std::pair<Sequence, AlignmentPath>& p)
	{
		const Sequence& sequence = p.first;
		const AlignmentPath& path = p.second;

		if(path.empty()) return (aStream << "-");

		std::pair<int, int> gap_range = std::make_pair(0, 0);
		for(unsigned int position = 0; position < sequence.length(); position++)
		{
			if(path.aligned(position))
			{
				gap_range = std::make_pair(gap_range.first + 1, position);
				if(gap_range.second - gap_range.first > 1)
					aStream << gap_range.first << ".." << gap_range.second << " ";
				else if(gap_range.second - gap_range.first == 1)
					aStream << gap_range.first << " ";

				const ZoneColumnPair& zc = path[position];
				aStream << "(" << zc.first << ", " << zc.second << ") ";
				gap_range = std::make_pair(position + 1, gap_range.second);
			}
		}
		gap_range = std::make_pair(gap_range.first + 1, sequence.length());
		if(gap_range.second - gap_range.first > 1)
			aStream << gap_range.first << ".." << gap_range.second << " ";
		else if(gap_range.second - gap_range.first == 1)
			aStream << gap_range.first << " ";

		return aStream;
	}
};

#endif
