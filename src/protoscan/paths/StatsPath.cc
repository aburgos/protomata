
#include "StatsPath.h"

bool StatsPath::operator<(const StatsPath& p) const
{
	if(m_mean < p.m_mean) return true;
	if(m_mean > p.m_mean) return false;
	if(m_var < p.m_var) return true;
	if(m_var > p.m_var) return false;
	if(m_zones < p.m_zones) return true;
	if(m_zones > p.m_zones) return false;
	if(m_zones_length < p.m_zones_length) return true;
	if(m_zones_length > p.m_zones_length) return false;
	if(m_transitions_cost < p.m_transitions_cost) return true;
	if(m_transitions_cost > p.m_transitions_cost) return false;
	return false;
}

void StatsPath::add(unsigned int bl, double mean, double var, double tc)
{
	m_zones++;
	m_zones_length += bl;
	m_mean += mean;
	m_var += var;
	m_transitions_cost += tc;
}
