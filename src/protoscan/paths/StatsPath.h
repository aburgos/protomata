
#ifndef STATSPATH_H
#define STATSPATH_H

#include <iostream>

class StatsPath
{
  private:
	double m_mean;
	double m_var;
	unsigned int m_zones;
	unsigned int m_zones_length;
	double m_transitions_cost;

  public:
	StatsPath(): m_mean(0), m_var(0), m_zones(0), m_zones_length(0), m_transitions_cost(0) {}
	StatsPath(double mean, double var, unsigned int b, unsigned int bl, double tc): m_mean(mean), m_var(var), m_zones(b), m_zones_length(bl), m_transitions_cost(tc) {}

	double mean() const { return m_mean; }
	double var() const { return m_var; }
	unsigned int zones() const { return m_zones; }
	unsigned int zones_length() const { return m_zones_length; }
	double transitions_cost() const { return m_transitions_cost; }

	bool operator<(const StatsPath& p) const;
	void add(unsigned int, double, double, double);
};

#endif
