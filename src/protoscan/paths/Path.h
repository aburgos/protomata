
#ifndef PATH_H
#define PATH_H

#include <vector>

class Path
{
  public:
	typedef unsigned int ZoneID;

  private:
	std::vector<ZoneID> m_path;

  public:
	Path() {}

	bool empty() const { return m_path.empty(); }
	unsigned int size() const { return m_path.size(); }
	void add(ZoneID zone) { m_path.push_back(zone); }

	typedef std::vector<ZoneID>::iterator iterator;
	iterator begin() { return m_path.begin(); }
	iterator end() { return m_path.end(); }

	typedef std::vector<ZoneID>::const_iterator const_iterator;
	const_iterator begin() const { return m_path.begin(); }
	const_iterator end() const { return m_path.end(); }

	typedef std::vector<ZoneID>::reverse_iterator reverse_iterator;
	reverse_iterator rbegin() { return m_path.rbegin(); }
	reverse_iterator rend() { return m_path.rend(); }

	typedef std::vector<ZoneID>::const_reverse_iterator const_reverse_iterator;
	const_reverse_iterator rbegin() const { return m_path.rbegin(); }
	const_reverse_iterator rend() const { return m_path.rend(); }

	friend std::ostream& operator<<(std::ostream& aStream, const Path& path)
	{
		aStream << "Zones: (";
		for(Path::const_iterator it = path.begin(); it != path.end(); ++it)
			aStream << *it << ", ";
		aStream << ")";
		return aStream;
	}
};

#endif
