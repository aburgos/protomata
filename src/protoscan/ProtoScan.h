
#ifndef PROTOSCAN_H
#define PROTOSCAN_H

#include "WAGenerator.h"
#include "MultiSequence.h"
#include "Significativity.h"
#include "ProtoscanParameters.h"
#include "ProtoParser.h"
#include "ProtoGraph.h"
#include "WeightedAutomata.h"
#include "StatsPathGenerator.h"
#include "editors/ProtoGraphEditor.h"
#include "XmlPathAggregator.h"
#include "ScoreFactory.h"
#include "translation/WATranslator.h"

template <typename CntT>
class ProtoScan
{
  private:
	typedef Significativity::StatsPathVector StatsPathVector;

  public:
	ProtoScan() {}
	ProtoScan(const ProtoscanParameters&);

	//! Outputs the computed weighted automata (in a matrix) to the file specified.
	void output_wa(const std::string&, const WeightedAutomata<CntT>&);
	void scan(const WeightedAutomata<CntT>&, const ProtoscanParameters&);
	void scan(const WeightedAutomata<CntT>&, const ProtoscanParameters&, const StatsPathVector&);
};

template <typename CntT>
ProtoScan<CntT>::ProtoScan(const ProtoscanParameters& params)
{
	typedef ProtoParser<CntT, AttributedColumn> ProtoParserType;
	typedef typename ProtoParserType::ProtoGraphType ProtoGraphType;
	typedef typename ProtoParserType::StatsPathVector StatsPathVector;

	// parse proto file
	ProtoParserType pp(params.input());
	ProtoGraphType pg = pp.parse_zones_edges();

	ProtoGraphEditor<ProtoGraphType> pg_editor;

	// apply pseudocounts
	if(!params.pseudocounts().empty())
		pg_editor.add_pseudocounts(pg, params.pseudocounts(), params.components_file());

	// compute the right output score
	pg_editor.set_viterbi_values(pg, params.scan_score());

	// create a weighted automata
	WAGenerator<ProtoGraphType> wagen(pg, params.exceptions_as_gaps());
	WAProperties<CntT> weighted_automata = wagen.weighted_automata();
	if(params.scan_dna())
	{
		WATranslator<CntT> translator;
		weighted_automata = translator.toDNA(weighted_automata);
	}
	if(params.output_wa()) output_wa(params.wa_file(), weighted_automata);

	// scan sequences
	if(params.significativity())
	{
		// check if paths were already computed
		StatsPathVector paths = pp.parse_paths();
		if(paths.empty())
		{
			// compute all paths and add them to the XML file for future use
			StatsPathGenerator<ProtoGraphType> path_generator(pg);
			paths = path_generator.paths_copy();
			XmlPathAggregator agg(params.input(), paths);
		}
		scan(weighted_automata, params, paths);
	}
	else scan(weighted_automata, params);
}

template <>
ProtoScan<int>::ProtoScan(const ProtoscanParameters& params)
{
	typedef ProtoParser<int, AttributedColumn> ProtoParserType;
	typedef ProtoParserType::ProtoGraphType ProtoGraphType;

	// parse proto file
	ProtoParserType pp(params.input());
	ProtoGraphType pg = pp.parse_zones_edges();

	// compute the right output score
	ProtoGraphEditor<ProtoGraphType> pg_editor;
	pg_editor.set_viterbi_values(pg, params.scan_score());

	// create a weighted automata
	WAGenerator<ProtoGraphType> wagen(pg, params.exceptions_as_gaps());
	WAProperties<int> weighted_automata = wagen.weighted_automata();
	if(params.scan_dna())
	{
		WATranslator<int> translator;
		weighted_automata = translator.toDNA(weighted_automata);
	}
	if(params.output_wa()) output_wa(params.wa_file(), weighted_automata);

	// scan sequences
	scan(weighted_automata, params);
}

template <typename CntT>
void ProtoScan<CntT>::output_wa(const std::string& wa_file, const WeightedAutomata<CntT>& wa)
{
	std::ofstream output(wa_file.c_str(), std::ios_base::out);
	output << wa << std::endl;
	output.close();
}

template <typename CntT>
void ProtoScan<CntT>::scan(const WeightedAutomata<CntT>& wa, const ProtoscanParameters& parameters)
{
	const bool allow_X = parameters.allow_X();
	const bool apsm = parameters.allow_partial_sequence_match();
	const bool apmm = parameters.allow_partial_model_match();
	const double score_threshold = parameters.score_threshold();
	const bool compute_exponential = (parameters.scan_score() == probability) || (parameters.scan_score() == LR);
	const unsigned int lobound = parameters.lobound();
	const unsigned int upbound = parameters.upbound();

	// get sequences to scan
	const MultiSequence ms(parameters.sequences(), false, allow_X, lobound, upbound);
	// get transitions from the weighted automata and "modify" the automata for scanning requirements
	ScoreFactory<CntT> sfact;
	Scan<CntT>* sc = sfact.create(wa, apsm, apmm, allow_X, parameters.forward(), parameters.x_strategy());

	std::ofstream output(parameters.output().c_str());
	std::ostream& output_scores = (output.is_open() ? output : std::cout);

	output_scores << "# Generated using protoscan version " << VERSION << std::endl;
	output_scores << "#" << std::endl;
	output_scores << "# score" << "\t" << "significativity" << "\t" << "seq_length" << "\t" << "seq_tag" << std::endl;
	output_scores << "#" << std::endl;

	// scan each sequence of the sequences file
	for(MultiSequence::const_iterator it = ms.begin(); it != ms.end(); ++it)
	{
		const Sequence& seq = *it;

		//double seq_scan_start, seq_scan_end;
		//seq_scan_start = (double)clock();

		CntT seq_score = sc->score(seq);
		if(compute_exponential) seq_score = exp(seq_score);

		if(seq_score >= score_threshold)
		{
			output_scores << seq_score;
			output_scores << "\t-";
			output_scores << "\t(" << seq.length() << ")";
			output_scores << "\t" << seq.long_tag() << std::endl;
		}

		//seq_scan_end = (double)clock();
		//double seq_scan_time_elapsed = double(seq_scan_end - seq_scan_start) / CLOCKS_PER_SEC;
		//output_scores << "\t\t" << seq_scan_time_elapsed << " secs" << std::endl;
	}

	delete sc;
}

template <typename CntT>
void ProtoScan<CntT>::scan(const WeightedAutomata<CntT>& wa, const ProtoscanParameters& parameters, const StatsPathVector& paths)
{
	const bool allow_X = parameters.allow_X();
	const bool apsm = parameters.allow_partial_sequence_match();
	const bool apmm = parameters.allow_partial_model_match();
	const double score_threshold = parameters.score_threshold();
	const bool compute_exponential = (parameters.scan_score() == probability) || (parameters.scan_score() == LR);
	const unsigned int lobound = parameters.lobound();
	const unsigned int upbound = parameters.upbound();

	// get sequences to scan
	const MultiSequence ms(parameters.sequences(), false, allow_X, lobound, upbound);
	// get transitions from the weighted automata and "modify" the automata for scanning requirements
	ScoreFactory<CntT> sfact;
	Scan<CntT>* sc = sfact.create(wa, apsm, apmm, allow_X, parameters.forward(), parameters.x_strategy());

	std::ofstream output(parameters.output().c_str());
	std::ostream& output_scores = (output.is_open() ? output : std::cout);

	output_scores << "# Generated using protoscan version " << VERSION << std::endl;
	output_scores << "#" << std::endl;
	output_scores << "# score" << "\t" << "significativity" << "\t" << "seq_length" << "\t" << "seq_tag" << std::endl;
	output_scores << "#" << std::endl;

	// create the object for computing significativity
	Significativity sig_calc;

	// scan each sequence of the sequences file
	for(MultiSequence::const_iterator it = ms.begin(); it != ms.end(); ++it)
	{
		const Sequence& seq = *it;

		//double seq_scan_start, seq_scan_end;
		//seq_scan_start = (double)clock();

		CntT seq_score = sc->score(seq);
		if(compute_exponential) seq_score = exp(seq_score);

		if(seq_score >= score_threshold)
		{
			output_scores << seq_score;
			output_scores << "\t" << sig_calc.significativity(seq, seq_score, paths);
			output_scores << "\t(" << seq.length() << ")";
			output_scores << "\t" << seq.long_tag() << std::endl;
		}

		//seq_scan_end = (double)clock();
		//double seq_scan_time_elapsed = double(seq_scan_end - seq_scan_start) / CLOCKS_PER_SEC;
		//output_scores << "\t\t" << seq_scan_time_elapsed << " secs" << std::endl;
	}

	delete sc;
}

#endif
