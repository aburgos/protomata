
#ifndef SIGNIFICATIVITY_H
#define SIGNIFICATIVITY_H

#include "paths/StatsPath.h"
#include "gsl_integration.h"

class Significativity
{
  public:
	typedef std::vector<StatsPath> StatsPathVector;

  private:
	//! Combinations of n taken by k.
	double C(int n, int k) const;
	//! Computes the normal distribution of a value using the GSL library.
	double ndtr(double) const;

  public:
	Significativity() {}

	double significativity(const Sequence& seq, double score, const StatsPathVector& paths) const;
};

double Significativity::significativity(const Sequence& seq, double score, const StatsPathVector& paths) const
{
/*
	// computed as in vincent work
	double mult = 1;

	for(set<StatsPath>::const_iterator it = paths.begin(); it != paths.end(); it++)
	{
		StatsPath p = *it;
		if(p.zones_length == 0) continue;
		double Ai = C(seq.length() - p.zones_length + p.zones, seq.length() - p.zones_length);
		double seq_score = 0;
		if(scores.find(seq) != scores.end()) seq_score = (*(scores.find(seq))).second;
		//double arg = (seq_score - p.mean - p.transitions_cost) / sqrt(p.var);
		double arg = (seq_score - p.mean) / sqrt(p.var);
		cout.precision(20);
		cout << "arg " << arg << "\t";
		cout << "ndtr = " << ndtr(arg) << "\t";
		cout << "Ai = " << Ai << "\t";
		cout << "ndtr ^ Ai = " << pow(ndtr(arg), Ai) << "\t";
		mult *= pow(ndtr(arg), Ai);
		cout << "mult = " << mult << endl;
	}
	cout << endl;
	return (1 - mult);
*/
	// computed as in vincent code
	double sum = 0;

	for(StatsPathVector::const_iterator it = paths.begin(); it != paths.end(); ++it)
	{
		const StatsPath& p = *it;
		if(p.zones_length() == 0) continue;
		double Ai = C(seq.length() - p.zones_length() + p.zones(), seq.length() - p.zones_length());
		double arg = (score - p.mean()) / sqrt(p.var());
		double ndtr_res = ndtr(arg);
		// scipy.special.ndtr max value is 1, but not here
		if(ndtr_res <= 1) sum += Ai * log(ndtr_res);
	}

	return (1 - exp(sum));
}

double Significativity::C(int n, int k) const
{
	if(n < 0 || k < 0) return 0;
	if(k > n) return 0;

	double result = 1;

	for(double i = n, j = k; j > 0; i--, j--)
		result *= (i / j);

	return result;
}

double G(double x, void*)
{
	return ((exp(-(pow(x, 2) / 2))) / (sqrt(2 * M_PI)));
	//return (exp(-(pow(x, 2) / 2)));
}

double Significativity::ndtr(double x) const
{
	gsl_integration_workspace* w = gsl_integration_workspace_alloc(1000);

	gsl_function F;
	F.function = &G;
	F.params = &x;

	double result, error;
	gsl_integration_qagil(&F, x, 1e-15, 1e-13, 1000, w, &result, &error);
	//result = result / sqrt(2 * M_PI);

	gsl_integration_workspace_free(w);

	return result;
}

#endif
