
#ifndef WAGENERATOR_H
#define WAGENERATOR_H

#include <cstdlib>
#include <fstream>
#include "AminoAcids.h"
#include "WAProperties.h"
#include "ProtoGraph.h"
#include "zones/BeginZone.h"
#include "zones/EndZone.h"

template <typename ProtoGraphType>
class WAGenerator
{
  public:
	typedef unsigned int State;
	typedef std::pair<unsigned int, unsigned int> ZoneColumnPair;
	typedef typename ProtoGraphType::CountType CountType;
	typedef typename ProtoGraphType::ZoneType ZoneType;
	typedef typename ProtoGraphType::TransitionType TransitionType;

  private:
	WAProperties<CountType> wa;
	//! Correspondance of states with a proto zone and its column.
	std::map<State, ZoneColumnPair> state2zonecol;
	//! Correspondance of states with the transition type.
	std::map<State, std::string> transition_type;

  public:
	WAGenerator(const ProtoGraphType&, bool = false);

	//! Returns the constructed weighted automata.
	WAProperties<CountType> weighted_automata() const { return wa; }
};

template <typename ProtoGraphType>
WAGenerator<ProtoGraphType>::WAGenerator(const ProtoGraphType& pg, bool exceptions_as_gaps)
{
	typedef std::map<char, CountType> CountsMap;

	// store the border states of each zone
	std::map<unsigned int, std::pair<State, State> > zone_borders;
	// store the last column of a zone
	std::map<unsigned int, CountsMap> last_columns;

	const unsigned int begin = BeginZone<void, Column>::begin_zone_id;
	const unsigned int end = EndZone<void, Column>::end_zone_id;

	// store the set of amino acids used for adding gaps later
	CountsMap amino_acids_zero;
	// set every amino acid to 0
	const AminoAcids& aa = AminoAcids::instance();
	for(AminoAcids::const_iterator it = aa.begin(); it != aa.end(); ++it)
		amino_acids_zero[*it] = 0;

	// set initial and end zone borders
	zone_borders[begin] = std::make_pair(begin, begin);
	zone_borders[end] = std::make_pair(end, end);

	// initial and end zones are fixed
	state2zonecol[begin] = std::make_pair(begin, begin);
	state2zonecol[end] = std::make_pair(end, begin);

	// initialize the states to be used for the automata
	State source = end + 1;
	State target = end + 2;

	// for each zone of the proto graph add its "internal" transitions
	for(typename ProtoGraphType::zone_const_iterator zit = pg.zone_begin(); zit != pg.zone_end(); ++zit)
	{
		const ZoneType& zone = *zit;
		const unsigned int zone_id = zone.id();

		// iterate through each column of the zone
		for(typename ZoneType::column_const_iterator cit = zone.columns_begin(); cit != zone.columns_end(); ++cit)
		{
			// stores the attributes of each amino acid
			const typename ProtoGraphType::ColumnType& column = *cit;

			// for borders of each block, save its states
			if(cit == zone.columns_begin()) zone_borders[zone_id] = std::make_pair(source, begin);
			typename ZoneType::column_const_iterator last = --(zone.columns_end());
			if(cit == last)
			{
				zone_borders[zone_id] = std::make_pair(zone_borders[zone_id].first, target - 1);
				last_columns.insert(std::make_pair(zone_id, column.attribute("viterbi")));
			}
			else
			{
				if(!wa.addTransitions(source, target, column.attribute("viterbi")))
				{
					std::cout << "WAGenerator: error adding transitions (00)." << std::endl;
					exit(1);
				}
			}

			// save zone's id and column
			unsigned int column_id = column.id();
			wa.zone_column(source, std::make_pair(zone_id, column_id));

			source++; target++;
		}
	}

	State first_unused_state = source;

	// iterate through each edge of the protograph
	for(typename ProtoGraphType::relation_const_iterator eit = pg.relation_begin(); eit != pg.relation_end(); ++eit)
	{
		const TransitionType& transition = (*eit).value();

		//unsigned int edge_id = transition.id();
		const unsigned int source = transition.source_id();
		const unsigned int target = transition.target_id();
		const std::string& type = transition.type();

		// add transitions
		if((type == "Gap") || (type == "Exception" && exceptions_as_gaps))
		{
			// 0: transition size 0
			// 1: transition size 1
			// 2: transition size 2 or more
			unsigned int transition_type[3];
			for(unsigned int i = 0; i < 3; i++) transition_type[i] = 0;

			typename TransitionType::subseq_const_iterator sit;
			for(sit = transition.subseqs_begin(); sit != transition.subseqs_end(); ++sit)
			{
				const std::string& str = (*sit).first;
				if(str.length() > 1) transition_type[2]++;
				else transition_type[str.length()]++;
			}

			if(transition_type[0] > 0)
			{
				if(source == begin)
				{
					/*
					// this solution would be better with automatas allowing several transitions for the same label
					// between two same states

					// for each source state from where we can reach the target state, add a transition from
					// the begin state to every one of those states with the (source, target) label
					std::set<State> target_states = wa.getTransitionsWithSource(target);
					for(std::set<State>::const_iterator it = target_states.begin(); it != target_states.end(); ++it)
					{
						const State& trgt = *it;
						if(!wa.addTransitions(zone_borders[begin].second, zone_borders[trgt].first, last_columns[source]))
						{
							std::cout << "WAGenerator: error adding transitions (00)." << std::endl;
							exit(1);
						}
					}
					*/
					wa.setStart(zone_borders[target].first);
				}
				else
				{
					// a transition from a zone to another
					if(!wa.addTransitions(zone_borders[source].second, zone_borders[target].first, last_columns[source]))
					{
						std::cout << "WAGenerator: error adding transitions (01)." << std::endl;
						exit(1);
					}
				}
			}
			if(transition_type[1] > 0 && transition_type[2] == 0)
			{
				if(source != begin)
				{
					// a transition from a zone to a gap state
					if(!wa.addTransitions(zone_borders[source].second, first_unused_state, last_columns[source]))
					{
						std::cout << "WAGenerator: error adding transitions (02)." << std::endl;
						exit(1);
					}
					// a sigma transition from a gap state to a zone
					if(!wa.addTransitions(first_unused_state, zone_borders[target].first, amino_acids_zero))
					{
						std::cout << "WAGenerator: error adding transitions (03)." << std::endl;
						exit(1);
					}
					wa.transition_type(first_unused_state, type);

					first_unused_state++;
				}
				else
				{
					// a sigma transition from the begin zone to other zone
					if(!wa.addTransitions(zone_borders[begin].second, zone_borders[target].first, amino_acids_zero))
					{
						std::cout << "WAGenerator: error adding transitions (04)." << std::endl;
						exit(1);
					}
					//wa.setStart(first_unused_state);
				}
			}
			if(transition_type[2] > 0)
			{
				if(source == begin)
				{
					// this state may already have sigma transitions, no checks here
					// a loop sigma transition
					wa.addTransitions(zone_borders[source].second, zone_borders[source].second, amino_acids_zero);
					// a sigma transition from a zone to another
					if(!wa.addTransitions(zone_borders[source].second, zone_borders[target].first, amino_acids_zero))
					{
						std::cout << "WAGenerator: error adding transitions (05)." << std::endl;
						exit(1);
					}
					wa.transition_type(zone_borders[source].second, type);
				}
				else
				{
					// a transition from a zone to a gap state
					if(!wa.addTransitions(zone_borders[source].second, first_unused_state, last_columns[source]))
					{
						std::cout << "WAGenerator: error adding transitions (06)." << std::endl;
						exit(1);
					}
					// a sigma transition for a gap state
					if(!wa.addTransitions(first_unused_state, first_unused_state, amino_acids_zero))
					{
						std::cout << "WAGenerator: error adding transitions (07)." << std::endl;
						exit(1);
					}
					// a sigma transition from a gap state to a zone
					if(!wa.addTransitions(first_unused_state, zone_borders[target].first, amino_acids_zero))
					{
						std::cout << "WAGenerator: error adding transitions (08)." << std::endl;
						exit(1);
					}
					wa.transition_type(first_unused_state, type);

					first_unused_state++;
				}
			}
		}
	}

	// set start and final states as defined by the proto file
	wa.setStart(zone_borders[begin].first);
	wa.setFinal(zone_borders[end].first);
}

#endif
