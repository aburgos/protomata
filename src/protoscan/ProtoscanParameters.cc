
#include "ProtoscanParameters.h"

ProtoscanParameters::ProtoscanParameters():
	m_protoscan_input(TCLAP::ValueArg<std::string>("i", "input", "Set proto file name.", true, "", "proto_filename")),
	m_protoscan_output(TCLAP::ValueArg<std::string>("o", "output", "Set output file name for scan results.", false, "", "text_filename")),
	m_protoscan_tag(TCLAP::ValueArg<std::string>("", "tag", "Add a tag to the end of the basename.", false, "", "string")),
	m_sequences(TCLAP::ValueArg<std::string>("s", "sequences", "Set of sequences to scan.", true, "", "fasta_filename")),
	m_sscr(TCLAP::ValueArg<std::string>("", "scan-score", "Output score when using Viterbi (default) values.", false, "logLR", protoscan_allowed.scan_scores_constraints())),
	m_score_threshold(TCLAP::ValueArg<double>("", "min-sequence-score", "Output only the scores of the scanned sequences greater or equal than this value.", false, -std::numeric_limits<double>::infinity(), "value")),
	m_lobound(TCLAP::ValueArg<unsigned int>("", "min-sequence-length", "Sequences with length lower or equal than this value won't be scanned.", false, std::numeric_limits<unsigned int>::min(), "value")),
	m_upbound(TCLAP::ValueArg<unsigned int>("", "max-sequence-length", "Sequences with length greater or equal than this value won't be scanned.", false, std::numeric_limits<unsigned int>::max(), "value")),
	m_pseudocounts(TCLAP::ValueArg<std::string>("", "pseudocounts", "Strategy for adding pseudocounts to each amino acid column for scanning sequences.", false, "", protobuild_allowed.pseudocounts_values_constraints())),
	m_components(TCLAP::ValueArg<std::string>("", "components", "The components file to be used with Dirichlet pseudocounts.", false, "", "components_file")),
	m_xstr(TCLAP::ValueArg<std::string>("", "X-strategy", "Assign strategy values relative to columns when the scanned amino acid is an X.", false, "minVal", protoscan_allowed.strategies_values_constraints())),
	m_disallow_X(TCLAP::SwitchArg("", "no-X", "Do not scan sequences containing at least one X.", false)),
	m_exceptions_as_gaps(TCLAP::SwitchArg("e", "exceptions-as-gaps", "Exceptions edges of the protomata will be considered as gaps.", false)),
	m_disallow_partial_sequence_match(TCLAP::SwitchArg("", "disallow-partial-sequence-match", "Do not allow subsequences to be accepted by the protomata. By default subsequences are accepted.", false)),
	m_allow_partial_model_match(TCLAP::SwitchArg("", "allow-partial-model-match", "Shorter sequences will be accepted by the protomata. Sequences can start and end anywhere in it.", false)),
	m_use_integers(TCLAP::SwitchArg("", "use-integers", "The weights of each column in the protomata will be rounded to an integer.", false)),
	m_significativity(TCLAP::SwitchArg("", "significativity", "Compute the significativity score for each scanned sequence.", false)),
	m_output_wa(TCLAP::SwitchArg("", "output-wa", "Outputs a transition matrix corresponding to the proto file. It forces the flag --use-integers in order to be compatible with wascan. Outputs to proto_basename.wa.", false)),
	m_forward(TCLAP::SwitchArg("f", "forward", "Computes forward scores.", false)),
	m_scan_dna(TCLAP::SwitchArg("", "scan-dna", "Scan DNA sequences.", false))
{}

void ProtoscanParameters::parse(int argc, char **argv)
{
	TCLAP::CmdLine cmd("Scan a set of sequences for scores using a Protomata model.", ' ', VERSION);
	ProtoscanParameters::add_arguments(cmd);
	cmd.parse(argc, argv);
	ProtoscanParameters::initialize_values();
	ProtoscanParameters::sanity_checks();
	ProtoscanParameters::add_string_parameters();
}

void ProtoscanParameters::add_arguments(TCLAP::CmdLine& cmd)
{
	cmd.add(m_forward);
	cmd.add(m_sscr);
	cmd.add(m_output_wa);
	cmd.add(m_significativity);
	cmd.add(m_use_integers);
	cmd.add(m_allow_partial_model_match);
	cmd.add(m_disallow_partial_sequence_match);
	cmd.add(m_exceptions_as_gaps);
	cmd.add(m_disallow_X);
	cmd.add(m_xstr);
	cmd.add(m_components);
	cmd.add(m_pseudocounts);
	cmd.add(m_upbound);
	cmd.add(m_lobound);
	cmd.add(m_score_threshold);
	cmd.add(m_scan_dna);
	cmd.add(m_sequences);
	cmd.add(m_protoscan_tag);
	cmd.add(m_protoscan_output);
	cmd.add(m_protoscan_input);
}

void ProtoscanParameters::sanity_checks()
{
	if(allow_partial_model_match() && allow_partial_sequence_match())
	{
		std::cerr << "Error: Cannot use options 'partial model match' and 'partial sequence match' togheter." << std::endl;
		exit(1);
	}
	if(pseudocounts() == "Dirichlet" && components_file().empty())
	{
		std::cerr << "Error: No components file specified." << std::endl;
		exit(1);
	}
	if(output_wa() && !use_integers())
	{
		std::cerr << "Warning: Using integers in columns for wascan compatibility." << std::endl;
		//m_use_integers = true;
	}
}

void ProtoscanParameters::add_string_parameters()
{
}

void ProtoscanParameters::initialize_values()
{
	if(m_xstr.getValue() == "minVal") m_x_strategy = minVal;
	else if(m_xstr.getValue() == "maxVal") m_x_strategy = maxVal;
	else if(m_xstr.getValue() == "meanVal") m_x_strategy = meanVal;

	if(m_sscr.getValue() == "probability") m_scan_score = probability;
	else if(m_sscr.getValue() == "log") m_scan_score = logT;
	else if(m_sscr.getValue() == "LR") m_scan_score = LR;
	else if(m_sscr.getValue() == "logLR") m_scan_score = logLR;
}

std::string ProtoscanParameters::compute_output_file(const std::string& filename) const
{
	size_t lst_path_pos = filename.find_last_of('/');
	size_t beg_base_pos = (lst_path_pos == std::string::npos ? 0 : (lst_path_pos + 1));
	size_t lst_base_pos = filename.find_last_of('.');
	size_t end_base_pos = (lst_base_pos == std::string::npos ? (filename.length() - 1) : (lst_base_pos - 1));
	std::string basename = filename.substr(beg_base_pos, end_base_pos - beg_base_pos + 1);

	std::ostringstream ss;
	ss << basename;
	if(!m_protoscan_tag.getValue().empty()) ss << "_" << tag();
	if(score_threshold() != -std::numeric_limits<double>::infinity()) ss << "_mss" << score_threshold();
	if(m_lobound.isSet()) ss << "_lo" << lobound();
	if(m_upbound.isSet()) ss << "_up" << upbound();
	if(allow_X()) ss << "_Xst:" << m_xstr.getValue();
	if(!allow_X()) ss << "_noX";
	if(exceptions_as_gaps()) ss << "_eag";
	if(allow_partial_sequence_match()) ss << "_psm";
	if(allow_partial_model_match()) ss << "_pmm";
	if(use_integers()) ss << "_ui";
	if(significativity()) ss << "_sig";
	ss << "_ss:" << m_sscr.getValue();
	if(forward()) ss << "_forward";
	if(scan_dna()) ss << "_DNA";
	if(!ProtoscanParameters::pseudocounts().empty()) ss << "_" << pseudocounts();
	if(!ProtoscanParameters::components_file().empty())
	{
		size_t pos = components_file().find_last_of('/');
		std::string components_basename = components_file().substr(pos + 1, components_file().length());
		ss << "_comp:" << components_basename;
	}
	if(!sequences().empty())
	{
		size_t pos = sequences().find_last_of('/');
		std::string sequences_basename = sequences().substr(pos + 1, sequences().length());
		ss << "_s:" << sequences_basename;
	}

	ss << "_scan.txt";
	return ss.str();
}

std::string ProtoscanParameters::output() const
{
	if(m_protoscan_output.getValue().empty()) return ProtoscanParameters::compute_output_file(input());
	else return m_protoscan_output.getValue();
}

std::string ProtoscanParameters::wa_file() const
{
	const std::string str_input = input();
	return str_input.substr(0, str_input.find_last_of('.')) + ".wa";
}

std::string ProtoscanParameters::components_file() const
{
	FileNameSearcher fnc;
	return fnc.filename(m_components.getValue());
}
