
#ifndef STATSPATHGENERATOR_H
#define STATSPATHGENERATOR_H

#include <iostream>
#include <cstdlib>
#include <vector>
#include <set>
#include <math.h>
#include "PathGenerator.h"
#include "paths/StatsPath.h"
#include "ProtoGraph.h"
#include "zones/EndZone.h"
#include "zones/BeginZone.h"
#include "columns/Column.h"

template <typename ProtoGraphType>
class StatsPathGenerator
{
  public:
	typedef std::vector<StatsPath> StatsPathVector;
	typedef typename ProtoGraphType::CountType CountType;
	typedef typename ProtoGraphType::ColumnType ColumnType;
	typedef typename ProtoGraphType::ZoneType ZoneType;
	typedef typename ProtoGraphType::TransitionType TransitionType;

  private:
	StatsPathVector m_paths;

  public:
	StatsPathGenerator(const ProtoGraphType&);

	StatsPathVector paths_copy() const { return m_paths; }
	const StatsPathVector& paths() const { return m_paths; }
};

template <typename ProtoGraphType>
StatsPathGenerator<ProtoGraphType>::StatsPathGenerator(const ProtoGraphType& pg)
{
	typedef unsigned int ZoneID;
	typedef std::set<ZoneID> ZoneIDSet;

	std::map<ZoneID, unsigned int> zone_length;
	std::map<ZoneID, double> zone_mean;
	std::map<ZoneID, double> zone_var;

	// iterate through each zone
	for(typename ProtoGraphType::zone_const_iterator zit = pg.zone_begin(); zit != pg.zone_end(); ++zit)
	{
		const ZoneType& zone = *zit;

		zone_length[zone.id()] = zone.size();
		zone_mean[zone.id()] = 0;
		zone_var[zone.id()] = 0;

		// iterate through each column of the zone
		for(typename ZoneType::column_const_iterator cit = zone.columns_begin(); cit != zone.columns_end(); ++cit)
		{
			const ColumnType& column = *cit;

			// compute mean and variance of the column
			double sum_weights = 0;
			double sum_var_weights = 0;
			unsigned int letters = 0;

			// iterate through each aminoacid of the column tag node
			typename ColumnType::counts_const_iterator ait;
			for(ait = column.counts_begin(); ait != column.counts_end(); ++ait)
			{
				// internally site's sequences and positions begin at 0
				double weight = (*ait).second;
				sum_weights += weight; letters++;
				sum_var_weights += pow(weight, 2);
			}

			// compute the mean and variance of the column
			double col_mean = sum_weights / letters;
			double col_var = (sum_var_weights / letters) - pow(col_mean, 2);

			zone_mean[zone.id()] += col_mean;
			zone_var[zone.id()] += col_var;
		}
	}

	typedef DiGraph<ZoneID, typename ProtoGraphType::CountType> EdgeGraph;
	EdgeGraph edgeGraph;

	// iterate through each edge of the protograph
	for(typename ProtoGraphType::relation_const_iterator eit = pg.relation_begin(); eit != pg.relation_end(); ++eit)
	{
		const TransitionType& transition = (*eit).value();

		//unsigned int edge_id = transition.id();
		const ZoneID source = transition.source_id();
		const ZoneID target = transition.target_id();

		typename ProtoGraphType::CountType transition_weight = 0;

		// iterate through each subsequence of the transition
		typename TransitionType::subseq_const_iterator sit;
		for(sit = transition.subseqs_begin(); sit != transition.subseqs_end(); ++sit)
			transition_weight += (*sit).second;

		edgeGraph.addEdge(source, target, transition_weight);
	}

	// IDs will be checked against the built edge graph, which uses zones IDs of the protograph
	const ZoneID begin_zone_id = BeginZone<void, Column>::begin_zone_id;
	const ZoneID end_zone_id = EndZone<void, Column>::end_zone_id;

	// get all paths between the begin and the end zone
	typedef PathGenerator<ProtoGraphType> PathGeneratorType;
	PathGeneratorType all_paths_generator(pg, begin_zone_id, end_zone_id);
	const typename PathGeneratorType::PathVector& all_paths = all_paths_generator.paths();

	// compute statistics for every path
	for(typename PathGeneratorType::PathVector::const_iterator it = all_paths.begin(); it != all_paths.end(); ++it)
	{
		const Path& path = *it;
		std::cout << path << std::endl;
		StatsPath stats_path;

		Path::const_iterator pit = path.begin();
		if(pit != path.end())
		{
			stats_path.add(zone_length[*pit], zone_mean[*pit], zone_var[*pit], 0);
			ZoneID last_zone = *pit;
			++pit;
			while(pit != path.end())
			{
				const ZoneID zone = *pit;

				const typename EdgeGraph::relation_iterator rit = edgeGraph.hasEdge(last_zone, zone);
				if(rit != edgeGraph.relation_end())
				{
					const typename EdgeGraph::relation& r = *rit;
					const typename ProtoGraphType::CountType transition_weight = r.value();
					stats_path.add(zone_length[zone], zone_mean[zone], zone_var[zone], transition_weight);
					++pit;
				}
				else { std::cout << "Path relation missing" << std::endl; exit(1); }

				last_zone = zone;
			}
		}

		m_paths.push_back(stats_path);
	}
}

#endif
