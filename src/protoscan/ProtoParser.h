
#ifndef PROTOPARSER_H
#define PROTOPARSER_H

#include "pugixml.hpp"
#include "ProtoGraph.h"
#include "zones/Zone.h"
#include "transitions/StringGap.h"
#include "transitions/StringException.h"
#include "paths/StatsPath.h"

template <typename CntT, template <typename T> class ColT>
class ProtoParser
{
  public:
	typedef std::vector<StatsPath> StatsPathVector;
	typedef ProtoGraph<CntT, ColT, Zone, StringTransition<CntT> > ProtoGraphType;

  private:
	typedef typename ProtoGraphType::ZoneType ZoneType;
	typedef typename ProtoGraphType::TransitionType TransitionType;

	std::string m_input_file;
	CntT round(double value) const;

  public:
	ProtoParser(const std::string& input_file): m_input_file(input_file) {}

	ProtoGraphType parse_zones_edges();
	StatsPathVector parse_paths();
};

template <typename CntT, template <typename T> class ColT>
typename ProtoParser<CntT, ColT>::ProtoGraphType ProtoParser<CntT, ColT>::parse_zones_edges()
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(m_input_file.c_str());
    if(!result) { std::cerr << "Error loading file " << m_input_file << std::endl; exit(1); }

	pugi::xml_node zones_node = doc.child("weighted_protomata").child("zones");
	if(!zones_node) { std::cerr << "Error parsing file " << m_input_file << std::endl; exit(1); }

	ProtoGraphType pg;

	// iterate through each zone of the zones tag node
	for(pugi::xml_node_iterator zit = zones_node.begin(); zit != zones_node.end(); ++zit)
	{
		ZoneType zone(atoi(zit->attribute("id").value()));

		// iterate through each column of the zone tag node
		for(pugi::xml_node_iterator cit = (*zit).begin(); cit != (*zit).end(); ++cit)
		{
			// stores the attributes of each amino acid
			typename ProtoGraphType::ColumnType column;

			column.id(atoi(cit->attribute("id").value()));

			// iterate through each aminoacid of the column tag node
			for(pugi::xml_node_iterator ait = (*cit).begin(); ait != (*cit).end(); ++ait)
			{
				const char* letter = ait->attribute("letter").value();
				const CntT count = atof(ait->attribute("count").value());
				column.count(letter[0], round(count));
			}

			zone.add(column);
		}

		pg.add_zone(zone);
	}

	// get the edges tag
	pugi::xml_node edges_node = doc.child("weighted_protomata").child("edges");
	if(!edges_node) { std::cerr << "Error parsing file " << m_input_file << std::endl; exit(1); }

	// iterate through each edge of the edges tag node
	for(pugi::xml_node_iterator eit = edges_node.begin(); eit != edges_node.end(); ++eit)
	{
		const unsigned int edge_id = atoi(eit->attribute("id").value());
		const unsigned int source = atoi(eit->attribute("source").value());
		const unsigned int target = atoi(eit->attribute("target").value());
		const std::string& type = eit->attribute("type").value();

		// I don't like this if (factory)
		if(type == "Gap")
		{
			typename TransitionType::SubSequenceWeightMap subseqs;
			for(pugi::xml_node_iterator sit = (*eit).begin(); sit != (*eit).end(); ++sit)
			{
				const std::string& str = sit->attribute("string").value();
				const CntT weight = round(atof(eit->attribute("source").value()));
				subseqs.insert(std::make_pair(str, weight));
			}
			StringGap<CntT> zt(edge_id, source, target, subseqs);
			pg.add_transition(zt);
		}
		else if(type == "Exception")
		{
			typename TransitionType::SubSequenceWeightMap subseqs;
			for(pugi::xml_node_iterator sit = (*eit).begin(); sit != (*eit).end(); ++sit)
			{
				const std::string& str = sit->attribute("string").value();
				const CntT weight = round(atof(eit->attribute("source").value()));
				subseqs.insert(std::make_pair(str, weight));
			}
			StringException<CntT> zt(edge_id, source, target, subseqs);
			pg.add_transition(zt);
		}
	}

	return pg;
}

template <typename CntT, template <typename T> class ColT>
typename ProtoParser<CntT, ColT>::StatsPathVector ProtoParser<CntT, ColT>::parse_paths()
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(m_input_file.c_str());
    if(!result) { std::cerr << "Error loading file " << m_input_file << std::endl; exit(1); }

	StatsPathVector paths;

	// get the paths tag
	pugi::xml_node paths_node = doc.child("weighted_protomata").child("paths");
	// no paths on the XML file
	if(!paths_node) return paths;

	// iterate through each path of the paths tag node
	for(pugi::xml_node_iterator pit = paths_node.begin(); pit != paths_node.end(); ++pit)
	{
		const double mean = atof(pit->attribute("mean").value());
		const double var = atof(pit->attribute("var").value());
		const unsigned int blocks = atoi(pit->attribute("blocks").value());
		const unsigned int blocks_length = atoi(pit->attribute("blocks_length").value());
		const double transitions_cost = atof(pit->attribute("transitions_cost").value());
		StatsPath p(mean, var, blocks, blocks_length, transitions_cost);
		paths.push_back(p);
	}

	return paths;
}

template <typename CntT, template <typename T> class ColT>
CntT ProtoParser<CntT, ColT>::round(double value) const
{
	return (CntT)value;
}
/*
template <template <typename T> class ColT>
int ProtoParser<int, ColT>::round(double value) const
{
	double cl = ceil(value);
	if(value < cl - 0.5) return (int)floor(value);
	else return (int)cl;
}
*/
#endif
