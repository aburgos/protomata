
#ifndef ATTRIBUTEDSITECOLUMN_H
#define ATTRIBUTEDSITECOLUMN_H

#include "SiteColumn.h"
#include "AttributedColumn.h"

/*!
This class allows to build protographs while being able to compute pseudocounts and also other attributes.
*/
template <typename CntT>
class AttributedSiteColumn: public SiteColumn<CntT>, public AttributedColumn<CntT>
{
  public:
	AttributedSiteColumn() {}
	AttributedSiteColumn(unsigned int id, const MultiSequence& ms, const SitePartitionElement& spe): Column<CntT>(id, ms, spe), SiteColumn<CntT>(id, ms, spe) {}
};

#endif
