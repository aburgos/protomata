
#ifndef ATTRIBUTEDCOLUMN_H
#define ATTRIBUTEDCOLUMN_H

#include "ProbabilityColumn.h"

/*!
This class adds attributes to a column with probabilities. It allows to define any number of attributes of the same type, the template type of the column. This is intended to be used for attaching odds, logs, etc. to a column.
*/
template <typename CntT>
class AttributedColumn: public virtual ProbabilityColumn<CntT>
{
  public:
	typedef std::map<char, CntT> AttributesMap;
	typedef std::map<std::string, AttributesMap> StringMapAttributes;

  private:
	//! Allows to add several attributes to each amino acid on the column.
	StringMapAttributes m_attributes;

  public:
	AttributedColumn() {}
	AttributedColumn(unsigned int, const MultiSequence&, const SitePartitionElement&);

	bool has_attribute(const std::string& attr) const { return (m_attributes.find(attr) != m_attributes.end()); }
	bool has_attribute(const std::string&, char) const;
	AttributesMap attribute(const std::string&) const;
	CntT attribute(const std::string&, char) const;
	void attribute(const std::string& attr, char aa, CntT s);
	CntT sum_attributes(const std::string&) const;

	typedef typename StringMapAttributes::iterator attribute_iterator;
	attribute_iterator attributes_begin() { return m_attributes.begin(); }
	attribute_iterator attributes_end() { return m_attributes.end(); }

	typedef typename StringMapAttributes::const_iterator attribute_const_iterator;
	attribute_const_iterator attributes_begin() const { return m_attributes.begin(); }
	attribute_const_iterator attributes_end() const { return m_attributes.end(); }
};

template <typename CntT>
AttributedColumn<CntT>::AttributedColumn(unsigned int id, const MultiSequence& ms, const SitePartitionElement& spe): Column<CntT>(id, ms, spe) {}

template <typename CntT>
bool AttributedColumn<CntT>::has_attribute(const std::string& attr, char aa) const
{
	typename StringMapAttributes::const_iterator it1;
	if((it1 = m_attributes.find(attr)) != m_attributes.end())
	{
		const AttributesMap& attr = (*it1).second;
		return (attr.find(aa) != attr.end());
	}
	else return false;
}

template <typename CntT>
CntT AttributedColumn<CntT>::attribute(const std::string& attribute, char aa) const
{
	typename StringMapAttributes::const_iterator it1;
	if((it1 = m_attributes.find(attribute)) != m_attributes.end())
	{
		const AttributesMap& attr = (*it1).second;
		typename AttributesMap::const_iterator it2;
		if((it2 = attr.find(aa)) != attr.end()) return (*it2).second;
		else { std::cerr << "Amino acid " << aa << " has no assigned " << attribute << "." << std::endl; exit(1); }
	}
	else { std::cerr << "No attribute " << attribute << " in column." << std::endl; exit(1); }
}

template <typename CntT>
CntT AttributedColumn<CntT>::sum_attributes(const std::string& attribute) const
{
	CntT total = 0;
	typename StringMapAttributes::const_iterator it1;
	if((it1 = m_attributes.find(attribute)) != m_attributes.end())
	{
		const AttributesMap& attr = (*it1).second;
		for(typename AttributesMap::const_iterator it2 = attr.begin(); it2 != attr.end(); ++it2)
			total += (*it2).second;
	}
	else { std::cerr << "No attribute " << attribute << " in column." << std::endl; exit(1); }
	return total;
}

template <typename CntT>
std::map<char, CntT> AttributedColumn<CntT>::attribute(const std::string& attr) const
{
	typename StringMapAttributes::const_iterator it;
	if((it = m_attributes.find(attr)) != m_attributes.end()) return (*it).second;
	else { std::cerr << "No attribute " << attr << " in column." << std::endl; exit(1); }
}

template <typename CntT>
void AttributedColumn<CntT>::attribute(const std::string& attr, char aa, CntT s)
{
	typename StringMapAttributes::const_iterator it;
	if((it = m_attributes.find(attr)) == m_attributes.end())
	{
		AttributesMap attr_map;
		attr_map[aa] = s;
		m_attributes[attr] = attr_map;
	}
	else
	{
		AttributesMap attr_map = (*it).second;
		attr_map[aa] = s;
		m_attributes[attr] = attr_map;
	}
}

#endif
