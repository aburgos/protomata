
#ifndef PROBABILITYCOLUMN_H
#define PROBABILITYCOLUMN_H

#include "Column.h"

/*!
This is a column which holds probabilities in addition to counts for each of its aminoacids. It should be use for ProtoGraph that need to compute pseudocounts or other kinds.
*/
template <typename CntT>
class ProbabilityColumn: public virtual Column<CntT>
{
  public:
	typedef std::map<char, double> ProbabilitiesMap;

  private:
	ProbabilitiesMap m_probs;

  public:
	ProbabilityColumn() {}
	ProbabilityColumn(unsigned int, const MultiSequence&, const SitePartitionElement&);

	bool has_probability(char aa) const { return (this->m_probs.find(aa) != this->m_probs.end()); }
	double probability(char) const;
	void probability(char aa, CntT s) { this->m_probs[aa] = s; }
	double sum_probabilities() const;

	typedef typename ProbabilitiesMap::iterator probability_iterator;
	probability_iterator probabilities_begin() { return this->m_probs.begin(); }
	probability_iterator probabilities_end() { return this->m_probs.end(); }

	typedef typename ProbabilitiesMap::const_iterator probability_const_iterator;
	probability_const_iterator probabilities_begin() const { return this->m_probs.begin(); }
	probability_const_iterator probabilities_end() const { return this->m_probs.end(); }
};

template <typename CntT>
ProbabilityColumn<CntT>::ProbabilityColumn(unsigned int id, const MultiSequence& ms, const SitePartitionElement& spe): Column<CntT>(id, ms, spe) {}

template <typename CntT>
double ProbabilityColumn<CntT>::probability(char aa) const
{
	typename ProbabilitiesMap::const_iterator it;
	if((it = this->m_probs.find(aa)) != this->m_probs.end()) return (*it).second;
	else return 0;
}

template <typename CntT>
double ProbabilityColumn<CntT>::sum_probabilities() const
{
	double colc = 0;
	for(probability_const_iterator it = this->m_probs.begin(); it != this->m_probs.end(); ++it)
		colc += (*it).second;
	return colc;
}

#endif
