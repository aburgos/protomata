
#ifndef SITECOLUMN_H
#define SITECOLUMN_H

#include "Column.h"

/*!
A Column which has also a site partition element associated. It should be use when building ProtoGraph, since sites are needed for computing where a zone starts and ends.
*/
template <typename CntT>
class SiteColumn: public virtual Column<CntT>
{
  private:
	//! The Site that represents the aminoacids of the column.
	SitePartitionElement m_spe;

  public:
	SiteColumn() {}
	SiteColumn(unsigned int, const MultiSequence&, const SitePartitionElement&);

	typedef typename SitePartitionElement::iterator site_iterator;
	site_iterator sites_begin() { return m_spe.begin(); }
	site_iterator sites_end() { return m_spe.end(); }

	typedef typename SitePartitionElement::const_iterator site_const_iterator;
	site_const_iterator sites_begin() const { return m_spe.begin(); }
	site_const_iterator sites_end() const { return m_spe.end(); }
};

template <typename CntT>
SiteColumn<CntT>::SiteColumn(unsigned int id, const MultiSequence& ms, const SitePartitionElement& spe): Column<CntT>(id, ms, spe), m_spe(spe) {}

#endif
