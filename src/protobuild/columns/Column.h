
#ifndef COLUMN_H
#define COLUMN_H

#include <map>
#include "Typedefs.h"
#include "MultiSequence.h"

/*!
This should be understand as a column in the protomata model. It provides counts for each aminoacid present on the column (which could be double) and also the chemichal group it represents (if there is one).
*/
template <typename CntT>
class Column
{
  public:
	typedef std::pair<char, CntT> CountPair;
	typedef std::map<char, CntT> CountsMap;

  private:
	unsigned int m_id;
	unsigned int m_sequences;
	//! The chemichal group that the column represents.
	std::string m_group;
	//! Stores counts for each amino acid.
	CountsMap m_counts;

  public:
	Column() {}
	Column(unsigned int, const MultiSequence&, const SitePartitionElement&);

	void id(unsigned int id) { m_id = id; }
	unsigned int id() const { return m_id; }

	unsigned int sequences() const { return m_sequences; }
	unsigned int aminoacids() const { return m_counts.size(); }

	void group(const std::string& gr) { m_group = gr; }
	const std::string& group() const { return m_group; }

	bool has_count(char aa) const { return (m_counts.find(aa) != m_counts.end()); }
	CntT count(char) const;
	void count(char aa, CntT s) { m_counts[aa] = s; }
	CntT sum_counts() const;
	CountsMap counts() const { return m_counts; }

	bool operator<(const Column<CntT>& col) const { return (id() < col.id()); }

	typedef typename CountsMap::iterator counts_iterator;
	counts_iterator counts_begin() { return m_counts.begin(); }
	counts_iterator counts_end() { return m_counts.end(); }

	typedef typename CountsMap::const_iterator counts_const_iterator;
	counts_const_iterator counts_begin() const { return m_counts.begin(); }
	counts_const_iterator counts_end() const { return m_counts.end(); }
};

template <typename CntT>
Column<CntT>::Column(unsigned int id, const MultiSequence& ms, const SitePartitionElement& spe): m_id(id), m_sequences(spe.size())
{
	// set the score of the amino acids involved to the sum of the weights of the sequences
	for(SitePartitionElement::const_iterator it = spe.begin(); it != spe.end(); ++it)
	{
		const Site& site = *it;
		const char aminoacid = ms[site];
		if(m_counts.find(aminoacid) != m_counts.end()) m_counts[aminoacid] += ms[site.sequence()].weight();
		else m_counts[aminoacid] = ms[site.sequence()].weight();
	}
}

template <typename CntT>
CntT Column<CntT>::count(char aa) const
{
	typename CountsMap::const_iterator it;
	if((it = m_counts.find(aa)) != m_counts.end()) return (*it).second;
	else return 0;
}

template <typename CntT>
CntT Column<CntT>::sum_counts() const
{
	CntT colc = 0;
	for(typename Column<CntT>::counts_const_iterator it = counts_begin(); it != counts_end(); ++it)
		colc += (*it).second;
	return colc;
}

#endif
