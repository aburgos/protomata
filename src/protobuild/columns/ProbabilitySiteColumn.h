
#ifndef PROBABILITYSITECOLUMN_H
#define PROBABILITYSITECOLUMN_H

#include "SiteColumn.h"
#include "ProbabilityColumn.h"

/*!
This class allows to build ProtoGraph while being able to compute pseudocounts on them.
*/
template <typename CntT>
class ProbabilitySiteColumn: public SiteColumn<CntT>, public ProbabilityColumn<CntT>
{
  public:
	ProbabilitySiteColumn() {}
	ProbabilitySiteColumn(unsigned int id, const MultiSequence& ms, const SitePartitionElement& spe): Column<CntT>(id, ms, spe), SiteColumn<CntT>(id, ms, spe) {}
};

#endif
