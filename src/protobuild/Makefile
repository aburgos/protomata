
# Makefile for protobuild

CONFIG = $(shell ../config.sh ../..)

include $(CONFIG)

# libraries flags
LIBFLAGS = -I$(PUGIXML_DIR) -I$(TCLAP_DIR) -I$(PROTOMATALEARNER_DIR) -I$(PALOMA_DIR) -I$(PLMA2DOT_DIR) -I$(PROTOBUILD_DIR) -I$(PROTOSCAN_DIR)

# compiler flags
CXXFLAGS = $(MODEFLAGS) $(VFLAG) $(SHPFLAG) $(LIBFLAGS)

TARGETS = protobuild
OBJECTS = $(PARSERS) $(PALOMA) $(PUGIXML) $(PROTOBUILD) $(GROUPS) $(BASIC)

.PHONY : all
all : $(TARGETS)

PALOMA = $(PALOMA_DIR)/MultiSequence.o $(PALOMA_DIR)/alignments/LocalAlignment_UFImp.o $(PALOMA_DIR)/alignments/Alignment_UFImp.o $(PALOMA_DIR)/iterators/AlignmentUFImpRelationIterator.o $(PALOMA_DIR)/iterators/AlignmentUFImpSiteIterator.o $(PALOMA_DIR)/lib/UnionFind.o $(PALOMA_DIR)/AminoAcids.o $(PALOMA_DIR)/parsers/WeightsParser_MapImp.o $(PALOMA_DIR)/parsers/ManualWeightsParser.o $(PALOMA_DIR)/parsers/ClustalWeightsParser.o $(PALOMA_DIR)/parsers/FastaParser.o

MultiSequence.o: $(PALOMA_DIR)/MultiSequence.h
UnionFind.o: $(PALOMA_DIR)/lib/UnionFind.h
Alignment_UFImp.o: $(PALOMA_DIR)/alignments/Alignment_UFImp.h
LocalAlignment_UFImp.o: $(PALOMA_DIR)/alignments/LocalAlignment_UFImp.h
AlignmentUFImpSiteIterator.o: $(PALOMA_DIR)/iterators/AlignmentUFImpSiteIterator.h
AlignmentUFImpRelationIterator.o: $(PALOMA_DIR)/iterators/AlignmentUFImpRelationIterator.h
AminoAcids.o: $(PALOMA_DIR)/AminoAcids.h
WeightsParser_MapImp.o: $(PALOMA_DIR)/parsers/WeightsParser_MapImp.h
ManualWeightsParser.o: $(PALOMA_DIR)/parsers/ManualWeightsParser.h
ClustalWeightsParser.o: $(PALOMA_DIR)/parsers/ClustalWeightsParser.h
FastaParser.o: $(PALOMA_DIR)/parsers/FastaParser.h

PUGIXML = $(PUGIXML_DIR)/pugixml.o

pugixml.o: $(PUGIXML_DIR)/pugixml.hpp

PARSERS = parsers/PlmaParser.o parsers/ComponentParser.o $(PLMA2DOT_DIR)/SequencesAttributesParser.o

PlmaParser.o: parsers/PlmaParser.h
ComponentParser.o: parsers/ComponentParser.h
SequencesAttributesParser.o: $(PLMA2DOT_DIR)/SequencesAttributesParser.h

GROUPS = groups/Group.o groups/GroupsParser.o

Group.o: groups/Group.h
GroupsParser.o: groups/GroupsParser.h

BASIC = ProtobuildParameters.o ProtobuildAllowedParameters.o

Main.o: Main.cc
ProtobuildParameters.o: ProtobuildParameters.h
ProtobuildAllowedParameters.o: ProtobuildAllowedParameters.h

protobuild: $(OBJECTS) Main.o
	$(CXX) $(CXXFLAGS) -lm $(OBJECTS) Main.o -o $(LOCAL_BIN_DIR)/protobuild

clean:
	cd parsers; rm -f -R *.o
	cd groups; rm -f -R *.o
	rm -f -R *.o

TEST_OBJECTS = $(GABIOS_DIR)/pratique.o $(GABIOS_DIR)/alig_graph_closure.o $(PALOMA_DIR)/lib/Gabios.o $(PALOMA_DIR)/lib/BorderMap.o

test:
	$(CXX) $(TEST_DIR)/protobuild/protobuild_tests.cc $(CXXFLAGS) $(TFLAG) -I$(GABIOS_DIR) -I$(UNITTEST_DIR)/src -lm $(OBJECTS) $(UNITTEST_LIB) $(TEST_OBJECTS) -o $(TEST_DIR)/protobuild/protobuild_tests
	$(TEST_DIR)/protobuild/protobuild_tests

doc:
	cp $(DOC_DIR)/protobuild/Doxyfile_template $(DOC_DIR)/protobuild/Doxyfile
	sed -i "s|INPUT                  = |INPUT                  = $(SRC_DIR)/protobuild|g" $(DOC_DIR)/protobuild/Doxyfile
	sed -i "s|OUTPUT_DIRECTORY       = |OUTPUT_DIRECTORY       = $(DOC_DIR)/protobuild|g" $(DOC_DIR)/protobuild/Doxyfile
	doxygen $(DOC_DIR)/protobuild/Doxyfile

