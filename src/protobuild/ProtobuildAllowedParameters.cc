
#include "ProtobuildAllowedParameters.h"

ProtobuildAllowedParameters::ProtobuildAllowedParameters()
{
	pseudocounts_values.push_back("sqrtFormula");
	pseudocounts_values.push_back("logFormula");
	pseudocounts_values.push_back("Dirichlet");
}

TCLAP::ValuesConstraint<std::string>* ProtobuildAllowedParameters::pseudocounts_values_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(pseudocounts_values);
	return &allowedVals;
}

