
#ifndef PROTOGRAPHEDITOR_H
#define PROTOGRAPHEDITOR_H

#include "ColumnEditor.h"
#include "groups/MostLikelyGroup.h"
#include "ProtobuildParameters.h"
#include "ProtoScanEnumTypes.h"

/*!
Provides methods for 'editing' a ProtoGraph. It traverses it and edits each of its columns with a ColumnEditor.
*/
template <typename ProtoGraphType>
class ProtoGraphEditor
{
  public:
	typedef typename ProtoGraphType::CountType CountType;
	typedef typename ProtoGraphType::ZoneType ZoneType;

  private:
	ColumnEditor<CountType> column_editor;

  public:
	void add_pseudocounts(ProtoGraphType&, const std::string&);
	void add_pseudocounts(ProtoGraphType&, const std::string&, const std::string&);
	void group_expansion(ProtoGraphType&, const std::string&, double, double, bool);
	void add_extra_values(ProtoGraphType&, ScanScore);
	void set_viterbi_values(ProtoGraphType&, ScanScore);
};

template <typename ProtoGraphType>
void ProtoGraphEditor<ProtoGraphType>::add_pseudocounts(ProtoGraphType& pg, const std::string& pseudocounts)
{
	for(typename ProtoGraphType::zone_iterator it = pg.zone_begin(); it != pg.zone_end(); ++it)
	{
		ZoneType& zone = *it;
		for(typename ZoneType::column_iterator zit = zone.columns_begin(); zit != zone.columns_end(); ++zit)
		{
			typename ProtoGraphType::ColumnType& column = *zit;

			if(pseudocounts == "sqrtFormula") column_editor.add_pseudocounts_sqrt(column);
			else if(pseudocounts == "logFormula") column_editor.add_pseudocounts_log(column);

			if(!pseudocounts.empty()) column_editor.probs2counts(column);
		}
	}
}

template <typename ProtoGraphType>
void ProtoGraphEditor<ProtoGraphType>::add_pseudocounts(ProtoGraphType& pg, const std::string& pseudocounts, const std::string& cmp_file)
{
	// parse the chosen component file
	const ComponentParser cp(cmp_file);
	// get the parsed parameters
	const DirichletMixtureComponents& dmp = cp.parameters();

	for(typename ProtoGraphType::zone_iterator it = pg.zone_begin(); it != pg.zone_end(); ++it)
	{
		ZoneType& zone = *it;
		for(typename ZoneType::column_iterator zit = zone.columns_begin(); zit != zone.columns_end(); ++zit)
		{
			typename ProtoGraphType::ColumnType& column = *zit;

			column_editor.add_pseudocounts_dirichlet(column, dmp);

			if(!pseudocounts.empty()) column_editor.probs2counts(column);
		}
	}
}

template <typename ProtoGraphType>
void ProtoGraphEditor<ProtoGraphType>::add_extra_values(ProtoGraphType& pg, ScanScore ss)
{
	for(typename ProtoGraphType::zone_iterator it = pg.zone_begin(); it != pg.zone_end(); ++it)
	{
		ZoneType& zone = *it;
		for(typename ZoneType::column_iterator zit = zone.columns_begin(); zit != zone.columns_end(); ++zit)
		{
			typename ProtoGraphType::ColumnType& column = *zit;

			// if no probabilities were computed, add frequency as probability
			if(column.probabilities_begin() == column.probabilities_end())
				column_editor.add_frequencies(column);

			if(ss == logT) column_editor.add_logs(column);
			else if(ss == LR) column_editor.add_odds(column);
			else if(ss == logLR) column_editor.add_logodds(column);
		}
	}
}

template <typename ProtoGraphType>
void ProtoGraphEditor<ProtoGraphType>::set_viterbi_values(ProtoGraphType& pg, ScanScore ss)
{
	for(typename ProtoGraphType::zone_iterator it = pg.zone_begin(); it != pg.zone_end(); ++it)
	{
		ZoneType& zone = *it;
		for(typename ZoneType::column_iterator zit = zone.columns_begin(); zit != zone.columns_end(); ++zit)
		{
			typename ProtoGraphType::ColumnType& column = *zit;

			// if no probabilities were computed, add frequency as probability
			if(column.probabilities_begin() == column.probabilities_end())
				column_editor.add_frequencies(column);

			// with probabilities and odds, the exponential of viterbi values are computed in ProtoScan.h
			if(ss == probability || ss == logT)
			{
				column_editor.add_logs(column);
				column_editor.add_viterbi_values(column, "log");
			}
			else if(ss == LR || ss == logLR)
			{
				column_editor.add_logodds(column);
				column_editor.add_viterbi_values(column, "logodd");
			}
		}
	}
}

template <typename ProtoGraphType>
void ProtoGraphEditor<ProtoGraphType>::group_expansion(ProtoGraphType& pg, const std::string& group_file, double group_threshold, double sigma_threshold, bool no_expansion)
{
	GroupsParser gp(group_file);
	GroupsParser::GroupSet groups = gp.groups();

	for(typename ProtoGraphType::zone_iterator it = pg.zone_begin(); it != pg.zone_end(); ++it)
	{
		ZoneType& zone = *it;
		for(typename ZoneType::column_iterator zit = zone.columns_begin(); zit != zone.columns_end(); ++zit)
		{
			typename ProtoGraphType::ColumnType& column = *zit;

			MostLikelyGroup<double> mlg(column, groups);
			column_editor.expand2group(column, mlg.group(), group_threshold, sigma_threshold, no_expansion);
		}
	}
}

#endif
