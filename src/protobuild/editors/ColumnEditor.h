
#ifndef COLUMNEDITOR_H
#define COLUMNEDITOR_H

#include <map>
#include <cmath>
#include <iostream>
#include "AminoAcids.h"
#include "columns/AttributedColumn.h"
#include "columns/ProbabilityColumn.h"
#include "pseudocounts/PseudoCountsLog.h"
#include "pseudocounts/PseudoCountsSqrt.h"
#include "pseudocounts/DirichletMixture.h"
#include "groups/Group.h"

/*!
Provides methods for 'editing' a Column.
*/
template <typename CntT>
class ColumnEditor
{
  public:
	typedef ProbabilityColumn<double>::ProbabilitiesMap ProbabilitiesMap;

	// gets counts from probabilities
	void probs2counts(ProbabilityColumn<CntT>&);

	// use other values for computing viterbi score
	void add_viterbi_values(AttributedColumn<CntT>&);
	void add_viterbi_values(AttributedColumn<CntT>&, const std::string&);

	//! Adds frequencies for present aminoacids.
	void add_frequencies(ProbabilityColumn<CntT>&);

	// "extra" attributes
	void add_logs(AttributedColumn<CntT>&);
	void add_odds(AttributedColumn<CntT>&);
	void add_logodds(AttributedColumn<CntT>&);

	// pseudocounts methods
	void add_pseudocounts_sqrt(ProbabilityColumn<CntT>&);
	void add_pseudocounts_log(ProbabilityColumn<CntT>&);
	void add_pseudocounts_dirichlet(ProbabilityColumn<CntT>&, const DirichletMixtureComponents&);

	// expansion method
	void expand2group(Column<double>&, const Group&, double, double, bool);
};

template <>
void ColumnEditor<double>::probs2counts(ProbabilityColumn<double>& column)
{
	// save the column total weight value before modifying its aminoacids' weights
	const double before_updates_column_weight = column.sum_counts();

	ProbabilityColumn<double>::probability_iterator it;
	for(it = column.probabilities_begin(); it != column.probabilities_end(); ++it)
	{
		const char aminoacid = (*it).first;
		const double probability = (*it).second;

		column.count(aminoacid, probability * before_updates_column_weight);
	}
}

template <typename CntT>
void ColumnEditor<CntT>::add_viterbi_values(AttributedColumn<CntT>& column)
{
	typename ProbabilityColumn<CntT>::probability_iterator it;
	for(it = column.probabilities_begin(); it != column.probabilities_end(); ++it)
	{
		const char aminoacid = (*it).first;
		const CntT probability = (*it).second;

		column.attribute("viterbi", aminoacid, probability);
	}
}

template <typename CntT>
void ColumnEditor<CntT>::add_viterbi_values(AttributedColumn<CntT>& column, const std::string& attr)
{
	const typename AttributedColumn<CntT>::AttributesMap& attributes = column.attribute(attr);
	typename AttributedColumn<CntT>::AttributesMap::const_iterator it;
	for(it = attributes.begin(); it != attributes.end(); ++it)
	{
		const char aminoacid = (*it).first;
		const CntT attr_value = (*it).second;

		column.attribute("viterbi", aminoacid, attr_value);
	}
}

template <typename CntT>
void ColumnEditor<CntT>::add_frequencies(ProbabilityColumn<CntT>& column)
{
	// save the column total weight value before modifying its aminoacids' weights
	const CntT before_updates_column_weight = column.sum_counts();

	// update the score for all the amino acids of the column
	for(typename Column<CntT>::counts_iterator it = column.counts_begin(); it != column.counts_end(); ++it)
	{
		const char aminoacid = (*it).first;
		const CntT count = (*it).second;

		const double probability = double(count) / double(before_updates_column_weight);

		column.probability(aminoacid, probability);
	}
}

template <typename CntT>
void ColumnEditor<CntT>::add_logs(AttributedColumn<CntT>& column)
{
	typename ProbabilityColumn<CntT>::probability_iterator it;
	for(it = column.probabilities_begin(); it != column.probabilities_end(); ++it)
	{
		const char aminoacid = (*it).first;
		const double probability = (*it).second;

		column.attribute("log", aminoacid, log(probability));
	}
}

template <typename CntT>
void ColumnEditor<CntT>::add_odds(AttributedColumn<CntT>& column)
{
	const AminoAcids& aa = AminoAcids::instance();
	typename ProbabilityColumn<CntT>::probability_iterator it;
	for(it = column.probabilities_begin(); it != column.probabilities_end(); ++it)
	{
		const char aminoacid = (*it).first;
		const double probability = (*it).second;

		column.attribute("odd", aminoacid, probability / aa.back_prob(aminoacid));
	}
}

template <typename CntT>
void ColumnEditor<CntT>::add_logodds(AttributedColumn<CntT>& column)
{
	const AminoAcids& aa = AminoAcids::instance();
	typename ProbabilityColumn<CntT>::probability_iterator it;
	for(it = column.probabilities_begin(); it != column.probabilities_end(); ++it)
	{
		const char aminoacid = (*it).first;
		const double probability = (*it).second;

		column.attribute("logodd", aminoacid, log(probability) - log(aa.back_prob(aminoacid)));
	}
}

template <typename CntT>
void ColumnEditor<CntT>::add_pseudocounts_sqrt(ProbabilityColumn<CntT>& column)
{
	PseudoCountsSqrt<CntT> pcs(column);
}

template <typename CntT>
void ColumnEditor<CntT>::add_pseudocounts_log(ProbabilityColumn<CntT>& column)
{
	PseudoCountsLog<CntT> pcl(column);
}

template <typename CntT>
void ColumnEditor<CntT>::add_pseudocounts_dirichlet(ProbabilityColumn<CntT>& column, const DirichletMixtureComponents& dmp)
{
	DirichletMixture<CntT> dm(column, dmp);
}

template <>
void ColumnEditor<double>::expand2group(Column<double>& column, const Group& group, double group_threshold, double sigma_threshold, bool no_expansion)
{
	const AminoAcids& aa = AminoAcids::instance();

	// test group expansion
	double sum_over_P = 0;
	for(Column<double>::counts_const_iterator it = column.counts_begin(); it != column.counts_end(); ++it)
		sum_over_P += aa.back_prob((*it).first);

	double sum_over_G = 0;
	for(Group::const_iterator it = group.begin(); it != group.end(); ++it)
		sum_over_G += aa.back_prob(*it);

	double likelihood_ratio_GP = pow(sum_over_P / sum_over_G, aa.size());

	if(likelihood_ratio_GP < group_threshold)
	{
		// test sigma expansion
		double sum_over_sigma = 0;
		for(AminoAcids::const_iterator it = aa.begin(); it != aa.end(); ++it)
			sum_over_sigma += aa.back_prob(*it);
		double likelihood_ratio_SP = pow(sum_over_sigma, aa.size());

		if(likelihood_ratio_SP < sigma_threshold) return;
		else
		{
	 		// expand to sigma
			for(AminoAcids::const_iterator it = aa.begin(); it != aa.end(); ++it)
			{
				const char aminoacid = *it;
				if(!no_expansion)
				{
					double modified_count = aa.back_prob(aminoacid);
					if(column.has_count(aminoacid)) modified_count *= column.count(aminoacid);
					column.count(aminoacid, modified_count);
				}
			}

			column.group("sigma");
		}
	}
	else
	{
		if(!no_expansion)
		{
			// expand to group
			for(Group::const_iterator it = group.begin(); it != group.end(); ++it)
			{
				const char aminoacid = *it;
				double modified_count = aa.back_prob(aminoacid) / sum_over_G;
				if(column.has_count(aminoacid)) modified_count *= column.count(aminoacid);
				column.count(aminoacid, modified_count);
			}
		}
		else
		{
			// change column values
			Column<double>::counts_const_iterator it;
			for(it = column.counts_begin(); it != column.counts_end(); ++it)
			{
				const char aminoacid = (*it).first;
				double modified_count = aa.back_prob(aminoacid) / sum_over_G;
				if(column.has_count(aminoacid)) modified_count *= column.count(aminoacid);
				column.count(aminoacid, modified_count);
			}
		}

		column.group(group.name());
	}
}

#endif
