
#include "PlmaParser.h"

PlmaParser::PlmaParser(const std::string& plma_file)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(plma_file.c_str());
    if(!result) { std::cerr << "Error loading file " << plma_file << std::endl; exit(1); }

	// search for the sequences first
	pugi::xml_node seqs_node = doc.child("plma").child("resume").child("sequences");
	if(!seqs_node) { std::cerr << "Error parsing file " << plma_file << std::endl; exit(1); }

	// for correctly parsing the data
	FastaParser fasta_parser;

	for(pugi::xml_node_iterator it = seqs_node.begin(); it != seqs_node.end(); ++it)
	{
		std::string tag = it->attribute("tag").value();
		std::string data = it->attribute("data").value();
		fasta_parser.create_sequence(tag, data, false, true);
	}

	// create a Multisequence object
	ms = new MultiSequence(fasta_parser.sequences());

	// generalized alignment that adds every alignment in the plma file
	ga = new GeneralizedAlignment<LocalAlignment_UFImp>(*ms);

	// search for the partition tag
	pugi::xml_node partition_node = doc.child("plma").child("partition");
	if(!partition_node) { std::cerr << "Error parsing file " << plma_file << std::endl; exit(1); }

	// iterate through the node childs of the partition tag node
	for(pugi::xml_node_iterator eit = partition_node.begin(); eit != partition_node.end(); ++eit)
	{
		SiteSet sp;
		// iterate through the element childs of the part tag node
		for(pugi::xml_node_iterator iit = eit->begin(); iit != eit->end(); ++iit)
		{
			// internally site's sequences and positions begin at 0
			unsigned int seq = atoi(iit->attribute("seq").value()) - 1;
			unsigned int pos = atoi(iit->attribute("pos").value()) - 1;
			Site s(seq, pos);
			sp.insert(s);
		}

		// create a local alignment representing the site's relations
		LocalAlignment_UFImp la(*ms);
		for(SiteSet::iterator it1 = sp.begin(); it1 != sp.end(); ++it1)
		{
			if(sp.size() == 1)
			{
				SitePairRelation spr(std::make_pair(*it1, *it1), 0);
				la.addMatching(spr);
			}
			SiteSet::iterator it2 = it1;
			for(++it2; it2 != sp.end(); ++it2)
			{
				SitePairRelation spr(std::make_pair(*it1, *it2), 0);
				la.addMatching(spr);
			}
		}

		// add the created local alignment to the generalized alignment
		ga->addMatching(la);
	}
}

PlmaParser::~PlmaParser()
{
	delete ga;
	delete ms;
}
