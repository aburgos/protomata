
#ifndef PLMAPARSER_H
#define PLMAPARSER_H

#include <cstdio>
#include "MultiSequence.h"
#include "alignments/LocalAlignment_UFImp.h"
#include "generalized_sets/GeneralizedAlignment.h"
#include "pugixml.hpp"
#include "parsers/FastaParser.h"

/*!
Parse a PLMA file in XML format. It creates two objects, a MultiSequence containing all the sequences and a Generalized Alignment with a site partition. This object should persist as much as these objects are needed.
*/
class PlmaParser
{
  private:
	//! The sequences read from the PLMA file.
	MultiSequence* ms;
	//! The relations between the sites. Each LocalAlignment_UFImp corresponds to a site partition element.
	GeneralizedAlignment<LocalAlignment_UFImp>* ga;

  public:
	/*!
		The constructor parse the XML plma file and then creates two objects, a MultiSequence and a GeneralizedAlignment objects.
		\param The name of the PLMA file.
		\param Weather it should compute weights for the sequences or not.
	*/
	PlmaParser(const std::string&);
	//!	The destructor deletes the MultiSequence and the GeneralizedAlignment objects.
	~PlmaParser();
	/*! \return The created MultiSequence object. */
	const MultiSequence& multisequence() const { return *ms; }
	/*! \return The created MultiSequence object. */
	MultiSequence& multisequence() { return *ms; }
	/*! \return The created GeneralizedAlignment. */
	const GeneralizedAlignment<LocalAlignment_UFImp>& generalized_alignment() const { return *ga; }

	MultiSequence multisequence_copy() const { return *ms; }
	GeneralizedAlignment<LocalAlignment_UFImp>* generalized_alignment_copy() const { return new GeneralizedAlignment<LocalAlignment_UFImp>(*ga); }
};

#endif
