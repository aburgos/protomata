
#include "ComponentParser.h"

ComponentParser::ComponentParser(const std::string& filename)
{
	std::ifstream file;
	file.open(filename.c_str(), std::ifstream::in);
	if(!file.good())
	{
		std::cerr << "Component file " << filename << " could not be opened." << std::endl;
		exit(1);
	}

	std::string ClassName = getValue<std::string>(file, "ClassName");
	// assuming Name comes after ClassName
	//file.seekg(std::ios_base::beg);
	std::string Name = getValue<std::string>(file, "Name");
	file.seekg(std::ios_base::beg);
	std::string Alphabet = getValue<std::string>(file, "Alphabet");
	file.seekg(std::ios_base::beg);
	std::vector<char> Order = getValues<char>(file, "Order");
	//file.seekg(std::ios_base::beg);
	//unsigned int AlphaChar = getValue<unsigned int>(file, "AlphaChar");
	file.seekg(std::ios_base::beg);
	unsigned int NumDistr = getValue<unsigned int>(file, "NumDistr");

	dm = new DirichletMixtureComponents(ClassName, Name, Alphabet, Order);

	for(unsigned int i = 0; i < NumDistr; i++)
	{
		unsigned int Number = getValue<unsigned int>(file, "Number");
		double Mixture = getValue<double>(file, "Mixture");
		std::vector<double> AlphaValues = getValues<double>(file, "Alpha");
		// the first value is the sum of all the others
		double sumAlpha = *(AlphaValues.begin());
		std::map<char, double> Alpha;
		unsigned int j = 1;
		DirichletMixtureComponents::alphabet_iterator ait;
		for(ait = dm->alphabet_begin(); ait != dm->alphabet_end(); ++ait)
			Alpha.insert(std::make_pair(*ait, AlphaValues[j++]));
		std::string CommentLine = getLine(file, "Comment");
		size_t eq_pos = CommentLine.find('=');
		std::string Comment = CommentLine.substr(eq_pos + 2, CommentLine.length());

		Component c(Number, Mixture, sumAlpha, Alpha, Comment);
		dm->addComponent(c);
	}

	file.close();
}

ComponentParser::~ComponentParser()
{
	delete dm;
}

std::string ComponentParser::getLine(std::ifstream& file, const std::string& tag)
{
	std::string line;
	while(getline(file, line))
	{
		size_t found = line.find(tag);
		if(found != std::string::npos) break;
	}
	return line;
}

template <typename T>
T ComponentParser::getValue(std::ifstream& file, const std::string& tag)
{
	std::vector<T> v = getValues<T>(file, tag);
	if(v.empty())
	{
		std::cout << "Error reading component file: could not find tag " << tag << std::endl;
		exit(1);
	}

	return *(v.begin());
}

template <typename T>
std::vector<T> ComponentParser::getValues(std::ifstream& file, const std::string& tag)
{
	T item;
	std::vector<T> result;

	std::string line = getLine(file, tag);
	std::istringstream iss(line);
	iss.ignore(1024, '=');
	while(iss >> item) result.push_back(item);

	return result;
}
