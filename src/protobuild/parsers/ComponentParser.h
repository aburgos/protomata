
#ifndef COMPONENTPARSER_H
#define COMPONENTPARSER_H

#include <cstdlib>
#include <fstream>
#include <sstream>
#include "components/DirichletMixtureComponents.h"

/*!
An order were assumed for the component files. The general component values don't need an order, but every distribution does, and that is 1.Number, 2.Mixture, 3.Alpha, 4.Comment.
*/
class ComponentParser
{
  private:
	DirichletMixtureComponents* dm;
	std::string getLine(std::ifstream&, const std::string&);
	template <typename T> T getValue(std::ifstream&, const std::string&);
	template <typename T> std::vector<T> getValues(std::ifstream&, const std::string&);

  public:
	ComponentParser(const std::string&);
	~ComponentParser();

	DirichletMixtureComponents& parameters() const { return *dm; }
};

#endif
