
#ifndef PSEUDOCOUNTS_H
#define PSEUDOCOUNTS_H

#include <cmath>
#include "columns/ProbabilityColumn.h"

template <typename CntT>
class PseudoCounts
{
  public:
	PseudoCounts(ProbabilityColumn<CntT>& column, const double B)
	{
		const AminoAcids& aa = AminoAcids::instance();

		// save the column total weight value before modifying its aminoacids' weights
		const CntT before_updates_column_weight = column.sum_counts();

		// maps an amino acid to an aproximation of its probability of occurring in the column
		for(AminoAcids::const_iterator it = aa.begin(); it != aa.end(); ++it)
		{
			const char aminoacid = *it;
			const CntT count = column.count(aminoacid);

			const double probability = (count + B * aa.back_prob(aminoacid)) / (before_updates_column_weight + B);
			column.probability(aminoacid, probability);
		}
	}
};

#endif
