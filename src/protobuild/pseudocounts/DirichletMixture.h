
#ifndef DIRICHLETMIXTURE_H
#define DIRICHLETMIXTURE_H

#include <cmath>
#include "parsers/ComponentParser.h"
#include "columns/ProbabilityColumn.h"

/*!
Dirichlet mixture weight scheme. It uses a specified mixture file.
*/
template <typename CntT>
class DirichletMixture
{
  public:
	DirichletMixture(ProbabilityColumn<CntT>&, const std::string&);
	DirichletMixture(ProbabilityColumn<CntT>&, const DirichletMixtureComponents&);
	double log_beta(const std::vector<double>&) const;
};

template <typename CntT>
DirichletMixture<CntT>::DirichletMixture(ProbabilityColumn<CntT>& column, const std::string& components_file)
{
	// parse the chosen component file
	const ComponentParser cp(components_file);
	// get the parsed parameters
	const DirichletMixtureComponents& dmp = cp.parameters();

	// 1: preliminary
	std::map<unsigned int, double> log_beta_diff;
	for(unsigned int k = 0; k < dmp.component_size(); k++)
	{
		std::vector<double> first, second;
		DirichletMixtureComponents::alphabet_iterator ait;
		for(ait = dmp.alphabet_begin(); ait != dmp.alphabet_end(); ++ait)
		{
			const char aminoacid = *ait;
			first.push_back(column.count(aminoacid) + dmp[k][aminoacid]);
			second.push_back(dmp[k][aminoacid]);
		}
		log_beta_diff[k] = log_beta(first) - log_beta(second);
	}

	// 2: posterior probabilities of components
	double M = log_beta_diff[0];
	for(unsigned int i = 1; i < log_beta_diff.size(); i++)
		if(M < log_beta_diff[i]) M = log_beta_diff[i];
	std::map<unsigned int, double> posterior_prob;
	for(unsigned int k = 0; k < dmp.component_size(); k++)
		posterior_prob[k] = dmp[k].mixture() * exp(log_beta_diff[k] - M);

	// 3: pseudocounts denominator
	std::map<unsigned int, double> pseudocounts_mix_total;
	for(unsigned int k = 0; k < dmp.component_size(); k++)
	{
		pseudocounts_mix_total[k] = 0;
		DirichletMixtureComponents::alphabet_iterator ait;
		for(ait = dmp.alphabet_begin(); ait != dmp.alphabet_end(); ++ait)
			pseudocounts_mix_total[k] += column.count(*ait);
		pseudocounts_mix_total[k] += dmp[k].sum_alpha();
	}

	// 4: Xi calculation
	std::map<char, double> X;
	DirichletMixtureComponents::alphabet_iterator ait;
	for(ait = dmp.alphabet_begin(); ait != dmp.alphabet_end(); ++ait)
	{
		X[*ait] = 0;
		for(unsigned int k = 0; k < dmp.component_size(); k++)
			X[*ait] += posterior_prob[k] * ((column.count(*ait) + dmp[k][*ait]) / pseudocounts_mix_total[k]);
	}

	// 5: Xi normalization
	double X_sum = 0;
	for(ait = dmp.alphabet_begin(); ait != dmp.alphabet_end(); ++ait)
		X_sum += X[*ait];
	for(ait = dmp.alphabet_begin(); ait != dmp.alphabet_end(); ++ait)
		if(X_sum != 0) column.probability(*ait, X[*ait] / X_sum);
}

template <typename CntT>
DirichletMixture<CntT>::DirichletMixture(ProbabilityColumn<CntT>& column, const DirichletMixtureComponents& dmp)
{
	// 1: preliminary
	std::map<unsigned int, double> log_beta_diff;
	for(unsigned int k = 0; k < dmp.component_size(); k++)
	{
		std::vector<double> first, second;
		DirichletMixtureComponents::alphabet_iterator ait;
		for(ait = dmp.alphabet_begin(); ait != dmp.alphabet_end(); ++ait)
		{
			const char aminoacid = *ait;
			first.push_back(column.count(aminoacid) + dmp[k][aminoacid]);
			second.push_back(dmp[k][aminoacid]);
		}
		log_beta_diff[k] = log_beta(first) - log_beta(second);
	}

	// 2: posterior probabilities of components
	double M = log_beta_diff[0];
	for(unsigned int i = 1; i < log_beta_diff.size(); i++)
		if(M < log_beta_diff[i]) M = log_beta_diff[i];
	std::map<unsigned int, double> posterior_prob;
	for(unsigned int k = 0; k < dmp.component_size(); k++)
		posterior_prob[k] = dmp[k].mixture() * exp(log_beta_diff[k] - M);

	// 3: pseudocounts denominator
	std::map<unsigned int, double> pseudocounts_mix_total;
	for(unsigned int k = 0; k < dmp.component_size(); k++)
	{
		pseudocounts_mix_total[k] = 0;
		DirichletMixtureComponents::alphabet_iterator ait;
		for(ait = dmp.alphabet_begin(); ait != dmp.alphabet_end(); ++ait)
			pseudocounts_mix_total[k] += column.count(*ait);
		pseudocounts_mix_total[k] += dmp[k].sum_alpha();
	}

	// 4: Xi calculation
	std::map<char, double> X;
	DirichletMixtureComponents::alphabet_iterator ait;
	for(ait = dmp.alphabet_begin(); ait != dmp.alphabet_end(); ++ait)
	{
		X[*ait] = 0;
		for(unsigned int k = 0; k < dmp.component_size(); k++)
			X[*ait] += posterior_prob[k] * ((column.count(*ait) + dmp[k][*ait]) / pseudocounts_mix_total[k]);
	}

	// 5: Xi normalization
	double X_sum = 0;
	for(ait = dmp.alphabet_begin(); ait != dmp.alphabet_end(); ++ait)
		X_sum += X[*ait];
	for(ait = dmp.alphabet_begin(); ait != dmp.alphabet_end(); ++ait)
		if(X_sum != 0) column.probability(*ait, X[*ait] / X_sum);
}

template <typename CntT>
double DirichletMixture<CntT>::log_beta(const std::vector<double>& v) const
{
	double sum = 0;
	double result = 0;
	std::vector<double>::const_iterator it;
	for(it = v.begin(); it != v.end(); ++it)
	{
		sum += *it;
		result += lgamma(*it);
	}
	return (result - lgamma(sum));
}

#endif
