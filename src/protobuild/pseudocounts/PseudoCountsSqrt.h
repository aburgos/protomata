
#ifndef PSEUDOCOUNTSSQRT_H
#define PSEUDOCOUNTSSQRT_H

#include "PseudoCounts.h"

template <typename CntT>
class PseudoCountsSqrt: public PseudoCounts<CntT>
{
  public:
	PseudoCountsSqrt(ProbabilityColumn<CntT>& column): PseudoCounts<CntT>(column, sqrt(column.sum_counts())) {}
};

#endif
