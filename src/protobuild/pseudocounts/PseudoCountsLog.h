
#ifndef PSEUDOCOUNTSLOG_H
#define PSEUDOCOUNTSLOG_H

#include "PseudoCounts.h"

template <typename CntT>
class PseudoCountsLog: public PseudoCounts<CntT>
{
  public:
	PseudoCountsLog(ProbabilityColumn<CntT>& column): PseudoCounts<CntT>(column, log(column.sum_counts())) {}
};

#endif
