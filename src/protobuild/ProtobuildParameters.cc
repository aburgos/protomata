
#include "ProtobuildParameters.h"

ProtobuildParameters::ProtobuildParameters():
	m_protobuild_input(TCLAP::ValueArg<std::string>("i", "input", "Set plma file name.", true, "", "plma_filename")),
	m_protobuild_output(TCLAP::ValueArg<std::string>("o", "output", "Set output file name.", false, "plma_basename.proto", "proto_filename")),
	m_protobuild_tag(TCLAP::ValueArg<std::string>("", "tag", "Add a tag to the end of the basename.", false, "", "string")),
	m_plma_dot_attributes(TCLAP::ValueArg<std::string>("a", "plma-dot-attributes", "A file specifying attributes for the dot output of each sequence.", false, "", "attr_file")),
	m_pseudocounts(TCLAP::ValueArg<std::string>("", "pseudocounts", "Strategy for adding pseudocounts to each amino acid column for building the model.", false, "", allowed.pseudocounts_values_constraints())),
	m_components(TCLAP::ValueArg<std::string>("", "components", "The components file to be used with Dirichlet pseudocounts.", false, "", "components_file")),
	m_groups(TCLAP::ValueArg<std::string>("", "groups", "Adds to each column the more likely physico-chemical group according to the groups file specified.", false, "", "groups_file")),
	m_plma_dot_output(TCLAP::ValueArg<std::string>("", "plma-dot-output", "Set dot output filename for a plma.", false, "", "dot_filename")),
	m_proto_dot_output(TCLAP::ValueArg<std::string>("", "proto-dot-output", "Set dot output filename for a proto.", false, "", "dot_filename")),
	m_quorum(TCLAP::ValueArg<double>("q", "quorum", "The minimum sum of the weights of the sequences for keeping a block.", false, 0, "value")),
	m_weight_percentage(TCLAP::ValueArg<double>("p", "percentage-quorum", "The minimum percentage for the sum of the weights of the sequences for keeping a block. The value should be in range [0,1].", false, 0, "value")),
	m_group_threshold(TCLAP::ValueArg<double>("G", "group-threshold", "Do not expand to the best fit physico-chemical group found if likelihood ratio is below the given value.", false, 0, "value")),
	m_sigma_threshold(TCLAP::ValueArg<double>("S", "sigma-threshold", "Do not expand to all amino acids if likelihood ratio is below the given value.", false, 0, "value")),
	m_weight_sequences(TCLAP::SwitchArg("w", "weight-sequences", "Each sequence will have a weight representing how unique it is on the set.", false)),
	m_weights_input(TCLAP::ValueArg<std::string>("", "weights-input", "User defined weights for sequences.", false, "", "weigths_filename")),
	m_no_expansion(TCLAP::SwitchArg("x", "no-expansion", "Do not expand to group's amino acids, only expand to all amino acids if no group were found.", false)),
	m_plma_dot(TCLAP::SwitchArg("", "plma-dot", "Generates a PLMA dot file. Output filename is plma_basename_plma.dot.", false)),
	m_proto_dot(TCLAP::SwitchArg("", "proto-dot", "Generates a protomata dot file. Output filename is plma_basename_proto.dot.", false)),
	m_short_tags(TCLAP::SwitchArg("", "short-headers", "Output truncated headers of sequences.", false))
{}

void ProtobuildParameters::parse(int argc, char **argv)
{
	TCLAP::CmdLine cmd("Generates a Protomata model from a PLMA.", ' ', VERSION);
	add_arguments(cmd);
	cmd.parse(argc, argv);
	sanity_checks();
	add_string_parameters();
}

void ProtobuildParameters::add_arguments(TCLAP::CmdLine& cmd)
{
	cmd.add(m_sigma_threshold);
	cmd.add(m_group_threshold);
	cmd.add(m_groups);
	cmd.add(m_components);
	cmd.add(m_pseudocounts);
	cmd.add(m_short_tags);
	cmd.add(m_plma_dot_attributes);
	cmd.add(m_plma_dot_output);
	cmd.add(m_plma_dot);
	cmd.add(m_proto_dot_output);
	cmd.add(m_proto_dot);
	cmd.add(m_weights_input);
	cmd.add(m_weight_sequences);
	cmd.add(m_weight_percentage);
	cmd.add(m_quorum);
	cmd.add(m_protobuild_tag);
	cmd.add(m_protobuild_output);
	cmd.add(m_protobuild_input);
}

void ProtobuildParameters::sanity_checks()
{
	if(pseudocounts() == "Dirichlet" && components_file().empty())
		{ std::cerr << "Error: No components file specified" << std::endl; exit(1); }
	if(!components_file().empty() && m_pseudocounts.getValue() != "Dirichlet")
		std::cerr << "Warning! Dirichlet pseudocounts will be used, since a components was set." << std::endl;
	if(!plma_dot_attributes_file().empty() && !plma_dot())
		std::cerr << "Warning! PLMA dot attributes file will not be used, PLMA dot output not activated." << std::endl;
}

void ProtobuildParameters::add_string_parameters()
{
	string_parameters.push_back(std::make_pair("--input", input()));
	string_parameters.push_back(std::make_pair("--output", output()));
	std::ostringstream ss1; ss1 << quorum();
	string_parameters.push_back(std::make_pair("--quorum", ss1.str()));
	std::ostringstream ss2; ss2 << weight_percentage();
	string_parameters.push_back(std::make_pair("--weight-percentage", ss2.str()));
	if(weight_sequences()) string_parameters.push_back(std::make_pair("--weight-sequences", ""));
	if(!weights_input().empty()) string_parameters.push_back(std::make_pair("--weights-input", weights_input()));
	if(!pseudocounts().empty()) string_parameters.push_back(std::make_pair("--pseudocounts", pseudocounts()));
	if(!components_file().empty()) string_parameters.push_back(std::make_pair("--components", components_file()));
	if(!groups_file().empty())
	{
		string_parameters.push_back(std::make_pair("--groups", groups_file()));
		std::ostringstream ss3; ss3 << group_threshold();
		string_parameters.push_back(std::make_pair("--group-threshold", ss3.str()));
		std::ostringstream ss4; ss4 << sigma_threshold();
		string_parameters.push_back(std::make_pair("--sigma-threshold", ss4.str()));
		if(no_expansion()) string_parameters.push_back(std::make_pair("--no-expansion", ""));
	}
	if(!plma_dot_attributes_file().empty())
		string_parameters.push_back(std::make_pair("--plma-dot-attributes", plma_dot_attributes_file()));
}

void ProtobuildParameters::initialize_values()
{
}

std::string ProtobuildParameters::compute_output_file(const std::string& filename) const
{
	std::ostringstream ss;
	ss << compute_basename(filename);
	if(!m_protobuild_tag.getValue().empty()) ss << "_" << tag();
	ss << "_q" << quorum() << "_p" << weight_percentage();
	if(weight_sequences()) ss << "_w";
	if(!weights_input().empty()) ss << "_w:" << weights_input();
	if(!ProtobuildParameters::pseudocounts().empty()) ss << "_" << pseudocounts();
	if(!ProtobuildParameters::components_file().empty())
	{
		size_t pos = components_file().find_last_of('/');
		std::string components_basename = components_file().substr(pos + 1, components_file().length());
		ss << "_comp:" << components_basename;
	}
	if(!groups_file().empty())
	{
		size_t pos = groups_file().find_last_of('/');
		ss << "_g:" << groups_file().substr(pos + 1, groups_file().length());
		ss << "_gt" << group_threshold() << "_st" << sigma_threshold();
		if(no_expansion()) ss << "_nexp";
	}
	if(!plma_dot_attributes_file().empty())
	{
		const std::string& dot_attr = plma_dot_attributes_file();
		size_t pos = dot_attr.find_last_of('/');
		std::string plma_dot_attr_basename = dot_attr.substr(pos + 1, dot_attr.length());
		ss << "_a:" << plma_dot_attr_basename;
	}
	/*
	if(plma_dot()) ss << "_pldot";
	if(plma_dot_output.isSet())
	{
		const std::string pldotfn = plma_dot_output();
		size_t pos = pldotfn.find_last_of('/');
		std::string pldotfn_basename = pldotfn.substr(pos + 1, pldotfn.length());
		ss << "_pldot:" << pldotfn_basename;
	}
	if(proto_dot()) ss << "_prdot";
	if(proto_dot_output.isSet())
	{
		const std::string prdotfn = proto_dot_output();
		size_t pos = prdotfn.find_last_of('/');
		std::string prdotfn_basename = prdotfn.substr(pos + 1, prdotfn.length());
		ss << "_prdot:" << prdotfn_basename;
	}
	*/
	ss << "_proto.xml";
	return ss.str();
}

std::string ProtobuildParameters::output() const
{
	if(!m_protobuild_output.isSet()) return ProtobuildParameters::compute_output_file(input());
	else return m_protobuild_output.getValue();
}

std::string ProtobuildParameters::plma_dot_output() const
{
	if(m_plma_dot_output.isSet()) return m_plma_dot_output.getValue();
	else
	{
		const std::string& proto_input = input();
		if(proto_input.find_last_of('.') == std::string::npos) return proto_input + "_plma.dot";
		else
		{
			std::ostringstream ss;
			ss << input().substr(0, input().find_last_of('.'));
			if(!m_protobuild_tag.getValue().empty()) ss << "_" << tag();
			if(weight_sequences()) ss << "_w";
			ss << "_q" << quorum() << "_p" << weight_percentage();
			if(m_short_tags.getValue()) ss << "_s";
			ss << "_plma.dot";
			return ss.str();
		}
	}
}

std::string ProtobuildParameters::proto_dot_output() const
{
	if(m_proto_dot_output.isSet()) return m_proto_dot_output.getValue();
	else
	{
		const std::string& proto_output = ProtobuildParameters::output();
		if(proto_output.find_last_of('.') == std::string::npos) return proto_output + "_proto.dot";
		else return proto_output.substr(0, proto_output.find_last_of('.')) + ".dot";
	}
}

std::string ProtobuildParameters::pseudocounts() const
{
	if(!components_file().empty()) return std::string("Dirichlet");
	else return m_pseudocounts.getValue();
}

std::string ProtobuildParameters::components_file() const
{
	FileNameSearcher fnc;
	return fnc.filename(m_components.getValue());
}

std::string ProtobuildParameters::groups_file() const
{
	FileNameSearcher fnc;
	return fnc.filename(m_groups.getValue());
}
