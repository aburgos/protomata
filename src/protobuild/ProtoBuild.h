
#ifndef PROTOBUILD_H
#define PROTOBUILD_H

#include "ProtobuildParameters.h"
#include "ProtoGraphBuilder.h"
#include "parsers/PlmaParser.h"
#include "parsers/ComponentParser.h"
#include "editors/ProtoGraphEditor.h"
#include "PlmaDotGenerator.h"
#include "output/ProtomataDotGenerator.h"
#include "output/ProtoXmlGenerator.h"
#include "columns/AttributedSiteColumn.h"
#include "columns/ProbabilitySiteColumn.h"
#include "zones/SegmentZone.h"

template <typename CntT, template <typename T> class ColT>
class ProtoBuild
{
  public:
	ProtoBuild(const ProtobuildParameters& parameters);
};

template <typename CntT, template <typename T> class ColT>
ProtoBuild<CntT, ColT>::ProtoBuild(const ProtobuildParameters& parameters)
{
	typedef ProtoGraphBuilder<LocalAlignment_UFImp, CntT, ColT> PGBuilderType;
	typedef typename PGBuilderType::ProtoGraphType ProtoGraphType;

	PlmaParser plma(parameters.input());

	MultiSequence& ms = plma.multisequence();
	if(!parameters.weights_input().empty()) ms.weight_sequences(parameters.weights_input());
	else if(parameters.weight_sequences()) ms.weight_sequences();
	const GeneralizedAlignment<LocalAlignment_UFImp>& ga = plma.generalized_alignment();

	PGBuilderType proto_builder(ms, ga, parameters.quorum(), parameters.weight_percentage());
	const ProtoGraphType& pg = proto_builder.proto_graph();

	bool st = parameters.short_tags();
	const std::string& attr = parameters.plma_dot_attributes_file();

	if(parameters.plma_dot()) PlmaDotGenerator<ProtoGraphType>(parameters.plma_dot_output(), ms, pg, attr, st);
	if(parameters.proto_dot()) ProtomataDotGenerator<ProtoGraphType>(parameters.proto_dot_output(), ms, pg);

	ProtoXmlGenerator<ProtoGraphType>(parameters, ms, pg);
}

template <>
ProtoBuild<double, SiteColumn>::ProtoBuild(const ProtobuildParameters& parameters)
{
	typedef ProtoGraphBuilder<LocalAlignment_UFImp, double, SiteColumn> PGBuilderType;
	typedef PGBuilderType::ProtoGraphType ProtoGraphType;

	PlmaParser plma(parameters.input());

	MultiSequence& ms = plma.multisequence();
	if(!parameters.weights_input().empty()) ms.weight_sequences(parameters.weights_input());
	else if(parameters.weight_sequences()) ms.weight_sequences();
	const GeneralizedAlignment<LocalAlignment_UFImp>& ga = plma.generalized_alignment();

	PGBuilderType proto_builder(ms, ga, parameters.quorum(), parameters.weight_percentage());
	ProtoGraphType& pg = proto_builder.proto_graph();

	const double grp_thr = parameters.group_threshold();
	const double sgm_thr = parameters.sigma_threshold();
	const bool nxp = parameters.no_expansion();
	const std::string& gf = parameters.groups_file();

	ProtoGraphEditor<ProtoGraphType> pg_editor;
	if(!gf.empty()) pg_editor.group_expansion(pg, gf, grp_thr, sgm_thr, nxp);

	bool st = parameters.short_tags();
	const std::string& attr = parameters.plma_dot_attributes_file();

	if(parameters.plma_dot()) PlmaDotGenerator<ProtoGraphType>(parameters.plma_dot_output(), ms, pg, attr, st);
	if(parameters.proto_dot()) ProtomataDotGenerator<ProtoGraphType>(parameters.proto_dot_output(), ms, pg);

	ProtoXmlGenerator<ProtoGraphType>(parameters, ms, pg);
}

template <>
ProtoBuild<double, ProbabilitySiteColumn>::ProtoBuild(const ProtobuildParameters& parameters)
{
	typedef ProtoGraphBuilder<LocalAlignment_UFImp, double, ProbabilitySiteColumn> PGBuilderType;
	typedef PGBuilderType::ProtoGraphType ProtoGraphType;

	PlmaParser plma(parameters.input());

	MultiSequence& ms = plma.multisequence();
	if(!parameters.weights_input().empty()) ms.weight_sequences(parameters.weights_input());
	else if(parameters.weight_sequences()) ms.weight_sequences();
	const GeneralizedAlignment<LocalAlignment_UFImp>& ga = plma.generalized_alignment();

	PGBuilderType proto_builder(ms, ga, parameters.quorum(), parameters.weight_percentage());
	ProtoGraphType& pg = proto_builder.proto_graph();

	const std::string& cmp_file = parameters.components_file();
	const double grp_thr = parameters.group_threshold();
	const double sgm_thr = parameters.sigma_threshold();
	const bool nxp = parameters.no_expansion();
	const std::string& gf = parameters.groups_file();

	ProtoGraphEditor<ProtoGraphType> pg_editor;
	if(!gf.empty()) pg_editor.group_expansion(pg, gf, grp_thr, sgm_thr, nxp);
	if(!parameters.pseudocounts().empty())
	{
		if(!cmp_file.empty()) pg_editor.add_pseudocounts(pg, parameters.pseudocounts(), cmp_file);
		else pg_editor.add_pseudocounts(pg, parameters.pseudocounts());
	}

	bool st = parameters.short_tags();
	const std::string& attr = parameters.plma_dot_attributes_file();

	if(parameters.plma_dot()) PlmaDotGenerator<ProtoGraphType>(parameters.plma_dot_output(), ms, pg, attr, st);
	if(parameters.proto_dot()) ProtomataDotGenerator<ProtoGraphType>(parameters.proto_dot_output(), ms, pg);

	ProtoXmlGenerator<ProtoGraphType>(parameters, ms, pg);
}

#endif
