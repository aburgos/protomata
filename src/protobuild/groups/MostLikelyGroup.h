
#ifndef MOSTLIKELYGROUP_H
#define MOSTLIKELYGROUP_H

#include <cmath>
#include <string>
#include "AminoAcids.h"
#include "GroupsParser.h"
#include "columns/Column.h"

template <typename CntT>
class MostLikelyGroup
{
  public:
	typedef Column<CntT> ColumnType;
	typedef std::set<Group> GroupSet;

  private:
	Group m_most_likely_group;
	std::string m_group_name;
	bool m_group_found;

  public:
	MostLikelyGroup(const ColumnType&, const GroupSet&);
	Group group() const { return m_most_likely_group; }
	bool group_found() const { return m_group_found; }
};

template <typename CntT>
MostLikelyGroup<CntT>::MostLikelyGroup(const ColumnType& column, const GroupSet& groups): m_most_likely_group(Group(""))
{
	typedef std::map<unsigned int, GroupSet> GroupSetCounts;

	// get how many times each group appears
	GroupSetCounts group_count;
	for(GroupSet::const_iterator git = groups.begin(); git != groups.end(); ++git)
	{
		const Group& current_group = *git;

		unsigned int hits = 0;
		bool update_group_set = true;
		for(typename ColumnType::counts_const_iterator it = column.counts_begin(); it != column.counts_end(); ++it)
		{
			const char current_aminoacid = (*it).first;

			if(current_group.has_amino_acid(current_aminoacid)) hits++;
			else { update_group_set = false; break; }
		}

		if(update_group_set)
		{
			if(group_count.find(hits) != group_count.end())
			{
				GroupSet current_group_set = group_count[hits];
				current_group_set.insert(current_group);
				group_count[hits] = current_group_set;
			}
			else
			{
				GroupSet new_group_set;
				new_group_set.insert(current_group);
				group_count[hits] = new_group_set;
			}
		}
	}

	// get the maximum number of counts
	unsigned int max_group_counts = 0;
	for(GroupSetCounts::const_iterator git = group_count.begin(); git != group_count.end(); ++git)
	{
		const unsigned int groups_counts = (*git).first;
		if(groups_counts > max_group_counts) max_group_counts = groups_counts;
	}

	// if the column fits in at least a group
	m_group_found = (group_count.find(max_group_counts) != group_count.end());

	// get the minimum group that contains all the amino acids that appears in the column
	if(m_group_found)
	{
		const GroupSet& groups = group_count[max_group_counts];
		Group base_group = *(groups.begin());
		for(GroupSet::const_iterator wit = groups.begin(); wit != groups.end(); ++wit)
		{
			const Group& current_group = *wit;
			if(current_group.size() < base_group.size())
				base_group = current_group;
		}
		m_most_likely_group = base_group;
	}
}

#endif
