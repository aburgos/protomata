
#include "GroupsParser.h"

GroupsParser::GroupsParser(const std::string& filename)
{
	std::ifstream file(filename.c_str(), std::ifstream::in);
	if(!file.good())
	{
		std::cerr << "File " << filename << " could not be opened." << std::endl;
		exit(1);
	}

	std::string line;
	while(getline(file, line))
	{
		std::istringstream iss(line);
		std::string group_aa;
		iss >> group_aa;
		Group g(group_aa);
		std::string name;
		iss >> name;
		g.name(name);
		m_groups.insert(g);
	}

	file.close();
}
