
#ifndef GROUPSPARSER_H
#define GROUPSPARSER_H

#include <cstdlib>
#include <fstream>
#include <sstream>
#include "Group.h"

class GroupsParser
{
  public:
	typedef std::set<Group> GroupSet;

  private:
	std::set<Group> m_groups;

  public:
	GroupsParser(const std::string&);
	const GroupSet& groups() const { return m_groups; }
};

#endif
