
#include "Group.h"

Group::Group(const std::string& aa)
{
	// initialize group amino acids
	for(std::string::const_iterator it = aa.begin(); it != aa.end(); ++it)
		m_amino_acids.insert(*it);
}

void Group::add_comment(const std::string& comment)
{
	comments.push_back(comment);
}

std::string Group::amino_acids() const
{
	std::string aas;
	for(const_iterator it = begin(); it != end(); ++it)
		aas += *it;
	return aas;
}

bool Group::operator<(const Group& g) const
{
	return (size() < g.size());
}

bool Group::operator==(const Group& g) const
{
	if(size() == g.size())
	{
		const_iterator it1 = begin();
		const_iterator it2 = g.begin();
		while(it1 != end() && *it1 == *it2)
		{
			++it1;
			++it2;
		}
		return (it1 == end());
	}
	else return 0;
}

bool Group::operator!=(const Group& g) const
{
	return !(operator==(g));
}
