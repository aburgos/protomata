
#ifndef AAGROUP_H
#define AAGROUP_H

#include <iostream>
#include <set>
#include <vector>

class Group
{
  private:
	std::string m_name;
	std::set<char> m_amino_acids;
	std::vector<std::string> comments;

  public:
	Group() {}
	Group(const std::string&);

	std::string name() const { return m_name; }
	void name(const std::string& name) { m_name = name; }

	void add_comment(const std::string&);

	void add_amino_acid(char aa) { m_amino_acids.insert(aa); }
	void rm_amino_acid(char aa) { m_amino_acids.erase(aa); }
	bool has_amino_acid(char aa) const { return (m_amino_acids.find(aa) != m_amino_acids.end()); }

	std::string amino_acids() const;
	unsigned int size() const { return m_amino_acids.size(); }

	bool operator<(const Group& g) const;
	bool operator==(const Group& g) const;
	bool operator!=(const Group& g) const;

	typedef std::set<char>::const_iterator const_iterator;
	const_iterator begin() const { return m_amino_acids.begin(); }
	const_iterator end() const { return m_amino_acids.end(); }

	typedef std::vector<std::string>::const_iterator comments_const_iterator;
	comments_const_iterator comments_begin() const { return comments.begin(); }
	comments_const_iterator comments_end() const { return comments.end(); }

	//!	Definition of the output (cout) operator for a Group
	friend std::ostream& operator<<(std::ostream& aStream, const Group& g)
	{
		aStream << "Group amino acids: ";
		for(const_iterator it = g.begin(); it != g.end(); ++it)
			aStream << *it;
		aStream << std::endl;

		aStream << "Group comments: ";
		for(comments_const_iterator it = g.comments_begin(); it != g.comments_end(); ++it)
			aStream << *it << "\t";
		aStream << std::endl;

		return aStream;
	}
};

#endif
