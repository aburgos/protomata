
#ifndef GAP_H
#define GAP_H

#include "Transition.h"

class Gap: public virtual Transition
{
  public:
	Gap() { this->m_type = "Gap"; }
	Gap(unsigned int, unsigned int, unsigned int);
};

Gap::Gap(unsigned int id, unsigned int z1_id, unsigned int z2_id): Transition(id, z1_id, z2_id) { this->m_type = "Gap"; }

#endif
