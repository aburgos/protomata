
#ifndef STRINGTRANSITION_H
#define STRINGTRANSITION_H

#include "lib/DiGraph.h"
#include "Transition.h"

template <typename CntT>
class StringTransition: public virtual Transition
{
  public:
	typedef std::map<std::string, CntT> SubSequenceWeightMap;

  protected:
	//! Graph with all the transitions between the two zones.
	SubSequenceWeightMap m_subseqs;

  public:
	StringTransition() {}
	StringTransition(unsigned int id, unsigned int z1_id, unsigned int z2_id, const SubSequenceWeightMap& subseqs): Transition(id, z1_id, z2_id), m_subseqs(subseqs) {}

	typedef typename SubSequenceWeightMap::const_iterator subseq_const_iterator;
	subseq_const_iterator subseqs_begin() const { return m_subseqs.begin(); }
	subseq_const_iterator subseqs_end() const { return m_subseqs.end(); }
};

#endif
