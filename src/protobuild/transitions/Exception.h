
#ifndef EXCEPTION_H
#define EXCEPTION_H

#include "Transition.h"

class Exception: public virtual Transition
{
  public:
	Exception() { this->m_type = "Exception"; }
	Exception(unsigned int, unsigned int, unsigned int);
};

Exception::Exception(unsigned int id, unsigned int z1_id, unsigned int z2_id): Transition(id, z1_id, z2_id) { this->m_type = "Exception"; }

#endif
