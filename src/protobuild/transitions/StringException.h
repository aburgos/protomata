
#ifndef STRINGEXCEPTION_H
#define STRINGEXCEPTION_H

#include "Exception.h"
#include "StringTransition.h"

template <typename CntT>
class StringException: public StringTransition<CntT>, public Exception
{
  public:
	typedef typename StringTransition<CntT>::SubSequenceWeightMap SubSequenceWeightMap;
	StringException(unsigned int, unsigned int, unsigned int, const SubSequenceWeightMap&);
};

template <typename CntT>
StringException<CntT>::StringException(unsigned int id, unsigned int z1_id, unsigned int z2_id, const SubSequenceWeightMap& subseqs): Transition(id, z1_id, z2_id), StringTransition<CntT>(id, z1_id, z2_id, subseqs) {}

#endif
