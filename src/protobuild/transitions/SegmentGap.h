
#ifndef SEGMENTGAP_H
#define SEGMENTGAP_H

#include "Gap.h"
#include "SegmentTransition.h"

template <typename CntT>
class SegmentGap: public SegmentTransition<CntT>, public Gap
{
  public:
	typedef typename SegmentTransition<CntT>::TransitionGraph TransitionGraph;
	SegmentGap(unsigned int, unsigned int, unsigned int, const TransitionGraph&, const MultiSequence&);
};

template <typename CntT>
SegmentGap<CntT>::SegmentGap(unsigned int id, unsigned int z1_id, unsigned int z2_id, const TransitionGraph& tg, const MultiSequence& ms): Transition(id, z1_id, z2_id), SegmentTransition<CntT>(id, z1_id, z2_id, tg, ms) {}

#endif
