
#ifndef TRANSITION_H
#define TRANSITION_H

#include "zones/Zone.h"

class Transition
{
  protected:
	unsigned int m_id;
	unsigned int m_z1_id;
	unsigned int m_z2_id;
	std::string m_type;

  public:
	Transition() {}
	Transition(unsigned int id, unsigned int z1_id, unsigned int z2_id): m_id(id), m_z1_id(z1_id), m_z2_id(z2_id) {}

	void id(unsigned int id) { m_id = id; }
	unsigned int id() const { return m_id; }

	unsigned int source_id() const { return m_z1_id; }
	unsigned int target_id() const { return m_z2_id; }

	std::string type() const { return m_type; }

	bool operator<(const Transition& zt) const { return (id() < zt.id()); }
	bool operator==(const Transition& zt) const { return (id() == zt.id()); }
	bool operator!=(const Transition& zt) const { return !operator==(zt); }
};

#endif
