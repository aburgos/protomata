
#ifndef STRINGGAP_H
#define STRINGGAP_H

#include "Gap.h"
#include "StringTransition.h"

template <typename CntT>
class StringGap: public StringTransition<CntT>, public Gap
{
  public:
	typedef typename StringTransition<CntT>::SubSequenceWeightMap SubSequenceWeightMap;
	StringGap(unsigned int, unsigned int, unsigned int, const SubSequenceWeightMap&);
};

template <typename CntT>
StringGap<CntT>::StringGap(unsigned int id, unsigned int z1_id, unsigned int z2_id, const SubSequenceWeightMap& subseqs): Transition(id, z1_id, z2_id), StringTransition<CntT>(id, z1_id, z2_id, subseqs) {}

#endif
