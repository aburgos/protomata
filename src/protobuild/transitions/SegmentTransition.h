
#ifndef SEGMENTTRANSITION_H
#define SEGMENTTRANSITION_H

#include "lib/DiGraph.h"
#include "StringTransition.h"

template <typename CntT>
class SegmentTransition: public virtual StringTransition<CntT>
{
  public:
	typedef DiGraph<Segment, Segment> TransitionGraph;

  protected:
	//! Graph with all the transitions between the two zones.
	TransitionGraph m_transition_graph;

  public:
	SegmentTransition() {}
	SegmentTransition(unsigned int, unsigned int, unsigned int, const TransitionGraph&, const MultiSequence&);

	// this method should disappear once ProtoGraphBuilder implements correctly the transitive closure
	TransitionGraph transitions() const { return m_transition_graph; }

	typedef TransitionGraph::relation_iterator relation_const_iterator;
	relation_const_iterator begin() const { return m_transition_graph.relation_begin(); }
	relation_const_iterator end() const { return m_transition_graph.relation_end(); }
};

template <typename CntT>
SegmentTransition<CntT>::SegmentTransition(unsigned int id, unsigned int z1_id, unsigned int z2_id, const TransitionGraph& tg, const MultiSequence& ms): Transition(id, z1_id, z2_id), m_transition_graph(tg)
{
	for(TransitionGraph::relation_iterator it = tg.relation_begin(); it != tg.relation_end(); ++it)
	{
		const TransitionGraph::relation& r = *it;
		const unsigned int sequence_id = r.first().sequence();
		this->m_subseqs.insert(std::make_pair(ms[r.value()], ms[sequence_id].weight()));
	}
}

#endif
