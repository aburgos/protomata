
#ifndef SEGMENTEXCEPTION_H
#define SEGMENTEXCEPTION_H

#include "Exception.h"
#include "SegmentTransition.h"

template <typename CntT>
class SegmentException: public SegmentTransition<CntT>, public Exception
{
  public:
	typedef typename SegmentTransition<CntT>::TransitionGraph TransitionGraph;
	SegmentException(unsigned int, unsigned int, unsigned int, const TransitionGraph&, const MultiSequence&);
};

template <typename CntT>
SegmentException<CntT>::SegmentException(unsigned int id, unsigned int z1_id, unsigned int z2_id, const TransitionGraph& tg, const MultiSequence& ms): Transition(id, z1_id, z2_id), SegmentTransition<CntT>(id, z1_id, z2_id, tg, ms) {}

#endif
