
#include <time.h>
#include <iostream>
#include "ProtoBuild.h"

int main(int argc, char **argv)
{
	//double start, end;
	//start = (double)clock();

	ProtobuildParameters parameters;
	parameters.parse(argc, argv);

	if(parameters.pseudocounts().empty())
	{
		if(parameters.weight_sequences()) ProtoBuild<double, SiteColumn> protobuild(parameters);
		else if(!parameters.groups_file().empty()) ProtoBuild<double, SiteColumn> protobuild(parameters);
		else ProtoBuild<unsigned int, SiteColumn> protobuild(parameters);
	}
	// for computing pseudocounts we need to temporarily store probabilities
	else ProtoBuild<double, ProbabilitySiteColumn> protobuild(parameters);

	//end = (double)clock();
	//double time_elapsed = double(end - start) / CLOCKS_PER_SEC;
	//std::cout << std::endl << "Time elapsed: " << time_elapsed << " secs" << std::endl;

	return 0;
}
