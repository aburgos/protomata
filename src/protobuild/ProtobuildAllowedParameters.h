
#ifndef PROTOBUILDALLOWEDPARAMETERS_H
#define PROTOBUILDALLOWEDPARAMETERS_H

#include "tclap/CmdLine.h"

class ProtobuildAllowedParameters
{
  private:
	std::vector<std::string> pseudocounts_values;

  public:
	ProtobuildAllowedParameters();

	TCLAP::ValuesConstraint<std::string>* pseudocounts_values_constraints();
};

#endif
