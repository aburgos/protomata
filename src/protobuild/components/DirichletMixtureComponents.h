
#ifndef DIRICHLETMIXTURECOMPONENTS_H
#define DIRICHLETMIXTURECOMPONENTS_H

#include <vector>
#include "Component.h"

class DirichletMixtureComponents
{
  private:
	std::string ClassName;
	std::string Name;
	std::string Alphabet;
	std::vector<char> Order;
	std::vector<Component> components;

  public:
	DirichletMixtureComponents(const std::string& cn, const std::string& n, const std::string& a, const std::vector<char>& o): ClassName(cn), Name(n), Alphabet(a), Order(o) {}

	void addComponent(const Component& c) { components.push_back(c); }
	const Component operator[](unsigned int pos) const { return components.at(pos); }

	std::string class_name() const { return ClassName; }
	std::string name() const { return Name; }
	std::string alphabet_type() const { return Alphabet; }
	unsigned int alphabet_size() const { return Order.size(); }
	unsigned int component_size() const { return components.size(); }

	typedef std::vector<char>::const_iterator alphabet_iterator;
	alphabet_iterator alphabet_begin() const { return Order.begin(); }
	alphabet_iterator alphabet_end() const { return Order.end(); }

	typedef std::vector<Component>::const_iterator component_iterator;
	component_iterator component_begin() const { return components.begin(); }
	component_iterator component_end() const { return components.end(); }

	//!	Definition of the output (cout) operator for a DirichletMixture
	friend std::ostream& operator<<(std::ostream& aStream, const DirichletMixtureComponents& dm)
	{
		aStream << "ClassName = " << dm.class_name() << std::endl;
		aStream << "Name = " << dm.name() << std::endl;
		aStream << "Alphabet = " << dm.alphabet_type() << std::endl;
		aStream << "Order =";
		DirichletMixtureComponents::alphabet_iterator ait;
		for(ait = dm.alphabet_begin(); ait != dm.alphabet_end(); ++ait)
			aStream << " " << *ait;
		aStream << std::endl;
		aStream << "AlphaChar = " << dm.alphabet_size() << std::endl;
		aStream << "NumDistr = " << dm.component_size() << std::endl;
		aStream << std::endl;

		DirichletMixtureComponents::component_iterator cit;
		for(cit = dm.component_begin(); cit != dm.component_end(); ++cit)
			aStream << *cit << std::endl;

		return aStream;
	}
};

#endif
