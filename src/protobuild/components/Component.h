
#ifndef COMPONENT_H
#define COMPONENT_H

#include <iostream>
#include <map>

class Component
{
  private:
	unsigned int Number;
	double Mixture;
	double sumAlpha;
	std::map<char, double> Alpha;
	std::string Comment;

  public:
	Component(unsigned int n, double m, double s, std::map<char, double> a, std::string c): Number(n), Mixture(m), sumAlpha(s), Alpha(a), Comment(c) {}

	double mixture() const { return Mixture; }
	double sum_alpha() const { return sumAlpha; }
	std::string comment() const { return Comment; }
	double operator[](char l) const { return (*(Alpha.find(l))).second; }

	//!	Definition of the output (cout) operator for a Component
	friend std::ostream& operator<<(std::ostream& aStream, const Component& c)
	{
		aStream << "Number = " << c.Number << std::endl;
		aStream << "Mixture = " << c.Mixture << std::endl;
		aStream << "Sum Alpha = " << c.sumAlpha << std::endl;
		aStream << "Alpha =";
		std::map<char, double>::const_iterator it;
		for(it = c.Alpha.begin(); it != c.Alpha.end(); ++it)
			aStream << " " << (*it).first << " = " << (*it).second;
		aStream << std::endl;
		aStream << "Comment = " << c.Comment << std::endl;

		return aStream;
	}
};

#endif
