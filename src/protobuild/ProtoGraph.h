
#ifndef PROTOGRAPH_H
#define PROTOGRAPH_H

#include <cstdlib>
#include "lib/DiGraph.h"

template <typename CntT, template <typename T> class ColT, template <typename V, template <typename T> class C> class ZneT, typename TrnT>
class ProtoGraph
{
  public:
	typedef CntT CountType;
	typedef ColT<CntT> ColumnType;
	typedef ZneT<CntT, ColT> ZoneType;
	typedef TrnT TransitionType;
	typedef unsigned int ZoneID;
	typedef std::vector<ZoneType> ZoneTypeVector;
	typedef DiGraph<ZoneID, TransitionType> TransitionsGraph;

  private:
	//! This should be a set but in order to edit the zones later, a different container is needed.
	ZoneTypeVector m_zones;
	//! A directed graph.
	TransitionsGraph m_zone_graph;

  public:
	ProtoGraph() {}

	unsigned int zones() const { return m_zones.size(); }
	unsigned int transitions() const { return m_zone_graph.edges(); }

	bool add_zone(const ZoneType& zone);
	bool add_transition(const TransitionType&);
	bool delete_transition(ZoneID, ZoneID);

	ZoneType operator[](unsigned int id) const;

	typedef typename ZoneTypeVector::iterator zone_iterator;
	zone_iterator zone_begin() { return m_zones.begin(); }
	zone_iterator zone_end() { return m_zones.end(); }

	typedef typename ZoneTypeVector::const_iterator zone_const_iterator;
	zone_const_iterator zone_begin() const { return m_zones.begin(); }
	zone_const_iterator zone_end() const { return m_zones.end(); }

	typedef typename TransitionsGraph::relation_iterator relation_const_iterator;
	relation_const_iterator relation_begin() const { return m_zone_graph.relation_begin(); }
	relation_const_iterator relation_end() const { return m_zone_graph.relation_end(); }

	template <typename T> friend class PathGenerator;
	template <class A, class C, template <typename T> class P> friend class ProtoGraphBuilder;
};

template <typename CntT, template <typename T> class ColT, template <typename V, template <typename T> class C> class ZneT, typename TrnT>
bool ProtoGraph<CntT, ColT, ZneT, TrnT>::add_zone(const ZoneType& zone)
{
	// maybe check that they are not already present on the vector?
	m_zones.push_back(zone);
	return true;
}

template <typename CntT, template <typename T> class ColT, template <typename V, template <typename T> class C> class ZneT, typename TrnT>
bool ProtoGraph<CntT, ColT, ZneT, TrnT>::add_transition(const TransitionType& t)
{
	return m_zone_graph.addEdge(t.source_id(), t.target_id(), t);
}

template <typename CntT, template <typename T> class ColT, template <typename V, template <typename T> class C> class ZneT, typename TrnT>
bool ProtoGraph<CntT, ColT, ZneT, TrnT>::delete_transition(ZoneID z1_id, ZoneID z2_id)
{
	return m_zone_graph.deleteEdge(z1_id, z2_id);
}

template <typename CntT, template <typename T> class ColT, template <typename V, template <typename T> class C> class ZneT, typename TrnT>
typename ProtoGraph<CntT, ColT, ZneT, TrnT>::ZoneType ProtoGraph<CntT, ColT, ZneT, TrnT>::operator[](unsigned int id) const
{
	for(typename ZoneTypeVector::const_iterator it = m_zones.begin(); it != m_zones.end(); ++it)
	{
		const ZoneType& zone = *it;
		if(zone.id() == id) return zone;
	}

	std::cout << "No zone with id " << id << " found" << std::endl;
	exit(1);
}

#endif
