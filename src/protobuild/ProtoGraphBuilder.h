
#ifndef PROTOGRAPHBUILDER_H
#define PROTOGRAPHBUILDER_H

#include "ProtoGraph.h"
#include "zones/EndZone.h"
#include "zones/BeginZone.h"
#include "zones/SegmentZone.h"
#include "transitions/SegmentGap.h"
#include "transitions/SegmentException.h"
#include "generalized_sets/GeneralizedAlignment.h"

/*!
This class generates the zone graph with its transitions, these representing either a gap or an exception.
*/
template <typename OutM, typename CntT, template <typename T> class ColT>
class ProtoGraphBuilder
{
  public:
	typedef ColT<CntT> ColumnType;
	typedef SegmentZone<CntT, ColT> ZoneType;
	typedef BeginZone<CntT, ColT> BeginZoneType;
	typedef EndZone<CntT, ColT> EndZoneType;

	typedef SegmentTransition<CntT> TransitionType;
	typedef SegmentGap<CntT> GapType;
	typedef SegmentException<CntT> ExceptionType;

	typedef ProtoGraph<CntT, ColT, SegmentZone, TransitionType> ProtoGraphType;

	typedef DiGraph<Segment, Segment> TransitionGraph;
	typedef DiGraph<Segment, unsigned int> ZoneSegmentsGraph;
	typedef std::map<unsigned int, TransitionGraph> Transitions;
	typedef std::vector<ZoneType> ZoneTypeVector;

  private:
	ProtoGraphType m_pg;
	const MultiSequence& m_ms;

	Transitions target_transitions(const ZoneType&, const ZoneTypeVector&) const;
	ZoneTypeVector get_zones(const GeneralizedAlignment<OutM>&, double, double);

  public:
	ProtoGraphBuilder(const MultiSequence&, const GeneralizedAlignment<OutM>&, double = 0, double = 0);

	ProtoGraphType proto_graph_copy() const { return m_pg; }
	ProtoGraphType& proto_graph() { return m_pg; }
	const ProtoGraphType& proto_graph() const { return m_pg; }
};

template <typename OutM, typename CntT, template <typename T> class ColT>
ProtoGraphBuilder<OutM, CntT, ColT>::ProtoGraphBuilder(const MultiSequence& ms, const GeneralizedAlignment<OutM>& ga, double quorum, double weight_percent): m_ms(ms)
{
	// build the zones out of the generalized alignment
	ZoneTypeVector zones = get_zones(ga, quorum, weight_percent);

	unsigned int transition_id = 1;

	// first add all gaps
	for(typename ZoneTypeVector::const_iterator it1 = zones.begin(); it1 != zones.end(); ++it1)
	{
		// add the zone to the protograph
		m_pg.add_zone(*it1);

		const Transitions transitions = target_transitions(*it1, zones);
		for(typename Transitions::const_iterator it2 = transitions.begin(); it2 != transitions.end(); ++it2)
		{
			const unsigned int z1_id = (*it1).id();
			const unsigned int z2_id = (*it2).first;
			const TransitionGraph& tg = (*it2).second;

			GapType g(transition_id++, z1_id, z2_id, tg, m_ms);
			m_pg.add_transition(g);
		}
	}

	typedef std::pair<unsigned int, unsigned int> ZoneIdPair;
	// set of edges to be replaced
	std::set<ZoneIdPair> to_replace;

	// now check for exceptions
	typename ProtoGraphType::TransitionsGraph::vertex_iterator vit;
	typename ProtoGraphType::TransitionsGraph::vertex_iterator::adjacent_iterator ait1, ait2;
	for(vit = m_pg.m_zone_graph.vertex_begin(); vit != m_pg.m_zone_graph.vertex_end(); vit++)
		for(ait1 = vit.adjacent_begin(); ait1 != vit.adjacent_end(); ait1++)
			for(ait2 = vit.adjacent_begin(); ait2 != vit.adjacent_end(); ait2++)
			{
				if(*ait1 == *ait2) continue;
				// condition for being an exception
				if(m_pg.m_zone_graph.existsPath(*ait1, *ait2))
					to_replace.insert(std::make_pair(*vit, *ait2));
			}

	// replace the exceptions found
	for(typename std::set<ZoneIdPair>::iterator pit = to_replace.begin(); pit != to_replace.end(); ++pit)
	{
		const ZoneIdPair& zp = *pit;
		typename ProtoGraphType::TransitionsGraph::relation_iterator rit;
		if((rit = m_pg.m_zone_graph.hasEdge(zp.first, zp.second)) != m_pg.m_zone_graph.relation_end())
		{
			const typename ProtoGraphType::TransitionsGraph::relation& rel = *rit;
			const TransitionType& zt = rel.value();
			const unsigned int z1_id = zt.source_id();
			const unsigned int z2_id = zt.target_id();
			ExceptionType e(zt.id(), z1_id, z2_id, zt.transitions(), m_ms);
			m_pg.delete_transition(zp.first, zp.second);
			m_pg.add_transition(e);
		}
	}
}

template <typename OutM, typename CntT, template <typename T> class ColT>
typename ProtoGraphBuilder<OutM, CntT, ColT>::ZoneTypeVector ProtoGraphBuilder<OutM, CntT, ColT>::get_zones(const GeneralizedAlignment<OutM>& ga, double quorum, double weight_percent)
{
	std::vector<ZoneType> zones;
	typedef std::set<SitePartitionElement> Partition;

	// stores the partition of translated sites from the generalized alignment
	Partition partition;

	// get from the generalized alignment the site partition
	const UnionFind::Partition& site_partition = ga.union_find.partition();
	for(UnionFind::Partition::iterator it = site_partition.begin(); it != site_partition.end(); ++it)
	{
		SitePartitionElement sp;
		for(UnionFind::PartitionElement::iterator sit = (*it).begin(); sit != (*it).end(); ++sit)
			sp.insert(ga.uint2site(*sit));
		partition.insert(sp);
	}

	double sequences_total_weight = 0;
	for(unsigned int i = 0; i < m_ms.size(); i++)
		sequences_total_weight += m_ms[i].weight();

	// add begin and end zones
	BeginZoneType begin(m_ms);
	zones.push_back(begin);

	EndZoneType end(m_ms);
	zones.push_back(end);

	unsigned int zone_id = EndZoneType::end_zone_id + 1;

	// while there is an element of the site partition, try to create a zone out of it
	while(partition.size() > 0)
	{
		ZoneType zone(zone_id, m_ms);
		unsigned int column_id = 1;

		Partition to_delete;
		for(Partition::const_iterator it = partition.begin(); it != partition.end(); ++it)
		{
			const SitePartitionElement& spe = *it;

			double sp_total_weight = 0;
			for(SitePartitionElement::const_iterator sit = (*it).begin(); sit != (*it).end(); ++sit)
				sp_total_weight += m_ms[(*sit).sequence()].weight();
			double sp_weight_percent = sp_total_weight / sequences_total_weight;

			if(sp_total_weight < quorum || sp_weight_percent < weight_percent)
			{
				to_delete.insert(spe); 
				continue;
			}
			if(zone.add(ColumnType(column_id++, m_ms, spe))) to_delete.insert(spe);
			else break;
		}
		if(zone.size() > 0)
		{
			zone_id++;
			zones.push_back(zone);
		}
		for(Partition::const_iterator it = to_delete.begin(); it != to_delete.end(); ++it)
			partition.erase(*it);
	}

	return zones;
}

template <typename OutM, typename CntT, template <typename T> class ColT>
std::map<unsigned int, DiGraph<Segment, Segment> > ProtoGraphBuilder<OutM, CntT, ColT>::target_transitions(const ZoneType& zone, const ZoneTypeVector& zones) const
{
	// directed graph to keep track of the transitions between the segments of this zone
	// and the segments of each of the adjacents zones
	ZoneSegmentsGraph transitions_graph;

	// for each zone of the protograph, check if there is a transition between the parameter and it
	for(typename ZoneTypeVector::const_iterator zit = zones.begin(); zit != zones.end(); ++zit)
	{
		const ZoneType& zone_it = *zit;

		// do not compare against the begin zone; its pointless
		if(zone_it.id() == BeginZoneType::begin_zone_id) continue;

		std::vector<Segment>::const_iterator it1 = zone.segments_begin();
		std::vector<Segment>::const_iterator it2 = zone_it.segments_begin();
		while(it1 != zone.segments_end() && it2 != zone_it.segments_end())
		{
			const Segment& s1 = *it1; const Segment& s2 = *it2;
			if(s1.sequence() < s2.sequence()) { it1++; it2 = zone_it.segments_begin(); continue; }
			else if(s2.sequence() < s1.sequence()) { it2++; continue; }
			if(zone.id() != BeginZoneType::begin_zone_id)
			{
				if(s2.begin() < s1.begin()) { it2++; continue; }
				else if(s1.begin() == s2.begin()) { it2++; continue; }
			}

			const typename ZoneSegmentsGraph::vertex_iterator& vit = transitions_graph.hasVertex(s1);
			if(vit != transitions_graph.vertex_end())
			{
				const typename ZoneSegmentsGraph::vertex_iterator::adjacent_iterator& ait = vit.adjacent_begin();
				if(ait != vit.adjacent_end())
				{
					if(s2.begin() < (*ait).begin())
					{
						transitions_graph.deleteEdge(s1, *ait);
						transitions_graph.addEdge(s1, s2, zone_it.id());
					}
				}
			}
			else transitions_graph.addEdge(s1, s2, zone_it.id());
			it2++;
		}
	}

	// return set
	Transitions adjacents_transitions;

	// for each zone, compute a transition graph
	typename ZoneSegmentsGraph::relation_iterator rit;
	for(rit = transitions_graph.relation_begin(); rit != transitions_graph.relation_end(); ++rit)
	{
		const typename ZoneSegmentsGraph::relation& r = *rit;
		TransitionGraph tg;

		typename Transitions::iterator tit;
		if((tit = adjacents_transitions.find(r.value())) != adjacents_transitions.end()) tg = (*tit).second;

		const unsigned int t_seq = r.first().sequence();
		unsigned int t_beg;
		unsigned int t_len;

		if(zone.id() == BeginZoneType::begin_zone_id)
		{
			t_beg = 0;
			t_len = r.second().begin();
		}
		else if(r.value() == EndZoneType::end_zone_id)
		{
			t_beg = r.first().end() + 1;
			t_len = r.second().begin() - r.first().end() - 1;
		}
		else
		{
			t_beg = r.first().end() + 1;
			t_len = r.second().begin() - r.first().end() - 1;
		}

		Segment segmentTransition(t_seq, t_beg, t_len);
		tg.addEdge(r.first(), r.second(), segmentTransition);

		adjacents_transitions[r.value()] = tg;
	}

	return adjacents_transitions;
}

#endif
