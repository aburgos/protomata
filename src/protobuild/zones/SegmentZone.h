
#ifndef SEGMENTZONE_H
#define SEGMENTZONE_H

#include "Zone.h"
#include "Segment.h"
#include "columns/SiteColumn.h"

/*!
A Zone that holds information for its 'Segment'. This means we can traverse the set of segments 'composing' a zone. The type of Column it contains should be SiteColumn, or some derive class of it.
*/
template <typename CntT, template <typename T> class ColT>
class SegmentZone: public Zone<CntT, ColT>
{
  public:
	typedef ColT<CntT> ColumnType;
	typedef Zone<CntT, ColT> ZoneType;

  protected:
	MultiSequence m_ms;
	std::vector<Segment> m_segments;

  public:
	SegmentZone() {}
	SegmentZone(unsigned int id, const MultiSequence& ms): Zone<CntT, ColT>(id), m_ms(ms) {}

	virtual bool add(const ColumnType&);

	typedef typename std::vector<Segment>::iterator segments_iterator;
	segments_iterator segments_begin() { return m_segments.begin(); }
	segments_iterator segments_end() { return m_segments.end(); }

	typedef typename std::vector<Segment>::const_iterator segment_const_iterator;
	segment_const_iterator segments_begin() const { return m_segments.begin(); }
	segment_const_iterator segments_end() const { return m_segments.end(); }
};

template <typename CntT, template <typename T> class ColT>
bool SegmentZone<CntT, ColT>::add(const ColumnType& site_column)
{
	if(!this->m_columns.empty())
	{
		const ColumnType& last_column = this->m_columns.back();
		if(site_column.sequences() != last_column.sequences()) return false;

		SitePartitionElement::const_iterator it1 = site_column.sites_begin();
		SitePartitionElement::const_iterator it2 = last_column.sites_begin();
		while(it1 != site_column.sites_end())
		{
			if((*it1).sequence() != (*it2).sequence() || (*it1).position() != (*it2).position() + 1) break;
			++it1;
			++it2;
		}
		if(it1 == site_column.sites_end() && it2 == last_column.sites_end())
		{
			this->m_columns.push_back(site_column);
			for(std::vector<Segment>::iterator it = m_segments.begin(); it != m_segments.end(); ++it)
			{
				const Segment& segment = *it;
				*it = Segment(segment.sequence(), segment.begin(), segment.length() + 1);
			}
		}
		else return false;
	}
	else
	{
		this->m_columns.push_back(site_column);
		for(typename ColumnType::site_const_iterator it = site_column.sites_begin(); it != site_column.sites_end(); ++it)
		{
			const Site& site = *it;
			m_segments.push_back(Segment(site.sequence(), site.position(), 1));
		}
	}

	return true;
}

#endif
