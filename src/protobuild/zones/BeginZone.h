
#ifndef BEGINZONE_H
#define BEGINZONE_H

#include "SegmentZone.h"

/*!
A special Zone in a ProtoGraph, with a fix id which can be accessed publicly. It is a segment zone for convenience for building the ProtoGraph by ProtoGraphBuilder, but segments are meaningless here.
*/
template <typename CntT, template <typename T> class ColT>
class BeginZone: public SegmentZone<CntT, ColT>
{
  public:
	static const unsigned int begin_zone_id = 1;
	BeginZone(const MultiSequence& ms);
};

template <typename CntT, template <typename T> class ColT>
BeginZone<CntT, ColT>::BeginZone(const MultiSequence& ms): SegmentZone<CntT, ColT>(begin_zone_id, ms)
{
	for(unsigned int i = 0; i < this->m_ms.size(); i++)
	{
		Segment segment(i, 0, 0);
		this->m_segments.push_back(segment);
	}
}

#endif
