
#ifndef ZONE_H
#define ZONE_H

#include <map>
#include <vector>
#include "Segment.h"
#include "columns/Column.h"

/*!
A vector of some type of Column. The first column is the one more to the 'left' in a visual representation.
*/
template <typename CntT, template <typename T> class ColT>
class Zone
{
  private:
	typedef ColT<CntT> ColumnType;
	typedef Zone<CntT, ColT> ZoneType;

  protected:
	unsigned int m_id;
	std::vector<ColumnType> m_columns;

  public:
	Zone() {}
	Zone(unsigned int id): m_id(id) {}
	virtual ~Zone() {}

	unsigned int id() const { return m_id; }
	unsigned int size() const { return m_columns.size(); }

	virtual bool add(const ColumnType& col) { m_columns.push_back(col); return true; }

	bool operator<(const ZoneType& zone) const { return (id() < zone.id()); }
	bool operator==(const ZoneType& zone) const { return (id() == zone.id()); }
	bool operator!=(const ZoneType& zone) const { return !operator==(zone); }

	typedef typename std::vector<ColumnType>::iterator column_iterator;
	column_iterator columns_begin() { return m_columns.begin(); }
	column_iterator columns_end() { return m_columns.end(); }

	typedef typename std::vector<ColumnType>::const_iterator column_const_iterator;
	column_const_iterator columns_begin() const { return m_columns.begin(); }
	column_const_iterator columns_end() const { return m_columns.end(); }
};

#endif
