
#ifndef ENDZONE_H
#define ENDZONE_H

#include "SegmentZone.h"

/*!
A special Zone in a ProtoGraph, with a fix id which can be accessed publicly. It is a segment zone for convenience for building the ProtoGraph by ProtoGraphBuilder, but segments are meaningless here.
*/
template <typename CntT, template <typename T> class ColT>
class EndZone: public SegmentZone<CntT, ColT>
{
  public:
	static const unsigned int end_zone_id = 2;
	EndZone(const MultiSequence& ms);
};

template <typename CntT, template <typename T> class ColT>
EndZone<CntT, ColT>::EndZone(const MultiSequence& ms): SegmentZone<CntT, ColT>(end_zone_id, ms)
{
	for(MultiSequence::const_iterator it = this->m_ms.begin(); it != this->m_ms.end(); ++it)
	{
		const Sequence& seq = *it;
		Segment segment(seq.id(), seq.length(), 0);
		this->m_segments.push_back(segment);
	}
}

#endif
