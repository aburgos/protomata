
#ifndef PROTOXMLGENERATOR_H
#define PROTOXMLGENERATOR_H

#include "pugixml.hpp"
#include "lib/DiGraph.h"
#include "ProtobuildParameters.h"
#include "alignments/LocalAlignment_UFImp.h"
#include "ProtoGraph.h"

/*!
Generates the output for proto file in XML format.
*/
template <typename ProtoGraphType>
class ProtoXmlGenerator
{
  private:
	typedef typename ProtoGraphType::ZoneType ZoneType;
	typedef typename ProtoGraphType::TransitionType TransitionType;

	const MultiSequence& m_ms;
	void output(const ZoneType&, pugi::xml_node&);
	void output(const TransitionType&, pugi::xml_node&);

  public:
	//! The constructor first outputs every block and after every transition between the blocks.
	ProtoXmlGenerator(const ProtobuildParameters&, const MultiSequence&, const ProtoGraphType&);
};

template <typename ProtoGraphType>
ProtoXmlGenerator<ProtoGraphType>::ProtoXmlGenerator(const ProtobuildParameters& parameters, const MultiSequence& ms, const ProtoGraphType& pg): m_ms(ms)
{
	pugi::xml_document doc;
	pugi::xml_node main_node = doc.append_child("weighted_protomata");
	main_node.append_attribute("version") = VERSION;

	pugi::xml_node resume_node = main_node.append_child("resume");
	resume_node.append_attribute("zones") = pg.zones();
	resume_node.append_attribute("edges") = pg.transitions();

	pugi::xml_node parameters_node = resume_node.append_child("parameters");
	for(ProtobuildParameters::const_iterator it = parameters.begin(); it != parameters.end(); ++it)
	{
		const std::string& flag = (*it).first;
		const std::string& value = (*it).second;

		pugi::xml_node param_node = parameters_node.append_child("param");
		param_node.append_attribute("f") = flag.c_str();
		if(!value.empty()) param_node.append_attribute("v") = value.c_str();
	}

	pugi::xml_node zones_node = main_node.append_child("zones");

	// output all zones
	for(typename ProtoGraphType::zone_const_iterator it = pg.zone_begin(); it != pg.zone_end(); ++it)
	{
		const ZoneType& zone = *it;
		output(zone, zones_node);
	}

	pugi::xml_node edges_node = main_node.append_child("edges");

	// output all transitions between zones
	typename ProtoGraphType::relation_const_iterator it;
	for(it = pg.relation_begin(); it != pg.relation_end(); ++it)
	{
		const TransitionType& zt = (*it).value();
		output(zt, edges_node);
	}

	doc.save_file(parameters.output().c_str());
}

template <typename ProtoGraphType>
void ProtoXmlGenerator<ProtoGraphType>::output(const ZoneType& zone, pugi::xml_node& main_node)
{
	if(zone.id() == BeginZone<void, Column>::begin_zone_id || zone.id() == EndZone<void, Column>::end_zone_id)
	{
		pugi::xml_node zone_node = main_node.append_child("zone");
		zone_node.append_attribute("id") = zone.id();
		zone_node.append_attribute("columns") = 0;
	}
	else
	{
		pugi::xml_node zone_node = main_node.append_child("zone");
		zone_node.append_attribute("id") = zone.id();
		zone_node.append_attribute("columns") = zone.size();

		for(typename ZoneType::column_const_iterator it = zone.columns_begin(); it != zone.columns_end(); ++it)
		{
			const typename ProtoGraphType::ColumnType& column = *it;

			pugi::xml_node column_node = zone_node.append_child("column");
			column_node.append_attribute("id") = column.id();

			if(!column.group().empty()) column_node.append_attribute("group") = column.group().c_str();

			// add all the aminoacid counts
			typename ProtoGraphType::ColumnType::counts_const_iterator it;
			for(it = column.counts_begin(); it != column.counts_end(); ++it)
			{
				const typename ProtoGraphType::ColumnType::CountPair& sp = *it;
				const char aminoacid = (*it).first;
				pugi::xml_node aminoacid_node = column_node.append_child("aminoacid");
				std::string letter; letter.push_back(sp.first);
				aminoacid_node.append_attribute("letter") = letter.c_str();
				aminoacid_node.append_attribute("count") = column.count(aminoacid);
			}
		}
	}
}

template <typename ProtoGraphType>
void ProtoXmlGenerator<ProtoGraphType>::output(const TransitionType& zt, pugi::xml_node& main_node)
{
	pugi::xml_node edge_node = main_node.append_child("edge");
	edge_node.append_attribute("id") = zt.id();
	edge_node.append_attribute("source") = zt.source_id();
	edge_node.append_attribute("target") = zt.target_id();
	edge_node.append_attribute("type") = zt.type().c_str();

	for(typename TransitionType::relation_const_iterator it = zt.begin(); it != zt.end(); ++it)
	{
		const typename TransitionType::TransitionGraph::relation& r = *it;
		const Segment& first = r.first();
		unsigned int seq = first.sequence();
		pugi::xml_node subseq_node = edge_node.append_child("subsequence");
		subseq_node.append_attribute("string") = m_ms[r.value()].c_str();
		subseq_node.append_attribute("weight") = m_ms[seq].weight();
	}
}

#endif
