
#ifndef PROTOMATADOTGENERATOR_H
#define PROTOMATADOTGENERATOR_H

#include <limits>
#include "ProtoGraph.h"
#include "alignments/LocalAlignment_UFImp.h"

/*!
Generates the output for plma file in dot format.
*/
template <typename ProtoGraphType>
class ProtomataDotGenerator
{
  private:
	typedef typename ProtoGraphType::ZoneType ZoneType;
	typedef typename ProtoGraphType::TransitionType TransitionType;

	//! The sequences.
	const MultiSequence& m_ms;
	//! The output file stream.
	std::ofstream output;
	void output_zone(const ZoneType&);
	void output_transition(const TransitionType&);

  public:
	//! The constructor first outputs every block and after every transition between the blocks. 
	ProtomataDotGenerator(const std::string&, const MultiSequence&, const ProtoGraphType&);
};

template <typename ProtoGraphType>
ProtomataDotGenerator<ProtoGraphType>::ProtomataDotGenerator(const std::string& output_file, const MultiSequence& ms, const ProtoGraphType& pg): m_ms(ms)
{
	output.open(output_file.c_str());

	// create the graph
	//output << "// File generated with protobuild version " << VERSION << "\n";
	output << "DiGraph G\n{\n";
	output << "\trankdir = LR;\n";
	output << "\tcenter = true;\n";
	output << "\tconcentrate = false;\n";
	output << "\tnode [shape = record, fontname = monospace];\n";
	output << "\tedge [color = green, style=\"setlinewidth(2)\", fontcolor = blue, weight = 1, headport = w, tailport = e];\n\n";

	// output all zones
	for(typename ProtoGraphType::zone_const_iterator it = pg.zone_begin(); it != pg.zone_end(); ++it)
	{
		const ZoneType& zone = *it;
		output_zone(zone);
	}

	// output all transitions between zones
	typename ProtoGraphType::relation_const_iterator it;
	for(it = pg.relation_begin(); it != pg.relation_end(); ++it)
	{
		const TransitionType& zt = (*it).value();
		output_transition(zt);
	}

	output << "}";
	output.close();
}

template <typename ProtoGraphType>
void ProtomataDotGenerator<ProtoGraphType>::output_zone(const ZoneType& zone)
{
	if(zone.id() == BeginZone<void, Column>::begin_zone_id)
	{
		output << "\t" << BeginZone<void, Column>::begin_zone_id;
		output << " [color = red, fontcolor = black, label = \"START\"];\n";
	}
	else if(zone.id() == EndZone<void, Column>::end_zone_id)
	{
		output << "\t" << EndZone<void, Column>::end_zone_id;
		output << " [color = red, fontcolor = black, label = \"END\"];\n\n";
	}
	else
	{
		std::map<unsigned int, std::set<char> > column_amino_acids;
		for(typename ZoneType::segment_const_iterator sit = zone.segments_begin(); sit != zone.segments_end(); ++sit)
		{
			const Segment& segment = *sit;
			for(unsigned int column = 0; column < segment.length(); column++)
			{
				const char current_aa = m_ms[Site(segment.sequence(), segment.begin() + column)];
				std::set<char> current_amino_acids;
				if(column_amino_acids.find(column) != column_amino_acids.end())
					current_amino_acids = (*(column_amino_acids.find(column))).second;
				current_amino_acids.insert(current_aa);
				column_amino_acids[column] = current_amino_acids;
			}
		}

		std::string formatted_output;
		std::map<unsigned int, std::set<char> >::iterator cit;
		for(cit = column_amino_acids.begin(); cit != column_amino_acids.end(); ++cit)
		{
			const std::set<char>& current_amino_acids = (*cit).second;
			std::set<char>::const_iterator ait;
			for(ait = current_amino_acids.begin(); ait != current_amino_acids.end(); ++ait)
			{
				formatted_output.push_back(*ait);
				formatted_output.append("\\n");
			}
			formatted_output.push_back('|');
		}

		// erase last '|' inserted
		if(column_amino_acids.begin() != column_amino_acids.end())
			formatted_output = formatted_output.erase(formatted_output.length() - 1);
		output << "\t" << zone.id() << " [shape = record, color = red, label = \"{" << formatted_output << "}\"];\n";
	}
}

template <typename ProtoGraphType>
void ProtomataDotGenerator<ProtoGraphType>::output_transition(const TransitionType& zt)
{
	if(zt.type() == "Gap")
	{
		unsigned int gap_begin = std::numeric_limits<unsigned int>::max();
		unsigned int gap_end = 0;

		for(typename TransitionType::relation_const_iterator rit = zt.begin(); rit != zt.end(); ++rit)
		{
			const Segment& first = (*rit).first();
			const Segment& second = (*rit).second();

			unsigned int diff = second.begin() - first.end() - 1;
			if(diff < gap_begin) gap_begin = diff;
			if(diff > gap_end) gap_end = diff;
		}

		if(gap_begin == 0 && gap_end == 0)
		{
			output << "\t" << zt.source_id() << " -> " << zt.target_id() << " [weight = 10];\n";
		}
		else
		{
			std::stringstream ss;
			ss << "(";

			if(gap_begin == gap_end)
				ss << gap_begin << ")";
			else
				ss << gap_begin << "," << gap_end << ")";

			std::string label;
			ss >> label;

			output << "\t" << zt.source_id() << " -> " << zt.target_id();
			output << " [color = green, weight = 5, fontcolor = blue, label = \"" << label << "\"];\n";
		}
	}
	else if(zt.type() == "Exception")
	{
		unsigned int exception_begin = std::numeric_limits<unsigned int>::max();
		unsigned int exception_end = 0;

		for(typename TransitionType::relation_const_iterator rit = zt.begin(); rit != zt.end(); ++rit)
		{
			const Segment& first = (*rit).first();
			const Segment& second = (*rit).second();

			unsigned int diff = second.begin() - first.end() - 1;
			if(diff < exception_begin) exception_begin = diff;
			if(diff > exception_end) exception_end = diff;
		}

		std::stringstream ss;
		ss << "(";

		if(exception_begin == exception_end)
			ss << exception_begin << ")";
		else
			ss << exception_begin << "," << exception_end << ")";

		std::string label;
		ss >> label;

		output << "\t" << zt.source_id() << " -> " << zt.target_id();
		output << " [style = dotted, color = grey30, weight = 1, fontcolor = grey30, label = \"" << label << "\"];\n";
	}
}

#endif
