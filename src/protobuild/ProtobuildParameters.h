
#ifndef PROTOBUILDPARAMETERS_H
#define PROTOBUILDPARAMETERS_H

#include "Parameters.h"
#include "FileNameSearcher.h"
#include "ProtobuildAllowedParameters.h"

class ProtobuildParameters: public virtual Parameters
{
  protected:
	ProtobuildAllowedParameters allowed;

	TCLAP::ValueArg<std::string> m_protobuild_input;
	TCLAP::ValueArg<std::string> m_protobuild_output;
	TCLAP::ValueArg<std::string> m_protobuild_tag;
	TCLAP::ValueArg<std::string> m_plma_dot_attributes;
	TCLAP::ValueArg<std::string> m_pseudocounts;
	TCLAP::ValueArg<std::string> m_components;
	TCLAP::ValueArg<std::string> m_groups;
	TCLAP::ValueArg<std::string> m_plma_dot_output;
	TCLAP::ValueArg<std::string> m_proto_dot_output;

	TCLAP::ValueArg<double> m_quorum;
	TCLAP::ValueArg<double> m_weight_percentage;
	TCLAP::ValueArg<double> m_group_threshold;
	TCLAP::ValueArg<double> m_sigma_threshold;
	TCLAP::SwitchArg m_weight_sequences;
	TCLAP::ValueArg<std::string> m_weights_input;
	TCLAP::SwitchArg m_no_expansion;
	TCLAP::SwitchArg m_plma_dot;
	TCLAP::SwitchArg m_proto_dot;
	TCLAP::SwitchArg m_short_tags;

	virtual void sanity_checks();
	virtual void initialize_values();
	virtual void add_string_parameters();
	virtual std::string compute_output_file(const std::string&) const;

  public:
	ProtobuildParameters();
	virtual ~ProtobuildParameters() {}

	virtual void parse(int argc, char **argv);
	virtual void add_arguments(TCLAP::CmdLine&);

	virtual std::string input() const { return m_protobuild_input.getValue(); }
	virtual std::string output() const;
	virtual std::string tag() const { return m_protobuild_tag.getValue(); }
	std::string plma_dot_attributes_file() const { return m_plma_dot_attributes.getValue(); }
	virtual std::string pseudocounts() const;
	virtual std::string components_file() const;
	std::string groups_file() const;
	virtual std::string plma_dot_output() const;
	virtual std::string proto_dot_output() const;
	virtual std::string weights_input() const { return m_weights_input.getValue(); }

	double quorum() const { return m_quorum.getValue(); }
	double weight_percentage() const { return m_weight_percentage.getValue(); }
	double group_threshold() const { return m_group_threshold.getValue(); }
	double sigma_threshold() const { return m_sigma_threshold.getValue(); }
	bool weight_sequences() const { return m_weight_sequences.getValue(); }
	bool no_expansion() const { return m_no_expansion.getValue(); }
	bool plma_dot() const { return (m_plma_dot.getValue() || m_plma_dot_output.isSet()); }
	bool proto_dot() const { return (m_proto_dot.getValue() || m_proto_dot_output.isSet()); }
	bool short_tags() const { return m_short_tags.getValue(); }
};

#endif
