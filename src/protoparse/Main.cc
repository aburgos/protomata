
#include <time.h>
#include <iostream>
#include "ProtoParse.h"
#include "ProtoparseParameters.h"

int main(int argc, char **argv)
{
	//double start, end;
	//start = (double)clock();

	ProtoparseParameters parameters;
	parameters.parse(argc, argv);

	if(parameters.use_integers()) ProtoParse<int> protoparse(parameters);
	else ProtoParse<double> protoparse(parameters);

	//end = (double)clock();
	//double time_elapsed = double(end - start) / CLOCKS_PER_SEC;
	//std::cout << std::endl << "Time elapsed: " << time_elapsed << " secs" << std::endl;
	return 0;
}
