
#include "ProtoparseParameters.h"

ProtoparseParameters::ProtoparseParameters():
	ProtoscanParameters(),
	m_dot_file(TCLAP::ValueArg<std::string>("p", "parsed-dot", "Adds the parsed sequences into the plma dot file.", false, "", "dot_filename")),
	m_parsed_dot_attributes(TCLAP::ValueArg<std::string>("a", "parsed-dot-attributes", "A file specifying attributes for the parsed dot of the scanned sequences.", false, "", "attr_file"))
{}

void ProtoparseParameters::parse(int argc, char **argv)
{
	TCLAP::CmdLine cmd("Parse a set of sequences for scores using a Protomata model.", ' ', VERSION);
	ProtoparseParameters::add_arguments(cmd);
	cmd.parse(argc, argv);
	ProtoscanParameters::initialize_values();
	ProtoscanParameters::sanity_checks();
	ProtoscanParameters::add_string_parameters();
}

void ProtoparseParameters::add_arguments(TCLAP::CmdLine& cmd)
{
	cmd.add(m_parsed_dot_attributes);
	cmd.add(m_dot_file);
	ProtoscanParameters::add_arguments(cmd);
}

std::string ProtoparseParameters::compute_output_file(const std::string& filename) const
{
	size_t lst_path_pos = filename.find_last_of('/');
	size_t beg_base_pos = (lst_path_pos == std::string::npos ? 0 : (lst_path_pos + 1));
	size_t lst_base_pos = filename.find_last_of('.');
	size_t end_base_pos = (lst_base_pos == std::string::npos ? (filename.length() - 1) : (lst_base_pos - 1));
	std::string basename = filename.substr(beg_base_pos, end_base_pos - beg_base_pos + 1);

	std::ostringstream ss;
	ss << basename;
	if(score_threshold() != -std::numeric_limits<double>::infinity()) ss << "_mss" << score_threshold();
	if(m_lobound.isSet()) ss << "_lo" << lobound();
	if(m_upbound.isSet()) ss << "_up" << upbound();
	if(allow_X()) ss << "_Xst:" << m_xstr.getValue();
	if(!allow_X()) ss << "_noX";
	if(exceptions_as_gaps()) ss << "_eag";
	if(allow_partial_sequence_match()) ss << "_psm";
	if(allow_partial_model_match()) ss << "_pmm";
	if(use_integers()) ss << "_ui";
	if(significativity()) ss << "_sig";
	ss << "_ss:" << m_sscr.getValue();
	if(!pseudocounts().empty()) ss << "_" << pseudocounts();
	if(!components_file().empty())
	{
		size_t pos = components_file().find_last_of('/');
		std::string components_basename = components_file().substr(pos + 1, components_file().length());
		ss << "_comp:" << components_basename;
	}
	if(!sequences().empty())
	{
		size_t pos = sequences().find_last_of('/');
		std::string scan_basename = sequences().substr(pos + 1, sequences().length());
		ss << "_p:" << scan_basename;
	}
	if(!dot_attributes_file().empty())
	{
		const std::string& dot_attr = dot_attributes_file();
		size_t pos = dot_attr.find_last_of('/');
		std::string dot_attr_basename = dot_attr.substr(pos + 1, dot_attr.length());
		ss << "_a:" << dot_attr_basename;
	}

	ss << "_parse.txt";
	return ss.str();
}

std::string ProtoparseParameters::output() const
{
	if(m_protoscan_output.getValue().empty()) return ProtoparseParameters::compute_output_file(input());
	else return m_protoscan_output.getValue();
}
