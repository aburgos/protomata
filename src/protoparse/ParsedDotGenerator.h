
#ifndef PARSEDDOTGENERATOR_H
#define PARSEDDOTGENERATOR_H

#include <map>
#include <vector>
#include <fstream>
#include "Sequence.h"

/*!
Generates the output for the parsed sequences in dot format.
*/
template <class TTran>
class ParsedDotGenerator
{
  public:
	typedef std::vector<std::pair<unsigned int, unsigned int> > ZoneColumnPath;
	//! The constructor first outputs every block and after every transition between the blocks. 
	ParsedDotGenerator(const std::string&, const std::map<Sequence, ZoneColumnPath>&);
	~ParsedDotGenerator() {}

  private:
	unsigned int getNumberColumns(const ZoneColumnPath&, unsigned int);
};

template <class TTran>
ParsedDotGenerator<TTran>::ParsedDotGenerator(const std::string& output_file, const std::map<Sequence, ZoneColumnPath>& paths)
{
	std::ofstream output(output_file.c_str());

	// create the graph
	//output << "// File generated with protoparse version " << VERSION << "\n";
	output << "DiGraph G\n{\n";
	output << "\trankdir = LR;\n";
	output << "\tconcentrate = true;\n";
	output << "\tnode [shape = record, fontname = monospace];\n";
	output << "\tedge [color = green, fontcolor = blue, weight = 1, headport = w, tailport = e];\n";

	std::map<Sequence, std::pair<unsigned int, unsigned int> > last_nodes;

	// begin cluster
	output << "\tsubgraph cluster_0" << "\n\t{\n";
	output << "\t\tnode [shape = record, color = blue, fontcolor = black];\n";
	output << "\t\tcolor = white;\n";

	typename std::map<Sequence, ZoneColumnPath>::const_iterator it;
	for(it = paths.begin(); it != paths.end(); ++it)
	{
		Sequence sequence = (*it).first;
		ZoneColumnPath path = (*it).second;

		last_nodes.insert(std::make_pair(sequence, std::make_pair(sequence.id(), 0)));
		output << "\t\t\"(" << sequence.id() << ", 0)\" [label = \"" << sequence.tag() << "\"];\n";
	}

	output << "\t}\n";

	// end cluster
	output << "\tsubgraph cluster_1" << "\n\t{\n";
	output << "\t\tnode [shape = record, color = green, fontcolor = green];\n";
	output << "\t\tcolor = white;\n";

	for(it = paths.begin(); it != paths.end(); ++it)
	{
		Sequence sequence = (*it).first;
		ZoneColumnPath path = (*it).second;

		output << "\t\t\"(" << sequence.id() << ", 1)\" [label = \"" << (sequence.id() + 1) << "\"];\n";
	}

	output << "\t}\n";

	// get all zones used
	std::set<unsigned int> zones;
	for(it = paths.begin(); it != paths.end(); ++it)
	{
		ZoneColumnPath path = (*it).second;
		for(typename ZoneColumnPath::const_iterator ait = path.begin(); ait != path.end(); ait++)
		{
			unsigned int current_zone = (*ait).first;
			if(current_zone != 0 && current_zone != 1)
				zones.insert(current_zone);
		}
	}

	unsigned int cluster_last_zone = 0;

	// zone clusters
	for(std::set<unsigned int>::iterator zit = zones.begin(); zit != zones.end(); ++zit)
	{
		unsigned int cluster_zone = *zit;
		if(cluster_zone == 0 || cluster_zone == 1) continue;

		std::vector<std::string> transitions;

		// zone cluster
		output << "\tsubgraph cluster_" << cluster_zone << "\n\t{\n";
		output << "\t\tnode [shape = record, style = filled, color = chartreuse, fontcolor = black];\n";
		output << "\t\tcolor = chocolate1;\n";

		for(it = paths.begin(); it != paths.end(); ++it)
		{
			Sequence sequence = (*it).first;
			ZoneColumnPath path = (*it).second;

			unsigned int last_zone = 0;
			unsigned int last_position = getNumberColumns(path, 0);

			for(typename ZoneColumnPath::const_iterator ait = path.begin(); ait != path.end(); ait++)
			{
				unsigned int zone = (*ait).first;
				if(zone != cluster_zone)
				{
					if(zone > 1 && last_zone != zone)
					{
						last_zone = zone;
						last_position += getNumberColumns(path, zone);
					}
					continue;
				}

				unsigned int columns = getNumberColumns(path, zone);

				// store transition for later output
				std::stringstream ss;
				ss << "\t\"(" << last_nodes[sequence].first << ", " << last_nodes[sequence].second << ")\" -> ";
				ss << "\"(" << sequence.id() << ", " << cluster_zone << ")\"";
				ss << " [label = \"" << (sequence.id() + 1) << "\"];\n";

				std::string transition = ss.str();
				transitions.push_back(transition);

				last_nodes[sequence] = std::make_pair(sequence.id(), cluster_zone);

				output << "\t\t\"(" << sequence.id() << ", " << cluster_zone;
				output << ")\" [label = \"" << sequence.data().substr(last_position, columns) << "\"];\n";

				break;
			}
		}

		output << "\t}\n";
		cluster_last_zone = cluster_zone;

		for(std::vector<std::string>::const_iterator sit = transitions.begin(); sit != transitions.end(); sit++)
			output << *sit;
	}

	std::map<Sequence, std::pair<unsigned int, unsigned int> >::iterator lit;
	for(lit = last_nodes.begin(); lit != last_nodes.end(); ++lit)
	{
		output << "\t\"(" << (*lit).second.first << ", " << (*lit).second.second << ")\" -> ";
		output << "\"(" << (*lit).first.id() << ", 1)\"";
		output << " [label = \"" << ((*lit).first.id() + 1) << "\"];\n";
	}

	output << "}";
}

template <class TTran>
unsigned int ParsedDotGenerator<TTran>::getNumberColumns(const ZoneColumnPath& path, unsigned int zone)
{
	unsigned int columns = 0;
	for(typename ZoneColumnPath::const_iterator ait = path.begin(); ait != path.end(); ait++)
	{
		unsigned int current_zone = (*ait).first;
		if(zone != current_zone) continue;
		else columns++;
	}

	return columns;
}

#endif
