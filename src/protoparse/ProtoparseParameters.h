
#ifndef PROTOPARSEPARAMETERS_H
#define PROTOPARSEPARAMETERS_H

#include "ProtoscanParameters.h"

class ProtoparseParameters: public ProtoscanParameters
{
  protected:
	TCLAP::ValueArg<std::string> m_dot_file;
	TCLAP::ValueArg<std::string> m_parsed_dot_attributes;

	virtual std::string compute_output_file(const std::string&) const;

  public:
	ProtoparseParameters();
	virtual ~ProtoparseParameters() {}

	virtual void parse(int argc, char **argv);
	virtual void add_arguments(TCLAP::CmdLine&);

	virtual std::string output() const;
	std::string dot_file() const { return m_dot_file.getValue(); }
	std::string dot_attributes_file() const { return m_parsed_dot_attributes.getValue(); }
};

#endif
