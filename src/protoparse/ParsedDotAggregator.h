
#ifndef PARSEDDOTAGGREGATOR_H
#define PARSEDDOTAGGREGATOR_H

#include <map>
#include <fstream>
#include "Sequence.h"
#include "zones/BeginZone.h"
#include "zones/EndZone.h"
#include "paths/AlignmentPath.h"
#include "SequencesAttributesParser.h"

/*!
Aggregates the parsed sequences to the parameter dot file.
*/
template <typename CntT>
class ParsedDotAggregator
{
  public:
	typedef std::pair<unsigned int, std::pair<unsigned int, unsigned int> > PositionZoneColumn;
	//! The constructor first outputs every block and after every transition between the blocks. 
	ParsedDotAggregator(const std::string&, const std::map<Sequence, AlignmentPath>&, const std::string&, const std::string& = "");
	~ParsedDotAggregator() {}
};

template <typename CntT>
ParsedDotAggregator<CntT>::ParsedDotAggregator(const std::string& output_file, const std::map<Sequence, AlignmentPath>& paths, const std::string& dot_file, const std::string& attr_file)
{
	std::ifstream input(dot_file.c_str());
	if(!input.good())
	{
		std::cerr << "Dot file " << dot_file << " could not be opened." << std::endl;
		exit(1);
	}
	std::ofstream output(output_file.c_str());

	const unsigned int begin_zone_id = BeginZone<void, Column>::begin_zone_id;
	const unsigned int end_zone_id = EndZone<void, Column>::end_zone_id;

	// get colors for transitions
	std::map<unsigned int, std::string> colors;
	if(!attr_file.empty())
	{
		SequencesAttributesParser attributes(attr_file);
		colors = attributes.colors();
	}
	else
	{
		for(std::map<Sequence, AlignmentPath>::const_iterator it = paths.begin(); it != paths.end(); ++it)
		{
			const Sequence& sequence = (*it).first;
			colors.insert(std::make_pair(sequence.id(), "green"));
		}
	}

	std::vector<std::string> transitions;
	// stores the nodes (in dot format) of each zone for later output
	std::map<unsigned int, std::vector<std::string> > zone2nodes;

	typename std::map<Sequence, AlignmentPath>::const_iterator it;
	for(it = paths.begin(); it != paths.end(); ++it)
	{
		const Sequence& sequence = (*it).first;
		const AlignmentPath& path = (*it).second;

		unsigned int last_zone = begin_zone_id;
		// stores the labels (in dot format) of each zone for later output
		std::map<unsigned int, std::string> zone2label;

		// store the gap between zones
		unsigned int last_position = 0;

		for(AlignmentPath::const_iterator ait = path.begin(); ait != path.end(); ++ait)
		{
			const unsigned int position = (*ait).first;
			const unsigned int zone = (*ait).second.first;

			if(zone2label.find(zone) == zone2label.end())
			{
				std::string label;
				zone2label[zone] = label;
			}
			zone2label[zone] += sequence[position];

			if(zone != last_zone)
			{
				if(last_zone == begin_zone_id) last_position = 0;

				// create the output for the transition
				std::stringstream ss;
				ss << "\t\"(" << (sequence.id() + 1) << ", " << last_zone << ")\" -> ";
				ss << "\"(" << (sequence.id() + 1) << ", " << zone << ")\"";
				if(position - last_position > 1)
				{
					ss << " [label = \"" << (sequence.id() + 1) << ": ";
					if(zone == end_zone_id) ss << (last_position + 1) << ".." << (position + 1) << "\"";
					else ss << (last_position + 1) << ".." << position << "\"";
				}
				else ss << " [label = \"" << (sequence.id() + 1) << "\"";

				// add corresponding color
				ss << ", color = \"" << colors[sequence.id()] << "\"];\n";

				std::string transition = ss.str();
				transitions.push_back(transition);

				std::stringstream nss;

				if(last_zone == begin_zone_id)
				{
					nss << "\t\t\"(" << (sequence.id() + 1) << ", " << begin_zone_id;
					nss << ")\" [label = \"" << sequence.tag() << "\", ";
					nss << "style = filled, fillcolor = grey80];\n";
				}
				else
				{
					// create the output for the node
					nss << "\t\t\"(" << (sequence.id() + 1) << ", " << last_zone;
					nss << ")\" [label = \"" << zone2label[last_zone] << "\"";
					nss << ", color = chartreuse];\n";
				}

				std::string node = nss.str();
				std::vector<std::string> zone_nodes = zone2nodes[last_zone];
				zone_nodes.push_back(node);
				zone2nodes[last_zone] = zone_nodes;

				last_zone = zone;
			}

			last_position = position + 1;
		}

		// add end zone's node
		std::stringstream fss;
		fss << "\t\t\"(" << (sequence.id() + 1) << ", " << end_zone_id;
		fss << ")\" [label = \"" << (sequence.id() + 1) << "\", ";
		fss << "style = filled, fillcolor = grey80];\n";
		std::string fnode = fss.str();
		std::vector<std::string> zone_nodes = zone2nodes[end_zone_id];
		zone_nodes.push_back(fnode);
		zone2nodes[end_zone_id] = zone_nodes;

		// check if the last zone of the path was not an end zone
		AlignmentPath::const_reverse_iterator rit = path.rbegin();
		const unsigned int seq_last_position = (*rit).first;
		const unsigned int zone = (*rit).second.first;
		if(zone != end_zone_id)
		{
			std::stringstream nss;
			nss << "\t\t\"(" << (sequence.id() + 1) << ", " << zone;
			nss << ")\" [label = \"" << zone2label[zone] << "\"";
			nss << ", color = chartreuse];\n";
			std::string node = nss.str();
			std::vector<std::string> zone_nodes = zone2nodes[zone];
			zone_nodes.push_back(node);
			zone2nodes[zone] = zone_nodes;

			// create the output for the transition
			std::stringstream ss;
			ss << "\t\"(" << (sequence.id() + 1) << ", " << zone << ")\" -> ";
			ss << "\"(" << (sequence.id() + 1) << ", " << end_zone_id << ")\"";
			if(sequence.length() - seq_last_position > 1)
			{
				ss << " [label = \"" << (sequence.id() + 1) << ": ";
				if(zone == end_zone_id) ss << (seq_last_position + 1) << ".." << (sequence.length() + 1) << "\"];\n";
				else ss << (seq_last_position + 1) << ".." << sequence.length() << "\"];\n";
			}
			else ss << " [label = \"" << (sequence.id() + 1) << "\"];\n";

			std::string transition = ss.str();
			transitions.push_back(transition);
		}
	}

	while(!input.eof())
	{
		std::string line;
		getline(input, line);

		std::istringstream iss(line, std::ios_base::in);
		std::string first_word;
		iss >> first_word;
		if(first_word == "subgraph")
		{
			std::string subgraph_name;
			iss >> subgraph_name;
			std::string zone = subgraph_name.substr(subgraph_name.find_last_of('_') + 1, subgraph_name.length());
			std::istringstream tss(zone, std::ios_base::in);
			unsigned int cluster_zone;
			tss >> cluster_zone;
			output << line << std::endl;

			if(zone2nodes.find(cluster_zone) != zone2nodes.end())
			{
				std::string new_line;
				getline(input, new_line);
				output << new_line << std::endl;
				getline(input, new_line);
				output << new_line << std::endl;
				getline(input, new_line);
				output << new_line << std::endl;

				std::vector<std::string> zone_nodes = zone2nodes[cluster_zone];
				for(std::vector<std::string>::iterator it = zone_nodes.begin(); it != zone_nodes.end(); ++it)
					output << *it;
			}
		}
		else output << line << std::endl;
	}

	// overwrite the closing '}' of the dot file
	long pos = output.tellp();
	output.seekp(pos - 2);

	// output all transitions
	for(std::vector<std::string>::const_iterator sit = transitions.begin(); sit != transitions.end(); sit++)
		output << *sit;

	output << "}";
	output.close();
}

#endif
