
#ifndef PROTOPARSE_H
#define PROTOPARSE_H

#include "scores/Viterbi_WithPath.h"
#include "WAGenerator.h"
#include "MultiSequence.h"
#include "Significativity.h"
//#include "ParsedDotGenerator.h"
#include "ParsedDotAggregator.h"
#include "ProtoparseParameters.h"
#include "ProtoParser.h"
#include "ProtoGraph.h"
#include "WAProperties.h"
#include "StatsPathGenerator.h"
#include "editors/ProtoGraphEditor.h"
#include "XmlPathAggregator.h"

/*!
Parse a set of sequences using a model passed by parameter as a wproto file to the constructor. It computes the score of each sequence, the significativity (if specified) and the path on the weighted automata.
*/
template <typename CntT>
class ProtoParse
{
  private:
	typedef Significativity::StatsPathVector StatsPathVector;
	typedef std::pair<unsigned int, std::pair<unsigned int, unsigned int> > PositionZoneColumn;
	typedef std::map<Sequence, AlignmentPath> SequencePath;

  public:
	ProtoParse() {}
	ProtoParse(const ProtoparseParameters&);

	SequencePath parse(const WAProperties<CntT>&, const ProtoparseParameters&);
	SequencePath parse(const WAProperties<CntT>&, const ProtoparseParameters&, const StatsPathVector&);

	//! Outputs the computed weighted automata (in matrix format) to the file specified.
	void output_wa(const std::string&, const WeightedAutomata<CntT>&);
	void output_parsed_sequences_dot(const ProtoparseParameters&, const SequencePath&) const;
};

template <typename CntT>
ProtoParse<CntT>::ProtoParse(const ProtoparseParameters& params)
{
	typedef ProtoParser<CntT, AttributedColumn> ProtoParserType;
	typedef typename ProtoParserType::ProtoGraphType ProtoGraphType;

	// parse proto file
	ProtoParserType pp(params.input());
	ProtoGraphType pg = pp.parse_zones_edges();

	ProtoGraphEditor<ProtoGraphType> pg_editor;

	// apply pseudocounts
	if(!params.pseudocounts().empty())
		pg_editor.add_pseudocounts(pg, params.pseudocounts(), params.components_file());

	// compute the right output score
	pg_editor.set_viterbi_values(pg, params.scan_score());

	// create a weighted automata
	WAGenerator<ProtoGraphType> wagen(pg, params.exceptions_as_gaps());
	if(params.output_wa()) output_wa(params.wa_file(), wagen.weighted_automata());

	SequencePath seq_paths;

	// parse sequences
	if(params.significativity())
	{
		// check if paths were already computed
		StatsPathVector paths = pp.parse_paths();
		if(paths.empty())
		{
			// compute all paths and add them to the XML file for future use
			StatsPathGenerator<ProtoGraphType> path_generator(pg);
			paths = path_generator.paths_copy();
			XmlPathAggregator agg(params.input(), paths);
		}
		seq_paths = parse(wagen.weighted_automata(), params, paths);
	}
	else seq_paths = parse(wagen.weighted_automata(), params);

	if(!params.dot_file().empty() && !seq_paths.empty())
		output_parsed_sequences_dot(params, seq_paths);
}

template <>
ProtoParse<int>::ProtoParse(const ProtoparseParameters& params)
{
	typedef ProtoParser<int, AttributedColumn> ProtoParserType;
	typedef ProtoParserType::ProtoGraphType ProtoGraphType;

	// parse proto file
	ProtoParserType pp(params.input());
	ProtoGraphType pg = pp.parse_zones_edges();

	// compute the right output score
	ProtoGraphEditor<ProtoGraphType> pg_editor;
	pg_editor.set_viterbi_values(pg, params.scan_score());

	// create a weighted automata
	WAGenerator<ProtoGraphType> wagen(pg, params.exceptions_as_gaps());
	if(params.output_wa()) output_wa(params.wa_file(), wagen.weighted_automata());

	// parse sequences
	parse(wagen.weighted_automata(), params);
}

template <typename CntT>
void ProtoParse<CntT>::output_wa(const std::string& wa_file, const WeightedAutomata<CntT>& wa)
{
	std::ofstream output(wa_file.c_str(), std::ios_base::out);
	output << wa << std::endl;
	output.close();
}

template <typename CntT>
typename ProtoParse<CntT>::SequencePath ProtoParse<CntT>::parse(const WAProperties<CntT>& wa, const ProtoparseParameters& parameters)
{
	const bool allow_X = parameters.allow_X();
	const bool apsm = parameters.allow_partial_sequence_match();
	const bool apmm = parameters.allow_partial_model_match();
	const double score_threshold = parameters.score_threshold();
	const bool compute_exponential = (parameters.scan_score() == probability) || (parameters.scan_score() == LR);
	const unsigned int lobound = parameters.lobound();
	const unsigned int upbound = parameters.upbound();

	// get sequences to parse
	const MultiSequence ms(parameters.sequences(), false, allow_X, lobound, upbound);
	// get transitions from the weighted automata and "modify" the automata for scanning requirements
	Viterbi_WithPath<CntT> sc(wa, apsm, apmm, allow_X, parameters.x_strategy());

	std::ofstream output(parameters.output().c_str());
	std::ostream& output_scores = (output.is_open() ? output : std::cout);

	output_scores << "# Generated using protoscan version " << VERSION << std::endl;
	output_scores << "#" << std::endl;
	output_scores << "# score" << "\t" << "significativity" << "\t" << "seq_length" << "\t" << "seq_tag" << std::endl;
	output_scores << "#" << std::endl;

	SequencePath paths;

	// parse each sequence of the sequences file
	for(MultiSequence::const_iterator it = ms.begin(); it != ms.end(); ++it)
	{
		const Sequence& seq = *it;

		//double seq_scan_start, seq_scan_end;
		//seq_scan_start = (double)clock();

		CntT seq_score = sc.score(seq);
		if(compute_exponential) seq_score = exp(seq_score);

		if(seq_score >= score_threshold)
		{
			output_scores << seq_score;
			output_scores << "\t-";
			output_scores << "\t(" << seq.length() << ")";
			output_scores << "\t" << seq.long_tag() << std::endl;

			// get scanned path (are they in reversed order?)
			const AlignmentPath& path = sc.path(seq);
			if(!path.empty()) paths.insert(std::make_pair(seq, path));

			output_scores << "AlignmentPath: " << std::make_pair(seq, path) << std::endl;
		}

		//seq_scan_end = (double)clock();
		//double seq_scan_time_elapsed = double(seq_scan_end - seq_scan_start) / CLOCKS_PER_SEC;
		//output_scores << "\t\t" << seq_scan_time_elapsed << " secs" << std::endl;
	}

	return paths;
}

template <typename CntT>
typename ProtoParse<CntT>::SequencePath ProtoParse<CntT>::parse(const WAProperties<CntT>& wa, const ProtoparseParameters& parameters, const StatsPathVector& stat_paths)
{
	const bool allow_X = parameters.allow_X();
	const bool apsm = parameters.allow_partial_sequence_match();
	const bool apmm = parameters.allow_partial_model_match();
	const double score_threshold = parameters.score_threshold();
	const bool compute_exponential = (parameters.scan_score() == probability) || (parameters.scan_score() == LR);
	const unsigned int lobound = parameters.lobound();
	const unsigned int upbound = parameters.upbound();

	// get sequences to parse
	const MultiSequence ms(parameters.sequences(), false, allow_X, lobound, upbound);
	// get transitions from the weighted automata and "modify" the automata for scanning requirements
	Viterbi_WithPath<CntT> sc(wa, apsm, apmm, allow_X, parameters.x_strategy());

	std::ofstream output(parameters.output().c_str());
	std::ostream& output_scores = (output.is_open() ? output : std::cout);

	output_scores << "# Generated using protoparse version " << VERSION << std::endl;
	output_scores << "#" << std::endl;
	output_scores << "# score" << "\t" << "significativity" << "\t" << "seq_length" << "\t" << "seq_tag" << std::endl;
	output_scores << "#" << std::endl;

	// create the object for computing significativity
	Significativity sig_calc;

	SequencePath paths;

	// parse each sequence of the sequences file
	for(MultiSequence::const_iterator it = ms.begin(); it != ms.end(); ++it)
	{
		const Sequence& seq = *it;

		//double seq_scan_start, seq_scan_end;
		//seq_scan_start = (double)clock();

		CntT seq_score = sc.score(seq);
		if(compute_exponential) seq_score = exp(seq_score);

		if(seq_score >= score_threshold)
		{
			output_scores << seq_score;
			output_scores << "\t" << sig_calc.significativity(seq, seq_score, stat_paths);
			output_scores << "\t(" << seq.length() << ")";
			output_scores << "\t" << seq.long_tag() << std::endl;

			const AlignmentPath& path = sc.path(seq);
			if(!path.empty()) paths.insert(std::make_pair(seq, path));

			output_scores << "AlignmentPath: " << std::make_pair(seq, path) << std::endl;
		}

		//seq_scan_end = (double)clock();
		//double seq_scan_time_elapsed = double(seq_scan_end - seq_scan_start) / CLOCKS_PER_SEC;
		//output_scores << "\t\t" << seq_scan_time_elapsed << " secs" << std::endl;
	}

	return paths;
}

template <typename CntT>
void ProtoParse<CntT>::output_parsed_sequences_dot(const ProtoparseParameters& parameters, const SequencePath& paths) const
{
	size_t pos_b = parameters.sequences().find_last_of('/');
	size_t pos_e = parameters.sequences().find_last_of('.');
	std::string new_output_file = parameters.sequences().substr(pos_b + 1, pos_e) + "_parser.dot";
	const std::string& attr_file = parameters.dot_attributes_file();
	ParsedDotAggregator<CntT> agg_dot(new_output_file, paths, parameters.dot_file(), attr_file);
}

#endif
