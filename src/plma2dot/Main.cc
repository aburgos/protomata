
#include <iostream>
#include "tclap/CmdLine.h"
#include "parsers/PlmaParser.h"
#include "ProtoGraphBuilder.h"
#include "PlmaDotGenerator.h"
#include "columns/SiteColumn.h"

int main(int argc, char **argv)
{
	TCLAP::CmdLine cmd("Generates a dot file for graphical output from a PLMA.", ' ', VERSION);
	TCLAP::ValueArg<std::string> input("i", "input", "Set plma file name.", true, "", "plma_filename");
	TCLAP::ValueArg<std::string> output("o", "output", "Set output file name.", false, "plma_basename.dot", "dot_filename");
	TCLAP::ValueArg<std::string> plma_dot_attributes("a", "plma-dot-attributes", "A file specifying attributes for the dot output of each sequence.", false, "", "attr_file");
	TCLAP::SwitchArg weight_sequences("w", "weight-sequences", "Each sequence will have a weight representing how unique it is on the set.", false);
	TCLAP::ValueArg<double> quorum("q", "quorum", "The minimum sum of the weights of the sequences for keeping a block.", false, 0, "value");
	TCLAP::ValueArg<double> weight_percentage("p", "percentage-quorum", "The minimum percentage for the sum of the weights of the sequences for keeping a block. The value should be in range [0,1].", false, 0, "value");
	TCLAP::ValueArg<std::string> display_dot("", "display-dot", "The file set will be executed in the background with the generated dot file as a parameter.", false, "", "command");
	TCLAP::SwitchArg short_tags("s", "short-headers", "Output truncated headers of sequences.", false);

	// add in reverse appereance order
	cmd.add(short_tags);
	cmd.add(display_dot);
	cmd.add(weight_percentage);
	cmd.add(quorum);
	cmd.add(weight_sequences);
	cmd.add(plma_dot_attributes);
	cmd.add(output);
	cmd.add(input);

	cmd.parse(argc, argv);

	// set output file name if it was not set
	std::string input_file = input.getValue();
	std::string output_file;
	if(output.isSet()) output_file = output.getValue();
	else
	{
		std::ostringstream ss;
		ss << input_file.substr(0, input_file.find_last_of('.'));
		if(weight_sequences.getValue()) ss << "_w";
		ss << "_q" << quorum.getValue() << "_p" << weight_percentage.getValue();
		if(short_tags.getValue()) ss << "_s";
		ss << "_plma.dot";
		output_file = ss.str();
	}

	PlmaParser parser(input_file);
	MultiSequence& ms = parser.multisequence();
	if(weight_sequences.getValue()) ms.weight_sequences();

	typedef ProtoGraphBuilder<LocalAlignment_UFImp, unsigned int, SiteColumn> PGBuilderType;
	typedef PGBuilderType::ProtoGraphType ProtoGraphType;

	PGBuilderType builder(ms, parser.generalized_alignment(), quorum.getValue(), weight_percentage.getValue());
	const ProtoGraphType& pg = builder.proto_graph();
	PlmaDotGenerator<ProtoGraphType>(output_file, ms, pg, plma_dot_attributes.getValue(), short_tags.getValue());

	if(!display_dot.getValue().empty())
	{
		std::string cmd = display_dot.getValue() + " " + output_file + "&";
		if(system(cmd.c_str())) std::cerr << "Error executing " << cmd << std::endl;
		else std::cout << cmd << std::endl;
	}

	return 0;
}
