
#include "SequencesAttributesParser.h"

SequencesAttributesParser::SequencesAttributesParser(const std::string& attr_file)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(attr_file.c_str());
    if(!result) { std::cerr << "Error loading file " << attr_file << std::endl; exit(1); }

	// search for the sequences first
	pugi::xml_node seqs_node = doc.child("sequences");
	if(!seqs_node) { std::cerr << "Error parsing file " << attr_file << std::endl; exit(1); }

	for(pugi::xml_node_iterator it = seqs_node.begin(); it != seqs_node.end(); ++it)
	{
		if(it->attribute("id"))
		{
			unsigned int seq = atoi(it->attribute("id").value());
			seq--;
			const std::string& color = it->attribute("color").value();
			m_colors.insert(std::make_pair(seq, color));
		}
		else if(it->attribute("range"))
		{
			const std::string& range = it->attribute("range").value();
			unsigned int beg = atoi((range.substr(0, range.find('-'))).c_str());
			beg--;
			unsigned int end = atoi((range.substr(range.find('-') + 1, range.length())).c_str());
			end--;
			const std::string& color = it->attribute("color").value();
			for(unsigned int i = beg; i < end + 1; i++)
				m_colors.insert(std::make_pair(i, color));
		}
	}
}
