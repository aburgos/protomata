
#ifndef SEQUENCESATTRIBUTESPARSER_H
#define SEQUENCESATTRIBUTESPARSER_H

#include <map>
#include <cstdlib>
#include <iostream>
#include "pugixml.hpp"

class SequencesAttributesParser
{
  private:
	// stores the colors of each sequence
	std::map<unsigned int, std::string> m_colors;

  public:
	SequencesAttributesParser(const std::string& attr_file);
	std::map<unsigned int, std::string> colors() const { return m_colors; }
	std::string color(unsigned int id) const { return (*(m_colors.find(id))).second; }
};

#endif
