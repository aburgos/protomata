#!/bin/sh

# script:	version.sh
# task:		obtains the last SVN version number and echoes it.
# author:	Andres Burgos
# e-Mail:	andres.burgos@irisa.fr
# updated:	2011-04-12

MAIN_VERSION="v1.9"

SVN_VERSION=$(svnversion -n .)
PARSED_SVN_VERSION=$(echo ${SVN_VERSION#*:} | sed 's/[mM]//g')

VERSION="$MAIN_VERSION ($PARSED_SVN_VERSION)"

echo $VERSION
