
#ifndef PROTOLEARN_H
#define PROTOLEARN_H

#include <fstream>
#include "Paloma.h"
#include "ProtoBuild.h"
#include "ProtoScan.h"
#include "ProtoParse.h"
#include "FileReusability.h"
#include "ProtolearnParameters.h"
#include "ParsedSequencesAlignment.h"
#include "ParsedSequencesDotGenerator.h"

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
class ProtoLearn
{
  public:
	typedef Paloma<InM, OutM> PalomaType;
	typedef ProtoGraphBuilder<OutM, CntT, ColT> PGBuilderType;
	typedef typename PGBuilderType::ProtoGraphType ProtoGraphType;
	typedef ProtoParser<CntT, ColT> ProtoParserType;
	typedef ProtoScan<CntT> ProtoScanType;
	typedef ProtoParse<CntT> ProtoParseType;
	typedef ProtoGraph<double, AttributedSiteColumn, SegmentZone, StringTransition<double> > AttrProtoGraphType;

  private:
	FileReusability f;

	std::vector<std::string> reused_files;
	std::vector<std::string> generated_files;

	void output_dots(ProtolearnParameters&);
	template <typename PrtGrpT> void output_dots(ProtolearnParameters&, const MultiSequence&, const PrtGrpT&);
	void output_files();

	void exec_display_dot(const ProtolearnParameters&) const;

  public:
	ProtoLearn(ProtolearnParameters&);

	std::pair<MultiSequence, GeneralizedAlignment<OutM> > paloma(const PalomaParameters&);
	ProtoGraphType protobuild(const ProtobuildParameters&, const MultiSequence&, const GeneralizedAlignment<OutM>&);
	template <typename PrtGrpT> void protoscan(const ProtoscanParameters&, const PrtGrpT&);
	template <typename PrtGrpT> std::map<Sequence, AlignmentPath> protoparse(const ProtoparseParameters&, const PrtGrpT&);

	template <typename PrtGrpT> void protoprescan(const ProtoscanParameters&, PrtGrpT&);
};

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
ProtoLearn<InM, OutM, CntT, ColT>::ProtoLearn(ProtolearnParameters& parameters): f(parameters)
{
	if((!parameters.protoscan_input_set() && !f.protobuild_output_reusable()) || parameters.output_parsed_seqs_dot())
	{
		if(!parameters.protobuild_input_set() && !f.paloma_output_reusable())
		{
			// execute paloma, protobuild & protoscan
			parameters.use_paloma_io();
			generated_files.push_back(parameters.output());
			std::pair<MultiSequence, GeneralizedAlignment<OutM> > r = paloma(parameters);
			parameters.use_protobuild_io();
			generated_files.push_back(parameters.output());
			if(!parameters.weights_input().empty()) r.first.weight_sequences(parameters.weights_input());
			else if(parameters.weight_sequences()) r.first.weight_sequences();
			ProtoGraphType pg = protobuild(parameters, r.first, r.second);
			output_dots(parameters, r.first, pg);
			// if a sequences file was specified
			if(!parameters.sequences().empty())
			{
				parameters.use_protoscan_io();
				generated_files.push_back(parameters.output());
				protoprescan(parameters, pg);
				// parse sequences if dot is required
				if(parameters.output_parsed_seqs_dot())
				{
					std::map<Sequence, AlignmentPath> paths = protoparse(parameters, pg);

					ParsedSequencesAlignment<ProtoGraphType> parsed_gal(r.first, r.second, pg, paths);

					bool st = parameters.short_tags();
					const std::string& psdo = parameters.parsed_seqs_dot_output();
					const std::string& attr = parameters.plma_dot_attributes_file();
					const unsigned int quorum = parameters.quorum();
					const unsigned int weight_percentage = parameters.weight_percentage();

					typedef ProtoGraphBuilder<LocalAlignment_UFImp, CntT, SiteColumn> PGBuilderType;
					typedef typename PGBuilderType::ProtoGraphType BuiltProtoGraphType;

					typedef typename BuiltProtoGraphType::ZoneType ZoneType;
					typedef typename BuiltProtoGraphType::ColumnType ColumnType;
					typedef typename BuiltProtoGraphType::TransitionType TransitionType;

					const MultiSequence& all_seqs = parsed_gal.sequences();
					std::set<unsigned int> addedIDs;
					for(unsigned int i = r.first.size(); i < r.first.size() + all_seqs.size(); i++)
						addedIDs.insert(i);

					// build protograph including parsed sequences
					PGBuilderType pg_builder(all_seqs, parsed_gal.alignment(), quorum, weight_percentage);
					const BuiltProtoGraphType& parsed_pg = pg_builder.proto_graph();

					ParsedSequencesDotGenerator<BuiltProtoGraphType> pdg(psdo, all_seqs, addedIDs, parsed_pg, attr, st);
					generated_files.push_back(psdo);
				}
				else protoscan(parameters, pg);
			}
		}
		else
		{
			// parse plma file
			parameters.use_protobuild_io();
			reused_files.push_back(parameters.input());
			generated_files.push_back(parameters.output());
			PlmaParser plma_parser(parameters.input());
			MultiSequence& ms = plma_parser.multisequence();
			if(!parameters.weights_input().empty()) ms.weight_sequences(parameters.weights_input());
			else if(parameters.weight_sequences()) ms.weight_sequences();
			const GeneralizedAlignment<OutM>& ga = plma_parser.generalized_alignment();
			// execute protobuild & protoscan
			ProtoGraphType pg = protobuild(parameters, ms, ga);
			output_dots(parameters, ms, pg);
			// if a sequences file was specified
			if(!parameters.sequences().empty())
			{
				parameters.use_protoscan_io();
				generated_files.push_back(parameters.output());
				protoprescan(parameters, pg);
				// parse sequences if dot is required
				if(parameters.output_parsed_seqs_dot())
				{
					std::map<Sequence, AlignmentPath> paths = protoparse(parameters, pg);
					ParsedSequencesAlignment<ProtoGraphType> parsed_gal(ms, ga, pg, paths);

					bool st = parameters.short_tags();
					const std::string& psdo = parameters.parsed_seqs_dot_output();
					const std::string& attr = parameters.plma_dot_attributes_file();
					const unsigned int quorum = parameters.quorum();
					const unsigned int weight_percentage = parameters.weight_percentage();

					typedef ProtoGraphBuilder<LocalAlignment_UFImp, CntT, SiteColumn> PGBuilderType;
					typedef typename PGBuilderType::ProtoGraphType BuiltProtoGraphType;

					typedef typename BuiltProtoGraphType::ZoneType ZoneType;
					typedef typename BuiltProtoGraphType::ColumnType ColumnType;
					typedef typename BuiltProtoGraphType::TransitionType TransitionType;

					const MultiSequence& all_seqs = parsed_gal.sequences();
					std::set<unsigned int> addedIDs;
					for(unsigned int i = ms.size(); i < ms.size() + all_seqs.size(); i++)
						addedIDs.insert(i);

					// build protograph including parsed sequences
					PGBuilderType pg_builder(all_seqs, parsed_gal.alignment(), quorum, weight_percentage);
					const BuiltProtoGraphType& parsed_pg = pg_builder.proto_graph();

					ParsedSequencesDotGenerator<BuiltProtoGraphType> pdg(psdo, all_seqs, addedIDs, parsed_pg, attr, st);
					generated_files.push_back(psdo);
				}
				else protoscan(parameters, pg);
			}
		}
	}
	else if(!parameters.sequences().empty())
	{
		parameters.use_protobuild_io();
		reused_files.push_back(parameters.output());
		// output dot files if needed
		output_dots(parameters);
		// parse proto file
		parameters.use_protoscan_io();
		generated_files.push_back(parameters.output());
		ProtoParserType proto_parser(parameters.input());
		// execute protoscan
		typename ProtoParserType::ProtoGraphType pg = proto_parser.parse_zones_edges();
		protoprescan(parameters, pg);
		protoscan(parameters, pg);
	}
	else
	{
		parameters.use_protobuild_io();
		reused_files.push_back(parameters.output());
		// output dot files if needed
		output_dots(parameters);
	}
	output_files();
}

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
std::pair<MultiSequence, GeneralizedAlignment<OutM> > ProtoLearn<InM, OutM, CntT, ColT>::paloma(const PalomaParameters& parameters)
{
	typedef Paloma<InM, OutM> PalomaType;
	typedef GeneralizedAlignment<LocalAlignment_UFImp> GAType;

	PalomaType l_paloma(parameters);
	return std::make_pair(l_paloma.multisequence(), *dynamic_cast<GAType*>(l_paloma.generalized_set()));
}

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
typename ProtoLearn<InM, OutM, CntT, ColT>::ProtoGraphType ProtoLearn<InM, OutM, CntT, ColT>::protobuild(const ProtobuildParameters& parameters, const MultiSequence& ms, const GeneralizedAlignment<OutM>& ga)
{
	PGBuilderType proto_builder(ms, ga, parameters.quorum(), parameters.weight_percentage());
	const ProtoGraphType& pg = proto_builder.proto_graph();

	ProtoXmlGenerator<ProtoGraphType>(parameters, ms, pg);

	return pg;
}

template <>
ProtoLearn<DialignPLA_Imp, LocalAlignment_UFImp, double, SiteColumn>::ProtoGraphType ProtoLearn<DialignPLA_Imp, LocalAlignment_UFImp, double, SiteColumn>::protobuild(const ProtobuildParameters& parameters, const MultiSequence& ms, const GeneralizedAlignment<LocalAlignment_UFImp>& ga)
{
	PGBuilderType proto_builder(ms, ga, parameters.quorum(), parameters.weight_percentage());
	ProtoGraphType& pg = proto_builder.proto_graph();

	const double grp_thr = parameters.group_threshold();
	const double sgm_thr = parameters.sigma_threshold();
	const bool nxp = parameters.no_expansion();
	const std::string& gf = parameters.groups_file();

	ProtoGraphEditor<ProtoGraphType> pg_editor;
	if(!gf.empty()) pg_editor.group_expansion(pg, gf, grp_thr, sgm_thr, nxp);

	ProtoXmlGenerator<ProtoGraphType>(parameters, ms, pg);

	return pg;
}

template <>
ProtoLearn<DialignPLA_Imp, LocalAlignment_UFImp, double, ProbabilitySiteColumn>::ProtoGraphType ProtoLearn<DialignPLA_Imp, LocalAlignment_UFImp, double, ProbabilitySiteColumn>::protobuild(const ProtobuildParameters& parameters, const MultiSequence& ms, const GeneralizedAlignment<LocalAlignment_UFImp>& ga)
{
	PGBuilderType proto_builder(ms, ga, parameters.quorum(), parameters.weight_percentage());
	ProtoGraphType& pg = proto_builder.proto_graph();

	const std::string& cmp_file = parameters.components_file();
	const double grp_thr = parameters.group_threshold();
	const double sgm_thr = parameters.sigma_threshold();
	const bool nxp = parameters.no_expansion();
	const std::string& gf = parameters.groups_file();

	ProtoGraphEditor<ProtoGraphType> pg_editor;
	if(!gf.empty()) pg_editor.group_expansion(pg, gf, grp_thr, sgm_thr, nxp);
	if(!parameters.pseudocounts().empty())
	{
		if(!cmp_file.empty()) pg_editor.add_pseudocounts(pg, parameters.pseudocounts(), cmp_file);
		else pg_editor.add_pseudocounts(pg, parameters.pseudocounts());
	}

	ProtoXmlGenerator<ProtoGraphType>(parameters, ms, pg);

	return pg;
}

template <>
ProtoLearn<DialignPLA_Imp, LocalAlignment_UFImp, double, AttributedSiteColumn>::ProtoGraphType ProtoLearn<DialignPLA_Imp, LocalAlignment_UFImp, double, AttributedSiteColumn>::protobuild(const ProtobuildParameters& parameters, const MultiSequence& ms, const GeneralizedAlignment<LocalAlignment_UFImp>& ga)
{
	PGBuilderType proto_builder(ms, ga, parameters.quorum(), parameters.weight_percentage());
	ProtoGraphType& pg = proto_builder.proto_graph();

	const std::string& cmp_file = parameters.components_file();
	const double grp_thr = parameters.group_threshold();
	const double sgm_thr = parameters.sigma_threshold();
	const bool nxp = parameters.no_expansion();
	const std::string& gf = parameters.groups_file();

	ProtoGraphEditor<ProtoGraphType> pg_editor;
	if(!gf.empty()) pg_editor.group_expansion(pg, gf, grp_thr, sgm_thr, nxp);
	if(!parameters.pseudocounts().empty())
	{
		if(!cmp_file.empty()) pg_editor.add_pseudocounts(pg, parameters.pseudocounts(), cmp_file);
		else pg_editor.add_pseudocounts(pg, parameters.pseudocounts());
	}

	ProtoXmlGenerator<ProtoGraphType>(parameters, ms, pg);

	return pg;
}

template <>
template <typename PrtGrpT>
void ProtoLearn<DialignPLA_Imp, LocalAlignment_UFImp, int, AttributedSiteColumn>::protoprescan(const ProtoscanParameters& parameters, PrtGrpT& pg)
{
	ProtoGraphEditor<PrtGrpT> pg_editor;
	pg_editor.set_viterbi_values(pg, parameters.scan_score());
}

template <>
template <typename PrtGrpT>
void ProtoLearn<DialignPLA_Imp, LocalAlignment_UFImp, double, AttributedSiteColumn>::protoprescan(const ProtoscanParameters& parameters, PrtGrpT& pg)
{
	ProtoGraphEditor<PrtGrpT> pg_editor;
	pg_editor.set_viterbi_values(pg, parameters.scan_score());
}

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
template <typename PrtGrpT>
void ProtoLearn<InM, OutM, CntT, ColT>::protoprescan(const ProtoscanParameters&, PrtGrpT&)
{
}

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
template <typename PrtGrpT>
void ProtoLearn<InM, OutM, CntT, ColT>::protoscan(const ProtoscanParameters& parameters, const PrtGrpT& pg)
{
	ProtoScanType protoscan;

	// create a weighted automata
	WAGenerator<PrtGrpT> wagen(pg, parameters.exceptions_as_gaps());
	WAProperties<CntT> weighted_automata = wagen.weighted_automata();
	if(parameters.scan_dna())
	{
		WATranslator<CntT> translator;
		weighted_automata = translator.toDNA(weighted_automata);
	}
	if(parameters.output_wa()) protoscan.output_wa(parameters.wa_file(), weighted_automata);

	// scan sequences
	if(parameters.significativity())
	{
		// check if paths were already computed
		ProtoParserType pp(parameters.input());
		typename StatsPathGenerator<PrtGrpT>::StatsPathVector paths = pp.parse_paths();
		if(paths.empty())
		{
			// compute all paths and add them to the XML file for future use
			StatsPathGenerator<PrtGrpT> path_generator(pg);
			paths = path_generator.paths_copy();
			XmlPathAggregator agg(parameters.input(), paths);
		}
		protoscan.scan(weighted_automata, parameters, paths);
	}
	else protoscan.scan(weighted_automata, parameters);
}

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
template <typename PrtGrpT>
typename std::map<Sequence, AlignmentPath> ProtoLearn<InM, OutM, CntT, ColT>::protoparse(const ProtoparseParameters& parameters, const PrtGrpT& pg)
{
	ProtoParseType protoparse;

	// create a weighted automata
	WAGenerator<PrtGrpT> wagen(pg, parameters.exceptions_as_gaps());
	WAProperties<CntT> weighted_automata = wagen.weighted_automata();
	if(parameters.scan_dna())
	{
		WATranslator<CntT> translator;
		weighted_automata = translator.toDNA(weighted_automata);
	}
	if(parameters.output_wa()) protoparse.output_wa(parameters.wa_file(), weighted_automata);

	// parse sequences
	if(parameters.significativity())
	{
		// check if paths were already computed
		ProtoParserType pp(parameters.input());
		typename StatsPathGenerator<PrtGrpT>::StatsPathVector paths = pp.parse_paths();
		if(paths.empty())
		{
			// compute all paths and add them to the XML file for future use
			StatsPathGenerator<PrtGrpT> path_generator(pg);
			paths = path_generator.paths_copy();
			XmlPathAggregator agg(parameters.input(), paths);
		}
		return protoparse.parse(weighted_automata, parameters, paths);
	}
	else return protoparse.parse(weighted_automata, parameters);
}

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
void ProtoLearn<InM, OutM, CntT, ColT>::output_dots(ProtolearnParameters& parameters)
{
	parameters.use_protobuild_io();

	bool basename = !parameters.basename().empty();
	bool plma_a = parameters.plma_dot();
	bool plma_f = f.fexists(parameters.plma_dot_output());
	bool proto_a = parameters.proto_dot();
	bool proto_f = f.fexists(parameters.proto_dot_output());

	bool generate_plma_dot = (plma_a && basename) || (plma_a && !basename && !plma_f);
	bool generate_proto_dot = (proto_a && basename) || (proto_a && !basename && !proto_f);

	if(generate_plma_dot || generate_proto_dot)
	{
		parameters.use_paloma_io();
		const std::string& plma_file = parameters.output();

		PlmaParser plma_parser(plma_file);
		const MultiSequence& ms = plma_parser.multisequence();
		const GeneralizedAlignment<OutM>& ga = plma_parser.generalized_alignment();

		parameters.use_protobuild_io();
		ProtoGraphType pg = protobuild(parameters, ms, ga);

		bool st = parameters.short_tags();
		const std::string& attr = parameters.plma_dot_attributes_file();

		if(generate_plma_dot)
		{
			generated_files.push_back(parameters.plma_dot_output());
			PlmaDotGenerator<ProtoGraphType>(parameters.plma_dot_output(), ms, pg, attr, st);
		}
		else reused_files.push_back(parameters.plma_dot_output());

		if(generate_proto_dot)
		{
			generated_files.push_back(parameters.proto_dot_output());
			ProtomataDotGenerator<ProtoGraphType>(parameters.proto_dot_output(), ms, pg);
		}
		else reused_files.push_back(parameters.proto_dot_output());
	}
	else
	{
		if(parameters.plma_dot()) reused_files.push_back(parameters.plma_dot_output());
		if(parameters.proto_dot()) reused_files.push_back(parameters.proto_dot_output());
	}

	exec_display_dot(parameters);
}

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
template <typename PrtGrpT>
void ProtoLearn<InM, OutM, CntT, ColT>::output_dots(ProtolearnParameters& parameters, const MultiSequence& ms, const PrtGrpT& pg)
{
	parameters.use_protobuild_io();
	bool st = parameters.short_tags();
	const std::string& attr = parameters.plma_dot_attributes_file();

	bool basename = !parameters.basename().empty();
	bool plma_a = parameters.plma_dot();
	bool plma_f = f.fexists(parameters.plma_dot_output());
	bool proto_a = parameters.proto_dot();
	bool proto_f = f.fexists(parameters.proto_dot_output());

	bool reuse_plma_dot = plma_a && !basename && plma_f;
	bool reuse_proto_dot = proto_a && !basename && proto_f;
	bool generate_plma_dot = (plma_a && basename) || (plma_a && !basename && !plma_f);
	bool generate_proto_dot = (proto_a && basename) || (proto_a && !basename && !proto_f);

	if(generate_plma_dot)
	{
		generated_files.push_back(parameters.plma_dot_output());
		PlmaDotGenerator<PrtGrpT>(parameters.plma_dot_output(), ms, pg, attr, st);
	}
	else if(reuse_plma_dot) reused_files.push_back(parameters.plma_dot_output());

	if(generate_proto_dot)
	{
		generated_files.push_back(parameters.proto_dot_output());
		ProtomataDotGenerator<PrtGrpT>(parameters.proto_dot_output(), ms, pg);
	}
	else if(reuse_proto_dot) reused_files.push_back(parameters.proto_dot_output());

	exec_display_dot(parameters);
}

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
void ProtoLearn<InM, OutM, CntT, ColT>::output_files()
{
	std::cout << "Generated files (" << generated_files.size() << ")" << std::endl;
	for(std::vector<std::string>::const_iterator it = generated_files.begin(); it != generated_files.end(); ++it)
		std::cout << *it << std::endl;

	std::cout << "Re-used files (" << reused_files.size() << ")" << std::endl;
	for(std::vector<std::string>::const_iterator it = reused_files.begin(); it != reused_files.end(); ++it)
		std::cout << *it << std::endl;
}

template <typename InM, typename OutM, typename CntT, template <typename T> class ColT>
void ProtoLearn<InM, OutM, CntT, ColT>::exec_display_dot(const ProtolearnParameters& parameters) const
{
	if(!parameters.display_dot().empty())
	{
		if(parameters.plma_dot())
		{
			std::string cmd = parameters.display_dot() + " " + parameters.plma_dot_output() + "&";
			if(system(cmd.c_str())) std::cerr << "Error executing " << cmd << std::endl;
			else std::cout << cmd << std::endl;
		}
		if(parameters.proto_dot())
		{
			std::string cmd = parameters.display_dot() + " " + parameters.proto_dot_output() + "&";
			if(system(cmd.c_str())) std::cerr << "Error executing " << cmd << std::endl;
			else std::cout << cmd << std::endl;
		}
		if(!parameters.parsed_seqs_dot_output().empty())
		{
			std::string cmd = parameters.display_dot() + " " + parameters.parsed_seqs_dot_output() + "&";
			if(system(cmd.c_str())) std::cerr << "Error executing " << cmd << std::endl;
			else std::cout << cmd << std::endl;
		}
	}
}

#endif
