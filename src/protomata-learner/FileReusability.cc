
#include "FileReusability.h"

FileReusability::FileReusability(ProtolearnParameters& parameters)
{
	m_paloma_reusability_checked = false;
	m_paloma_output_reusable = paloma_output_reusable(parameters);
	m_protobuild_output_reusable = protobuild_output_reusable(parameters);
}

bool FileReusability::fexists(const std::string& filename) const
{
	std::ifstream file(filename.c_str());
	return file.good();
}

bool FileReusability::same_version(const std::string& filename) const
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(filename.c_str());
    if(!result) { std::cerr << "Error loading file " << filename << std::endl; exit(1); }

	pugi::xml_node main_node = doc.child("plma");
	if(!main_node) main_node = doc.child("weighted_protomata");
	else if(!main_node) { std::cerr << "Error parsing file " << filename << std::endl; exit(1); }

	std::string file_version = main_node.attribute("version").value();
	if(VERSION != file_version) return false;
	else return true;
}

bool FileReusability::paloma_output_reusable(ProtolearnParameters& parameters)
{
	m_paloma_reusability_checked = true;

	if(!fexists(parameters.paloma_output())) return false;
	else if(!same_version(parameters.paloma_output())) return false;

	// parse arguments from file
	ArgumentParser args_parser(parameters.paloma_output());

	// for iterating only through paloma parameters
	parameters.use_paloma_io();

	// for each current argument
	for(ProtolearnParameters::const_iterator curr_it = parameters.begin(); curr_it != parameters.end(); ++curr_it)
	{
		const std::string& curr_flag = (*curr_it).first;
		const std::string& curr_value = (*curr_it).second;

		if(!args_parser.has_flag(curr_flag)) return false;

		if(args_parser.value(curr_flag) != curr_value) return false;
	}

	return true;
}

bool FileReusability::protobuild_output_reusable(ProtolearnParameters& parameters)
{
	if(!fexists(parameters.protobuild_output())) return false;
	else if(!same_version(parameters.protobuild_output())) return false;

	// check if paloma parameters are the same
	if(!m_paloma_reusability_checked)
	{
		if(!paloma_output_reusable(parameters)) return false;
	}
	else if(!paloma_output_reusable()) return false;

	// parse arguments from file
	ArgumentParser args_parser(parameters.protobuild_output());

	// for iterating only through paloma parameters
	parameters.use_protobuild_io();

	// for each current argument
	for(ProtolearnParameters::const_iterator curr_it = parameters.begin(); curr_it != parameters.end(); ++curr_it)
	{
		const std::string& curr_flag = (*curr_it).first;
		const std::string& curr_value = (*curr_it).second;

		if(!args_parser.has_flag(curr_flag)) return false;

		if(args_parser.value(curr_flag) != curr_value) return false;
	}

	return true;
}
