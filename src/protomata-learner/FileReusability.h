
#ifndef FILEREUSABILITY_H
#define FILEREUSABILITY_H

#include "ArgumentParser.h"
#include "ProtolearnParameters.h"

class FileReusability
{
  private:
	bool m_paloma_output_reusable;
	bool m_protobuild_output_reusable;
	bool m_paloma_reusability_checked;
	bool same_version(const std::string&) const;

  public:
	FileReusability(ProtolearnParameters&);

	bool fexists(const std::string&) const;
	bool paloma_output_reusable() const { return m_paloma_output_reusable; }
	bool protobuild_output_reusable() const { return m_protobuild_output_reusable; }
	bool paloma_output_reusable(ProtolearnParameters&);
	bool protobuild_output_reusable(ProtolearnParameters&);
};

#endif
