
#include "ProtolearnParameters.h"

ProtolearnParameters::ProtolearnParameters():
	PalomaParameters(),
	ProtobuildParameters(),
	m_param_input(TCLAP::ValueArg<std::string>("P", "parameters", "Take parameters from the XML parameters file.", false, "", "param_filename")),
	m_output_path(TCLAP::ValueArg<std::string>("", "output-path", "Set the output path for all generated files unless specified.", false, "", "your_path/")),
	m_protobuild_input_notreq(TCLAP::ValueArg<std::string>("", "protobuild-input", "Skip paloma execution and use the set file as protobuild input.", false, "", "plma_filename")),
	m_protoscan_input_notreq(TCLAP::ValueArg<std::string>("", "protoscan-input", "Skip paloma and protobuild execution, executing only protoscan with the set input file.", false, "", "proto_filename")),
	m_protoscan_output_ptl(TCLAP::ValueArg<std::string>("", "protoscan-output", "Set the output filename for the scores.", false, "", "txt_filename")),
	m_sequences_notreq(TCLAP::ValueArg<std::string>("s", "sequences", "Set of sequences to scan.", false, "", "fasta_filename")),
	m_basename(TCLAP::ValueArg<std::string>("b", "basename", "Set output name for all generated files.", false, "", "basename")),
	m_display_dot(TCLAP::ValueArg<std::string>("", "display-dot", "The command will be executed in the background for each generated dot file with it as parameter.", false, "", "command")),
	m_output_parsed_seqs_dot(TCLAP::SwitchArg("", "parsed-sequences-dot", "Outputs the PLMA dot including the sequences parsed.", false))
{
}

void ProtolearnParameters::parse(int argc, char **argv)
{
	TCLAP::CmdLine cmd("Generates a protein family model from a fasta file.", ' ', VERSION);
	add_arguments(cmd);

	int m_argc = argc;
	char** m_argv = new char*[argc + 1];
	memcpy(m_argv, argv, sizeof(char*) * (argc + 1));

	// if parameters should be taken from a file, replace argc and argv
	ArgumentParser main_args_parser(m_argc, m_argv);
	main_args_parser.all_long_flags(cmd);
	pre_sanity_checks(main_args_parser);
	const std::string& param_flag = "--parameters";
	if(main_args_parser.has_flag(param_flag))
	{
		FileNameSearcher fnc;
		ArgumentParser xml_args_parser(fnc.filename(main_args_parser.value(param_flag)));
		xml_args_parser.all_long_flags(cmd);
		pre_sanity_checks(xml_args_parser);

		// arguments provided by command line have priority to those on the XML file
		for(ArgumentParser::Arguments::const_iterator it = main_args_parser.begin(); it != main_args_parser.end(); ++it)
		{
			const std::string& flag = (*it).first;
			const std::string& value = (*it).second;

			if(flag != "--parameters")
				xml_args_parser.value(flag, value);
		}

		if(main_args_parser.has_flag("--strong-consensus")) xml_args_parser.remove_flag("--weak-consensus");
		if(main_args_parser.has_flag("--weak-consensus")) xml_args_parser.remove_flag("--strong-consensus");

		if(main_args_parser.has_flag("--local-consistency")) xml_args_parser.remove_flag("--global-consistency");
		if(main_args_parser.has_flag("--global-consistency")) xml_args_parser.remove_flag("--local-consistency");

		if(main_args_parser.has_flag("--pseudocounts") && xml_args_parser.has_flag("--components"))
		{
			if(main_args_parser.value("--pseudocounts") != "Dirichlet")
			{
				xml_args_parser.remove_flag("--components");
				std::cout << "Warning! Components set on XML parameters will not be used." << std::endl;
			}
		}

		std::cout << "Generated command from XML parameters: " << std::endl << xml_args_parser.command() << std::endl;
		std::pair<int, char**> args = xml_args_parser.main_args();
		m_argc = args.first;
		delete [] m_argv;
		m_argv = args.second;
	}
	else std::cout << "Command: " << std::endl << main_args_parser.command() << std::endl;

	cmd.parse(m_argc, m_argv);
	initialize_values();
	sanity_checks();
	add_string_parameters();
	use_paloma_io();

	delete [] m_argv;
}

void ProtolearnParameters::add_arguments(TCLAP::CmdLine& cmd)
{
	cmd.add(m_verbose);
	//cmd.add(m_test_seeds);
	//cmd.add(m_deactivate_global_opt);
	cmd.add(m_no_repeats);
	cmd.add(m_local_consistency);
	cmd.add(m_global_consistency);

	//cmd.add(m_df);
	//cmd.add(m_ds);
	//cmd.add(m_ot);
	//cmd.add(m_st);
	//cmd.add(m_gs);
	//cmd.add(m_sg);
	//cmd.add(m_bg);
	//cmd.add(m_bm);

	cmd.add(m_forward);
	cmd.add(m_sscr);
	cmd.add(m_output_wa);
	cmd.add(m_significativity);
	cmd.add(m_use_integers);
	cmd.add(m_allow_partial_model_match);
	cmd.add(m_disallow_partial_sequence_match);
	cmd.add(m_exceptions_as_gaps);
	cmd.add(m_upbound);
	cmd.add(m_lobound);
	cmd.add(m_score_threshold);
	cmd.add(m_scan_dna);
	cmd.add(m_sequences_notreq);

	cmd.add(m_output_parsed_seqs_dot);
	cmd.add(m_display_dot);
	cmd.add(m_short_tags);
	cmd.add(m_plma_dot_attributes);
	cmd.add(m_plma_dot_output);
	cmd.add(m_plma_dot);
	cmd.add(m_proto_dot_output);
	cmd.add(m_proto_dot);
	cmd.add(m_sigma_threshold);
	cmd.add(m_group_threshold);
	cmd.add(m_groups);
	cmd.add(m_disallow_X);
	cmd.add(m_xstr);
	cmd.add(ProtobuildParameters::m_components);
	cmd.add(ProtobuildParameters::m_pseudocounts);
	cmd.add(m_weight_percentage);
	cmd.add(m_quorum);

	cmd.add(m_weights_input);
	cmd.add(m_weight_sequences);
	cmd.add(m_weak_consensus);
	cmd.add(m_strong_consensus);
	cmd.add(m_max_size);
	cmd.add(m_min_size);
	cmd.add(m_threshold);
	cmd.add(m_quorum_plma);

	cmd.add(m_smithwaterman);
	cmd.add(m_afc_file);
	cmd.add(m_dialign);
	cmd.add(m_paloma_tag);
	cmd.add(m_basename);
	cmd.add(m_protoscan_output_ptl);
	cmd.add(m_protoscan_input_notreq);
	cmd.add(m_protobuild_input_notreq);
	cmd.add(m_protobuild_output);
	cmd.add(m_output_path);
	cmd.add(m_param_input);
	cmd.add(m_paloma_input);
}

void ProtolearnParameters::initialize_values()
{
	PalomaParameters::initialize_values();
	ProtobuildParameters::initialize_values();
	ProtoscanParameters::initialize_values();
}

void ProtolearnParameters::sanity_checks()
{
	PalomaParameters::sanity_checks();
	ProtobuildParameters::sanity_checks();
	ProtoscanParameters::sanity_checks();
	if(!m_sequences_notreq.isSet() && m_output_wa.getValue())
		std::cout << "Warning! Weighted automata will only be outputted if the --sequences option is set." << std::endl;
	if(!m_sequences_notreq.isSet() && m_output_parsed_seqs_dot.getValue())
	{
		std::cout << "Warning! Parsed sequences PLMA dot file will only be outputted if the ";
		std::cout << "--sequences option is set." << std::endl;
	}
}

void ProtolearnParameters::pre_sanity_checks(const ArgumentParser& args) const
{
	if(args.has_flag("--strong-consensus") && args.has_flag("--weak-consensus"))
		{ std::cout << "Error: flags for strong and weak consensus are both set." << std::endl; exit(1); }
	if(args.has_flag("--global-consistency") && args.has_flag("--local-consistency"))
		{ std::cout << "Error: flags for global and local consistency are both set." << std::endl; exit(1); }
}

void ProtolearnParameters::add_string_parameters()
{
}

std::string ProtolearnParameters::compute_output_file(const std::string&) const
{
	return std::string();
}

void ProtolearnParameters::use_paloma_io()
{
	m_input = paloma_input();
	m_output = paloma_output();
	string_parameters.clear();
	PalomaParameters::add_string_parameters();
}

void ProtolearnParameters::use_protobuild_io()
{
	m_input = protobuild_input();
	m_output = protobuild_output();
	string_parameters.clear();
	ProtobuildParameters::add_string_parameters();
}

void ProtolearnParameters::use_protoscan_io()
{
	m_input = protoscan_input();
	m_output = protoscan_output();
	string_parameters.clear();
	ProtoscanParameters::add_string_parameters();
}

std::string ProtolearnParameters::paloma_input() const
{
	return PalomaParameters::input();
}

std::string ProtolearnParameters::protobuild_input() const
{
	if(protobuild_input_set()) return m_protobuild_input_notreq.getValue();
	else if(basename().empty()) return paloma_output();
	else return (output_path() + basename() + tag() + "_plma.xml");
}

std::string ProtolearnParameters::protoscan_input() const
{
	if(protoscan_input_set()) return m_protoscan_input_notreq.getValue();
	else if(basename().empty()) return protobuild_output();
	else return (output_path() + basename() + tag() + "_proto.xml");
}

std::string ProtolearnParameters::paloma_output() const
{
	if(basename().empty()) return (output_path() + PalomaParameters::output());
	else return (output_path() + basename() + tag() + "_plma.xml");
}

std::string ProtolearnParameters::protobuild_output() const
{
	if(m_protobuild_output.isSet()) return m_protobuild_output.getValue();
	if(basename().empty()) return (output_path() + ProtobuildParameters::compute_output_file(protobuild_input()));
	else return (output_path() + basename() + tag() + "_proto.xml");
}

std::string ProtolearnParameters::protoscan_output() const
{
	if(!m_protoscan_output_ptl.getValue().empty()) return m_protoscan_output_ptl.getValue();
	else if(basename().empty()) return (output_path() + ProtoscanParameters::compute_output_file(protoscan_input()));
	else return (output_path() + basename() + tag() + "_scan.txt");
}

std::string ProtolearnParameters::pseudocounts() const
{
	if(use_integers()) return std::string();
	else return ProtobuildParameters::pseudocounts();
}

std::string ProtolearnParameters::components_file() const
{
	if(use_integers()) return std::string();
	else
	{
		FileNameSearcher fnc;
		return fnc.filename(ProtobuildParameters::m_components.getValue());
	}
}

std::string ProtolearnParameters::plma_dot_output() const
{
	if(basename().empty()) return ProtobuildParameters::plma_dot_output();
	else return (output_path() + basename() + tag() + "_plma.dot");
}

std::string ProtolearnParameters::proto_dot_output() const
{
	if(basename().empty()) return (output_path() + ProtobuildParameters::proto_dot_output());
	else return (output_path() + basename() + tag() + "_proto.dot");
}

std::string ProtolearnParameters::parsed_seqs_dot_output() const
{
	if(basename().empty())
	{
		std::ostringstream ss;
		ss << input().substr(0, input().find_last_of('.'));
		if(weight_sequences()) ss << "_w";
		ss << "_q" << quorum() << "_p" << weight_percentage();
		//if(short_tags.getValue()) ss << "_s";
		if(!sequences().empty())
		{
			size_t pos = sequences().find_last_of('/');
			std::string sequences_basename = sequences().substr(pos + 1, sequences().length());
			ss << "_s:" << sequences_basename;
		}
		ss << "_parsed.dot";

		return (output_path() + ss.str());
	}
	else return (output_path() + basename() + tag() + "_parsed.dot");
}

std::string ProtolearnParameters::afc_file() const
{
	if(!m_afc_file.getValue().empty()) return m_afc_file.getValue();
	else
	{
		std::ostringstream ss;
		ss << output_path() << compute_basename(input()) << "_t" << threshold() << "_M" << max_size();
		ss << "_dialign.afc";
		return ss.str();
	}
}
