
#ifndef PROTOLEARNPARAMETERS_H
#define PROTOLEARNPARAMETERS_H

#include "PalomaParameters.h"
#include "ProtobuildParameters.h"
#include "ProtoparseParameters.h"
#include "ArgumentParser.h"

class ProtolearnParameters: public PalomaParameters, public ProtobuildParameters, public ProtoparseParameters
{
  protected:
	std::string m_input;
	std::string m_output;

	TCLAP::ValueArg<std::string> m_param_input;
	TCLAP::ValueArg<std::string> m_output_path;
	TCLAP::ValueArg<std::string> m_protobuild_input_notreq;
	TCLAP::ValueArg<std::string> m_protoscan_input_notreq;
	TCLAP::ValueArg<std::string> m_protoscan_output_ptl;
	TCLAP::ValueArg<std::string> m_sequences_notreq;
	TCLAP::ValueArg<std::string> m_basename;
	TCLAP::ValueArg<std::string> m_display_dot;

	TCLAP::SwitchArg m_output_parsed_seqs_dot;

	virtual void sanity_checks();
	virtual void add_string_parameters();
	virtual std::string compute_output_file(const std::string&) const;
	virtual void initialize_values();
	void pre_sanity_checks(const ArgumentParser&) const;

  public:
	ProtolearnParameters();
	virtual ~ProtolearnParameters() {}

	void use_paloma_io();
	void use_protobuild_io();
	void use_protoscan_io();

	bool protobuild_input_set() const { return !m_protobuild_input_notreq.getValue().empty(); }
	bool protoscan_input_set() const { return !m_protoscan_input_notreq.getValue().empty(); }

	std::string paloma_input() const;
	std::string protobuild_input() const;
	std::string protoscan_input() const;

	std::string paloma_output() const;
	std::string protobuild_output() const;
	std::string protoscan_output() const;

	virtual void parse(int argc, char **argv);
	virtual void add_arguments(TCLAP::CmdLine&);

	virtual std::string input() const { return m_input; }
	virtual std::string output() const { return m_output; }
	virtual std::string output_path() const { return m_output_path.getValue(); }
	virtual std::string sequences() const { return m_sequences_notreq.getValue(); }
	virtual std::string basename() const { return m_basename.getValue(); }
	virtual std::string tag() const { return m_paloma_tag.getValue(); }
	virtual std::string afc_file() const;
	virtual std::string pseudocounts() const;
	virtual std::string components_file() const;
	virtual std::string plma_dot_output() const;
	virtual std::string proto_dot_output() const;
	virtual std::string parsed_seqs_dot_output() const;
	virtual std::string display_dot() const { return m_display_dot.getValue(); }

	virtual bool output_parsed_seqs_dot() const { return m_output_parsed_seqs_dot.getValue(); }
};

#endif
