
#include "ProtoLearn.h"

int main(int argc, char **argv)
{
	ProtolearnParameters parameters;
	parameters.parse(argc, argv);

	if(parameters.use_integers())
		ProtoLearn<DialignPLA_Imp, LocalAlignment_UFImp, int, AttributedSiteColumn> protolearn(parameters);
	else
		ProtoLearn<DialignPLA_Imp, LocalAlignment_UFImp, double, AttributedSiteColumn> protolearn(parameters);

	return 0;
}
