
#ifndef PARSEDSEQUENCESALIGNMENT_H
#define PARSEDSEQUENCESALIGNMENT_H

#include "ProtoGraph.h"
#include "paths/AlignmentPath.h"
#include "alignments/LocalAlignment_UFImp.h"
#include "ProtoAlign.h"

template <typename ProtoGraphType>
class ParsedSequencesAlignment
{
  public:
	typedef std::map<unsigned int, unsigned int> SeqIDMap;

  private:
	MultiSequence* m_ms;
	GeneralizedAlignment<LocalAlignment_UFImp>* m_ga;

	GeneralizedAlignment<LocalAlignment_UFImp> alignment(const MultiSequence&, const std::map<Sequence, AlignmentPath>&, const GeneralizedAlignment<LocalAlignment_UFImp>&, const ProtoGraphType&, const SeqIDMap&) const;

  public:
	ParsedSequencesAlignment(const MultiSequence&, const GeneralizedAlignment<LocalAlignment_UFImp>&, const ProtoGraphType&, const std::map<Sequence, AlignmentPath>&);
	~ParsedSequencesAlignment() { delete m_ms; delete m_ga; }

	MultiSequence sequences() const { return *m_ms; }
	GeneralizedAlignment<LocalAlignment_UFImp> alignment() const { return *m_ga; }
};

template <typename ProtoGraphType>
ParsedSequencesAlignment<ProtoGraphType>::ParsedSequencesAlignment(const MultiSequence& ms, const GeneralizedAlignment<LocalAlignment_UFImp>& ga, const ProtoGraphType& pg, const std::map<Sequence, AlignmentPath>& paths)
{
	// concatenate parsed sequences to the ones used to build the model
	std::vector<Sequence> concat_seqs;
	for(MultiSequence::const_iterator it = ms.begin(); it != ms.end(); ++it)
	{
		const Sequence& sequence = *it;
		concat_seqs.push_back(sequence);
	}
	SeqIDMap new_sequence_position;
	for(std::map<Sequence, AlignmentPath>::const_iterator it = paths.begin(); it != paths.end(); ++it)
	{
		const Sequence& old_seq = (*it).first;
		const std::string new_tag = "PARSED_" + old_seq.tag();
		const std::string new_long_tag = "PARSED_" + old_seq.long_tag();
		new_sequence_position[old_seq.id()] = concat_seqs.size();
		Sequence new_sequence(new_tag, new_long_tag, old_seq.data(), concat_seqs.size(), old_seq.weight());
		concat_seqs.push_back(new_sequence);
	}

	// set of all sequences
	m_ms = new MultiSequence(concat_seqs);
	m_ga = new GeneralizedAlignment<LocalAlignment_UFImp>(alignment(*m_ms, paths, ga, pg, new_sequence_position));
}

template <typename ProtoGraphType>
GeneralizedAlignment<LocalAlignment_UFImp> ParsedSequencesAlignment<ProtoGraphType>::alignment(const MultiSequence& ms, const std::map<Sequence, AlignmentPath>& paths, const GeneralizedAlignment<LocalAlignment_UFImp>& ga, const ProtoGraphType& pg, const SeqIDMap& new_sequence_position) const
{
	// for using the buildAlignment function only
	ProtoAlign<typename ProtoGraphType::CountType> protoalign;

	// each (zone, column) pair of the protograph maps to a set of sites of a sequence
	std::map<std::pair<unsigned int, unsigned int>, SiteSet> zonecolumn2sites;

	// store the sites for each sequence
	for(std::map<Sequence, AlignmentPath>::const_iterator it = paths.begin(); it != paths.end(); ++it)
	{
		const Sequence& sequence = (*it).first;
		const AlignmentPath& path = (*it).second;

		for(AlignmentPath::const_iterator pit = path.begin(); pit != path.end(); ++pit)
		{
			const unsigned int position = (*pit).first;
			const typename AlignmentPath::ZoneColumnPair& zcp = path[position];
			const unsigned int new_seq_id = (*(new_sequence_position.find(sequence.id()))).second;
			zonecolumn2sites[zcp].insert(Site(new_seq_id, position));
		}
	}

	// generalized alignment comprising the model and the parsed sequences
	GeneralizedAlignment<LocalAlignment_UFImp> t_ga(ms);
	LocalAlignment_UFImp al(ms);

	// add relations from the original alignment
	relation_visitor v = ga.site_relations();
	while(v.valid())
	{
		al.addMatching(v.get());
		v.next();
	}

	// add the partition resulting from the parsed sequences
	std::map<std::pair<unsigned int, unsigned int>, SiteSet>::const_iterator it;
	for(it = zonecolumn2sites.begin(); it != zonecolumn2sites.end(); ++it)
	{
		const SitePartitionElement& spe = (*it).second;
		al.addMatching(protoalign.buildAlignment(ms, spe));
	}

	// relate the added partition and the original one
	for(it = zonecolumn2sites.begin(); it != zonecolumn2sites.end(); ++it)
	{
		const SitePartitionElement& spe = (*it).second;
		const Site& original_partition_element_site = *(spe.begin());

		const typename AlignmentPath::ZoneColumnPair& zcp = (*it).first;
		typename ProtoGraphType::ZoneType zone = pg[zcp.first];

		typename ProtoGraphType::ZoneType::column_const_iterator cit;
		for(cit = zone.columns_begin(); cit != zone.columns_end(); ++cit)
		{
			const typename ProtoGraphType::ColumnType& column = *cit;

			if(column.id() != zcp.second) continue;

			const Site& related_partition_element_site = *(column.sites_begin());
			SitePairRelation spr(std::make_pair(original_partition_element_site, related_partition_element_site), 0);
			al.addMatching(spr);
		}
	}
	t_ga.addMatching(al);

	return t_ga;
}

#endif

