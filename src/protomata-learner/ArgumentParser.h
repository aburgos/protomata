
#ifndef ARGUMENTPARSER_H
#define ARGUMENTPARSER_H

#include <map>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include "pugixml.hpp"
#include "tclap/CmdLine.h"

class ArgumentParser
{
  public:
	typedef std::map<std::string, std::string> Arguments;

  private:
	Arguments m_arguments;

  public:
	ArgumentParser(int, char**);
	ArgumentParser(const std::string&);

	Arguments arguments() const { return m_arguments; }

	std::string command() const;
	std::pair<int, char**> main_args() const;

	bool all_long_flags(TCLAP::CmdLine&);
	bool has_flag(const std::string& flag) const { return (m_arguments.find(flag) != m_arguments.end()); }
	std::string value(const std::string& flag) const { return (*(m_arguments.find(flag))).second; }

	void remove_flag(const std::string& flag) { m_arguments.erase(flag); }
	void value(const std::string& flag, const std::string& value) { m_arguments[flag] = value; }

	typedef Arguments::iterator iterator;
	iterator begin() { return m_arguments.begin(); }
	iterator end() { return m_arguments.end(); }

	typedef Arguments::const_iterator const_iterator;
	const_iterator begin() const { return m_arguments.begin(); }
	const_iterator end() const { return m_arguments.end(); }
};

#endif
