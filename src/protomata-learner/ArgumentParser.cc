
#include "ArgumentParser.h"

ArgumentParser::ArgumentParser(int argc, char** argv)
{
	for(int i = 1; i < argc; i++)
	{
		// check if it is a flag
		if(argv[i][0] == '-')
		{
			if(argv[i + 1] == NULL || argv[i + 1][0] == '-') m_arguments.insert(std::make_pair(argv[i], ""));
			else
			{
				m_arguments.insert(std::make_pair(argv[i], argv[i + 1]));
				i++;
			}
		}
	}
}

ArgumentParser::ArgumentParser(const std::string& filename)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(filename.c_str());
    if(!result) { std::cerr << "Error loading file " << filename << std::endl; exit(1); }

	pugi::xml_node parameters_node = doc.child("parameters");
	if(!parameters_node) parameters_node = doc.child("plma").child("resume").child("parameters");
	if(!parameters_node) parameters_node = doc.child("weighted_protomata").child("resume").child("parameters");
	if(!parameters_node) parameters_node = doc.child("protomata").child("program");
	if(!parameters_node) { std::cerr << "Error parsing file " << filename << std::endl; exit(1); }

	// iterate through parameters
	for(pugi::xml_node_iterator pit = parameters_node.begin(); pit != parameters_node.end(); ++pit)
	{
		// ignore all childs that are not a parameter
		if(strcmp(pit->name(), "param") != 0) continue;

		const std::string& flag = pit->attribute("f").value();
		const std::string& value = pit->attribute("v").value();

		m_arguments.insert(std::make_pair(flag, value));
	}
}

std::string ArgumentParser::command() const
{
	std::string cmd = "protomata-learner";
	for(Arguments::const_iterator it = m_arguments.begin(); it != m_arguments.end(); ++it)
	{
		const std::string& flag = (*it).first;
		const std::string& value = (*it).second;

		cmd += " " + flag;
		if(!value.empty()) cmd += " " + value;
	}

	return cmd;
}

std::pair<int, char**> ArgumentParser::main_args() const
{
	int argc = 0;
	char** argv = NULL;

	// add the program's name
	std::string program_name = "protomata-learner";
	char *str = new char[program_name.length() + 1];
	strcpy(str, program_name.c_str());
	argv = new char*[++argc];
	argv[0] = str;

	for(Arguments::const_iterator it = m_arguments.begin(); it != m_arguments.end(); ++it)
	{
		const std::string& flag = (*it).first;
		const std::string& value = (*it).second;

		unsigned int new_spaces = 1;
		if(!value.empty()) new_spaces = 2;

		char** mod_argv = new char*[argc + new_spaces];
		memcpy(mod_argv, argv, sizeof(char*) * argc);
		delete [] argv;

		char* str_flag = new char[flag.length() + 1];
		strcpy(str_flag, flag.c_str());
		mod_argv[argc++] = str_flag;

		if(!value.empty())
		{
			char* str_value = new char[value.length() + 1];
			strcpy(str_value, value.c_str());
			mod_argv[argc++] = str_value;
		}

		argv = mod_argv;
	}

	// by convention, argv[argc] is a NULL pointer
	char** mod_argv = new char*[argc + 1];
	memcpy(mod_argv, argv, sizeof(char*) * argc);
	delete [] argv;
	mod_argv[argc] = NULL;
	argv = mod_argv;

	return std::make_pair(argc, argv);
}

bool ArgumentParser::all_long_flags(TCLAP::CmdLine& cmd)
{
	std::list<TCLAP::Arg*>& args = cmd.getArgList();
	// replace all short flags found by long ones
	for(std::list<TCLAP::Arg*>::const_iterator it = args.begin(); it != args.end(); ++it)
	{
		TCLAP::Arg* arg = *it;
		const std::string& short_flag = "-" + arg->getFlag();
		if(has_flag(short_flag))
		{
			std::string flag_value = value(short_flag);
			m_arguments.erase(short_flag);
			const std::string& long_flag = "--" + arg->getName();
			m_arguments[long_flag] = flag_value;
		}
	}

	return true;
}
