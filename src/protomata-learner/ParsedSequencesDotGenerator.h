
#ifndef PARSEDSEQUENCESDOTGENERATOR_H
#define PARSEDSEQUENCESDOTGENERATOR_H

#include "SequencesAttributesParser.h"

/*!
Generates the output for plma file and the parsed sequences in dot format.
*/
template <typename ProtoGraphType>
class ParsedSequencesDotGenerator
{
  private:
	typedef std::map<unsigned int, std::string> SeqColor;
	typedef typename ProtoGraphType::ZoneType ZoneType;
	typedef typename ProtoGraphType::TransitionType TransitionType;

	//! The output file stream.
	std::ofstream output;
	bool m_short_tags;
	//! Used to distinguish begin, end and common blocks.
	void output_zone(const ZoneType&, const MultiSequence&, const std::set<unsigned int>&);
	//! Special output for the begin block.
	void output_begin_zone(const ZoneType&, const MultiSequence&, const std::set<unsigned int>&);
	//! Special output for the end block.
	void output_end_zone(const ZoneType&, const std::set<unsigned int>&);
	void output_transition(const TransitionType&, const SeqColor&);

  public:
	//! The constructor first outputs every block and after every transition between the blocks. 
	ParsedSequencesDotGenerator(const std::string&, const MultiSequence&, const std::set<unsigned int>&, const ProtoGraphType&, const std::string&, bool);
};

template <typename ProtoGraphType>
ParsedSequencesDotGenerator<ProtoGraphType>::ParsedSequencesDotGenerator(const std::string& output_file, const MultiSequence& ms, const std::set<unsigned int>& parsed_sequences, const ProtoGraphType& pg, const std::string& attr_file, bool short_tags): m_short_tags(short_tags)
{
	output.open(output_file.c_str());

	// create the graph
	//output << "// File generated with protobuild version " << VERSION << "\n";
	output << "DiGraph G\n{\n";
	output << "\trankdir = LR;\n";
	output << "\tconcentrate = true;\n";
	output << "\tnode [shape = record, fontname = monospace];\n";
	output << "\tedge [color = green, fontcolor = blue, weight = 1, headport = w, tailport = e];\n";

	// output all zones
	for(typename ProtoGraphType::zone_const_iterator it = pg.zone_begin(); it != pg.zone_end(); ++it)
	{
		const typename ProtoGraphType::ZoneType& zone = *it;
		output_zone(zone, ms, parsed_sequences);
	}

	// get colors for transitions
	SeqColor transition_color;
	if(!attr_file.empty())
	{
		SequencesAttributesParser attributes(attr_file);
		transition_color = attributes.colors();
	}
	else
	{
		for(MultiSequence::const_iterator it = ms.begin(); it != ms.end(); ++it)
		{
			const Sequence& sequence = *it;
			transition_color.insert(std::make_pair(sequence.id(), "green"));
		}
	}

	// output all transitions between zones
	for(typename ProtoGraphType::relation_const_iterator it = pg.relation_begin(); it != pg.relation_end(); ++it)
	{
		const typename ProtoGraphType::TransitionType& zt = (*it).value();
		output_transition(zt, transition_color);
	}

	output << "}";
	output.close();
}

template <typename ProtoGraphType>
void ParsedSequencesDotGenerator<ProtoGraphType>::output_zone(const ZoneType& zone, const MultiSequence& ms, const std::set<unsigned int>& parsed_sequences)
{
	// is it possible to have a method that infers the zone type?
	if(zone.id() == BeginZone<void, Column>::begin_zone_id) output_begin_zone(zone, ms, parsed_sequences);
	else if(zone.id() == EndZone<void, Column>::end_zone_id) output_end_zone(zone, parsed_sequences);
	else
	{
		output << "\tsubgraph cluster_" << zone.id() << "\n\t{\n";
		output << "\t\tnode [shape = record, style = filled, color = yellow, fontcolor = black];\n";
		output << "\t\tcolor = chocolate1;\n";

		for(typename ZoneType::segment_const_iterator sit = zone.segments_begin(); sit != zone.segments_end(); ++sit)
		{
			const Segment& segment = *sit;
			const std::string& label = (ms[segment.sequence()].data()).substr(segment.begin(), segment.length());
			output << "\t\t\"" << segment << "\" [label = \"" << label << "\"";
			if(parsed_sequences.find(segment.sequence()) != parsed_sequences.end())
				output << ", color = chartreuse";
			output << "];\n";
		}
		output << "\t}\n";
	}
}

template <typename ProtoGraphType>
void ParsedSequencesDotGenerator<ProtoGraphType>::output_begin_zone(const ZoneType& zone, const MultiSequence& ms, const std::set<unsigned int>& parsed_sequences)
{
	output << "\tsubgraph cluster_" << zone.id() << "\n\t{\n";
	output << "\t\tnode [shape = record, color = blue, fontcolor = black];\n";
	output << "\t\tcolor = white;\n";

	for(typename ZoneType::segment_const_iterator sit = zone.segments_begin(); sit != zone.segments_end(); ++sit)
	{
		const Segment& seg = *sit;
		const std::string tag = (m_short_tags ? ms[seg.sequence()].tag() : ms[seg.sequence()].long_tag());
		std::string escaped_tag;
		for(std::string::const_iterator it = tag.begin(); it != tag.end(); ++it)
		{
			if((*it) == '|') escaped_tag.append("\\|");
			else escaped_tag.append(1, *it);
		}
		output << "\t\t\"" << seg << "\" [label = \"" << (seg.sequence() + 1) << ": " << escaped_tag << "\"";
		if(parsed_sequences.find(seg.sequence()) != parsed_sequences.end())
			output << ", style = filled, fillcolor = grey80";
		output << "];\n";
	}
	output << "\t}\n";	
}

template <typename ProtoGraphType>
void ParsedSequencesDotGenerator<ProtoGraphType>::output_end_zone(const ZoneType& zone, const std::set<unsigned int>& parsed_sequences)
{
	output << "\tsubgraph cluster_" << zone.id() << "\n\t{\n";
	output << "\t\tnode [shape = record, color = green, fontcolor = green];\n";
	output << "\t\tcolor = white;\n";

	for(typename ZoneType::segment_const_iterator sit = zone.segments_begin(); sit != zone.segments_end(); ++sit)
	{
		const Segment& seg = *sit;
		output << "\t\t\"" << seg << "\" [label = \"" << (seg.sequence() + 1) << "\"";
		if(parsed_sequences.find(seg.sequence()) != parsed_sequences.end())
			output << ", style = filled, fillcolor = grey80";
		output << "];\n";
	}
	output << "\t}\n";	
}

template <typename ProtoGraphType>
void ParsedSequencesDotGenerator<ProtoGraphType>::output_transition(const TransitionType& zt, const std::map<unsigned int, std::string>& colors)
{
	if(zt.type() == "Gap")
	{
		for(typename TransitionType::relation_const_iterator it = zt.begin(); it != zt.end(); ++it)
		{
			const Segment& from = (*it).first();
			const Segment& to = (*it).second();
			const Segment& transition = (*it).value();
			const std::string& color = (*(colors.find(from.sequence()))).second;

			if(transition.length() > 1)
			{
				output << "\t\"" << from << "\" -> \"" << to << "\" ";
				output << "[label = \"" << (from.sequence() + 1) << ": " << (transition.begin() + 1);
				output << ".." << (transition.end() + 1) << "\", color = " << color;
				output << " green, fontcolor = blue, weight = 5];\n";
			}
			else if(transition.length() == 1)
			{
				output << "\t\"" << from << "\" -> \"" << to << "\" ";
				output << "[label = \"" << (from.sequence() + 1) << ": " << (transition.begin() + 1);
				output << "\", color = " << color << ", fontcolor = blue, weight = 5];\n";
			}
			else
			{
				output << "\t\"" << from << "\" -> \"" << to << "\"";
				output << "[label = " << (from.sequence() + 1) << ", color = " << color;
				output << ", fontcolor = blue, weight = 10];\n";
			}
		}
	}
	else if(zt.type() == "Exception")
	{
		for(typename TransitionType::relation_const_iterator it = zt.begin(); it != zt.end(); ++it)
		{
			const Segment& from = (*it).first();
			const Segment& to = (*it).second();
			const Segment& transition = (*it).value();
			const std::string& color = (*(colors.find(from.sequence()))).second;

			if(transition.length() > 1)
			{
				output << "\t\"" << from << "\" -> \"" << to << "\" ";
				output << "[label = \"" << (from.sequence() + 1) << ": " << (transition.begin() + 1) << "..";
				output << (transition.end() + 1) << "\", color = " << color;
				output << " , fontcolor = grey30, style = dotted, weight = 1];\n";
			}
			else if(transition.length() == 1)
			{
				output << "\t\"" << from << "\" -> \"" << to << "\" ";
				output << "[label = \"" << (from.sequence() + 1) << ": " << (transition.begin() + 1);
				output << "\", color = " << color << ", fontcolor = grey30, style = dotted, weight = 1];\n";
			}
			else
			{
				output << "\t\"" << from << "\" -> \"" << to << "\"";
				output << "[label = " << (from.sequence() + 1);
				output << ", fontcolor = grey30, color = " + color + ", style = dotted, weight = 5];\n";
			}
		}
	}
}

#endif
