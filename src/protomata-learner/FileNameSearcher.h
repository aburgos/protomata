
#ifndef FILENAMESEARCHER_H
#define FILENAMESEARCHER_H

#include <cstring>
#include <fstream>
#include <vector>

class FileNameSearcher
{
  public:
	FileNameSearcher() {}
	std::string filename(const std::string&);
};

inline std::string FileNameSearcher::filename(const std::string& ifilename)
{
	if(ifilename.empty()) return ifilename;

	const std::string& sharepath(SHAREPATH);

	// no enviroment variable
	if(!sharepath.empty())
	{
		std::ifstream input(ifilename.c_str());
		// if a file with filename exists on the local directory, use it
		if(input.is_open())
		{
			input.close();
			return ifilename;
		}
		else
		{
			// which are the directories to look in
			std::vector<std::string> dirs;
			dirs.push_back("groups");
			dirs.push_back("components");
			dirs.push_back("parameters");

			// try to find a file with filename in one of the environmet directories
			for(std::vector<std::string>::const_iterator it = dirs.begin(); it != dirs.end(); ++it)
			{
				const std::string& directory = *it;

				std::string path_file = sharepath + "/" + directory + "/" + ifilename;
				std::ifstream f(path_file.c_str());
				if(f.is_open())
				{
					f.close();
					return path_file;
				}
			}
		}
	}

	return ifilename;
}

#endif
