
#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <vector>
#include <iostream>
#include "tclap/CmdLine.h"

class Parameters
{
  protected:
	std::vector<std::pair<std::string, std::string> > string_parameters;

	virtual void sanity_checks() = 0;
	virtual void initialize_values() = 0;
	virtual void add_string_parameters() = 0;
	virtual std::string compute_output_file(const std::string&) const = 0;
	std::string compute_basename(const std::string& filename) const
	{
		size_t lst_path_pos = filename.find_last_of('/');
		size_t beg_base_pos = (lst_path_pos == std::string::npos ? 0 : (lst_path_pos + 1));
		size_t lst_base_pos = filename.find_last_of('.');
		size_t end_base_pos = (lst_base_pos == std::string::npos ? (filename.length() - 1) : (lst_base_pos - 1));
		return filename.substr(beg_base_pos, end_base_pos - beg_base_pos + 1);
	}

  public:
	Parameters() {}
	virtual ~Parameters() {}

	virtual void parse(int argc, char **argv) = 0;
	virtual void add_arguments(TCLAP::CmdLine&) = 0;

	virtual std::string input() const = 0;
	virtual std::string output() const = 0;
	virtual std::string tag() const = 0;

	typedef std::vector<std::pair<std::string, std::string> >::const_iterator const_iterator;
	const_iterator begin() const { return string_parameters.begin(); }
	const_iterator end() const { return string_parameters.end(); }
};

#endif
