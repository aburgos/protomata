
#ifndef PROTOALIGNPARAMETERS_H
#define PROTOALIGNPARAMETERS_H

#include "ProtoscanParameters.h"

class ProtoalignParameters: public ProtoscanParameters
{
  protected:
	TCLAP::ValueArg<std::string> m_protoalign_output;

	virtual void add_string_parameters();
	virtual std::string compute_output_file(const std::string&) const;

  public:
	ProtoalignParameters();
	virtual ~ProtoalignParameters() {}

	virtual void parse(int argc, char **argv);
	virtual void add_arguments(TCLAP::CmdLine&);

	virtual std::string output() const;
};

#endif
