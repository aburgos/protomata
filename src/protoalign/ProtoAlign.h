
#ifndef PROTOALIGN_H
#define PROTOALIGN_H

#include "Site.h"
#include "outputs/PLMA.h"
#include "scores/Viterbi_WithPath.h"
#include "WAGenerator.h"
#include "MultiSequence.h"
#include "PalomaParameters.h"
#include "ProtoalignParameters.h"
#include "generalized_sets/GeneralizedAlignment.h"
#include "ProtoParser.h"
#include "columns/Column.h"
#include "editors/ProtoGraphEditor.h"
#include "alignments/LocalAlignment_UFImp.h"

/*!
From a model of a family of sequences, computes the alignment of a set of sequences.
*/
template <class CntT>
class ProtoAlign
{
  private:
	const MultiSequence m_ms;

  public:
	ProtoAlign() {}
	ProtoAlign(const ProtoalignParameters&);

	GeneralizedAlignment<LocalAlignment_UFImp> align(const WAProperties<CntT>&, bool, bool);
	void output_plma(const MultiSequence&, const GeneralizedAlignment<LocalAlignment_UFImp>&, const ProtoalignParameters&) const;
	const LocalAlignment_UFImp buildAlignment(const MultiSequence&, const SitePartitionElement&) const;
};

template <class CntT>
ProtoAlign<CntT>::ProtoAlign(const ProtoalignParameters& parameters): m_ms(parameters.sequences(), false, parameters.allow_X(), parameters.lobound(), parameters.upbound())
{
	typedef ProtoParser<CntT, AttributedColumn> ProtoParserType;
	typedef typename ProtoParserType::ProtoGraphType ProtoGraphType;

	// parse proto file
	ProtoParserType pp(parameters.input());
	ProtoGraphType pg = pp.parse_zones_edges();

	// set viterbi values
	ProtoGraphEditor<ProtoGraphType> pg_editor;
	pg_editor.set_viterbi_values(pg, parameters.scan_score());

	// create a weighted automata
	WAGenerator<ProtoGraphType> wagen(pg, parameters.exceptions_as_gaps());

	const bool apsm = parameters.allow_partial_sequence_match();
	const bool apmm = parameters.allow_partial_model_match();

	GeneralizedAlignment<LocalAlignment_UFImp> ga = align(wagen.weighted_automata(), apsm, apmm);
	output_plma(m_ms, ga, parameters);
}

template <class CntT>
GeneralizedAlignment<LocalAlignment_UFImp> ProtoAlign<CntT>::align(const WAProperties<CntT>& wa, bool allow_partial_sequence_match, bool allow_partial_model_match)
{
	// setup for computing the paths of each sequence
	Viterbi_WithPath<CntT> sc(wa, allow_partial_sequence_match, allow_partial_model_match);

	// each (zone, column) pair of the protograph maps to a set of sites of a sequence
	std::map<std::pair<unsigned int, unsigned int>, SiteSet> zonecolumn2sites;

	// store the sites for each sequence
	for(MultiSequence::const_iterator it = m_ms.begin(); it != m_ms.end(); ++it)
	{
		const Sequence& seq = *it;

		// compute the path (score function) of the current sequence
		sc.score(seq);

		const AlignmentPath& path = sc.path(seq);
		for(AlignmentPath::const_iterator pit = path.begin(); pit != path.end(); ++pit)
		{
			const unsigned int position = (*pit).first;
			const typename AlignmentPath::ZoneColumnPair& zcp = path[position];
			zonecolumn2sites[zcp].insert(Site(seq.id(), position));
		}
	}

	// build the alignment
	GeneralizedAlignment<LocalAlignment_UFImp> ga(m_ms);

	// each set of sites of the zone2sites map is a site partition element
	std::map<std::pair<unsigned int, unsigned int>, SiteSet>::const_iterator it;
	for(it = zonecolumn2sites.begin(); it != zonecolumn2sites.end(); ++it)
	{
		const SitePartitionElement& spe = (*it).second;
		ga.addMatching(buildAlignment(m_ms, spe));
	}

	return ga;
}

template <class CntT>
void ProtoAlign<CntT>::output_plma(const MultiSequence& ms, const GeneralizedAlignment<LocalAlignment_UFImp>& ga, const ProtoalignParameters& parameters) const
{
	PLMA<LocalAlignment_UFImp> plma(ms, ga, parameters);
}

template <class CntT>
const LocalAlignment_UFImp ProtoAlign<CntT>::buildAlignment(const MultiSequence& ms, const SitePartitionElement& sites) const
{
	LocalAlignment_UFImp alignment(ms);

	SitePartitionElement::const_iterator it1 = sites.begin();
	if(it1 != sites.end())
	{
		if(sites.size() == 1)
		{
			SitePairRelation spr(std::make_pair(*it1, *it1), 0);
			alignment.addMatching(spr);
		}

		SitePartitionElement::const_iterator it2 = it1; it2++;
		while(it2 != sites.end())
		{
			SitePairRelation spr(std::make_pair(*it1, *it2), 0);
			alignment.addMatching(spr);
			it2++;
		}
	}

	return alignment;
}

#endif
