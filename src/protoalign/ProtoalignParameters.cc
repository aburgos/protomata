
#include "ProtoalignParameters.h"

ProtoalignParameters::ProtoalignParameters(): ProtoscanParameters(),
	m_protoalign_output(TCLAP::ValueArg<std::string>("o", "output", "Set output file name for the resulting PLMA.", false, "", "plma_filename"))
{}

void ProtoalignParameters::parse(int argc, char **argv)
{
	TCLAP::CmdLine cmd("Align a set of sequences using a Protomata model.", ' ', VERSION);
	ProtoalignParameters::add_arguments(cmd);
	cmd.parse(argc, argv);
	initialize_values();
	sanity_checks();
	add_string_parameters();
}

void ProtoalignParameters::add_arguments(TCLAP::CmdLine& cmd)
{
	cmd.add(m_output_wa);
	cmd.add(m_significativity);
	cmd.add(m_use_integers);
	cmd.add(m_allow_partial_model_match);
	cmd.add(m_disallow_partial_sequence_match);
	cmd.add(m_exceptions_as_gaps);
	cmd.add(m_sequences);
	cmd.add(m_protoalign_output);
	cmd.add(m_protoscan_input);
}

void ProtoalignParameters::add_string_parameters()
{
	string_parameters.push_back(std::make_pair("input", input()));
	string_parameters.push_back(std::make_pair("output", output()));
	string_parameters.push_back(std::make_pair("sequences", sequences()));
	if(!pseudocounts().empty())
	{
		string_parameters.push_back(std::make_pair("pseudocounts", pseudocounts()));
		string_parameters.push_back(std::make_pair("components", components_file()));
	}
	string_parameters.push_back(std::make_pair("exception_as_gaps", exceptions_as_gaps() ? "true" : "false"));
	string_parameters.push_back(std::make_pair("allow_partial_model_match", allow_partial_model_match() ? "true" : "false"));
	string_parameters.push_back(std::make_pair("allow_partial_sequence_match", allow_partial_sequence_match() ? "true" : "false"));
}

std::string ProtoalignParameters::compute_output_file(const std::string& filename) const
{
	size_t lst_path_pos = filename.find_last_of('/');
	size_t beg_base_pos = (lst_path_pos == std::string::npos ? 0 : (lst_path_pos + 1));
	size_t lst_base_pos = filename.find_last_of('.');
	size_t end_base_pos = (lst_base_pos == std::string::npos ? (filename.length() - 1) : (lst_base_pos - 1));
	std::string basename = filename.substr(beg_base_pos, end_base_pos - beg_base_pos + 1);

	std::ostringstream ss;
	ss << basename;
	if(allow_X()) ss << "_Xst:" << m_xstr.getValue();
	if(!allow_X()) ss << "_noX";
	if(exceptions_as_gaps()) ss << "_eag";
	if(allow_partial_sequence_match()) ss << "_psm";
	if(allow_partial_model_match()) ss << "_pmm";
	if(use_integers()) ss << "_ui";
	if(!sequences().empty())
	{
		size_t pos = sequences().find_last_of('/');
		std::string sequences_basename = sequences().substr(pos + 1, sequences().length());
		ss << "_p:" << sequences_basename;
	}

	ss << "_plma.xml";
	return ss.str();
}

std::string ProtoalignParameters::output() const
{
	if(m_protoalign_output.getValue().empty()) return ProtoalignParameters::compute_output_file(input());
	else return m_protoalign_output.getValue();
}
