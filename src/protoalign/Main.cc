
#include <time.h>
#include <iostream>
#include "ProtoAlign.h"
#include "ProtoalignParameters.h"

int main(int argc, char **argv)
{
	//double start, end;
	//start = (double)clock();

	ProtoalignParameters parameters;
	parameters.parse(argc, argv);

	if(parameters.use_integers()) ProtoAlign<int> protoalign(parameters);
	else ProtoAlign<double> protoalign(parameters);

	//end = (double)clock();
	//double time_elapsed = double(end - start) / CLOCKS_PER_SEC;
	//std::cout << std::endl << "Time elapsed: " << time_elapsed << " secs" << std::endl;
	return 0;
}
