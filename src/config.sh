#!/bin/sh

# script:	config.sh
# task:		creates Makefile.config from file Makefile.config_template and echoes its filename.
# author:	Andres Burgos
# e-Mail:	andres.burgos@irisa.fr
# updated:	2011-04-13

REQUIERED_ARGS=1
E_BADARGS=65

if [ $# -lt $REQUIERED_ARGS ]
then
	echo "Usage: `basename $0` <base_directory>"
#	echo "Usage: `basename $0` <base_directory> [OPTIONS]"
	echo "Creates Makefile.config from file Makefile.config_template and echoes its filename."
	echo "The argument directory should be a relative or absolute path to the base directory."
#	echo ""
#	echo "Options:"
#	echo "\t--prefix\t\tInstall directory."
#	echo "\t--bindir\t\tBinaries directory."
#	echo "\t--sharedir\t\tShared files directory."
#	echo "\t--libdir\t\tLibraries directory."
#	echo "\t--gsl-includes\t\tGSL source directory."
#	echo "\t--gsl-libraries\t\tGSL libraries directory."
#	echo "\t--debug\t\t\tCompile with debug flags."
	exit $E_BADARGS
fi

BASE_PATH=$1
#PREFIX=""
#BINDIR=""
#SHAREDIR=""
#LIBDIR=""
#GSLINCDIR=""
#GSLLIBDIR=""
#DEBUG=0

#while true; do
#	case $2 in
#		--prefix) PREFIX=$3; shift 2 ;;
#		--bindir) BINDIR=$3; shift 2 ;;
#		--sharedir) SHAREDIR=$3; shift 2 ;;
#		--libdir) LIBDIR=$3; shift 2 ;;
#		--gsl-includes) GSLINCDIR=$3; shift 2 ;;
#		--gsl-libraries) GSLLIBDIR=$3; shift 2 ;;
#		--debug) DEBUG=1; shift 2 ;;
#		*) break ;;
#	esac
#done

MAKEFILE_CONFIG="$BASE_PATH/src/Makefile.config"
MAKEFILE_CONFIG_TEMPLATE="$BASE_PATH/src/Makefile.config_template"

if [ ! -f $MAKEFILE_CONFIG ]
then
	if [ ! -f $MAKEFILE_CONFIG_TEMPLATE ]
	then
		echo "No $MAKEFILE_CONFIG_TEMPLATE found!"
	else
		# get the absolute path
		BASE_DIR=`readlink -f $BASE_PATH`
		OLD_TEXT="BASE_DIR = .."
		NEW_TEXT="BASE_DIR = $BASE_DIR"
		# replace the base directory in the user Makefile.config
		sed -e "s|$OLD_TEXT|$NEW_TEXT|g" $MAKEFILE_CONFIG_TEMPLATE > $MAKEFILE_CONFIG
	fi
fi

echo $MAKEFILE_CONFIG
