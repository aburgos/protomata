
#ifndef ENUMTYPES_H
#define ENUMTYPES_H

enum BlockType
{
	connCompExhaustive,
	connCompGreedy,
	connCompBestWeights,
	cliqueExhaustive,
	cliqueGreedy,
	cliqueBestWeights
};

enum CandGenType
{
	maxWeightFragments,
	maxSupportFragments,
	bestConnCompOnly,
	bestCompatibleConnComp
};

enum GeneralizedSetType
{
	GM,
	GLM,
	GA,
	GLA,
	GA_GC,
	GLA_LC
};

enum OutputType
{
	PLMAt,
	ALNt
};

enum ReaderType
{
	DialignR,
	SWR
};

enum SeedType
{
	dialignW,
	support
};

enum MatchingSetType
{
	MSet,
	PLMSet,
	SMSet,
	SPLMSet
};

enum DiscSegOpt
{
	blockSegments,
	insideSegments,
	overlappedSegments,
	noSegments
};

enum DiscFragOpt
{
	blockFragments,
	blockAdjFragments,
	seedsToDiscAdjFragments,
	noFragments
};

enum ScoreType
{
	DNA,
	Blosum30,
	Blosum62,
	Blosum80
};

enum WeightsParserType
{
	Manual,
	Clustal
};

#endif
