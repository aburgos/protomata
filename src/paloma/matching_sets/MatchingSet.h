
#ifndef MATCHINGSET_H
#define MATCHINGSET_H

#include <list>
#include "MultiSequence.h"
#include "matchings/PairwiseLocalMatching.h"

/*!
A MatchingSet is a set of Matching objects or any derived class from it.
*/
template <typename InM>
class MatchingSet
{
  protected:
	//! A list of matchings. A list is necessary since it keeps the position in memory of its objects.
	std::list<InM> matchings;
	/*!
		Gets the address of the latest added matching. This is needed to construct the pairwise segment graph.
		\return A pointer to a pairwise local matching.
	*/
	const PairwiseLocalMatching* lastAddedAddress(const InM&) const;
	//!	Definition of the output (cout) operator for a MatchingSet.
	virtual void print(std::ostream& o) const;

  public:
	//! The constructor. The MultiSequence object is not used.
	MatchingSet(const MultiSequence&) {}
	// The desctructor.
	virtual ~MatchingSet() {}
	//! Number of matchings.
	unsigned int size() const { return this->matchings.size(); }
	/*!
		Adds a matching to the list.
		\return Always True.
	*/
	virtual bool storeMatching(const InM&);
	//!	Definition of the output (cout) operator for a set. It calls the protected function print defined on each class.
	friend std::ostream& operator<<(std::ostream& o, const MatchingSet<InM>& mset)	{ mset.print(o); return o; }
	//! Alias for the iterator through the list of matchings.
	typedef typename std::list<InM>::const_iterator matching_iterator;
	/*! \return An iterator to the begin of the matchings list. */
	matching_iterator matching_begin() const { return matchings.begin(); }
	/*! \return An iterator to the end of the matchings list. */
	matching_iterator matching_end() const { return matchings.end(); }
};

template <typename InM>
bool MatchingSet<InM>::storeMatching(const InM& m)
{
	matchings.push_back(m);
	return true;
}

template <typename InM>
void MatchingSet<InM>::print(std::ostream& o) const
{
	o << std::endl << "\033[1;32m" << "Matching set is of size " << matchings.size();
	o << "\033[m" << std::endl << std::endl;

	//typename list<InM>::const_iterator it;
	//for(it = matchings.begin(); it != matchings.end(); it++)
		//o << *it << endl;
}

template <typename InM>
const PairwiseLocalMatching* MatchingSet<InM>::lastAddedAddress(const InM&) const
{
	// is this GCC compiler specific?
	return &matchings.back();
}

#endif
