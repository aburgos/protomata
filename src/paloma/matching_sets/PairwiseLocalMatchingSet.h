
#ifndef LOCALMATCHINGSET_H
#define LOCALMATCHINGSET_H

#include "MatchingSet.h"
#include "matchings/PairwiseLocalMatching.h"

/*!
A PairwiseLocalMatchingSet is a MatchingSet that keeps an updated PairwiseSegmentGraph representing the relations between the segments of the Matchings added to the set.
*/
template <typename InM>
class PairwiseLocalMatchingSet: public virtual MatchingSet<InM>
{
  protected:
	//! A pairwise segment graph, which also contains pointers to the pairwise matchings.
	PairwiseSegmentGraph pairwiseSegmentGraph;
	//!	Definition of the output (cout) operator for a PairwiseLocalMatchingSet.
	virtual void print(std::ostream& o) const;

  public:
	//! The constructor. The MultiSequence object is not used.
	PairwiseLocalMatchingSet(const MultiSequence& ms): MatchingSet<InM>(ms) {}
	// The desctructor.
	virtual ~PairwiseLocalMatchingSet() {}
	/*!
		First the parameter is store in the list. After that, for each segment relation of the stored matching, an edge between the segments is added. Notice that the stored matching should be pairwise for this to work properly, because a pointer to the pairwise matching should be stored in the graph. This condition is not checked anywhere.
		\return Always True.
	*/
	virtual bool storeMatching(const InM&);
	//! Alias for the iterator through the pairwise segment relations.
	typedef PairwiseSegmentGraph::relation_iterator psr_iterator;
	/*! \return An iterator to the begin of the pairwise segment relations. */
	psr_iterator psr_begin() const { return pairwiseSegmentGraph.relation_begin(); }
	/*! \return An iterator to the end of the pairwise segment relations. */
	psr_iterator psr_end() const { return pairwiseSegmentGraph.relation_end(); }
	//! Alias for the iterator through the segments of the graph.
	typedef PairwiseSegmentGraph::vertex_iterator segment_iterator;
	/*! \return An iterator to the begin of the segments of the graph. */
	segment_iterator segment_begin() const { return pairwiseSegmentGraph.vertex_begin(); }
	/*! \return An iterator to the end of the segments of the graph. */
	segment_iterator segment_end() const { return pairwiseSegmentGraph.vertex_end(); }
	//! Needs to manipulate the graph efficiently
	template <typename In, typename Out> friend class SeedApproach;
	//! Needs to manipulate the graph efficiently
	template <typename In, typename Out> friend class ConnectedComponent;
};

template <typename InM>
bool PairwiseLocalMatchingSet<InM>::storeMatching(const InM& m)
{
	MatchingSet<InM>::storeMatching(m);

	typename InM::segment_iterator it1, it2;
	for(it1 = m.segment_begin(); it1 != m.segment_end(); it1++)
		for(it2 = ++it1, it1--; it2 != m.segment_end(); it2++)
			if(!pairwiseSegmentGraph.addEdge(*it1, *it2, lastAddedAddress(m)))
				return false;

	return true;
}

template <typename InM>
void PairwiseLocalMatchingSet<InM>::print(std::ostream& o) const
{
	MatchingSet<InM>::print(o);

	o << "\033[1;35m" << "Segment Graph:" << "\033[m" << std::endl << std::endl;
	o << pairwiseSegmentGraph << std::endl;
}

#endif
