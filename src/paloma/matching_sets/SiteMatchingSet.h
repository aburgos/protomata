
#ifndef SITEMATSET_H
#define SITEMATSET_H

#include "MatchingSet.h"
#include "matchings/Matching_SiGrImp.h"

/*!
A SiteMatchingSet is a MatchingSet but it also keeps an updated SiteGraph representing the relations between the sites of the matchings added to the set.
*/
template <typename InM>
class SiteMatchingSet: public virtual MatchingSet<InM>, protected virtual Matching_SiGrImp
{
  protected:
	//!	Definition of the output (cout) operator for a SiteMatchingSet.
	virtual void print(std::ostream& o) const;

  public:
	//! The constructor. The MultiSequence object is not used.
	SiteMatchingSet(const MultiSequence& ms): MatchingSet<InM>(ms), Matching_SiGrImp(ms) {}
	// The desctructor.
	virtual ~SiteMatchingSet() {}
	/*!
		First the site relations of the matchings are added to the graph. If it was succesfully added, the matching is added to the list of matchings.
		\return True if the site relations and the matching are successfully added. False otherwise.
	*/
	virtual bool storeMatching(const InM&);
	//! Alias for the iterator through the sites.
	typedef SiteGraph::vertex_iterator site_iterator;
	/*! \return An iterator to the begin of the site relations. */
	site_iterator site_begin() const { return siteGraph.vertex_begin(); }
	/*! \return An iterator to the end of the site relations. */
	site_iterator site_end() const { return siteGraph.vertex_end(); }
	//! Alias for the iterator through the site relations.
	typedef SiteGraph::relation_iterator relation_iterator;
	/*! \return An iterator to the begin of the site relations. */
	relation_iterator relation_begin() const { return siteGraph.relation_begin(); }
	/*! \return An iterator to the end of the site relations. */
	relation_iterator relation_end() const { return siteGraph.relation_end(); }
};

template <typename InM>
bool SiteMatchingSet<InM>::storeMatching(const InM& m)
{
	if(Matching_SiGrImp::addMatching(m)) return MatchingSet<InM>::storeMatching(m);
	else return false;
}

template <typename InM>
void SiteMatchingSet<InM>::print(std::ostream& o) const
{
	MatchingSet<InM>::print(o);

	o << "\033[1;35m" << "Site Graph:" << "\033[m" << std::endl << std::endl;
	o << siteGraph << std::endl;
}

#endif
