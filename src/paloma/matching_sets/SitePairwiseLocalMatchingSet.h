
#ifndef SITELOCMATSET_H
#define SITELOCMATSET_H

#include "PairwiseLocalMatchingSet.h"
#include "matchings/Matching_SiGrImp.h"

/*!
A SitePairwiseLocalMatchingSet is keeps both an updated SiteGraph and an updated PairwiseSegmentGraph with the relations of the matchings added to the set.
*/
template <typename InM>
class SitePairwiseLocalMatchingSet: public virtual PairwiseLocalMatchingSet<InM>, private virtual Matching_SiGrImp
{
  protected:
	//!	Definition of the output (cout) operator for a SitePairwiseLocalMatchingSet.
	virtual void print(std::ostream& o) const;

  public:
	//! The constructor. The MultiSequence object is not used.
	SitePairwiseLocalMatchingSet(const MultiSequence& ms): MatchingSet<InM>(ms), PairwiseLocalMatchingSet<InM>(ms), Matching_SiGrImp(ms) {}
	// The desctructor.
	virtual ~SitePairwiseLocalMatchingSet() {}
	/*!
		First it adds the matching to the list, then it updates the pairwise segment graph and after the site graph.
		\return True if everything is added. False otherwise.
	*/
	virtual bool storeMatching(const InM&);
};

template <typename InM>
bool SitePairwiseLocalMatchingSet<InM>::storeMatching(const InM& m)
{
	if(PairwiseLocalMatchingSet<InM>::storeMatching(m)) return Matching_SiGrImp::addMatching(m);
	else return false;
}

template <typename InM>
void SitePairwiseLocalMatchingSet<InM>::print(std::ostream& o) const
{
	PairwiseLocalMatchingSet<InM>::print(o);
	Matching_SiGrImp::print(o);
}

#endif
