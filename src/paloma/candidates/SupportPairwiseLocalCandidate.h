
#ifndef SUPPORTPAIR_H
#define SUPPORTPAIR_H

#include "CandidateGenerator.h"

/*!
Returns the current maximum support Matching. The support is computed as in SFP characterization. The matchings generated are exactly the same as the local pairwise matchings read from input, with a type conversion to fulfil the output matching type requirement.
*/
template <typename InM, typename OutM>
class SupportPairwiseLocalCandidate: public CandidateGenerator<InM, OutM>
{
  private:
	typedef typename PairwiseLocalMatchingSet<InM>::psr_iterator psr_it;
	typedef typename PairwiseLocalMatchingSet<InM>::segment_iterator segment_it;
	typedef typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator adjacent_it;
	//! A container to store the candidates and their respective support.
	std::multimap<unsigned int, const PairwiseLocalMatching*> candidates;
	/*!
		Gets the support of a pairwise matching.
		\param psr_it is an pair segment relation iterator.
		\return The support of the Matching.
	*/
	unsigned int getSupport(psr_it);
	/*!
		Gets the support of a pairwise matching.
		\param psr_it is an pair segment relation iterator.
		\param adjacent_it is an adjacent vertex iterator.
		\param adjacent_it is an adjacent vertex iterator.
		\return A boolean telling whether the segment v is a support or not.
	*/
	bool isSupportedBySegment(psr_it, adjacent_it, adjacent_it);

  public:
	//! The constructor adds every Matching in the segment graph to the candidates container, after having computed its support.
	SupportPairwiseLocalCandidate(const MultiSequence&, const GeneralizedSet<OutM>&, const PairwiseLocalMatchingSet<InM>&, const PalomaParameters&);
	//! The destructor.
	~SupportPairwiseLocalCandidate() {}
	/*!
		Gets the maximum weight matching in the container.
		\return A pointer to a Matching object.
	*/
	const OutM* getNextMatching();
};

template <typename InM, typename OutM>
SupportPairwiseLocalCandidate<InM, OutM>::SupportPairwiseLocalCandidate(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, const PairwiseLocalMatchingSet<InM>& plms, const PalomaParameters& params): CandidateGenerator<InM, OutM>(ms, gs, params)
{
	for(psr_it it = plms.psr_begin(); it != plms.psr_end(); ++it)
	{
		unsigned int support = 0;
		if((support = getSupport(it)) > 0) 
			candidates.insert(std::make_pair(support, (*it).value()));
	}
}

template <typename InM, typename OutM>
const OutM* SupportPairwiseLocalCandidate<InM, OutM>::getNextMatching()
{
	do
	{
		if(candidates.empty()) return NULL;
		std::multimap<unsigned int, const PairwiseLocalMatching*>::reverse_iterator rit;
		rit = candidates.rbegin();
		const PairwiseLocalMatching* plm = (*rit).second;
		candidates.erase(--rit.base());

		OutM* m = new OutM(this->m_ms);
		m->addMatching(*plm);
		if(this->m_gs.isCompatible(*m)) { this->m_toDestroy.push_back(m); return m; }
		else delete m;
	}
	while(true);

	return NULL;
}

template <typename InM, typename OutM>
bool SupportPairwiseLocalCandidate<InM, OutM>::isSupportedBySegment(psr_it it, adjacent_it it1, adjacent_it it2)
{
	double f1 = (!it1)->segmentsWeight();
	double f2 = (!it2)->segmentsWeight();
	double f3 = (*it).value()->segmentsWeight();

	return (f1 + f2 >= f3);
}

template <typename InM, typename OutM>
unsigned int SupportPairwiseLocalCandidate<InM, OutM>::getSupport(psr_it it)
{
	unsigned int support = 0;

	// compute intersection
	std::vector<std::pair<adjacent_it, adjacent_it> > intersection;
	segment_it it1 = it.first_iterator();
	segment_it it2 = it.second_iterator();
	for(adjacent_it ait1 = it1.adjacent_begin(); ait1 != it1.adjacent_end(); ++ait1)
		for(adjacent_it ait2 = it2.adjacent_begin(); ait2 != it2.adjacent_end(); ++ait2)
			if(*ait1 == *ait2) intersection.push_back(std::make_pair(ait1, ait2));

	std::vector<bool> seqSupporter(this->m_ms.size(), 0);

	typename std::vector<std::pair<adjacent_it, adjacent_it> >::iterator vit;
	for(vit = intersection.begin(); vit != intersection.end(); ++vit)
	{
		unsigned int sequence = (*((*vit).first)).sequence();
		if(seqSupporter[sequence]) continue;
		if(isSupportedBySegment(it, (*vit).first, (*vit).second)) seqSupporter[sequence] = 1;
	}

	for(std::vector<bool>::const_iterator bit = seqSupporter.begin(); bit != seqSupporter.end(); ++bit)
		support += *bit;

	return support;
}

#endif
