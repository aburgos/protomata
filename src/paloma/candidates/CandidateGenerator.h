
#ifndef CANDIDATEGENERATOR_H
#define CANDIDATEGENERATOR_H

#include "PalomaParameters.h"
#include "generalized_sets/GeneralizedSet.h"

/*!
A pure abstract class. Obtains a matching somehow. The matching type will be OutM (output matching), and will be obtained from a set of matchings of type InM (input matching). The type OutM can't be pairwise, since some derived candidates generators construct output matchings objects by calling an incompatible constructor with the pairwise ones.
*/
template <typename InM, typename OutM>
class CandidateGenerator
{
  protected:
	//! All program's parameters.
	const PalomaParameters& m_params;
	//! The sequences on which the class will work.
	const MultiSequence& m_ms;
	//! The generalized set to check for compatibility of the generated matchings.
	const GeneralizedSet<OutM>& m_gs;
	//! Store the pointers to the created candidates.
	std::vector<OutM*> m_toDestroy;

  public:
	CandidateGenerator(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, const PalomaParameters& params): m_params(params), m_ms(ms), m_gs(gs) {}
	//! The destructor deletes the created and accepted (compatibles) candidates.
	virtual ~CandidateGenerator()
	{
		typename std::vector<OutM*>::const_iterator it;
		for(it = this->m_toDestroy.begin(); it != this->m_toDestroy.end(); ++it)
			delete *it;
	}
	/*!
		Gets the next matching generated.
		\return A pointer to a Matching object.
	*/
	virtual const OutM* getNextMatching() = 0;
};

#endif
