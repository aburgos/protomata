
#ifndef BESTCONNCOMPSEEDAPPROACH_H
#define BESTCONNCOMPSEEDAPPROACH_H

#include "SeedApproach.h"

/*!
Using the chosen block generator, it computes a matching for each seed. If this matching is compatible, it will be returned when getNextMatching is called. If it is not compatible, the seed is discarded and a new matching is computed using the next seed.
*/
template <typename InM, typename OutM>
class BestConnCompOnly: public SeedApproach<InM, OutM>
{
  public:
	//! The constructor.
	BestConnCompOnly(const MultiSequence&, const GeneralizedSet<OutM>&, PairwiseLocalMatchingSet<InM>&, SegmentSeedGenerator&, const PalomaParameters&);
	//! The destructor.
	virtual ~BestConnCompOnly() {}
	/*!
		First it calls getNextSegmentSet to get the next seed, a segment set. Then it instantiate a block generator object and a new block (only ONE block, by calling computeOneBlock) is created. It is checked that this block satisfies the quorum constraint. If it does, a new matching (of type OutM) is constructed from the block and compatibility with the generalized set is checked. If it's compatible, a pointer to the matching object is returned, otherwise the whole process start over again. If there is no more seeds (or the ones left will be discarded), it returns NULL.
		\return A pointer to a Matching object.
	*/
	OutM* getNextMatching();
};

template <typename InM, typename OutM>
BestConnCompOnly<InM, OutM>::BestConnCompOnly(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, SegmentSeedGenerator& ssg, const PalomaParameters& params): SeedApproach<InM, OutM>(ms, gs, plms, ssg, params)
{
}

template <typename InM, typename OutM>
OutM* BestConnCompOnly<InM, OutM>::getNextMatching()
{
	static unsigned int acceptedBlocks = 0;
	static unsigned int rejectedBlocks = 0;
	static unsigned int rejectedSeeds = 0;
	static bool matchingReturned = false;
	OutM* candidate = NULL;
	do
	{
		unsigned int seedsLeftToTry = this->m_seeds->seeds_left() - this->discardedSeeds.size();

		if(this->m_params.verbose() && matchingReturned)
		{
			std::cout << "\033[1;33m" << this->m_seeds->seeds_left() << " seeds left" << "\033[m";
			std::cout << "\033[1;32m" << " (" << seedsLeftToTry << " to check; " << "\033[m";
			std::cout << "\033[1;31m" << this->discardedSeeds.size() << " incompatible)" << "\033[m" << std::endl;

			std::cout << "\033[1;37m" << this->checkedSeeds.size() << " seeds were already checked" << "\033[m" << std::endl;
			std::cout << "\033[1;30m" << rejectedSeeds << " seeds were rejected" << "\033[m" << std::endl;
			std::cout << "\033[1;32m" << acceptedBlocks << " blocks were accepted" << "\033[m" << std::endl;
			std::cout << "\033[1;33m" << rejectedBlocks << " blocks were rejected" << "\033[m" << std::endl;
			std::cout << "\033[1;34m" << this->discarded << " incompatible fragments deleted" << "\033[m" << std::endl;
			//std::cout << "\033[1;31m" << candidateBlock.weight << " block weight" << "\033[m" << std::endl;
			std::cout << std::endl;
		}

		matchingReturned = false;

		// if there are no more seeds to try, stop
		if(seedsLeftToTry == 0) return NULL;

		const std::pair<bool, SegmentSet>& p = this->m_seeds->getNextSegmentSet();
		if(!p.first) return NULL;

		bool discardCurrentSeed = false;

		const SegmentSet& check = p.second;
		for(SegmentSet::const_iterator sit = check.begin(); sit != check.end(); ++sit)
		{
			this->checkedSeeds.insert(*sit);
			SegmentSet::const_iterator dit;
			if((dit = this->discardedSeeds.find(*sit)) != this->discardedSeeds.end())
				{ discardCurrentSeed = true; this->discardedSeeds.erase(dit); break; }
		}

		if(discardCurrentSeed) continue;

		// instantiate the chosen block generator implementation
		BlockFactory<InM, OutM> cfact;
		this->blockImp = cfact.create(this->m_ms, this->m_gs, *this->m_plms, p.second, this->m_params);
		this->blockImp->computeOneBlock();
		CandidateBlock candidateBlock = this->blockImp->getNextBlock();

		if(candidateBlock.isEmpty() || candidateBlock.segments.size() < this->m_params.plma_quorum())
			{ if(this->m_params.verbose()) std::cout << "Rejected!" << std::endl; rejectedSeeds++; delete this->blockImp; continue; }

		candidate = SeedApproach<InM, OutM>::getMatchingFromBlock(candidateBlock);
		delete this->blockImp;

		if(!this->m_gs.isCompatible(*candidate))
			{ if(this->m_params.verbose()) std::cout << "Not compatible!" << std::endl; delete candidate; rejectedSeeds++; rejectedBlocks++; continue; }
		else
		{
			if(this->m_params.verbose()) std::cout << "Accepted! (" << candidateBlock.weight << ")" << std::endl;
			++acceptedBlocks;
			matchingReturned = true;
			this->m_toDestroy.push_back(candidate);

			switch(this->m_params.discard_segments())
			{
				case blockSegments: this->discardBlockSegments(candidateBlock); break;
				case insideSegments: this->discardInsideSegments(candidateBlock); break;
				case overlappedSegments: this->discardOverlappedSegments(candidateBlock); break;
				default: break;
			}
			switch(this->m_params.discard_fragments())
			{
				case blockFragments: this->discardBlockFragments(candidateBlock); break;
				case blockAdjFragments: this->discardBlockAdjacentsFragments(candidateBlock); break;
				case seedsToDiscAdjFragments: this->discardSeedsToDiscardAdjacentsFragments(); break;
				default: break;
			}

			break;
		}
	} while(true);

	return candidate;
}

#endif
