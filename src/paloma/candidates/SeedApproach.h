
#ifndef SEEDAPPROACH_H
#define SEEDAPPROACH_H

#include "EnumTypes.h"
#include "CandidateGenerator.h"
#include "alignments/LocalAlignment_UFImp.h"
#include "factories/BlockFactory.h"
#include "generalized_sets/GlobalConsistentGA.h"
#include "seeds/SegmentSeedGenerator.h"

/*!
Implements differet heuristics approaches for generating a candidate from a seed.
*/
template <class InM, class OutM>
class SeedApproach: public CandidateGenerator<InM, OutM>
{
  protected:
	//! A pointer to a seed generator passed as parameter.
	SegmentSeedGenerator* m_seeds;
	//! A pointer to the block generator to be used.
	BlockGenerator* blockImp;
	//! Input set with all the matchings read somewhere.
	PairwiseLocalMatchingSet<InM>* m_plms;
	//! Fragments discarded by the heuristics approaches applied.
	unsigned int discarded;
	//! Set of seeds that were "checked", i.e. were utilized for creating a candidate.
	SegmentSet checkedSeeds;
	//! Set of seeds that were not checked but don't need to be checked. This is determined by the heuristic approach used.
	SegmentSet discardedSeeds;
	/*!
		Discards all the segments that belongs to the parameter block. This means that this segments won't be considered as future seeds, thus they are inserted in the discardedSeeds segment set.
		\param CandidateBlock contains the segments that will be discarded as seeds. It is intended to be used with Blocks that represents compatible candidates that will be added to the final set.
	*/
	void discardBlockSegments(const CandidateBlock&);
	/*!
		Discards all the segments that belongs or are "inside" the parameter block. Inside means those segments that are completely included in any of the segments of the block. This means that this segments won't be considered as future seeds, thus they are inserted in the discardedSeeds segment set. This approach includes the discardBlockSegments approach.
		\param CandidateBlock contains the segments that will be used to search for included segments and then discard them as seeds. It is intended to be used with Blocks that represents compatible candidates that will be added to the final set.
	*/
	void discardInsideSegments(const CandidateBlock&);
	/*!
		Discards all the segments that belongs or are "overlapped" the parameter block. Overlapped means those segments that share at least one site in any of the segments of the block. This means that this segments won't be considered as future seeds, thus they are inserted in the discardedSeeds segment set. This approach includes the discardInsideSegments approach.
		\param CandidateBlock contains the segments that will be used to search for overlapped segments and then discard them as seeds. It is intended to be used with Blocks that represents compatible candidates that will be added to the final set.
	*/
	void discardOverlappedSegments(const CandidateBlock&);
	/*!
		Discards all the fragments that forms the block. This manipulates the segment graph, deleting the edges corresponding to this fragments. This means that this segments won't be considered as future seeds, thus they are inserted in the discardedSeeds segment set.
		\param CandidateBlock contains the fragments that will be erased from the segment graph, and whose segments will be discarded as seeds. It is intended to be used with Blocks that represents compatible candidates that will be added to the final set.
	*/
	void discardBlockFragments(const CandidateBlock&);
	/*!
		Discards all the fragments that are formed by any of the segments that belongs to the parameter block. This manipulates the segment graph, deleting the edges corresponding to this fragments. This means that this segments won't be considered as future seeds, thus they are inserted in the discardedSeeds segment set.
		\param CandidateBlock contains the segments whose fragments they belong to will be erased from the segment graph. The segments of the erased fragments will be discarded as seeds. It is intended to be used with Blocks that represents compatible candidates that will be added to the final set.
	*/
	void discardBlockAdjacentsFragments(const CandidateBlock&);
	/*!
		Discards all the fragments that contains at least one segment in the seedsToDiscard segment set. This manipulates the segment graph, deleting the edges corresponding to this fragments. This means that this segments won't be considered as future seeds, thus they are inserted in the discardedSeeds segment set.
	*/
	void discardSeedsToDiscardAdjacentsFragments();
	/*!
		Deletes from the segment graph all the edges that are not compatible with the final global consistent generalized alignment.
		\param The GlobalConsistentGA that will be used to check for compatibility with each fragment.
	*/
	void discardIncompatibleFragments(const GlobalConsistentGA<OutM>&);
	/*!
		Adds to discardedSeeds the segments that, depending on the segment graph, can't take part of a candidate that fulfil the quorum constraint.
		\param An unsigned int determining the minimum segment size of a candidate.
	*/
	void discardSeedsByQuorum(unsigned int);
	/*!
		Creates an output matching from a block. This is done by adding all the pairwise local matchings from the block to the generated candidate.
		\param A block from which a candidate will be generated.
		\return A pointer to an output Matching object, a generated candidate.
	*/
	OutM* getMatchingFromBlock(const CandidateBlock&);
	/*!
		Checks whether a segment is "inside" any of the segments of a block.
		\return True if the segment is "inside" any of the segments of the block. False otherwise.
	*/
	bool isInsideBlock(const Segment&, const CandidateBlock&);
	/*!
		Checks whether a segment overlapps any segment of a block.
		\return True if the segment overlapps any segment of the block. False otherwise.
	*/
	bool overlapsBlock(const Segment&, const CandidateBlock&);

  public:
	//! The constructor sets all private variables and discard the all seeds by quorum.
	SeedApproach(const MultiSequence&, const GeneralizedSet<OutM>&, PairwiseLocalMatchingSet<InM>&, SegmentSeedGenerator&, const PalomaParameters&);
	virtual ~SeedApproach() {}
};

template <class InM, class OutM>
SeedApproach<InM, OutM>::SeedApproach(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, SegmentSeedGenerator& seeds, const PalomaParameters& params): CandidateGenerator<InM, OutM>(ms, gs, params), m_seeds(&seeds), m_plms(&plms), discarded(0)
{
	discardSeedsByQuorum(params.plma_quorum());
}

template <class InM, class OutM>
void SeedApproach<InM, OutM>::discardBlockSegments(const CandidateBlock& b)
{
	for(SegmentSet::const_iterator it = b.segments.begin(); it != b.segments.end(); ++it)
		if(checkedSeeds.find(*it) == checkedSeeds.end())
			discardedSeeds.insert(*it);
}

template <class InM, class OutM>
void SeedApproach<InM, OutM>::discardInsideSegments(const CandidateBlock& b)
{
	// if a segment is included in some segment of the connected component, discard it
	typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
	for(vit = m_plms->segment_begin(); vit != m_plms->segment_end(); ++vit)
		if(checkedSeeds.find(*vit) == checkedSeeds.end())
			if(isInsideBlock(*vit, b))
				discardedSeeds.insert(*vit);
}

template <class InM, class OutM>
void SeedApproach<InM, OutM>::discardOverlappedSegments(const CandidateBlock& b)
{
	// if a segment is included in some segment of the connected component, discard it
	typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
	for(vit = m_plms->segment_begin(); vit != m_plms->segment_end(); ++vit)
		if(checkedSeeds.find(*vit) == checkedSeeds.end())
			if(overlapsBlock(*vit, b))
				discardedSeeds.insert(*vit);
}

template <class InM, class OutM>
void SeedApproach<InM, OutM>::discardBlockFragments(const CandidateBlock& b)
{
	// discard the edges between the segments of the block
	SegmentSet::const_iterator it1, it2;
	for(it1 = b.segments.begin(); it1 != b.segments.end(); ++it1)
		for(it2 = ++it1, it1--; it2 != b.segments.end(); ++it2)
		{
			if(m_plms->pairwiseSegmentGraph.deleteEdge(*it1, *it2)) discarded++;

			if(m_plms->pairwiseSegmentGraph.degree(*it1) == 0)
				if(checkedSeeds.find(*it1) == checkedSeeds.end())
					discardedSeeds.insert(*it1);
			if(m_plms->pairwiseSegmentGraph.degree(*it2) == 0)
				if(checkedSeeds.find(*it2) == checkedSeeds.end())
					discardedSeeds.insert(*it2);
		}
}

template <class InM, class OutM>
void SeedApproach<InM, OutM>::discardBlockAdjacentsFragments(const CandidateBlock& b)
{
	for(SegmentSet::const_iterator it = b.segments.begin(); it != b.segments.end(); ++it)
	{
		typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
		vit = m_plms->pairwiseSegmentGraph.hasVertex(*it);
		typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
		for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
			if(m_plms->pairwiseSegmentGraph.deleteEdge(*it, *ait)) discarded++;
	}
}

template <class InM, class OutM>
void SeedApproach<InM, OutM>::discardSeedsToDiscardAdjacentsFragments()
{
	std::set<SegmentPairwiseRelation> sprToDelete;

	for(SegmentSet::const_iterator sit = discardedSeeds.begin(); sit != discardedSeeds.end(); ++sit)
	{
		typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
		vit = m_plms->pairwiseSegmentGraph.hasVertex(*sit);
		typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
		for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
		{
			SegmentPairwiseRelation r (std::make_pair(*sit, *ait), !ait);
			sprToDelete.insert(r);
		}
	}

	// delete them
	std::set<SegmentPairwiseRelation>::const_iterator it;
	for(it = sprToDelete.begin(); it != sprToDelete.end(); ++it)
	{
		const SegmentPairwiseRelation& r = *it;
		if(m_plms->pairwiseSegmentGraph.deleteEdge(r.first(), r.second())) discarded++;

		if(m_plms->pairwiseSegmentGraph.degree(r.first()) == 0)
			if(checkedSeeds.find(r.first()) == checkedSeeds.end())
				discardedSeeds.insert(r.first());
		if(m_plms->pairwiseSegmentGraph.degree(r.second()) == 0)
			if(checkedSeeds.find(r.second()) == checkedSeeds.end())
				discardedSeeds.insert(r.second());
	}
}

template <class InM, class OutM>
void SeedApproach<InM, OutM>::discardIncompatibleFragments(const GlobalConsistentGA<OutM>& gcga)
{
	std::set<SegmentPairwiseRelation> sprToDelete;

	typename PairwiseLocalMatchingSet<InM>::psr_iterator rit;
	for(rit = m_plms->psr_begin(); rit != m_plms->psr_end(); ++rit)
	{
		const SegmentPairwiseRelation& r = *rit;
		// look for discarded fragments
		if(!gcga.gabios->areAlignable(r.first(), r.second())) sprToDelete.insert(r);
	}

	// delete them
	std::set<SegmentPairwiseRelation>::iterator it;
	for(it = sprToDelete.begin(); it != sprToDelete.end(); ++it)
	{
		const SegmentPairwiseRelation& r = *it;
		if(m_plms->pairwiseSegmentGraph.deleteEdge(r.first(), r.second())) discarded++;

		if(m_plms->pairwiseSegmentGraph.degree(r.first()) == 0)
			if(checkedSeeds.find(r.first()) == checkedSeeds.end())
				discardedSeeds.insert(r.first());
		if(m_plms->pairwiseSegmentGraph.degree(r.second()) == 0)
			if(checkedSeeds.find(r.second()) == checkedSeeds.end())
				discardedSeeds.insert(r.second());
	}
}

template <class InM, class OutM>
void SeedApproach<InM, OutM>::discardSeedsByQuorum(unsigned int quorum)
{
/*
	// works only for cliques
	SegmentGraph::vertex_iterator vit;
	for(vit = segmentGraph->begin(); vit != segmentGraph->end(); vit++)
		if(segmentGraph->degree((*vit).first) < quorum)
			discardedSeeds.insert((*vit).first);

	SegmentSet::iterator it;
	for(it = discardedSeeds.begin(); it != discardedSeeds.end(); it++)
		segmentGraph->deleteVertex(*it);
*/

	// works for connected components
	SegmentSet acceptedSegments;
	typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
	for(vit = m_plms->segment_begin(); vit != m_plms->segment_end(); ++vit)
	{
		const Segment& segment = *vit;
		if(acceptedSegments.find(segment) != acceptedSegments.end()) continue;
		if(discardedSeeds.find(segment) != discardedSeeds.end()) continue;

		// create a new connected component
		SegmentSet connectedComponent;
		connectedComponent.insert(segment);

		SegmentSet connCompAdjacents;
		connCompAdjacents.insert(segment);

		while(connCompAdjacents.size() > 0)
		{
			SegmentSet lastAdded;
			for(SegmentSet::const_iterator ait = connCompAdjacents.begin(); ait != connCompAdjacents.end(); ++ait)
			{
				typename PairwiseLocalMatchingSet<InM>::segment_iterator sit;
				sit = m_plms->pairwiseSegmentGraph.hasVertex(*ait);
				typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator dit;
				for(dit = sit.adjacent_begin(); dit != sit.adjacent_end(); ++dit)
					if(connectedComponent.insert(*dit).second)
						lastAdded.insert(*dit);
			}
			std::swap(connCompAdjacents, lastAdded);
		}

		if(connectedComponent.size() < quorum)
			for(SegmentSet::const_iterator it = connectedComponent.begin(); it != connectedComponent.end(); ++it)
				discardedSeeds.insert(*it);
		else
		{
			for(SegmentSet::const_iterator it = connectedComponent.begin(); it != connectedComponent.end(); ++it)
				acceptedSegments.insert(*it);
		}
	}
}

template <class InM, class OutM>
OutM* SeedApproach<InM, OutM>::getMatchingFromBlock(const CandidateBlock& b)
{
	OutM* bestBlock = new OutM(this->m_ms);
	/*
	Segment seg = *b.segments.begin();
	cout << endl << seg << " of degree " << m_plms->pairwiseSegmentGraph.degree(seg) << endl;;
	typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
	vit = m_plms->pairwiseSegmentGraph.hasVertex(seg);
	typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
	for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ait++)
		cout << "\t" << *ait << endl;
	*/
	std::set<const PairwiseLocalMatching*>::const_iterator it;
	for(it = b.matchings.begin(); it != b.matchings.end(); ++it)
		bestBlock->addMatching(**it);

	return bestBlock;
}

template <class InM, class OutM>
bool SeedApproach<InM, OutM>::isInsideBlock(const Segment& check, const CandidateBlock& b)
{
	for(SegmentSet::const_iterator it = b.segments.begin(); it != b.segments.end(); ++it)
	{
		const Segment& seg = *it;
		if(check.sequence() == seg.sequence())
			if(check.begin() >= seg.begin())
				if(check.end() <= seg.end())
					return true;
	}

	return false;
}

template <class InM, class OutM>
bool SeedApproach<InM, OutM>::overlapsBlock(const Segment& check, const CandidateBlock& b)
{
	for(SegmentSet::const_iterator it = b.segments.begin(); it != b.segments.end(); ++it)
	{
		const Segment& seg = *it;
		if(check.sequence() == seg.sequence())
		{
			if(check.begin() >= seg.begin() && check.begin() <= seg.end())
				return true;
			if(check.end() <= seg.end() && check.end() >= seg.begin())
				return true;
		}
	}

	return false;
}

#endif
