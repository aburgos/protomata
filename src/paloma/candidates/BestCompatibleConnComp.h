
#ifndef BESTCOMPCONNCOMPSEGSEEDAPPROACH_H
#define BESTCOMPCONNCOMPSEGSEEDAPPROACH_H

#include "SeedApproach.h"

/*!
Using the chosen block generator, it computes a set of matchings from each seed. For each matching, if it is compatible, it will be returned when getNextMatching is called. If it is not compatible, the next matching will be tried. In the case when all matchings returned by the block generator were tried and rejected, the next seed is used for computing a new set of matchings.
*/
template <typename InM, typename OutM>
class BestCompatibleConnComp: public SeedApproach<InM, OutM>
{
  public:
	//! The constructor.
	BestCompatibleConnComp(const MultiSequence&, const GeneralizedSet<OutM>&, PairwiseLocalMatchingSet<InM>&, SegmentSeedGenerator&, const PalomaParameters&);
	//! The destructor.
	virtual ~BestCompatibleConnComp() {}
	/*!
		First it calls getNextSegmentSet to get the next seed, a segment set. Then it instantiate a block generator object and a set of blocks is created. The first block is retrieved, it is checked that this block satisfies the quorum constraint. If it does, a new matching (of type OutM) is constructed from the block and compatibility with the generalized set is checked. If it's compatible, a pointer to the matching object is returned, otherwise a next block is tried. When no more blocks are available, i.e. an empty block is returned from the block generator, the process start over again. If there is no more seeds (or the ones left will be discarded), it returns NULL.
		\return A pointer to a Matching object.
	*/
	OutM* getNextMatching();
};

template <typename InM, typename OutM>
BestCompatibleConnComp<InM, OutM>::BestCompatibleConnComp(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, SegmentSeedGenerator& ssg, const PalomaParameters& params): SeedApproach<InM, OutM>(ms, gs, plms, ssg, params)
{
}

template <typename InM, typename OutM>
OutM* BestCompatibleConnComp<InM, OutM>::getNextMatching()
{
	static unsigned int acceptedBlocks = 0;
	static unsigned int rejectedBlocks = 0;
	static unsigned int rejectedSeeds = 0;
	OutM* candidate = NULL;
	do
	{
		unsigned int seedsLeftToTry = this->m_seeds->seeds_left() - this->discardedSeeds.size();
		// if there are no more seeds to try, stop
		if(seedsLeftToTry == 0) return NULL;

		const std::pair<bool, SegmentSet>& p = this->m_seeds->getNextSegmentSet();
		if(!p.first) return NULL;

		bool discardCurrentSeed = false;

		const SegmentSet& check = p.second;
		for(SegmentSet::const_iterator sit = check.begin(); sit != check.end(); ++sit)
		{
			this->checkedSeeds.insert(*sit);
			SegmentSet::const_iterator dit;
			if((dit = this->discardedSeeds.find(*sit)) != this->discardedSeeds.end())
				{ discardCurrentSeed = true; this->discardedSeeds.erase(dit); break; }
		}

		if(discardCurrentSeed) continue;

		CandidateBlock candidateBlock;
		// instantiate the chosen block generator implementation
		BlockFactory<InM, OutM> cfact;
		this->blockImp = cfact.create(this->m_ms, this->m_gs, *this->m_plms, p.second, this->m_params);
		this->blockImp->computeAllBlocks();

		do
		{
			candidateBlock = this->blockImp->getNextBlock();

			if(candidateBlock.isEmpty()) { rejectedSeeds++; rejectedBlocks++; delete this->blockImp; return NULL; }
			// maybe it would have to go to the next seed instead of the next block
			if(candidateBlock.segments.size() < this->m_params.plma_quorum()) { rejectedBlocks++; continue; }

			candidate = SeedApproach<InM, OutM>::getMatchingFromBlock(candidateBlock);
			if(!this->m_gs.isCompatible(*candidate)) { rejectedBlocks++; delete candidate; }
			else break;

		} while(true);

		delete this->blockImp;

		switch(this->m_params.discard_segments())
		{
			case blockSegments: this->discardBlockSegments(candidateBlock); break;
			case insideSegments: this->discardInsideSegments(candidateBlock); break;
			case overlappedSegments: this->discardOverlappedSegments(candidateBlock); break;
			default: break;
		}
		switch(this->m_params.discard_fragments())
		{
			case blockFragments: this->discardBlockFragments(candidateBlock); break;
			case blockAdjFragments: this->discardBlockAdjacentsFragments(candidateBlock); break;
			case seedsToDiscAdjFragments: this->discardSeedsToDiscardAdjacentsFragments(); break;
			default: break;
		}

		++acceptedBlocks;
		this->m_toDestroy.push_back(candidate);

		if(this->m_params.verbose())
		{
			std::cout << "\033[1;31m" << this->m_seeds->seeds_left() << " seeds are left" << "\033[m" << std::endl;
			std::cout << "\033[1;36m" << this->discardedSeeds.size() << " seeds will be discarded" << "\033[m" << std::endl;
			std::cout << "\033[1;35m" << seedsLeftToTry << " seeds will be checked" << "\033[m" << std::endl;
			std::cout << "\033[1;37m" << this->checkedSeeds.size() << " seeds were checked" << "\033[m" << std::endl;
			std::cout << "\033[1;30m" << rejectedSeeds << " seeds were rejected" << "\033[m" << std::endl;
			std::cout << "\033[1;32m" << acceptedBlocks << " blocks were accepted" << "\033[m" << std::endl;
			std::cout << "\033[1;33m" << rejectedBlocks << " blocks were rejected" << "\033[m" << std::endl;
			std::cout << "\033[1;34m" << this->discarded << " incompatible fragments deleted" << "\033[m" << std::endl;
			std::cout << std::endl;
		}

		break;

	} while(true);

	return candidate;
}

#endif
