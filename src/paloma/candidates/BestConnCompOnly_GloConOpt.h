
#ifndef BESTCONNCOMPGLOCONOPTSEEDAPPROACH_H
#define BESTCONNCOMPGLOCONOPTSEEDAPPROACH_H

#include "SeedApproach.h"

/*!
This class is a specialization when the result set is a global consistent generalized alignment. Using the chosen block generator, it computes a matching for each seed. If this matching is compatible, it will be returned when getNextMatching is called. If it is not compatible, the seed is discarded and a new matching is computed using the next seed.
*/
template <typename InM, typename OutM>
class BestConnCompOnly_GloConOpt: public SeedApproach<InM, OutM>
{
  public:
	//! The constructor.
	BestConnCompOnly_GloConOpt(const MultiSequence&, const GlobalConsistentGA<OutM>&, PairwiseLocalMatchingSet<InM>&, SegmentSeedGenerator&, const PalomaParameters&);
	//! The destructor.
	virtual ~BestConnCompOnly_GloConOpt() {}
	/*!
		First, if a matching was returned (in another call), incompatible fragments are discarded. This means that the pairwise segment graph will be traversed and each fragment will be checked for compatibility with the global consistent generalized alignment. Then it calls getNextSegmentSet to get the next seed, a segment set. Then it instantiate a block generator object and a new block (only ONE block, by calling computeOneBlock) is created. It is checked that this block satisfies the quorum constraint. If it does, a new matching (of type OutM) is constructed from the block and compatibility with the global consistent generalized alignment is checked. If it's compatible, a pointer to the matching object is returned, otherwise the whole process start over again. If there is no more seeds (or the ones left will be discarded), it returns NULL.
		\return A pointer to a Matching object.
	*/
	OutM* getNextMatching();
};

template <typename InM, typename OutM>
BestConnCompOnly_GloConOpt<InM, OutM>::BestConnCompOnly_GloConOpt(const MultiSequence& ms, const GlobalConsistentGA<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, SegmentSeedGenerator& ssg, const PalomaParameters& params): SeedApproach<InM, OutM>(ms, gs, plms, ssg, params)
{
}

template <typename InM, typename OutM>
OutM* BestConnCompOnly_GloConOpt<InM, OutM>::getNextMatching()
{
	static unsigned int acceptedBlocks = 0;
	static unsigned int rejectedBlocks = 0;
	static unsigned int rejectedSeeds = 0;
	static bool matchingReturned = false;
	OutM* candidate = NULL;
	const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(this->m_gs);
	do
	{
		// before computing a block, if some block was added, discard the incompatible fragments
		if(matchingReturned) this->discardIncompatibleFragments(gcga);
		unsigned int seedsLeftToTry = this->m_seeds->seeds_left() - this->discardedSeeds.size();

		if(this->m_params.verbose() && matchingReturned)
		{
			std::cout << "\033[1;33m" << this->m_seeds->seeds_left() << " seeds left" << "\033[m";
			std::cout << "\033[1;32m" << " (" << seedsLeftToTry << " to check; " << "\033[m";
			std::cout << "\033[1;31m" << this->discardedSeeds.size() << " incompatible)" << "\033[m" << std::endl;

			std::cout << "\033[1;37m" << this->checkedSeeds.size() << " seeds were already checked" << "\033[m" << std::endl;
			std::cout << "\033[1;30m" << rejectedSeeds << " seeds were rejected" << "\033[m" << std::endl;
			std::cout << "\033[1;32m" << acceptedBlocks << " blocks were accepted" << "\033[m" << std::endl;
			std::cout << "\033[1;33m" << rejectedBlocks << " blocks were rejected" << "\033[m" << std::endl;
			std::cout << "\033[1;34m" << this->discarded << " incompatible fragments deleted" << "\033[m" << std::endl;
			//std::cout << "\033[1;31m" << candidateBlock.weight << " block weight" << "\033[m" << std::endl;
			std::cout << std::endl;
		}

		matchingReturned = false;

		// if there are no more seeds to try, stop
		if(seedsLeftToTry == 0) return NULL;

		const std::pair<bool, SegmentSet>& p = this->m_seeds->getNextSegmentSet();
		if(!p.first) return NULL;

		bool discardCurrentSeed = false;

		const SegmentSet& check = p.second;
		for(SegmentSet::const_iterator sit = check.begin(); sit != check.end(); sit++)
		{
			this->checkedSeeds.insert(*sit);
			SegmentSet::iterator dit;
			if((dit = this->discardedSeeds.find(*sit)) != this->discardedSeeds.end())
				{ discardCurrentSeed = true; this->discardedSeeds.erase(dit); break; }
		}

		if(discardCurrentSeed) continue;

		// instantiate the chosen block generator implementation
		BlockFactory<InM, OutM> cfact;
		this->blockImp = cfact.create(this->m_ms, gcga, *this->m_plms, p.second, this->m_params);
		this->blockImp->computeOneBlock();
		CandidateBlock candidateBlock = this->blockImp->getNextBlock();

		if(candidateBlock.isEmpty() || candidateBlock.segments.size() < this->m_params.plma_quorum())
		{
			if(this->m_params.verbose()) std::cout << "Rejected!" << std::endl;

			delete this->blockImp;
			rejectedSeeds++;

			continue;
		}

		candidate = SeedApproach<InM, OutM>::getMatchingFromBlock(candidateBlock);
		delete this->blockImp;

		if(!gcga.isCompatible(*candidate))
		{
			if(this->m_params.verbose())
			{
				std::cout << "Not compatible! (" << candidateBlock.weight << ") from seed ";
				std::cout << *check.begin() << std::endl;
			}

			delete candidate;
			rejectedSeeds++;
			rejectedBlocks++;

			continue;
		}
		else
		{
			if(this->m_params.verbose())
				std::cout << "Accepted! (" << candidateBlock.weight << ") from seed " << *check.begin() << std::endl;
			++acceptedBlocks;
			matchingReturned = true;
			this->m_toDestroy.push_back(candidate);

			switch(this->m_params.discard_segments())
			{
				case blockSegments: this->discardBlockSegments(candidateBlock); break;
				case insideSegments: this->discardInsideSegments(candidateBlock); break;
				case overlappedSegments: this->discardOverlappedSegments(candidateBlock); break;
				default: break;
			}
			switch(this->m_params.discard_fragments())
			{
				case blockFragments: this->discardBlockFragments(candidateBlock); break;
				case blockAdjFragments: this->discardBlockAdjacentsFragments(candidateBlock); break;
				case seedsToDiscAdjFragments: this->discardSeedsToDiscardAdjacentsFragments(); break;
				default: break;
			}

			break;
		}
	} while(true);

	return candidate;
}

#endif
