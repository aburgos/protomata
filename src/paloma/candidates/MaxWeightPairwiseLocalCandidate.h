
#ifndef MAXWEIGHTPAIR_H
#define MAXWEIGHTPAIR_H

#include <map>
#include "CandidateGenerator.h"

/*!
Returns the current maximum weight matching. The matchings generated are exactly the same as the local pairwise matchings read from input, with a type conversion to fulfil the output matching type requirement.
*/
template <typename InM, typename OutM>
class MaxWeightPairwiseLocalCandidate: public CandidateGenerator<InM, OutM>
{
  private:
	//! A container to store the candidates and their respective weight.
	std::multimap<double, const PairwiseLocalMatching*> candidates;

  public:
	//! The constructor adds every Matching in the segment graph to the candidates container.
	MaxWeightPairwiseLocalCandidate(const MultiSequence&, const GeneralizedSet<OutM>&, const PairwiseLocalMatchingSet<InM>&, const PalomaParameters&);
	~MaxWeightPairwiseLocalCandidate() {}
	/*!
		Gets the maximum weight matching in the container.
		\return A pointer to an output Matching object.
	*/
	const OutM* getNextMatching();
};

template <typename InM, typename OutM>
MaxWeightPairwiseLocalCandidate<InM, OutM>::MaxWeightPairwiseLocalCandidate(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, const PairwiseLocalMatchingSet<InM>& plms, const PalomaParameters& params): CandidateGenerator<InM, OutM>(ms, gs, params)
{
	typename PairwiseLocalMatchingSet<InM>::psr_iterator it;
	for(it = plms.psr_begin(); it != plms.psr_end(); ++it)
	{
		const SegmentPairwiseRelation& r = *it;
		candidates.insert(std::make_pair(r.value()->segmentsWeight(), r.value()));
	}
}

template <typename InM, typename OutM>
const OutM* MaxWeightPairwiseLocalCandidate<InM, OutM>::getNextMatching()
{
	static unsigned int acceptedBlocks = 0;
	static unsigned int rejectedBlocks = 0;
	do
	{
		if(candidates.empty()) return NULL;
		std::multimap<double, const PairwiseLocalMatching*>::reverse_iterator rit;
		rit = candidates.rbegin();
		const PairwiseLocalMatching* plm = (*rit).second;
		candidates.erase(--rit.base());

		// this allow only to use output matchings of type different than pairwise
		OutM* m = new OutM(this->m_ms);
		m->addMatching(*plm);
		if(this->m_gs.isCompatible(*m))
		{
			acceptedBlocks++;
			this->m_toDestroy.push_back(m);

			if(this->m_params.verbose())
			{
				std::cout << "\033[1;31m" << candidates.size() << " candidates left" << "\033[m" << std::endl;
				std::cout << "\033[1;32m" << acceptedBlocks << " blocks were accepted" << "\033[m" << std::endl;
				std::cout << "\033[1;33m" << rejectedBlocks << " blocks were rejected" << "\033[m" << std::endl;
				std::cout << std::endl;
			}

			return m;
		}
		else
		{
			rejectedBlocks++;
			delete m;
		}
	}
	while(true);

	return NULL;
}

#endif
