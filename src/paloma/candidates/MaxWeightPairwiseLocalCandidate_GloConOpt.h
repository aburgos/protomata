
#ifndef MAXWEIGHTPAIRGLOCONOPT_H
#define MAXWEIGHTPAIRGLOCONOPT_H

#include <map>
#include "CandidateGenerator.h"
#include "generalized_sets/GlobalConsistentGA.h"

/*!
Returns the current maximum weight matching. The matchings generated are exactly the same as the local pairwise matchings read from input, with a type conversion to fulfil the output matching type requirement.
*/
template <typename InM, typename OutM>
class MaxWeightPairwiseLocalCandidate_GloConOpt: public CandidateGenerator<InM, OutM>
{
  private:
	//! A container to store the candidates and their respective weight.
	std::multimap<double, const PairwiseLocalMatching*> candidates;

  public:
	//! The constructor adds every Matching in the segment graph to the candidates container.
	MaxWeightPairwiseLocalCandidate_GloConOpt(const MultiSequence&, const GlobalConsistentGA<OutM>&, const PairwiseLocalMatchingSet<InM>&, const PalomaParameters&);
	~MaxWeightPairwiseLocalCandidate_GloConOpt() {}
	/*!
		Gets the maximum weight matching in the container.
		\return A pointer to an output Matching object.
	*/
	const OutM* getNextMatching();
	/*!
		Deletes from the segment graph all the edges that are not compatible eith the final global consistent generalized alignment.
		\param The GlobalConsistentGA that will be used to check for compatibility with each fragment.
	*/
	unsigned int discardIncompatibleFragments(const GlobalConsistentGA<OutM>&);
};

template <typename InM, typename OutM>
MaxWeightPairwiseLocalCandidate_GloConOpt<InM, OutM>::MaxWeightPairwiseLocalCandidate_GloConOpt(const MultiSequence& ms, const GlobalConsistentGA<OutM>& gs, const PairwiseLocalMatchingSet<InM>& plms, const PalomaParameters& params): CandidateGenerator<InM, OutM>(ms, gs, params)
{
	typename PairwiseLocalMatchingSet<InM>::psr_iterator it;
	for(it = plms.psr_begin(); it != plms.psr_end(); ++it)
	{
		const SegmentPairwiseRelation& r = *it;
		candidates.insert(std::make_pair(r.value()->segmentsWeight(), r.value()));
	}
}

template <typename InM, typename OutM>
const OutM* MaxWeightPairwiseLocalCandidate_GloConOpt<InM, OutM>::getNextMatching()
{
	static unsigned int acceptedBlocks = 0;
	static unsigned int rejectedBlocks = 0;
	static unsigned int discardedCandidates = 0;
	do
	{
		if(candidates.empty()) return NULL;
		std::multimap<double, const PairwiseLocalMatching*>::reverse_iterator rit;
		rit = candidates.rbegin();
		const PairwiseLocalMatching* plm = (*rit).second;
		candidates.erase(--rit.base());

		// this allow only to use output matchings of type different than pairwise
		OutM* m = new OutM(this->m_ms);
		m->addMatching(*plm);
		if(this->m_gs.isCompatible(*m))
		{
			acceptedBlocks++;
			this->m_toDestroy.push_back(m);

			const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(this->m_gs);
			discardedCandidates += discardIncompatibleFragments(gcga);

			if(this->m_params.verbose())
			{
				std::cout << "\033[1;31m" << candidates.size() << " candidates left" << "\033[m" << std::endl;
				std::cout << "\033[1;34m" << discardedCandidates << " candidates were discarded" << "\033[m" << std::endl;
				std::cout << "\033[1;32m" << acceptedBlocks << " blocks were accepted" << "\033[m" << std::endl;
				std::cout << "\033[1;33m" << rejectedBlocks << " blocks were rejected" << "\033[m" << std::endl;
				std::cout << std::endl;
			}

			return m;
		}
		else
		{
			rejectedBlocks++;
			delete m;
		}
	}
	while(true);

	return NULL;
}

template <typename InM, typename OutM>
unsigned int MaxWeightPairwiseLocalCandidate_GloConOpt<InM, OutM>::discardIncompatibleFragments(const GlobalConsistentGA<OutM>& gcga)
{
	unsigned int discarded = 0;

	typedef std::multimap<double, const PairwiseLocalMatching*>::iterator multimap_iterator;
	std::multimap<double, const PairwiseLocalMatching*> toDelete;

	for(multimap_iterator it = candidates.begin(); it != candidates.end(); ++it)
	{
		const PairwiseLocalMatching* plm = (*it).second;
		PairwiseLocalMatching::segment_iterator sit = plm->segment_begin();
		const Segment& first = *sit;
		sit++;
		const Segment& second = *sit;
		if(!gcga.gabios->areAlignable(first, second)) toDelete.insert(*it);
	}

	// delete them
	for(multimap_iterator it = toDelete.begin(); it != toDelete.end(); ++it)
	{
		std::pair<multimap_iterator, multimap_iterator> p = candidates.equal_range((*it).first);
		for(multimap_iterator mit = p.first; mit != p.second; ++mit)
			if((*mit).second == (*it).second) { candidates.erase(mit); discarded++; break; }
	}

	return discarded;
}

#endif
