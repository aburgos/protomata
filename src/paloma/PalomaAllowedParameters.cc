
#include "PalomaAllowedParameters.h"

PalomaAllowedParameters::PalomaAllowedParameters()
{
	block_mode_values.push_back("maxWeightFragments");
	block_mode_values.push_back("maxSupportFragments");
	block_mode_values.push_back("bestConnCompOnly");
	block_mode_values.push_back("bestCompatibleConnComp");

	block_generator_values.push_back("connCompExhaustive");
	block_generator_values.push_back("connCompGreedy");
	block_generator_values.push_back("connCompBestWeights");
	block_generator_values.push_back("cliqueExhaustive");
	block_generator_values.push_back("cliqueGreedy");
	block_generator_values.push_back("cliqueBestWeights");

	seed_generator_values.push_back("dialignW");
	seed_generator_values.push_back("support");

	generalized_set_values.push_back("GM");
	generalized_set_values.push_back("GLM");
	generalized_set_values.push_back("GA");
	generalized_set_values.push_back("GLA");
	generalized_set_values.push_back("GA-GC");
	generalized_set_values.push_back("GLA-LC");

	score_type_values.push_back("DNA");
	score_type_values.push_back("Blosum30");
	score_type_values.push_back("Blosum62");
	score_type_values.push_back("Blosum80");

	output_type_values.push_back("PLMA");
	output_type_values.push_back("ALN");

	discard_segments_values.push_back("blockSegments");
	discard_segments_values.push_back("insideSegments");
	discard_segments_values.push_back("overlappedSegments");
	discard_segments_values.push_back("noSegments");

	discard_fragments_values.push_back("blockFragments");
	discard_fragments_values.push_back("blockAdjFragments");
	discard_fragments_values.push_back("seedsToDiscAdjFragments");
	discard_fragments_values.push_back("noFragments");
}

TCLAP::ValuesConstraint<std::string>* PalomaAllowedParameters::block_mode_values_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(block_mode_values);
	return &allowedVals;
}

TCLAP::ValuesConstraint<std::string>* PalomaAllowedParameters::block_generator_values_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(block_generator_values);
	return &allowedVals;
}

TCLAP::ValuesConstraint<std::string>* PalomaAllowedParameters::seed_generator_values_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(seed_generator_values);
	return &allowedVals;
}

TCLAP::ValuesConstraint<std::string>* PalomaAllowedParameters::generalized_set_values_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(generalized_set_values);
	return &allowedVals;
}

TCLAP::ValuesConstraint<std::string>* PalomaAllowedParameters::score_type_values_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(score_type_values);
	return &allowedVals;
}

TCLAP::ValuesConstraint<std::string>* PalomaAllowedParameters::output_type_values_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(output_type_values);
	return &allowedVals;
}

TCLAP::ValuesConstraint<std::string>* PalomaAllowedParameters::discard_segments_values_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(discard_segments_values);
	return &allowedVals;
}

TCLAP::ValuesConstraint<std::string>* PalomaAllowedParameters::discard_fragments_values_constraints()
{
	static TCLAP::ValuesConstraint<std::string> allowedVals(discard_fragments_values);
	return &allowedVals;
}
