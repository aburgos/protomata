
#ifndef ALIGNMENTUFIMP_H
#define ALIGNMENTUFIMP_H

#include <map>

#include "Alignment.h"
#include "lib/UnionFind.h"
#include "MultiSequence.h"

/*!
A UnionFind implementation of an Alignment.
*/
class Alignment_UFImp: public virtual Alignment
{
  protected:
	//! Holds the undefined weight of the sites.
	double sites_weight;
	//! A union find structure representing the site relations.
	UnionFind union_find;
	//! Value needed to compute the uint-site conversion.
	unsigned int numSequences;
	//! Value needed to compute the uint-site conversion.
	unsigned int totalSites;
	//! Values needed to compute the uint-site conversion. The lengths of each sequence.
	std::vector<unsigned int> length;
	/*!
		Efficiently translates a site into an unsigned int in order to use it in the union find structure. This function is bijective.
		\return An unsigned int corresponding to the site parameter.
	*/
	unsigned int site2uint(const Site&) const;
	/*!
		Efficiently translates an unsigned int into a site in order to use it in the union find structure. This function is bijective.
		\return A site corresponding to the unsigned int parameter.
	*/
	Site uint2site(unsigned int) const;
	/*!
		The goal of this method is to check if the adding matching will keep the alignment constrain. For efficiency, first it makes a copy of this object. For every site pair relation r, it checks that there is a relation between each adjacent of one site of r and each adjacent of the other site of r. The copy alignment adds the site pair relation checked and accepted, and can tell in advance if a site relation to be checked is already in the alignment.
		\return True if this object will still be an alignment after adding the parameter. False otherwise.
	*/
	bool willBeAlignmentAfterAdding(const Matching&) const;
	/*!
		This method is optimized for alignments.
	*/
	//bool willBeAlignmentAfterAdding(const Alignment&) const;
	/*!
		Get the sequences of the adjacents of a site. Sequences can be repeated.
		\return A multiset of the sequences of the adjacents sites of the parameter site.
	*/
	std::multiset<unsigned int> sequencesAdjacents(const Site&) const;
	/*!
		Get the adjacent sites of a site. This is done by calling the findSet method of union find.
		\return A partition of adjacent sites of the parameter site.
	*/
	SitePartitionElement adjacentSites(const Site&) const;
	SitePartition site_partition() const;
	//!	Prints every site pair relation.
	virtual void print(std::ostream& aStream) const;

  public:
	/*!
		The constructor computes the total number of sites in the MultiSequence object and then sets the size of the union find object with that quantity. This way there is no need of re-allocation and all the sites have a correspondant unsigned int in the union find structure.
	*/
	Alignment_UFImp(const MultiSequence&);
	//! The destructor.
	virtual ~Alignment_UFImp() {}
	//! Number of sites
	unsigned int sites_size() const;
	//! Number of relations
	unsigned int relations_size() const;
	/*!
		First translates the sites into unsigned ints and then calls union_find.makeUnion.
		\return True if it's pairwise. False otherwise.
	*/
	virtual bool addMatching(const SitePairRelation&);
	/*!
		Iterates through the matching parameter adding each site pair relation to this alignment. If any of these is not added, it continues with the others, since the relation can exist already. It is assumed that every relation between two sites have the same value.
		\param A matching.
		\return Always True.
	*/
	virtual bool addMatching(const Matching&);
	//!	Undefined. It is 0 for now, initialize in the constructor and never changed.
	virtual double sitesWeight() const { return sites_weight; }
	/*!
		Checks if a specific relation exists in the alignment. It checks for an edge between the sites of the relation, without caring about the weight, that is supposed to be the same for all relations betweem these sites.
		\param A site pair relation.
		\return True if the site pair relation exists. False otherwise.
	*/
	virtual bool hasSiteRelation(const SitePairRelation& r) const;
	/*!
		Gets a visitor to access the all the sites of this matching. Implemented by a AlignmentUFImpSiteIterator.
		\return A visitor to all the sites of this matching.
	*/
	virtual site_visitor sites() const;
	/*!
		Gets a visitor to access the all the relations of this alignment. Implemented by a AlignmentUFImpRelationIterator.
		\return A visitor to all the relations of this alignment.
	*/
	virtual relation_visitor site_relations() const;
	//! This class needs access to the union find structure.
	friend class AlignmentUFImpSiteIterator;
	//! This class needs access to the union find structure.
	friend class AlignmentUFImpRelationIterator;
	//! This class needs access to the union find structure.
	template <typename OutM> friend class AlignmentOutput;
	//! This class needs access to the union find structure.
	//template <typename OutM> friend class LocalConsistentGLA;
};

#endif
