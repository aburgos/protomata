
#include "DialignPLA_Imp.h"

DialignPLA_Imp::DialignPLA_Imp(const SegmentPairRelation& r)
{
	seg1 = r.first();
	seg2 = r.second();
	segments.insert(seg1);
	segments.insert(seg2);
	sites_weight = 0;
	segments_weight = r.value();
}

bool DialignPLA_Imp::hasSiteRelation(const SitePairRelation& r) const
{
	// is this optimized by compiler?
	bool cmp1 = seg1.includes(r.first()) && seg2.includes(r.second());
	bool cmp2 = seg1.includes(r.second()) && seg2.includes(r.first());
	return (cmp1 || cmp2);
}

site_visitor DialignPLA_Imp::sites() const
{
	site_iterator* it = new DialignPLASiteIterator(this, false);
	site_iterator* end = new DialignPLASiteIterator(this, true);
	return site_visitor(it, end);
}

relation_visitor DialignPLA_Imp::site_relations() const
{
	relation_iterator* it = new DialignPLARelationIterator(this, false);
	relation_iterator* end = new DialignPLARelationIterator(this, true);
	return relation_visitor(it, end);
}

void DialignPLA_Imp::print(std::ostream& aStream) const
{
	aStream << "DialignPLA_Imp s1 = " << seg1 << std::endl;
	aStream << "DialignPLA_Imp s2 = " << seg2 << std::endl;
}
