
#ifndef LOCALALIGNMENT_H
#define LOCALALIGNMENT_H

#include "Alignment.h"
#include "matchings/LocalMatching.h"

/*!
A LocalAlignment is an Alignment with also relations between segments. Each sequence involved in the local alignment has one and only one segment. This segment is the smaller contiguos set of sites containing all the sites of the sequence in the local alignment. There is a relation between two segments if and only if there is a relation between a site of one segment and a site of the other segment.
*/
class LocalAlignment: public virtual LocalMatching, public virtual Alignment
{
  public:
	//! The constructor.
	LocalAlignment() {}
	//! The destructor.
	virtual ~LocalAlignment() {}
};

#endif
