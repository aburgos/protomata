
#ifndef DPLA_H
#define DPLA_H

#include "matchings/PairwiseLocalMatching.h"

/*!
A DialignPLA is a PairwiseLocalAlignment with two restrictions: 1) alignments happen between segments of equal length and 2) the site of position i in on segment must be align with the site of position i on the other segment.
*/
class DialignPLA: public PairwiseLocalMatching
{
  public:
	//! The constructor.
	DialignPLA() {}
	//! The destructor.
	virtual ~DialignPLA() {}
};

#endif
