
#ifndef ALIGNMENT_H
#define ALIGNMENT_H

#include "matchings/Matching.h"

/*!
An Alignment is a Matching where the relation on sites is an equivalence relation.
*/
class Alignment: public virtual Matching
{
  protected:
	/*!
		Get the sequences of the adjacents of a site. Sequences can be repeated.
		\return A multiset of the sequences of the adjacents sites of the parameter site.
	*/
	virtual std::multiset<unsigned int> sequencesAdjacents(const Site&) const = 0;
	/*!
		Get the adjacent sites of a site.
		\return A partition of adjacent sites of the parameter site.
	*/
	virtual SitePartitionElement adjacentSites(const Site&) const = 0;

  public:
	Alignment() {}
	virtual ~Alignment() {}

	virtual SitePartition site_partition() const = 0;
};

#endif
