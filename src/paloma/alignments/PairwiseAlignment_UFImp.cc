
#include "PairwiseAlignment_UFImp.h"

PairwiseAlignment_UFImp::PairwiseAlignment_UFImp(const MultiSequence& ms, unsigned int s1, unsigned int s2): Alignment_UFImp(ms)
{
	seq1 = s1;
	seq2 = s2;
}

bool PairwiseAlignment_UFImp::addMatching(const SitePairRelation& r)
{
	if(!this->isPairwise(r)) return false;
	else return Alignment_UFImp::addMatching(r);
}

bool PairwiseAlignment_UFImp::addMatching(const Matching& m)
{
	return Alignment_UFImp::addMatching(m);
}

bool PairwiseAlignment_UFImp::isPairwise(const SitePairRelation& r) const
{
	if(r.first().sequence() != seq1 && r.first().sequence() != seq2) return false;
	if(r.second().sequence() != seq1 && r.second().sequence() != seq2) return false;
	return true;
}
