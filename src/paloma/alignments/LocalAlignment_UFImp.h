#ifndef LOCALALIGNMENTSGUFIMP_H
#define LOCALALIGNMENTSGUFIMP_H

#include "LocalAlignment.h"
#include "Alignment_UFImp.h"

/*!
A UnionFind with a SegmentGraph implementation of a LocalAlignment.
*/
class LocalAlignment_UFImp: public virtual LocalAlignment, public virtual Alignment_UFImp
{
  protected:
	//! Holds the undefined weight of the segments.
	double segments_weight;
	//!	First prints all segments relations, after the site relations.
	virtual void print(std::ostream& aStream) const;

  public:
	//! The constructor calls the Alignment_UFImp constructor.
	LocalAlignment_UFImp(const MultiSequence&);
	//! The destructor.
	virtual ~LocalAlignment_UFImp() {}
	/*!
		First it calls Alignment_UFImp::addMatching. Then it adds the borders and finally a relation between the sequences of the sites of the relation.
		\param A relation between sites.
		\return Always True.
	*/
	virtual bool addMatching(const SitePairRelation&);

	virtual bool addSegment(const Segment&);
	/*! \return Always False since its not allowed. */
	virtual bool addMatching(const Matching&);
	/*!
		First it adds all the segment relations and then it calls Alignment_UFImp::addMatching for adding the site relations. Notice that each call to addMatching(SitePairRelation) made inside Alignment_UFImp::addMatching calls the method of THIS class, since there is a "this->" preceding the call.
		\param A matching.
		\return Always True.
	*/
	virtual bool addMatching(const LocalMatching&);
	//! Undefined.
	virtual double segmentsWeight() const { return segments_weight; }
};

#endif
