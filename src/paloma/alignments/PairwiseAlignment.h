
#ifndef PAIRWISEALIGNMENT_H
#define PAIRWISEALIGNMENT_H

#include "Alignment.h"
#include "matchings/PairwiseMatching.h"

/*!
A PairwiseAlignment is an Alignment with the condition that there are only two sequences involved in the site relations.
*/
class PairwiseAlignment: public virtual Alignment, public virtual PairwiseMatching
{
  protected:
	/*!
		Checks whether a site pair relation can be added to the pairwise alignment.
		\return True if the sequences involved in the site relation are included in the sequences of the pairwise alignment. False otherwise.
	*/
	virtual bool isPairwise(const SitePairRelation&) const = 0;

  public:
	//! The constructor.
	PairwiseAlignment() {}
	//! The destructor.
	virtual ~PairwiseAlignment() {}
};

#endif
