#include "PairwiseLocalAlignment_PairUFImp.h"

PairwiseLocalAlignment_PairUFImp::PairwiseLocalAlignment_PairUFImp(const MultiSequence& ms,  unsigned int s1, unsigned int s2, double wgt): Alignment_UFImp(ms), LocalAlignment_UFImp(ms), PairwiseAlignment_UFImp(ms, s1, s2)
{
	segments_weight = wgt;
}

bool PairwiseLocalAlignment_PairUFImp::addMatching(const SitePairRelation& r)
{
	if(!PairwiseAlignment_UFImp::isPairwise(r)) return false;
	return LocalAlignment_UFImp::addMatching(r);
}

bool PairwiseLocalAlignment_PairUFImp::addSegment(const Segment& seg)
{
	if(seg.sequence() != seq1 && seg.sequence() != seq2) return false;
	return LocalAlignment_UFImp::addSegment(seg);
}

bool PairwiseLocalAlignment_PairUFImp::addMatching(const Matching&)
{
	return false;
}

bool PairwiseLocalAlignment_PairUFImp::addMatching(const LocalMatching& m)
{
	return LocalAlignment_UFImp::addMatching(m);
}
