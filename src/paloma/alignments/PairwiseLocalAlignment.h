
#ifndef PAIRWISELOCALALIGNMENT_H
#define PAIRWISELOCALALIGNMENT_H

#include "LocalAlignment.h"
#include "PairwiseAlignment.h"
#include "matchings/PairwiseLocalMatching.h"

/*!
A PairwiseLocalAlignment is the sum of the restrictions applied by PairwiseAlignment and LocalAlignment to an Alignment.
*/
class PairwiseLocalAlignment: public virtual LocalAlignment, public virtual PairwiseAlignment, public PairwiseLocalMatching
{
  public:
	//! The constructor.
	PairwiseLocalAlignment() {}
	//! The destructor.
	virtual ~PairwiseLocalAlignment() {}
};

#endif
