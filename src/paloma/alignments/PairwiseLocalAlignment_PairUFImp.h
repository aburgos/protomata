#ifndef PAIRWISELOCALALIGNMENTPSGUFIMP_H
#define PAIRWISELOCALALIGNMENTPSGUFIMP_H

#include "PairwiseLocalAlignment.h"
#include "LocalAlignment_UFImp.h"
#include "PairwiseAlignment_UFImp.h"

/*!
A UnionFind with a SegmentGraph implementation of a PairwiseLocalAlignment.
*/
class PairwiseLocalAlignment_PairUFImp: public PairwiseLocalAlignment, public LocalAlignment_UFImp, public PairwiseAlignment_UFImp
{
  public:
	//! The constructor calls other constructors and set the sequences' IDs that will be used.
	PairwiseLocalAlignment_PairUFImp(const MultiSequence&, unsigned int, unsigned int, double);
	//! The destructor.
	virtual ~PairwiseLocalAlignment_PairUFImp() {}
	/*!
		First checks if it's pairwise. If so, LocalAlignment_SeGrUFImp::addMatching is called.
		\return True if it's pairwise. False otherwise.
	*/
	virtual bool addMatching(const SitePairRelation&);
	/*!
		First checks if it's pairwise. If so, LocalAlignment_SeGrUFImp::addMatching is called.
		\return True if it's pairwise. False otherwise.
	*/
	virtual bool addSegment(const Segment&);
	/*! \return Always False since its not allowed. */
	virtual bool addMatching(const Matching&);
	/*!
		It calls LocalAlignment_SeGrUFImp::addMatching. If the sequences of the site relations in the parameter matching don't correspond to the sequences of this matching, they won't be added.
		\return Always True.
	*/
	virtual bool addMatching(const LocalMatching&);
};

#endif
