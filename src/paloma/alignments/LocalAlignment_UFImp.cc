
#include "LocalAlignment_UFImp.h"

LocalAlignment_UFImp::LocalAlignment_UFImp(const MultiSequence& ms): Alignment_UFImp(ms)
{
	segments_weight = 0;
}

bool LocalAlignment_UFImp::addMatching(const SitePairRelation& r)
{
	//if(!segmentGraph.hasEdge(r.first(), r.second()).first) return false;
	return Alignment_UFImp::addMatching(r);
}

bool LocalAlignment_UFImp::addSegment(const Segment& seg)
{
	// should I check that the segment is legal?
	return segments.insert(seg).second;
}

bool LocalAlignment_UFImp::addMatching(const Matching&)
{
	return false;
}

bool LocalAlignment_UFImp::addMatching(const LocalMatching& m)
{
	for(segment_iterator it = m.segment_begin(); it != m.segment_end(); it++)
		this->addSegment(*it);
	return Alignment_UFImp::addMatching(m);
}

void LocalAlignment_UFImp::print(std::ostream& aStream) const
{
	for(segment_iterator it = segment_begin(); it != segment_end(); it++)
		aStream << *it << std::endl;
	Alignment_UFImp::print(aStream);
}
