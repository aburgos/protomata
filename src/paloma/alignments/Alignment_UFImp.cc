
#include "Alignment_UFImp.h"
#include "iterators/AlignmentUFImpSiteIterator.h"
#include "iterators/AlignmentUFImpRelationIterator.h"

Alignment_UFImp::Alignment_UFImp(const MultiSequence& ms)
{
	// taken as argument a multisequence object ms
	totalSites = 0;
	sites_weight = 0;
	numSequences = ms.size();
	// set values
	for(unsigned int i = 0; i < ms.size(); i++)
	{
		length.push_back(ms[i].length());
		totalSites += length[i];
	}

	// set the final size of the union find structure
	union_find.increase_size(totalSites);
}

bool Alignment_UFImp::addMatching(const SitePairRelation& r)
{
	const unsigned int e1 = site2uint(r.first());
	const unsigned int e2 = site2uint(r.second());
	//if(e1 > totalSites || e2 > totalSites) return false;
	return union_find.makeUnion(e1, e2);
}

bool Alignment_UFImp::addMatching(const Matching& m)
{
	relation_visitor v = m.site_relations();
	while(v.valid())
	{
		//if(!this->addMatching(v.get())) return false;
		this->addMatching(v.get());
		v.next();
	}
	return true;
}

bool Alignment_UFImp::willBeAlignmentAfterAdding(const Matching& m) const
{
	// copy alignment for efficiency on checks
	Alignment_UFImp copy(*this);

	relation_visitor v = m.site_relations();
	while(v.valid())
	{
		const SitePairRelation& r = v.get();
		v.next();

		// if the relation is already in the copy alignment, then continue with next iteration
		if(copy.hasSiteRelation(r)) continue;

		// compute each site partition
		SitePartitionElement adjs1 = copy.adjacentSites(r.first());
		SitePartitionElement adjs2 = copy.adjacentSites(r.second());

		// add sites to be added
		adjs1.insert(r.first());
		adjs2.insert(r.second());

		// check that there is an edge between every pair of sites in the partitions
		for(SitePartitionElement::const_iterator it1 = adjs1.begin(); it1 != adjs1.end(); ++it1)
			for(SitePartitionElement::const_iterator it2 = adjs2.begin(); it2 != adjs2.end(); ++it2)
			{
				SitePairRelation ri(std::make_pair(*it1, *it2), 0);
				if(!m.hasSiteRelation(ri)) return false;
			}

		// add the site relation to the copy alignment
		copy.addMatching(r);
	}

	return true;
}
/*
bool Alignment_UFImp::willBeAlignmentAfterAdding(const Alignment& al) const
{
	const SitePartition& al_site_partition = al.site_partition();
	for(SitePartition::const_iterator it = al_site_partition.begin(); it != al_site_partition.end(); ++it)
	{
		const SitePartitionElement& spe = *it;

		SitePartitionElement shared;

		for(SitePartitionElement::const_iterator sit = spe.begin(); sit != spe.end(); ++sit)
		{
			const Site& site = *sit;
			const SitePartitionElement& sp = adjacentSites(site);
			for(SitePartitionElement::const_iterator cit = sp.begin(); cit != sp.end(); ++cit)
				shared.insert(*cit);
		}

		for(SitePartitionElement::const_iterator cit = shared.begin(); cit != shared.end(); ++cit)
			if(spe.find(*cit) == spe.end()) return false;
	}

	return true;
}
*/
std::multiset<unsigned int> Alignment_UFImp::sequencesAdjacents(const Site& s) const
{
	std::multiset<unsigned int> seqsAdj;
	const SitePartitionElement& adjacents = adjacentSites(s);

	for(SitePartitionElement::iterator sit = adjacents.begin(); sit != adjacents.end(); ++sit)
		seqsAdj.insert((*sit).sequence());

	return seqsAdj;
}

SitePartitionElement Alignment_UFImp::adjacentSites(const Site& s) const
{
	SitePartitionElement adjSites;
	const std::set<unsigned int>& adjacents = union_find.findSet(site2uint(s));

	for(std::set<unsigned int>::iterator sit = adjacents.begin(); sit != adjacents.end(); ++sit)
		adjSites.insert(uint2site(*sit));

	return adjSites;
}

SitePartition Alignment_UFImp::site_partition() const
{
	SitePartition site_partition;

	const UnionFind::Partition& uf_partition = union_find.partition();
	for(UnionFind::Partition::const_iterator it = uf_partition.begin(); it != uf_partition.end(); ++it)
	{
		SitePartitionElement spe;

		const UnionFind::PartitionElement& uf_pe = *it;
		for(UnionFind::PartitionElement::const_iterator eit = uf_pe.begin(); eit != uf_pe.end(); ++eit)
			spe.insert(uint2site(*eit));

		site_partition.insert(spe);
	}

	return site_partition;
}

bool Alignment_UFImp::hasSiteRelation(const SitePairRelation& r) const
{
	return union_find.hasRelation(site2uint(r.first()), site2uint(r.second()));
}

unsigned int Alignment_UFImp::site2uint(const Site& s) const
{
	unsigned int value = 0;
	for(unsigned int i = 0; i < s.sequence() && i < numSequences; i++)
		value += length[i];
	value += s.position() + 1;

	return value;
}

unsigned int Alignment_UFImp::sites_size() const
{
	unsigned int sites = 0;

	UnionFind::Partition partition = union_find.partition();
	for(UnionFind::Partition::const_iterator it = partition.begin(); it != partition.end(); ++it)
		sites += (*it).size();

	return sites;
}

unsigned int Alignment_UFImp::relations_size() const
{
	unsigned int relations = 0;

	UnionFind::Partition partition = union_find.partition();
	for(UnionFind::Partition::const_iterator it = partition.begin(); it != partition.end(); ++it)
	{
		const UnionFind::PartitionElement& elem = *it;
		const unsigned int n = elem.size();
		relations += (n * (n - 1)) / 2;
	}

	return relations;
}

Site Alignment_UFImp::uint2site(unsigned int n) const
{
	unsigned int seq = 0;
	unsigned int pos = 0;

	unsigned int sumLengths = 0;
	while(sumLengths + length[seq] < n && seq < numSequences)
	{
		sumLengths += length[seq];
		seq++;
	}
	pos = n - sumLengths - 1;

	Site s(seq, pos);
	return s;
}

void Alignment_UFImp::print(std::ostream& aStream) const
{
	//aStream << "\033[1;33m" << "Union find array" << "\033[m" << endl;
	//aStream << union_find;

	relation_visitor v = site_relations();
	while(v.valid()) { aStream << v.get() << std::endl; v.next(); }
}

site_visitor Alignment_UFImp::sites() const
{
	site_iterator* it = new AlignmentUFImpSiteIterator(this, false);
	site_iterator* end = new AlignmentUFImpSiteIterator(this, true);
	return site_visitor(it, end);
}

relation_visitor Alignment_UFImp::site_relations() const
{
	relation_iterator* it = new AlignmentUFImpRelationIterator(this, false);
	relation_iterator* end = new AlignmentUFImpRelationIterator(this, true);
	return relation_visitor(it, end);
}
