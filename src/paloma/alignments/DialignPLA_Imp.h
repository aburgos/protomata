
#ifndef DPLAIMP_H
#define DPLAIMP_H

#include <set>
#include <vector>
#include "DialignPLA.h"
#include "iterators/DialignPLASiteIterator.h"
#include "iterators/DialignPLARelationIterator.h"

/*!
An implementation defining the segments involved.
*/
class DialignPLA_Imp: public DialignPLA
{
  protected:
	//! Store the segments.
	Segment seg1, seg2;
	//! Weight between the sites.
	double sites_weight;
	//! Weight between the segments.
	double segments_weight;
	/*!	\return Always False. */
	virtual bool isPairwise(const SitePairRelation&) const { return false; }
	//! Prints both segments.
	void print(std::ostream& aStream) const;

  public:
	//! The constructor assigns the segments and the segments weight.
	DialignPLA_Imp(const SegmentPairRelation&);
	//! The destructor.
	virtual ~DialignPLA_Imp() {}
	//! Number of sites
	unsigned int sites_size() const { return (seg1.length() + seg2.length()); }
	//! Number of relations
	unsigned int relations_size() const { return seg1.length(); }
	//! Not possible for a Dialign Object.
	bool addMatching(const SitePairRelation&) { return false; }
	//! Not possible for a Dialign Object.
	bool addSegment(const Segment&) { return false; }
	//! Not possible for a Dialign Object.
	bool addMatching(const Matching&) { return false; }
	//! Not possible for a Dialign Object.
	bool addMatching(const LocalMatching&) { return false; }
	//! Undefined.
	double sitesWeight() const { return sites_weight; }
	/*!
		Checks if a specific site relation exists in the pla.
		\param A site pair relation.
		\return True if the site pair relation exists. False otherwise.
	*/
	bool hasSiteRelation(const SitePairRelation&) const;
	/*!
		Gets a visitor to access the all the sites of this matching. Implemented by a DialignPLASiteIterator.
		\return A visitor to all the sites of this matching.
	*/
	site_visitor sites() const;
	/*!
		Gets a visitor to access the all the site relations of this pla. Implemented by a DialignPLARelationIterator.
		\return A visitor to all the relations of this alignment.
	*/
	relation_visitor site_relations() const;
	//! Initialize in the constructor and never changed.
	double segmentsWeight() const { return segments_weight; }
	//! Needs to access private data.
	friend class DialignPLASiteIterator;
	//! Needs to access private data.
	friend class DialignPLARelationIterator;
	//! Needs to access private data.
	friend class DialignPLASegmentIterator;
};

#endif
