
#ifndef PAIRWISEALIGNMENTUFIMP_H
#define PAIRWISEALIGNMENTUFIMP_H

#include "PairwiseAlignment.h"
#include "Alignment_UFImp.h"

/*!
A UnionFind implementation of a PairwiseAlignment.
*/
class PairwiseAlignment_UFImp: public virtual PairwiseAlignment, public virtual Alignment_UFImp
{
  protected:
	//! Holds the sequence's IDs of the sequences in constructor
	unsigned int seq1, seq2;
	/*!
		Checks that each site's sequence is already a sequence of the pairwise alignment.
		\return True if the sequences involved in the site relation are included in the sequences of the pairwise alignment. False otherwise.
	*/
	virtual bool isPairwise(const SitePairRelation&) const;

  public:
	//! The constructor calls the Alignment_UFImp constructor and set the sequences' IDs that will be used.
	PairwiseAlignment_UFImp(const MultiSequence&, unsigned int, unsigned int);
	//! The destructor.
	virtual ~PairwiseAlignment_UFImp() {}
	/*!
		First checks if the site relation being added is pairwise. In that case, Alignment_UFImp::addMatching is called.
		\return True if it's pairwise. False otherwise.
	*/
	virtual bool addMatching(const SitePairRelation&);
	/*!
		It calls Alignment_UFImp::addMatching.
		\param A matching.
		\return Always True.
	*/
	virtual bool addMatching(const Matching&);
};

#endif
