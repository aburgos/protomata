
#include "ALN.h"

CoreBlock::CoreBlock(const MultiSequence& ms): m_ms(ms)
{
	beg.assign(ms.size(), 0);
	end.assign(ms.size(), 0);
	firstPartition = 1;
}

bool CoreBlock::operator<(const CoreBlock& b) const
{
	// compute the "size" of the CoreBlock (is rectangular, works with dialign only)
	return ((end[0] - beg[0]) * usedSequences.size() < (b.end[0] - b.beg[0]) * b.usedSequences.size());
}

bool CoreBlock::add(const SitePartitionElement& p)
{
	if(firstPartition)
	{
		SitePartitionElement::iterator it;
		for(it = p.begin(); it != p.end(); ++it)
		{
			usedSequences.insert((*it).sequence());
			beg[(*it).sequence()] = (*it).position();
			end[(*it).sequence()] = (*it).position();
		}

		firstPartition = 0;
	}
	else
	{
		// get the used sequences by this partition
		std::set<unsigned int> usedSeq;
		SitePartitionElement::iterator it;
		for(it = p.begin(); it != p.end(); ++it)
			usedSeq.insert((*it).sequence());

		// check that all sequences are used
		std::set<unsigned int>::iterator sit;
		for(sit = usedSequences.begin(); sit != usedSequences.end(); ++sit)
			if(usedSeq.find(*sit) == usedSeq.end()) return false;

		// check that the partition added is compatible
		for(it = p.begin(); it != p.end(); ++it)
			if(end[(*it).sequence()] + 1 != (*it).position()) return false;

		// update the ends of the CoreBlock
		for(it = p.begin(); it != p.end(); ++it)
			end[(*it).sequence()] += 1;
	}

	return true;
}

bool CoreBlock::add(const CoreBlock& b)
{
	std::set<unsigned int> intersection;

	// get the sequences that are in both blocks
	for(unsigned int i = 0; i < m_ms.size(); i++)
		if(usedSequences.find(i) != usedSequences.end() && b.usedSequences.find(i) != b.usedSequences.end()) intersection.insert(i);

	// extend the end of the adding block for the intersection sequences
	std::set<unsigned int>::iterator it;
	for(it = intersection.begin(); it != intersection.end(); ++it)
		end[*it] = b.end[*it];

	// for the unused sequences of this block that are in b, set the same extremes as b
	std::set<unsigned int>::iterator sit;
	for(sit = b.usedSequences.begin(); sit != b.usedSequences.end(); ++sit)
		if(usedSequences.find(*sit) == usedSequences.end())
		{
			beg[*sit] = b.beg[*sit];
			end[*sit] = b.end[*sit];
		}

	// this is for filling up the rectangule
	unsigned int addedDistance = 0;
	it = intersection.begin(); addedDistance = b.end[*it] - b.beg[*it] + 1;
	for(sit = usedSequences.begin(); sit != usedSequences.end(); ++sit)
		if(b.usedSequences.find(*sit) == b.usedSequences.end())
			end[*sit] += addedDistance;

	return true;
}

bool CoreBlock::isEmpty()
{
	for(unsigned int i = 0; i < m_ms.size(); i++)
		if(beg[i] != 0 || end[i] != 0) return false;
	return true;
}

bool CoreBlock::canBeAdded(const CoreBlock& b)
{
	std::set<unsigned int>::iterator it;
	for(it = usedSequences.begin(); it != usedSequences.end(); ++it)
		if(b.usedSequences.find(*it) != b.usedSequences.end())
			if(end[*it] + 1 != b.beg[*it]) return false;
	return true;
}

int CoreBlock::gainByAdding(const CoreBlock& b)
{
	int gain = b.usedSequences.size() * (end[0] - beg[0]);
	int diffSeq = usedSequences.size() - b.usedSequences.size();
	if(diffSeq < 0) diffSeq = diffSeq * -1;
	int loss = diffSeq * (end[0] - beg[0]);

	return (gain - loss);
}

void CoreBlock::print()
{
	// first print which sequences are present in the CoreBlock and its range
	for(unsigned int i = 0; i < m_ms.size(); i++)
		if(beg[i] != 0 && end[i] != 0)
			std::cout << m_ms[i].tag() << "(" << beg[i] << "-" << end[i] << ") ";
	std::cout << std::endl << std::endl;

	// get the maximum size tag
	unsigned int maxTagSpace = (unsigned int)(m_ms[0].tag()).size();
	for(unsigned int i = 1; i < m_ms.size(); i++)
		if((m_ms[i].tag()).size() > maxTagSpace)
			maxTagSpace = (m_ms[i].tag()).size();
	maxTagSpace += 4;

	for(unsigned int i = 0; i < m_ms.size(); i++)
		if(beg[i] != 0 && end[i] != 0)
		{
			std::string tag = m_ms[i].tag();
			std::cout << tag;
			for(unsigned int j = tag.size(); j < maxTagSpace; j++) std::cout << " ";
			std::string seq = m_ms[i].data();
			std::cout << seq.substr(beg[i], end[i] - beg[i] + 1) << std::endl;
		}

	std::cout << std::endl << std::endl;
}

void CoreBlock::print(std::ofstream &output)
{
	// first print which sequences are present in the CoreBlock and its range
	for(unsigned int i = 0; i < m_ms.size(); i++)
		if(beg[i] != 0 && end[i] != 0)
			output << m_ms[i].tag() << "(" << beg[i] << "-" << end[i] << ") ";
	output << std::endl << std::endl;

	// get the maximum size tag
	unsigned int maxTagSpace = (unsigned int)(m_ms[0].tag()).size();
	for(unsigned int i = 1; i < m_ms.size(); i++)
		if((m_ms[i].tag()).size() > maxTagSpace)
			maxTagSpace = (m_ms[i].tag()).size();
	maxTagSpace += 4;

	for(unsigned int i = 0; i < m_ms.size(); i++)
		if(beg[i] != 0 && end[i] != 0)
		{
			std::string tag = m_ms[i].tag();
			output << tag;
			for(unsigned int j = tag.size(); j < maxTagSpace; j++) output << " ";
			std::string seq = m_ms[i].data();
			output << seq.substr(beg[i], end[i] - beg[i] + 1) << std::endl;
		}

	output << std::endl << std::endl;
}

