
#ifndef ALN_H
#define ALN_H

#include "Parameters.h"
#include "AlignmentOutput.h"

class CoreBlock
{
  private:
	const MultiSequence& m_ms;
	std::set<unsigned int> usedSequences;
	std::vector<unsigned int> beg;
	std::vector<unsigned int> end;
	bool firstPartition;

  public:
	CoreBlock(const MultiSequence&);

	bool operator<(const CoreBlock&) const;

	bool add(const SitePartitionElement&);
	bool add(const CoreBlock&);

	bool isEmpty();
	bool canBeAdded(const CoreBlock&);
	int gainByAdding(const CoreBlock&);

	void print();
	void print(std::ofstream&);
};

/*!
This class outputs the Alignment in ALN format. It only works for the UnionFind implementation of Alignment.
*/
template <typename OutM>
class ALN: public AlignmentOutput<OutM>
{
  private:
	std::ofstream output;

  public:
	/*!
		The constructor generates the ALN.
		\param filename is the destination file.
		\param ms is a MultiSequence object with all sequences.
		\param al is a GeneralizedAlignment object.
	*/
	ALN(const MultiSequence&, const GeneralizedAlignment<OutM>&, const Parameters&);
};

template <typename OutM>
ALN<OutM>::ALN(const MultiSequence& ms, const GeneralizedAlignment<OutM>& al, const Parameters& parameters): AlignmentOutput<OutM>(ms, al), output(parameters.output().c_str())
{
	// get partitions and set map
	std::vector<SitePartitionElement> sitePartitions = this->getSitePartitionSet(al);
	//cout << "partitions = " << sitePartitions.size() << endl;
	//this->numPartitions = sitePartitions.size();

	std::set<SitePartitionElement> partitions;

	std::vector<SitePartitionElement>::iterator it;
	for(it = sitePartitions.begin(); it != sitePartitions.end(); it++)
		partitions.insert(*it);

	std::set<CoreBlock> CoreBlocks;

	while(partitions.size() > 0)
	{
		CoreBlock b(ms);

		std::set<SitePartitionElement> toDelete;
		std::set<SitePartitionElement>::iterator it = partitions.begin();
		unsigned int i = 1;
		while(it != partitions.end())
		{
			if(b.add(*it)) { toDelete.insert(*it); it++; i++; }
			else break;
		}
		if(!b.isEmpty()) CoreBlocks.insert(b);
		for(it = toDelete.begin(); it != toDelete.end(); it++)
			partitions.erase(*it);
	}
/*
	unsigned int gains = 1;
	while(gains > 0)
	{
		gains = 0;
		std::set<CoreBlock>::iterator bit1, bit2, savedit;
		for(bit1 = CoreBlocks.begin(); bit1 != CoreBlocks.end(); bit1++)
		{
			CoreBlock a = *bit1;
			CoreBlock best = *bit1;
			int highestGain = 0;
			for(bit2 = ++bit1, bit1--; bit2 != CoreBlocks.end(); bit2++)
			{
				CoreBlock b = *bit2;
				if(!a.canBeAdded(b)) continue;
				if(a.gainByAdding(b) > highestGain)
				{
					highestGain = a.gainByAdding(b);
					if(highestGain > 0) gains++;
					savedit = bit2;
					best = b;
				}
			}
			if(highestGain > 0)
			{
				a.addCoreBlock(best);
				CoreBlocks.erase(savedit);
				CoreBlocks.erase(bit1);
				CoreBlocks.insert(a);
				break;
			}		
		}
	}
*/
	std::set<CoreBlock>::iterator bit;
	for(bit = CoreBlocks.begin(); bit != CoreBlocks.end(); bit++)
		{ CoreBlock b = (*bit); b.print(this->output); }

	// print a resume on standard output
	//cout << "\033[1;32m" << endl << CoreBlocks.size() << " blocks obtained." << "\033[m" << endl;
	//cout << "\033[1;36m" << "ALN output in " << filename << "\033[m" << endl << endl;

	output.close();
}

#endif
