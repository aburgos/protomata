
#ifndef ALIGNMENTOUTPUT_H
#define ALIGNMENTOUTPUT_H

#include <fstream>
#include "Output.h"
#include "Site.h"
#include "generalized_sets/GeneralizedAlignment.h"

/*!
This class implements common methods for alignment outputs.
*/
template <typename OutM>
class AlignmentOutput: public Output<OutM>
{
  protected:
	/*!
		Computes a set of site partitions.
		\param A pointer to an UnionFind Alignment.
		\return A vector of site partitions.
	*/
	std::vector<SitePartitionElement> getSitePartitionSet(const GeneralizedAlignment<OutM>&);

  public:
	//! The constructor.
	AlignmentOutput(const MultiSequence&, const GeneralizedAlignment<OutM>&);
	//! The destructor.
	virtual ~AlignmentOutput() {}
};

template <typename OutM>
AlignmentOutput<OutM>::AlignmentOutput(const MultiSequence& ms, const GeneralizedAlignment<OutM>&): Output<OutM>(ms)
{
}

template <typename OutM>
std::vector<SitePartitionElement> AlignmentOutput<OutM>::getSitePartitionSet(const GeneralizedAlignment<OutM>& ga)
{
	typedef std::set<std::set<unsigned int> > Partition;
	std::vector<SitePartitionElement> site_partition;

	const Partition& partition = ga.union_find.partition();

	for(Partition::const_iterator it = partition.begin(); it != partition.end(); ++it)
	{
		SitePartitionElement sp;
		for(std::set<unsigned int>::iterator sit = (*it).begin(); sit != (*it).end(); ++sit)
			sp.insert(ga.uint2site(*sit));
		site_partition.push_back(sp);
	}

	return site_partition;
}

#endif
