
#ifndef OUTPUT_H
#define OUTPUT_H

#include <iostream>
#include "MultiSequence.h"

/*!
Abstract class to handle result output.
*/
template <typename OutM>
class Output
{
  protected:
	const MultiSequence& m_ms;

  public:
	Output(const MultiSequence& ms): m_ms(ms) {}
	virtual ~Output() {}
};

#endif
