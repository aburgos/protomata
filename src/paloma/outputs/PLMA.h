
#ifndef PLMA_H
#define PLMA_H

#include "pugixml.hpp"
#include "Parameters.h"
#include "AlignmentOutput.h"

/*!
This class outputs the Alignment in PLMA format. It only works for the UnionFind implementation of Alignment.
*/
template <typename OutM>
class PLMA: public AlignmentOutput<OutM>
{
  private:
	void append_resume(pugi::xml_node&, const Parameters&);
	void append_partitions(pugi::xml_node&, const GeneralizedAlignment<OutM>&);

  public:
	PLMA(const MultiSequence&, const GeneralizedAlignment<OutM>&, const Parameters&);
};

template <typename OutM>
PLMA<OutM>::PLMA(const MultiSequence& ms, const GeneralizedAlignment<OutM>& ga, const Parameters& parameters): AlignmentOutput<OutM>(ms, ga)
{
	pugi::xml_document doc;
	pugi::xml_node main_node = doc.append_child("plma");
	main_node.append_attribute("version") = VERSION;

	append_resume(main_node, parameters);
	append_partitions(main_node, ga);

	doc.save_file(parameters.output().c_str());
}

template <typename OutM>
void PLMA<OutM>::append_resume(pugi::xml_node& main_node, const Parameters& parameters)
{
	pugi::xml_node resume_node = main_node.append_child("resume");
	pugi::xml_node sequences_node = resume_node.append_child("sequences");
	sequences_node.append_attribute("size") = this->m_ms.size();

	for(MultiSequence::const_iterator it = this->m_ms.begin(); it != this->m_ms.end(); ++it)
	{
		const Sequence& seq = *it;
		pugi::xml_node sequence_node = sequences_node.append_child("sequence");
		sequence_node.append_attribute("tag") = seq.long_tag().c_str();
		sequence_node.append_attribute("data") = seq.data().c_str();
	}

	pugi::xml_node parameters_node = resume_node.append_child("parameters");
	for(Parameters::const_iterator it = parameters.begin(); it != parameters.end(); ++it)
	{
		const std::string& flag = (*it).first;
		const std::string& value = (*it).second;

		pugi::xml_node param_node = parameters_node.append_child("param");
		param_node.append_attribute("f") = flag.c_str();
		if(!value.empty()) param_node.append_attribute("v") = value.c_str();
	}
}

template <typename OutM>
void PLMA<OutM>::append_partitions(pugi::xml_node& main_node, const GeneralizedAlignment<OutM>& ga)
{
	pugi::xml_node partition_node = main_node.append_child("partition");

	const std::vector<SitePartitionElement>& sitePartitions = getSitePartitionSet(ga);
	for(std::vector<SitePartitionElement>::const_iterator it = sitePartitions.begin(); it != sitePartitions.end(); ++it)
	{
		const SitePartitionElement& sp = *it;
		pugi::xml_node part_node = partition_node.append_child("part");
		for(SitePartitionElement::const_iterator sit = sp.begin(); sit != sp.end(); ++sit)
		{
			pugi::xml_node aa_node = part_node.append_child("aa");
			aa_node.append_attribute("pos") = ((*sit).position() + 1);
			aa_node.append_attribute("seq") = ((*sit).sequence() + 1);
		}
	}
}

#endif
