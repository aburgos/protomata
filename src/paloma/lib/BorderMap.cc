
#include "BorderMap.h"

void BorderMap::updateBorders(const Site& s)
{
	if(borders.find(s.sequence()) == borders.end())
		borders[s.sequence()] = std::make_pair(s.position(), s.position() + 1);
	borders[s.sequence()] = std::make_pair(std::min(borders[s.sequence()].first, s.position()), std::max(borders[s.sequence()].second, s.position() + 1));
}

void BorderMap::updateBorders(const Segment& s)
{
	if(borders.find(s.sequence()) == borders.end())
		borders[s.sequence()] = std::make_pair(s.begin(), s.begin() + 1);
	borders[s.sequence()] = std::make_pair(std::min(borders[s.sequence()].first, s.begin()), std::max(borders[s.sequence()].second, s.end() + 1));
}
