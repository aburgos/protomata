
#include "BlockSet.h"

bool BlockSet::insert(const CandidateBlock& b)
{
	const std::pair<BlockSet::iterator, BlockSet::iterator>& range = blocks.equal_range(b);
	if(range.first == range.second) { blocks.insert(b); return true; }
	// it was not inserted, so there is a set of blocks with the same segments as the one trying to be added
	else
	{
		// if it doesn't have any candidate, the candidate block is useless
		if(b.candidates.empty()) return false;

		// set of blocks to erase, the ones that are included in b
		std::multiset<CandidateBlock> toErase;

		for(std::multiset<CandidateBlock>::iterator it = range.first; it != range.second; ++it)
		{
			const CandidateBlock& inside = *it;

			// if the inside's candidates are included in b's candidates, erase it (but after)
			if(isIncluded(inside.candidates, b.candidates)) toErase.insert(inside);
			// else don't add it since there is a candidate block containing b
			else if(isIncluded(b.candidates, inside.candidates)) return false;
		}

		// now candidates blocks can be erased (before they couldn't since an iterator were being used)
		for(std::multiset<CandidateBlock>::iterator it = toErase.begin(); it != toErase.end(); ++it)
			blocks.erase(*it);

		// notice that having reached this point, either some blocks were erased or not, being the last case when b has no candidate segments in common with all the blocks that shared the same segments.
		blocks.insert(b);
		return true;
	}
}

CandidateBlock BlockSet::getMaxWeight() const
{
	CandidateBlock ret;
	if(blocks.empty()) return ret;

	ret = *(blocks.begin());
	for(std::multiset<CandidateBlock>::iterator it = blocks.begin(); it != blocks.end(); ++it)
		if((*it).weight > ret.weight) ret = *it;

	return ret;
}

bool BlockSet::isIncluded(const SegmentSet& s1, const SegmentSet& s2) const
{
	SegmentSet::const_iterator it1 = s1.begin(), it2 = s2.begin();
	while(it2 != s2.end())
	{
		if(*it1 < *it2) return false;
		else if(*it2 < *it1) it2++;
		else if(*it1 == *it2) { it1++; it2++; }

		if(it1 == s1.end()) return true;
	}

	return false;
}

