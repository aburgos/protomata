
#ifndef BLOCKSET_H
#define BLOCKSET_H

#include "CandidateBlock.h"

/*!
Defines how a set of blocks should be handled. Only the insertion and get the maximum weighted block operations were implemented, as they were the only ones needed. For understanding this class, is mandatory that the class CandidateBlock is understood.
*/
class BlockSet
{
  private:
	//! The container of the blocks. It is a multiset since it should allows "repeats" of blocks, that are repeats according to the order definition of a CandidateBlock, but are actually different.
	std::multiset<CandidateBlock> blocks;
	/*! \return True if the set of segments in the first parameter are included in the set of segments of the second parameter. False otherwise. */
	bool isIncluded(const SegmentSet&, const SegmentSet&) const;

  public:
	//! The constructor.
	BlockSet() {}
	/*! \return The current number of candidates blocks in the BlockSet. */
	unsigned int size() const { return blocks.size(); }
	/*!
		Inserts the parameter candidate block only if there is no candidate with its same segments or if there is, if its candidate segments includes other candidate block's candidate segments or the intersection is empty.
		\return True if the candidate block was added. False otherwise.
	*/
	bool insert(const CandidateBlock&);
	/*! \return The maximum weight candidate block. */
	CandidateBlock getMaxWeight() const;
	//! Alias for an iterator.
	typedef std::multiset<CandidateBlock>::iterator iterator;
	/*! An iterator to the begin of the blocks. */
	iterator begin() { return blocks.begin(); }
	/*! An iterator to the end of the blocks. */
	iterator end() { return blocks.end(); }
	//! Alias for a constant iterator.
	typedef std::multiset<CandidateBlock>::const_iterator const_iterator;
	/*! A constant iterator to the begin of the blocks. */
	const_iterator begin() const { return blocks.begin(); }
	/*! A constant iterator to the end of the blocks. */
	const_iterator end() const { return blocks.end(); }
};

#endif
