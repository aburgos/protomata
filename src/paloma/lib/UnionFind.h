
#ifndef UNIONFIND_H
#define UNIONFIND_H

#include <iostream>
#include <cstring>
#include <cassert>
#include <set>

/*!
A structure that implements efficiently the findSet and makeUnion operations. It accepts elements starting from 1, i.e. makeSet(0) is not possible.
*/
class UnionFind
{
  public:
	typedef std::set<unsigned int> PartitionElement;
	typedef std::set<std::set<unsigned int> > Partition;

  private:
	//! An array to store the integers. Internally, integers will be stored from position 0.
	unsigned int* elements;
	//! The number of elements in the array.
	unsigned int elements_size;

  public:
	UnionFind();
	//! A constructor specifying the size of the elements.
	UnionFind(unsigned int);
	UnionFind(const UnionFind& uf);
	~UnionFind();
	UnionFind& operator=(const UnionFind&);
	//! Increase elements size.
	void increase_size(unsigned int);
	/*!
		Adds an element to the elements array, checking first if it already exists.
		\param element is an integer greater than 0.
		\return True if the element was added, False otherwise.
	*/
	bool makeSet(unsigned int);
	/*!
		Finds the set to where the argument belongs.
		\param element is an integer greater than 0.
		\return A set of integers in the same set as the argument, and the argument itself.
		The integers returned are all greater than 0.
	*/
	PartitionElement findSet(unsigned int) const;
	/*!
		Computes the union of two sets. First it checks that both elements exists in some set, second
		it checks whether they are already in the same set. Then it swaps the values in the array.
		\param e1 is an integer greater than 0.
		\param e2 is an integer greater than 0.
		\return True if the union was made, False otherwise.
	*/
	bool makeUnion(unsigned int, unsigned int);
	/*!
		Gets the partition.
		\return A set of elements.
	*/
	Partition partition() const;
	/*!
		Checks if a relation between elements exists.
		\return True if the relation is found. False otherwise.
	*/
	bool hasRelation(unsigned int, unsigned int) const;
	//!	Definition of the output operator for a UnionFind structure.
	friend std::ostream& operator<<(std::ostream& o, const UnionFind& uf)
	{
		Partition partitions = uf.partition();
		for(Partition::iterator it = partitions.begin(); it != partitions.end(); ++it)
		{
			for(PartitionElement::iterator eit = (*it).begin(); eit != (*it).end(); ++eit)
				o << *eit << " ";
			o << std::endl;
		}
		return o;
	}
};

inline UnionFind::PartitionElement UnionFind::findSet(unsigned int element) const
{
	assert(element > 0);

	PartitionElement ret;

	if(element >= elements_size || elements[element] == 0) return ret;

	// add the element to the set
	ret.insert(element);

	// add elements of the same set
	unsigned int next = element;
	while(elements[next] != element)
	{
		ret.insert(elements[next]);
		next = elements[next];
	}

	return ret;	
}

inline UnionFind::Partition UnionFind::partition() const
{
	Partition partition;

	PartitionElement missing;
	for(unsigned int i = 1; i < elements_size; i++)
		if(elements[i] != 0) missing.insert(i);

	while(!missing.empty())
	{
		const PartitionElement& new_partition_element = findSet(*missing.begin());
		for(PartitionElement::iterator it = new_partition_element.begin(); it != new_partition_element.end(); ++it)
			missing.erase(*it);
		partition.insert(new_partition_element);
	}

	return partition;
}

#endif
