
#include "Gabios.h"

Gabios::Gabios(const MultiSequence& ms)
{
	int numSeq = ms.size();
	int* longSeq = new int[numSeq];
	for(int i = 0; i < numSeq; i++)
	{
		Segment seg(i, 0, ms[i].length());
		segmentID[seg] = i;
		longSeq[i] = seg.length();
	}
	closure = newAligGraphClosure(numSeq, longSeq, 0, NULL);
	delete[] longSeq;

	//cout << __PRETTY_FUNCTION__ << endl;
	//map<Segment, unsigned int>::iterator it;
	//for(it = segmentID.begin(); it != segmentID.end(); it++)
		//cout << (*it).first << endl;
}

Gabios::Gabios(const SegmentSet& segments, bool allowRepeats)
{
	//cout << "allowRepeats = " << allowRepeats << endl;
	SegmentSet modifiedSegments;
	if(allowRepeats)
	{
		// overlapped segments will become one segment
		// non-overlapped segments belonging to the same sequence will get different sequence ID in the CLOSURE
		modifiedSegments = segments;
		bool modified;
		do
		{
			modified = false;

			SegmentSet::iterator it1, it2;
			for(it1 = modifiedSegments.begin(); it1 != modifiedSegments.end(); ++it1)
			{
				it2 = ++it1; it1--;
				if(it2 == modifiedSegments.end()) break;
				if((*it1).overlapps(*it2))
				{
					//cout << *it1 << " overlapps " << *it2 << endl;
					unsigned int sequence = (*it1).sequence();
					unsigned int begin = std::min((*it1).begin(), (*it2).begin());
					unsigned int length = std::max((*it1).end(), (*it2).end()) - begin;
					Segment newSegment(sequence, begin, length);
					modifiedSegments.erase(*it1);
					modifiedSegments.erase(*it2);
					modifiedSegments.insert(newSegment);
					modified = true;
					break;
				}
			}

		} while(modified);
	}
	else
	{
		// get the borders of each sequence and build a new segment set with segments corresponding to these borders
		BorderMap borders;
		SegmentSet::iterator it;
		for(it = segments.begin(); it != segments.end(); ++it)
			borders.updateBorders(*it);
		BorderMap::border_iterator bit;
		for(bit = borders.begin(); bit != borders.end(); ++bit)
		{
			std::pair<unsigned int, uipair> sp = *bit;
			Segment newSegment(sp.first, sp.second.first, sp.second.second - sp.second.first);
			modifiedSegments.insert(newSegment);
		}
	}

	int numSeq = modifiedSegments.size();
	int* longSeq = new int[numSeq];
	SegmentSet::iterator it = modifiedSegments.begin();
	for(int i = 0; i < numSeq; i++, it++)
	{
		Segment seg = *it;
		segmentID[seg] = i;
		longSeq[i] = seg.length();
	}
	closure = newAligGraphClosure(numSeq, longSeq, 0, NULL);
	delete[] longSeq;

	//cout << __PRETTY_FUNCTION__ << endl;
	//for(it = segments.begin(); it != segments.end(); it++)
	//	cout << (*it) << endl;
	//cout << endl;
	//map<Segment, unsigned int>::iterator mit;
	//for(mit = segmentID.begin(); mit != segmentID.end(); mit++)
	//	cout << (*mit).first << endl;
}

Gabios::~Gabios()
{
	freeAligGraphClosure(closure);
}

bool Gabios::addAlignment(const Site& s1, const Site& s2)
{
	const uipair& ps1 = site2uint(s1);
	const uipair& ps2 = site2uint(s2);
	return addAlignedPositions(closure, ps1.first, ps1.second, ps2.first, ps2.second);
}

bool Gabios::areAlignable(const Site& s1, const Site& s2)
{
	const uipair& ps1 = site2uint(s1);
	const uipair& ps2 = site2uint(s2);
	return alignablePositions(closure, ps1.first, ps1.second, ps2.first, ps2.second);
}

bool Gabios::areAligned(const Site& s1, const Site& s2)
{
	const uipair& ps1 = site2uint(s1);
	const uipair& ps2 = site2uint(s2);
	return alignedPositions(closure, ps1.first, ps1.second, ps2.first, ps2.second);
}

bool Gabios::addAlignment(const Segment& s1, const Segment& s2)
{
	const uipair& ps1 = segment2uint(s1);
	const uipair& ps2 = segment2uint(s2);
	return addAlignedSegments(closure, ps1.first, ps1.second, ps2.first, ps2.second, s1.length());
}

bool Gabios::areAlignable(const Segment& s1, const Segment& s2)
{
	const uipair& ps1 = segment2uint(s1);
	const uipair& ps2 = segment2uint(s2);
	return alignableSegments(closure, ps1.first, ps1.second, ps2.first, ps2.second, s1.length());
}

bool Gabios::areAligned(const Segment& s1, const Segment& s2)
{
	const uipair& ps1 = segment2uint(s1);
	const uipair& ps2 = segment2uint(s2);
	return alignedSegments(closure, ps1.first, ps1.second, ps2.first, ps2.second, s1.length());
}
