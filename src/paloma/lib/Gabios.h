
#ifndef GABIOS_H
#define GABIOS_H

#include <map>
#include <set>
#include "Site.h"
#include "Segment.h"
#include "Typedefs.h"
#include "MultiSequence.h"
#include "BorderMap.h"

extern "C"
{
	#include "alig_graph_closure.h"
}

/*!
Class to hide the use of the GABIOS library. All methods are overloaded in order to accept either sites or segments as parameters.
*/
class Gabios
{
  private:
	//! Alias for a pair of unsigned int.
	typedef std::pair<unsigned int, unsigned int> uipair;
	//! A pointer to the CLOSURE.
	CLOSURE* closure;
	//! Maps every segment with its original id to the id of the CLOSURE.
	std::map<Segment, unsigned int> segmentID;

  public:
	//! A constructor considering all sequences in the CLOSURE.
	Gabios(const MultiSequence&);
	//! A constructor considering a block in the CLOSURE and whether repeats should or not be allowed.
	Gabios(const SegmentSet&, bool);
	//! The destructor frees the memory asked in the constructor.
	~Gabios();
	/*!
		Converts a site to the internal representation used by the CLOSURE.
		\return A pair of unsigned ints, the first representing the sequence used in CLOSURE and the second representing the position.
	*/
	uipair site2uint(const Site&);
	/*!
		Converts a segment to the internal representation used by the CLOSURE.
		\return A pair of unsigned ints, the first representing the sequence used in CLOSURE and the second representing the begin of the segment in the CLOSURE.
	*/
	uipair segment2uint(const Segment&);
	/*!
		Aligns two sites, even if they are not alignable.
		\return True if the sites are now aligned. False otherwise.
	*/
	bool addAlignment(const Site&, const Site&);
	/*!
		Checks if two sites are alignable.
		\return True if the sites are alignable. False otherwise.
	*/
	bool areAlignable(const Site&, const Site&);
	/*!
		Checks if two sites are aligned.
		\return True if the sites are aligned. False otherwise.
	*/
	bool areAligned(const Site&, const Site&);
	/*!
		Aligns two segments.
		\return True if the segments are now aligned. False otherwise.
	*/
	bool addAlignment(const Segment&, const Segment&);
	/*!
		Checks if two segments are alignable, even if they are not alignable.
		\return True if the segments are alignable. False otherwise.
	*/
	bool areAlignable(const Segment&, const Segment&);
	/*!
		Checks if two segments are aligned.
		\return True if the segments are aligned. False otherwise.
	*/
	bool areAligned(const Segment&, const Segment&);
};

inline Gabios::uipair Gabios::site2uint(const Site& s)
{
	// check if the site trying to convert is included in any segment of the set defined by the constructor
	std::map<Segment, unsigned int>::iterator it;
	for(it = segmentID.begin(); it != segmentID.end(); ++it)
		if((*it).first.includes(s)) break;
	if(it == segmentID.end()) { std::cerr << "Fatal error in Gabios site2uint function." << std::endl; exit(1); }

	// Gabios internally starts sequences in 0, while positions starts in 1.
	const unsigned int seq = (*it).second;
	const unsigned int pos = s.position() - (*it).first.begin() + 1;

	return std::make_pair(seq, pos);
}

inline Gabios::uipair Gabios::segment2uint(const Segment& s)
{
	// check if the segment trying to convert is included in any segment of the set defined by the constructor
	std::map<Segment, unsigned int>::iterator it;
	for(it = segmentID.begin(); it != segmentID.end(); ++it)
		if((*it).first.includes(s)) break;
	if(it == segmentID.end()) { std::cerr << "Fatal error in Gabios segment2uint function." << std::endl; exit(1); }

	// Gabios internally starts sequences in 0, while positions starts in 1.
	const unsigned int seq = (*it).second;
	const unsigned int beg = s.begin() - (*it).first.begin() + 1;

	return std::make_pair(seq, beg);
}

#endif
