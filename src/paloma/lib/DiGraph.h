
#ifndef DIRECTEDGRAPH_H
#define DIRECTEDGRAPH_H

#include <cassert>
#include <iostream>
#include <set>
#include <map>
#include <vector>

/*!
Templated class that represents a graph. The graph doesn't allow to have repeated vertices or repeated edges. There can't be two edges between two vertices, even if they have different values associated to them by the TEdge class. The graph is represented as an adjacency list, using the STL map container. Each vertex is mapped to its adjacency list which is also a map. This last one, maps each adjacent vertex to an edge. NOTE: what I call EdgeDescriptor is really a VertexDescriptor, it would be left this way but should be changed.
The class offers a vertex iterator, and each vertex iterator can be traversed through an adjacent iterator. There is also a relation iterator which give access to each pair of vertices in the relation and the value of the edge between them.
*/
template <typename TVertex, typename TEdge>
class DiGraph
{
  public:
	typedef std::set<TVertex> TVertexSet;
	typedef std::map<TVertex, TEdge> EdgeDescriptor;
	typedef std::map<TVertex, EdgeDescriptor> VertexDescriptor;
	typedef typename VertexDescriptor::const_iterator vertex_desc_iterator;
	typedef typename EdgeDescriptor::const_iterator edge_desc_iterator;

  private:
	//! Each vertex maps to a list of adjacents.
	VertexDescriptor vertex_descriptors;
	//! Store the number of edges.
	unsigned long graph_edges;
	/*!
		Checks if there is a path from one vertex to another, keeping a set of visited vertices.
		\param TVertex is a vertex.
		\param TVertex is a vertex.
		\param TVertexSet is a set of visited vertices.
		\return True if a path exists, False otherwise.
	*/
	bool existsPath(const TVertex&, TVertexSet&, TVertexSet&) const;

  protected:
	/*!
		Method to be used internally for efficiency only.
		\param data is the vertex to be found.
		\return An iterator to the vertex descriptor if it was found, otherwise an iterator to vertices.end() is returned.
	*/
	vertex_desc_iterator getDescriptor(const TVertex& data) const { return vertex_descriptors.find(data); }

  public:
	//! The constructor.
	DiGraph();
	//! Get the number of vertex_descriptors in the graph.
	unsigned long vertices() const { return vertex_descriptors.size(); }
	//! Get the number of edges in the graph.
	unsigned long edges() const { return graph_edges; }
	//! Get the degree of a vertex
	unsigned long degree(const TVertex&) const;
	/*!
		Deletes a vertex and all its edges.
		\param TVertex is the vertex to be deleted.
		\return True if the vertex was erased, False otherwise.
	*/
	bool deleteVertex(const TVertex&);
	/*!
		Adds an edge between two vertices. If any vertex doesn't exists, it is added. If there is already an edge between the vertices, it is not added.
		\param TVertex is the first vertex.
		\param TVertex is the second vertex.
		\param TEdge is the edge between the vertices.
		\return True if the edge was added, False otherwise.
	*/
	bool addEdge(const TVertex&, const TVertex&, const TEdge&);
	/*!
		Deletes the edge between two vertices, if there is one.
		\param TVertex is the first vertex.
		\param TVertex is the second vertex.
		\return True if the edge was erased, False otherwise.
	*/
	bool deleteEdge(const TVertex&, const TVertex&);
	/*!
		Checks if there is a path from one vertex to another. I assume that there is always a path if vertices are equal.
		\param TVertex is a vertex.
		\param TVertex is a vertex.
		\return True if a path exists, False otherwise.
	*/
	bool existsPath(const TVertex&, const TVertex&) const;
	/*!
	Defines a way to iterate through the vertices of the graph.
	*/
	class vertex_iterator
	{
	  private:
		//! Current vertex of the iterator.
		vertex_desc_iterator vdit;

	  public:
		//! The default constructor.
		vertex_iterator() {}
		//! A constructor initializing the vertex descriptor.
		vertex_iterator(vertex_desc_iterator it): vdit(it) {}
		/*!
			The dereference operator.
			\return The current vertex.
		*/
		TVertex operator*() const { return (*vdit).first; }
		/*!
			The pre-increment operator.
			\return An incremented iterator.
		*/
		vertex_iterator& operator++() { vdit++; return *this; }
		/*!
			The pos-increment operator.
			\return The former state of the iterator incremented.
		*/
		vertex_iterator operator++(int) { vertex_iterator tmp = *this; vdit++; return tmp; }
		/*!
			Comparisson operator for two vertex operators.
			\return True if both iterators "points" to the same vertex. False otherwise.
		*/
		bool operator==(const vertex_iterator& it) const { return (vdit == it.vdit); }
		/*!
			Comparisson operator for two vertex operators.
			\return True if the iterators "points" to different vertices. False otherwise.
		*/
		bool operator!=(const vertex_iterator& it) const { return (vdit != it.vdit); }
		/*!
		Defines a way to iterate through the adjacents of a vertex of the graph.
		*/
		class adjacent_iterator
		{
		  private:
			//! Current adjacent of the iterator.
			edge_desc_iterator edit;

		  public:
			//! The default constructor.
			adjacent_iterator() {}
			//! A constructor initializing the edge descriptor.
			adjacent_iterator(edge_desc_iterator it): edit(it) {}
			/*!
				The dereference operator.
				\return The current adjacent.
			*/
			TVertex operator*() const { return (*edit).first; }
			/*!
				The pre-increment operator.
				\return An incremented iterator.
			*/
			adjacent_iterator& operator++() { edit++; return *this; }
			/*!
				The pos-increment operator.
				\return The former state of the iterator incremented.
			*/
			adjacent_iterator operator++(int) { adjacent_iterator tmp = *this; edit++; return tmp; }
			/*!
				Comparisson operator for two vertex operators.
				\return True if both iterators "points" to the same vertex. False otherwise.
			*/
			bool operator==(const adjacent_iterator& it) const { return (edit == it.edit); }
			/*!
				Comparisson operator for two vertex operators.
				\return True if the iterators "points" to different vertices. False otherwise.
			*/
			bool operator!=(const adjacent_iterator& it) const { return (edit != it.edit); }
			/*!
				Operator to get the value between the current vertex and its current adjacent.
				\return The value of the edge between the vertex and its adjacent.
			*/
			TEdge operator!() const { return (*edit).second; }
		};
		/*! \return An adjacent iterator to the beginning of a vertex' adjacents. */
		adjacent_iterator adjacent_begin() const { return adjacent_iterator(((*vdit).second).begin()); }
		/*! \return An adjacent iterator to the end of a vertex' adjacents. */
		adjacent_iterator adjacent_end() const { return adjacent_iterator(((*vdit).second).end()); }
	};
	/*! \return A vertex iterator to the beginning of the vertex_descriptors. */
	vertex_iterator vertex_begin() const { return vertex_iterator(vertex_descriptors.begin()); }
	/*! \return A vertex iterator to the end of the vertex_descriptors. */
	vertex_iterator vertex_end() const { return vertex_iterator(vertex_descriptors.end()); }
	/*!
		Searches for a vertex.
		\param data is the vertex to be searched for.
		\return A vertex iterator pointing to a vertex iterator if it was found, or pointing to vertex_end() if it was not found.
	*/
	vertex_iterator hasVertex(const TVertex& data) const { return vertex_iterator(vertex_descriptors.find(data)); }
	/*!
		Adds a vertex.
		\param data is the vertex to be added.
		\return A pair containing a vertex iterator pointing to the added vertex and a boolean indicating whether the vertex was created or if it was already in the graph.
	*/
	std::pair<vertex_iterator, bool> addVertex(const TVertex&);
	/*!
	A relation gives access to the vertex_descriptors which are related and the edge between them.
	*/
	class relation
	{
	  private:
		TVertex v1, v2;
		TEdge edge;

	  public:
		//! The default constructor.
		relation() {}
		//! A constructor that initializes its variables.
		relation(const std::pair<TVertex, TVertex>& p, TEdge rel)
		{
			v1 = p.first;
			v2 = p.second;
			edge = rel;
		}
		/*! \return The first vertex of the relation. */
		TVertex first() const { return v1; }
		/*! \return The second vertex of the relation. */
		TVertex second() const { return v2; }
		/*! \return The edge of the relation. */
		TEdge value() const { return edge; }
		/*!
			The order operator for two relations.
			\param A relation to be compared with.
			\return True if this relation is "smaller" than the one passed as parameter. False otherwise.
		*/
		bool operator<(const relation& rel) const
		{
			if(v1 < rel.v1) return true;
			else if(v1 == rel.v1) return (v2 < rel.v2);
			else return false;
		}
		/*!
			The equal comparisson operator for two relations.
			\param A relation to be compared with.
			\return True if source, target and edge are equal. False otherwise.
		*/
		bool operator==(const relation& rel) const
		{
			if(v1 != rel.v1) return false;
			else if(v2 != rel.v2) return false;
			else if(edge != rel.edge) return false;
			else return true;
		}
		/*!
			The unequal comparisson operator for two relations.
			\param A relation to be compared with.
			\return The opposite of the equal comparisson operator.
		*/
		bool operator!=(const relation& rel) const
		{
			return !operator==(rel);
		}
		//!	Definition of the output (cout) operator for a Relation.
		inline friend std::ostream& operator<<(std::ostream& aStream, const relation& r)
		{
			aStream << r.v1 << " -- " << r.edge << " --> " << r.v2;
			return aStream;
		}
	};
	/*!
	Defines a way to iterate through the relations of the graph. These are created as they are asked for, they don't exist anywhere. Since the graph is undirected represented in an adjacency list, this iterator takes care of not repeating any relation, i.e. if there is an edge between vertex A and B, being A < B, the relation [(A,B),value] will be traversed but not the relation [(B,A),value] since both are the same. This is coded in the increment operators.
	*/
	class relation_iterator
	{
	  private:
		//! A pointer to the graph used (maybe "this" is enough?)
		const DiGraph<TVertex, TEdge>* graph;
		typename DiGraph<TVertex, TEdge>::vertex_desc_iterator vit;
		typename DiGraph<TVertex, TEdge>::edge_desc_iterator eit;
		/*!
			Checks if all vertex_descriptors were traversed.
			\return True if there is no other vertex to visit. False otherwise.
		*/
		bool atEnd() const { return (vit == graph->vertex_descriptors.end()); }

	  public:
		//! The default constructor.
		relation_iterator() {}
		//! A constructor that initializes the iterator to begin or end.
		relation_iterator(const DiGraph<TVertex, TEdge>* g, bool end)
		{
			graph = g;
			if(graph->edges() == 0 || end) vit = graph->vertex_descriptors.end();
			else
			{
				vit = graph->vertex_descriptors.begin();
				while(vit != graph->vertex_descriptors.end() && graph->degree((*vit).first) == 0) vit++;
				if(vit != graph->vertex_descriptors.end()) eit = ((*vit).second).begin();
			}
		}
		/*!
			This method gives access to the iterator of the first vertex in the relation.
			\return The vertex iterator corresponding to the first vertex in the relation.
		*/
		vertex_iterator first_iterator() const { return graph->hasVertex((*vit).first); }
		/*!
			This method gives access to the iterator of the second vertex in the relation.
			\return The vertex iterator corresponding to the second vertex in the relation.
		*/
		vertex_iterator second_iterator() const { return graph->hasVertex((*eit).first); }
		/*!
			The dereference operator.
			\return The current relation.
		*/
		relation operator*() const
		{
			relation r (std::make_pair((*vit).first, (*eit).first), (*eit).second);
			return r;
		}
		/*!
			The pre-increment operator. It tries to find a vertex iterator and an edge iterator so that each one of them is pointing to a vertex and the vertex pointed by vit is smaller than the one pointed by eit.
			\return An incremented iterator.
		*/
		relation_iterator& operator++()
		{
			eit++;
			if(eit == (*vit).second.end())
			{
				vit++;
				while(vit != graph->vertex_descriptors.end() && graph->degree((*vit).first) == 0) vit++;
				if(vit != graph->vertex_descriptors.end()) eit = ((*vit).second).begin();
				else return *this;
			}
			return *this;
		}
		/*!
			The pos-increment operator. The logic is the same as in the pre-increment operator.
			\return An incremented iterator.
		*/
		relation_iterator operator++(int)
		{
			relation_iterator tmp = *this;
			eit++;
			if(eit == (*vit).second.end())
			{
				vit++;
				while(vit != graph->vertex_descriptors.end() && graph->degree((*vit).first) == 0) vit++;
				if(vit != graph->vertex_descriptors.end()) eit = ((*vit).second).begin();
				else return tmp;
			}
			return tmp;
		}
		/*!
			The equal comparisson operator for two relation iterators.
			\param A relation iterator to be compared with.
			\return True if this relation iterator is "equal" than the one passed as parameter. False otherwise.
		*/
		bool operator==(const relation_iterator& it) const
		{
			if(!atEnd() && !it.atEnd()) return (operator*() == *it);
			else if(atEnd() && it.atEnd()) return true;
			else return false;
		}
		/*!
			The unequal comparisson operator for two relation iterators.
			\param A relation iterator to be compared with.
			\return The opposite of the equal operator.
		*/
		bool operator!=(const relation_iterator& it) const { return !operator==(it); }
	};
	/*! \return A relation iterator pointing to the first relation in the graph, if there is one. */
	relation_iterator relation_begin() const { return relation_iterator(this, false); }
	/*! \return A relation iterator pointing to the end of the vertex_descriptors. */
	relation_iterator relation_end() const { return relation_iterator(this, true); }
	/*!
		Searches for an edge between two specified vertex_descriptors.
		\return A pair with the first element being equal to True if the edge was found and False otherwise. The second element is a relation. It is meaningless, i.e. empty, if the edge was not found.
	*/
	relation_iterator hasEdge(const TVertex&, const TVertex&) const;
	//!	Definition of the output (cout) operator for a DiGraph.
	friend std::ostream& operator<<(std::ostream& o, const DiGraph<TVertex, TEdge>& g)
	{
		o << "\033[1;33m" << "Vertices = " << g.vertices() << "\033[m" << std::endl;
		o << "\033[1;33m" << "Edges    = " << g.edges() << "\033[m" << std::endl << std::endl;
/*
		typename DiGraph<TVertex, TEdge>::vertex_desc_iterator vit;
		for(vit = g.vertex_descriptors.begin(); vit != g.vertex_descriptors.end(); vit++)
		{
			o << "\033[1;38m" << (*vit).first << "\033[m";
			typename DiGraph<TVertex, TEdge>::edge_desc_iterator eit;
			typename DiGraph<TVertex, TEdge>::EdgeDescriptor adj = (*vit).second;
			for(eit = adj.begin(); eit != adj.end(); eit++)
				o << " -- " << (*eit).second << " --> " << (*eit).first;
			o << endl;
		}
		o << endl;
*/
		return o;
	}
};

// the graph is double edged
template <typename TVertex, typename TEdge>
DiGraph<TVertex, TEdge>::DiGraph()
{
	graph_edges = 0;
}

template <typename TVertex, typename TEdge>
unsigned long DiGraph<TVertex, TEdge>::degree(const TVertex& data) const
{
	vertex_desc_iterator vit = getDescriptor(data);
	if(vit == vertex_descriptors.end()) return 0;
	else return ((*vit).second).size();
}

template <typename TVertex, typename TEdge>
std::pair<typename DiGraph<TVertex, TEdge>::vertex_iterator, bool> DiGraph<TVertex, TEdge>::addVertex(const TVertex& data)
{
	std::pair<vertex_desc_iterator, bool> rp = vertex_descriptors.insert(std::make_pair(data, EdgeDescriptor()));
	return std::make_pair(vertex_iterator(rp.first), rp.second);
}

template <typename TVertex, typename TEdge>
bool DiGraph<TVertex, TEdge>::deleteVertex(const TVertex& data)
{
	vertex_desc_iterator vit = getDescriptor(data);
	if(vit == vertex_descriptors.end()) return false;

	// delete all edges ending in this vertex
	for(vertex_iterator it = vertex_begin(); it != vertex_end(); ++it)
	{
		typedef typename std::set<std::pair<TVertex, TVertex> > EdgeSet;
		EdgeSet edgesToDelete;
		for(typename vertex_iterator::adjacent_iterator ait = it.adjacent_begin(); ait != it.adjacent_end(); ++ait)
			if(*ait == data) edgesToDelete.insert(std::make_pair(*it, *ait));
		for(typename EdgeSet::iterator dit = edgesToDelete.begin(); dit != edgesToDelete.end(); ++dit)
		{
			const std::pair<TVertex, TVertex>& p = *dit;
			deleteEdge(p.first, p.second);
		}
	}

	// delete the vertex
	graph_edges -= degree(data);
	vertex_descriptors.erase((*vit).first);

	return true;
}

template <typename TVertex, typename TEdge>
typename DiGraph<TVertex, TEdge>::relation_iterator DiGraph<TVertex, TEdge>::hasEdge(const TVertex& data1, const TVertex& data2) const
{
	for(relation_iterator it = relation_begin(); it != relation_end(); ++it)
		if((*it).first() == data1 && (*it).second() == data2) return it;

	return relation_end();
}

// adds an edge from data, adds the vertex_descriptors if they are not in the graph
template <typename TVertex, typename TEdge>
bool DiGraph<TVertex, TEdge>::addEdge(const TVertex& data1, const TVertex& data2, const TEdge& data)
{
	std::pair<vertex_desc_iterator, bool> padd1 = vertex_descriptors.insert(std::make_pair(data1, EdgeDescriptor()));
	EdgeDescriptor v1Adj = (*padd1.first).second;
	std::pair<edge_desc_iterator, bool> add = v1Adj.insert(std::pair<TVertex, TEdge>(data2, data));

	if(!add.second) return false;

	vertex_descriptors.erase(data1);
	vertex_descriptors.insert(std::make_pair(data1, v1Adj));

	addVertex(data2);

	graph_edges++;
	return true;
}

template <typename TVertex, typename TEdge>
bool DiGraph<TVertex, TEdge>::deleteEdge(const TVertex& data1, const TVertex& data2)
{
	vertex_desc_iterator v1 = getDescriptor(data1);
	if(v1 == vertex_descriptors.end()) return false;

	if(vertex_descriptors[data1].erase(data2))
	{
		graph_edges--;
		return true;
	}
	else return false;
}

template <typename TVertex, typename TEdge>
bool DiGraph<TVertex, TEdge>::existsPath(const TVertex& target, TVertexSet& visited, TVertexSet& toVisit) const
{
	if(toVisit.find(target) != toVisit.end()) return true;
	if(toVisit.empty()) return false;

	TVertex current = *toVisit.begin();
	toVisit.erase(toVisit.begin());
	visited.insert(current);

	vertex_iterator vit = hasVertex(current);
	if(vit == vertex_end()) return false;

	typename vertex_iterator::adjacent_iterator ait;
	for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ait++)
		if(visited.find(*ait) == visited.end())
			toVisit.insert(*ait);

	return existsPath(target, visited, toVisit);
}

template <typename TVertex, typename TEdge>
bool DiGraph<TVertex, TEdge>::existsPath(const TVertex& data1, const TVertex& data2) const
{
	if(hasVertex(data1) == vertex_end() || hasVertex(data2) == vertex_end()) return false;

	TVertexSet empty;
	TVertexSet toVisit; toVisit.insert(data1);
	return existsPath(data2, empty, toVisit);
}

#endif
