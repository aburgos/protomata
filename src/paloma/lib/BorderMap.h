
#ifndef BORDERMAP_H
#define BORDERMAP_H

#include <map>
#include "Site.h"
#include "Segment.h"

/*!
This class is intended to be used to keep track of the borders of each sequence involved in a local matching.
*/
class BorderMap
{
  private:
	//! An unsigned int pair.
	typedef std::pair<unsigned int, unsigned int> ui_pair;
	//! A map from each sequence to its left and right borders, i.e. an ui_pair.
	std::map<unsigned int, ui_pair> borders;

  public:
	//! The constructor.
	BorderMap() {}
	/*!
		Gets the borders of a sequence specified by its id.
		\return An unsigned int pair with the borders of the specified sequence.
	*/
	ui_pair operator[](unsigned int key) const { return borders.find(key)->second; }
	/*!
		Updates the borders of the sequence of the site.
		\param Site is a site in the relation.
	*/
	void updateBorders(const Site&);
	/*!
		Updates the borders of the sequence of the segment.
		\param Segment is a segment in the relation.
	*/
	void updateBorders(const Segment&);
	typedef std::map<unsigned int, ui_pair>::iterator border_iterator;
	border_iterator begin() { return borders.begin(); }
	border_iterator end() { return borders.end(); }
};

#endif
