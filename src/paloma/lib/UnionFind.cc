
#include "UnionFind.h"

UnionFind::UnionFind()
{
	elements = NULL;
	elements_size = 0;
}

UnionFind::UnionFind(unsigned int elem_size)
{
	elements_size = elem_size + 1;
	elements = new unsigned int[elements_size];
	for(unsigned int i = 1; i < elements_size; i++)
		elements[i] = 0;
}

UnionFind::UnionFind(const UnionFind& uf)
{
	elements_size = uf.elements_size;
	elements = new unsigned int[elements_size];
	memcpy(elements, uf.elements, sizeof(unsigned int) * elements_size);
}

UnionFind::~UnionFind()
{
	delete [] elements;
}

UnionFind& UnionFind::operator=(const UnionFind& uf)
{
	elements_size = uf.elements_size;
	elements = new unsigned int[elements_size];
	memcpy(elements, uf.elements, sizeof(unsigned int) * elements_size);

	return *this;
}

void UnionFind::increase_size(unsigned int new_size)
{
	assert(new_size >= elements_size);

	unsigned int former_size = elements_size;
	elements_size = new_size + 1;

	unsigned int* t_elem = new unsigned int[elements_size];
	memcpy(t_elem, elements, sizeof(unsigned int) * former_size);
	for(unsigned int i = former_size; i < elements_size; i++)
		t_elem[i] = 0;

	delete [] elements;
	elements = t_elem;
}

bool UnionFind::makeSet(unsigned int element)
{
	assert(element > 0);

	if(element >= elements_size)
	{
		increase_size(element);
		elements[element] = element;
	}
	else if(elements[element] != 0) return true;
	else elements[element] = element;

	return true;
}

bool UnionFind::makeUnion(unsigned int e1, unsigned int e2)
{
	assert(e1 > 0);
	assert(e2 > 0);

	if(e1 == e2) return makeSet(e1);
	else if(e1 < e2) { makeSet(e2); makeSet(e1); }
	else { makeSet(e1); makeSet(e2); }

	// check first if they are already on the same set
	unsigned int next = e1;
	while(elements[next] != e1)
	{
		if(elements[next] == e2) return true;
		next = elements[next];
	}

	// swap values
	std::swap(elements[e1], elements[e2]);

	return true;
}

bool UnionFind::hasRelation(unsigned int e1, unsigned int e2) const
{
	assert(e1 > 0);
	assert(e2 > 0);

	if(e1 >= elements_size || e2 >= elements_size) return false;
	if(elements[e1] == 0 || elements[e2] == 0) return false;
	if(e1 == e2) return true;

	unsigned int next = e1;
	while(elements[next] != e1)
	{
		if(elements[next] == e2) return true;
		next = elements[next];
	}

	return false;
}
