
#ifndef PALOMA_H
#define PALOMA_H

#include "MultiSequence.h"
#include "PalomaParameters.h"
#include "factories/ReaderFactory.h"
#include "factories/MatchingSetFactory.h"
#include "factories/GeneralizedSetFactory.h"
#include "factories/SeedFactory.h"
#include "factories/CandidateFactory.h"
#include "factories/OutputFactory.h"

template <typename InM, typename OutM>
class Paloma
{
  private:
	MultiSequence m_ms;
	GeneralizedSet<OutM>* m_generalized_set;

  public:
	Paloma() {}
	Paloma(const PalomaParameters&);
	~Paloma();

	void read(const MultiSequence&, MatchingSet<InM>*, const PalomaParameters&);
	void build(const MultiSequence&, MatchingSet<InM>*, GeneralizedSet<OutM>*, const PalomaParameters&);
	void output(const MultiSequence&, GeneralizedSet<OutM>*, const PalomaParameters&);

	MultiSequence multisequence() const { return m_ms; }
	GeneralizedSet<OutM>* generalized_set() const { return m_generalized_set; }
};

template <typename InM, typename OutM>
Paloma<InM, OutM>::Paloma(const PalomaParameters& parameters)
{
	unsigned int lobound = std::numeric_limits<unsigned int>::min();
	unsigned int upbound = std::numeric_limits<unsigned int>::max();

	m_ms = MultiSequence(parameters.input(), true, false, lobound, upbound, parameters.verbose());

	MatchingSetFactory<InM> msfact;
	MatchingSet<InM>* matching_set = msfact.create(m_ms, parameters.matching_set());

	read(m_ms, matching_set, parameters);

	// this is used just to instantiate the right class in the generalized set factory
	const OutM* matching = NULL;

	GeneralizedSetFactory gsfact;
	m_generalized_set = gsfact.create(matching, m_ms, parameters.generalized_set(), !parameters.no_repeats());

	build(m_ms, matching_set, m_generalized_set, parameters);
	output(m_ms, m_generalized_set, parameters);

	delete matching_set;
}

template <typename InM, typename OutM>
Paloma<InM, OutM>::~Paloma()
{
	delete m_generalized_set;
}

template <typename InM, typename OutM>
void Paloma<InM, OutM>::read(const MultiSequence& ms, MatchingSet<InM>* matching_set, const PalomaParameters& parameters)
{
	ReaderFactory rfact;
	InputReader<InM>* ir = rfact.create(matching_set, ms, parameters);

	const InM* m;
	// add the matchings to the set
	while((m = ir->nextInput()) != NULL)
		if(!matching_set->storeMatching(*m)) std::cout << *m << " not added" << std::endl;

	delete ir;
}

template <typename InM, typename OutM>
void Paloma<InM, OutM>::build(const MultiSequence& ms, MatchingSet<InM>* matching_set, GeneralizedSet<OutM>* generalized_set, const PalomaParameters& parameters)
{
	const PairwiseLocalMatchingSet<InM>* plms = dynamic_cast<const PairwiseLocalMatchingSet<InM>*>(matching_set);

	SeedFactory seedfact;
	SegmentSeedGenerator* seed_generator = seedfact.create(ms, *plms, parameters.seed_generator());

	// create new candidate generator
	CandidateFactory<InM, OutM> cfact;
	CandidateGenerator<InM, OutM>* candidate_generator = cfact.create(ms, *matching_set, *seed_generator, *generalized_set, parameters);
	if(candidate_generator == NULL) std::cout << "No candidate generator" << std::endl;

	// add candidates to the final set
	const OutM* m;
	if(parameters.test_seeds() == 0)
	{
		// test all seeds
		while((m = candidate_generator->getNextMatching()) != NULL)
			if(!generalized_set->addMatching(*m)) std::cout << "Error adding matching" << std::endl;
	}
	else
	{
		// test a few seeds only
		for(unsigned int i = 0; i < parameters.test_seeds(); i++)
			if((m = candidate_generator->getNextMatching()) != NULL)
				if(!generalized_set->addMatching(*m)) std::cout << "Error adding matching" << std::endl;
	}

	delete candidate_generator;
	delete seed_generator;
}

template <typename InM, typename OutM>
void Paloma<InM, OutM>::output(const MultiSequence& ms, GeneralizedSet<OutM>* generalized_set, const PalomaParameters& parameters)
{
	OutputFactory<OutM> ofact;
	Output<OutM>* out = ofact.create(ms, *generalized_set, parameters);
	delete out;
}

#endif
