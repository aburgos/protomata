
#ifndef CONNCOMPGREEDYIMP_H
#define CONNCOMPGREEDYIMP_H

#include "ConnComp_ExhaustiveImp.h"

/*!
Computes a connected component in a greedy way starting from a set of segments.
*/
template <typename InM, typename OutM>
class ConnComp_GreedyImp: public virtual ConnComp_ExhaustiveImp<InM, OutM>
{
  public:
	ConnComp_GreedyImp(const MultiSequence&, const GeneralizedSet<OutM>&, PairwiseLocalMatchingSet<InM>&, const SegmentSet&);
	virtual ~ConnComp_GreedyImp() {}
	//! Starting from a seed block, it is increased at each step and the best weighted block is stored.
	virtual void computeOneBlock();
	//! Same as computeOneBlock, but it stores every block computed.
	virtual void computeAllBlocks();
};

template <typename InM, typename OutM>
ConnComp_GreedyImp<InM, OutM>::ConnComp_GreedyImp(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs), ConnComp_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs) {}

template <typename InM, typename OutM>
void ConnComp_GreedyImp<InM, OutM>::computeOneBlock()
{
	CandidateBlock bestConnComp = this->initialConnComp;
	BlockSet lastAddedCandidates = this->increaseConnComp(this->initialConnComp);

	while(lastAddedCandidates.size() > 0)
	{
		CandidateBlock formerBestConnComp = bestConnComp;

		// select the best one between the latest added candidates
		for(BlockSet::const_iterator it = lastAddedCandidates.begin(); it != lastAddedCandidates.end(); ++it)
			if((*it).weight > bestConnComp.weight) bestConnComp = *it;

		if(bestConnComp == formerBestConnComp) break;
		lastAddedCandidates = this->increaseConnComp(bestConnComp);
	}

	this->orderedConnComp.insert(std::make_pair(bestConnComp.weight, bestConnComp));
}

template <typename InM, typename OutM>
void ConnComp_GreedyImp<InM, OutM>::computeAllBlocks()
{
	CandidateBlock bestConnComp = this->initialConnComp;
	BlockSet connCompCandidates = this->increaseConnComp(bestConnComp);
	BlockSet lastAddedCandidates = connCompCandidates;

	while(lastAddedCandidates.size() > 0)
	{
		CandidateBlock formerBestConnComp = bestConnComp;

		// select the best one between the latest added candidates
		for(BlockSet::const_iterator it = lastAddedCandidates.begin(); it != lastAddedCandidates.end(); ++it)
			if((*it).weight >= bestConnComp.weight) bestConnComp = *it;

		if(bestConnComp == formerBestConnComp) break;

		BlockSet lastAdded;
		const BlockSet& temp = this->increaseConnComp(bestConnComp);

		// order in descending order according to clique weight
		std::map<double, CandidateBlock> orderedConnComp;
		for(BlockSet::const_iterator cit = temp.begin(); cit != temp.end(); ++cit)
			orderedConnComp.insert(std::pair<double, CandidateBlock>((*cit).weight, *cit));

		if(orderedConnComp.size() > 0)
		{
			std::map<double, CandidateBlock>::reverse_iterator rit = orderedConnComp.rbegin();
			CandidateBlock maxWeightedConnCompData = (*rit).second;
			orderedConnComp.erase(--rit.base());

			if(connCompCandidates.insert(maxWeightedConnCompData))
				lastAdded.insert(maxWeightedConnCompData);
		}

		std::swap(lastAddedCandidates, lastAdded);
	}

	// order in descending order according to clique weight
	for(BlockSet::const_iterator cit = connCompCandidates.begin(); cit != connCompCandidates.end(); ++cit)
		this->orderedConnComp.insert(std::pair<double, CandidateBlock>((*cit).weight, *cit));
}

#endif
