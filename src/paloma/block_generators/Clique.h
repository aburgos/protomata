
#ifndef CLIQUE_H
#define CLIQUE_H

#include "ConnectedComponent.h"

/*!
Abstract class for clique blocks generators.
*/
template <typename InM, typename OutM>
class Clique: public virtual ConnectedComponent<InM, OutM>
{
  public:
	Clique(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs) {}
	virtual ~Clique() {}
};

#endif
