
#ifndef CLIQUEGREEDYBESTWGTIMP_H
#define CLIQUEGREEDYBESTWGTIMP_H

#include "Clique_GreedyImp.h"

/*!
Computes a clique in a greedy way starting from a set of segments, but preprocessing the segment candidates for increassing a block.
*/
template <typename InM, typename OutM>
class Clique_GreedyBestWgtImp: public Clique_GreedyImp<InM, OutM>
{
  protected:
	/*!
		Check whether the segment set passed as argument to the constructor is a clique. It also computes the initial clique, by choosing for the candidates of it the maximum weight segment of each sequence (being the segment adjacent to all of the current segments).
		/return True if the segments in the segment set taken by the constructor form a clique. False otherwise.
	*/
	virtual bool isSegmentSetConnComp();

  public:
	//! A constructor that sets the isSegSetConnComp variable.
	Clique_GreedyBestWgtImp(const MultiSequence&, const GeneralizedSet<OutM>&, PairwiseLocalMatchingSet<InM>&, const SegmentSet&);
	virtual ~Clique_GreedyBestWgtImp() {}
	//! Starting from a seed block, it is increased at each step and the best weighted block is stored.
	virtual void computeOneBlock();
	//! Same as computeOneBlock, but it stores every block computed.
	virtual void computeAllBlocks();
};

template <typename InM, typename OutM>
Clique_GreedyBestWgtImp<InM, OutM>::Clique_GreedyBestWgtImp(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs), Clique<InM, OutM>(ms, gs, plms, segs), ConnComp_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), Clique_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), ConnComp_GreedyImp<InM, OutM>(ms, gs, plms, segs), Clique_GreedyImp<InM, OutM>(ms, gs, plms, segs)
{
	this->isSegSetConnComp = this->isSegmentSetConnComp();
}

template <typename InM, typename OutM>
void Clique_GreedyBestWgtImp<InM, OutM>::computeOneBlock()
{
	Clique_GreedyImp<InM, OutM>::computeOneBlock();
}

template <typename InM, typename OutM>
void Clique_GreedyBestWgtImp<InM, OutM>::computeAllBlocks()
{
	Clique_GreedyImp<InM, OutM>::computeAllBlocks();
}

template <typename InM, typename OutM>
bool Clique_GreedyBestWgtImp<InM, OutM>::isSegmentSetConnComp()
{
	this->initialConnComp.weight = 0;

	// first check that it is a clique
	for(SegmentSet::const_iterator it1 = this->m_currentSegments.begin(); it1 != this->m_currentSegments.end(); ++it1)
	{
		SegmentSet::const_iterator it2;
		for(it2 = ++it1, it1--; it2 != this->m_currentSegments.end(); ++it2)
		{
			PairwiseSegmentGraph::relation_iterator rit = this->m_pairwiseSegmentGraph.hasEdge(*it1, *it2);
			if(rit == this->m_pairwiseSegmentGraph.relation_end()) return false;
			else
			{
				const SegmentPairwiseRelation& r = *rit;
				const PairwiseLocalMatching* plm = r.value();
				this->initialConnComp.matchings.insert(plm);
				this->initialConnComp.weight += plm->segmentsWeight();
			}
		}
	}

	this->initialConnComp.segments = this->m_currentSegments;

	std::map<unsigned int, std::pair<double, Segment> > bestWgtCandidates;

	// get all possible candidates to be on the clique
	const Segment& segment = *(this->m_currentSegments.begin());
	typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
	vit = this->m_pairwiseSegmentGraph.hasVertex(segment);
	typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
	for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
	{
		const PairwiseLocalMatching* plm = !ait;
		if(isCandidate(*ait, this->m_currentSegments))
		{
			if(bestWgtCandidates.find((*ait).sequence()) == bestWgtCandidates.end())
				bestWgtCandidates[(*ait).sequence()] = std::make_pair(plm->segmentsWeight(), (*ait));
			else if(plm->segmentsWeight() > bestWgtCandidates[(*ait).sequence()].first)
				bestWgtCandidates[(*ait).sequence()] = std::make_pair(plm->segmentsWeight(), (*ait));
		}
	}

	std::map<unsigned int, std::pair<double, Segment> >::const_iterator mit;
	for(mit = bestWgtCandidates.begin(); mit != bestWgtCandidates.end(); ++mit)
	{
		const std::pair<double, Segment>& sp = (*mit).second;
		this->initialConnComp.candidates.insert(sp.second);
	}

	return true;
}

#endif
