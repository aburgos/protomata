
#ifndef CONNCOMPEXHAUSTIVE_H
#define CONNCOMPEXHAUSTIVE_H

#include "ConnectedComponent.h"

/*!
Computes all possible connected components starting from a set of segments.
*/
template <typename InM, typename OutM>
class ConnComp_ExhaustiveImp: public virtual ConnectedComponent<InM, OutM>
{
  protected:
	//! Saves the result of checking if the initial segment set is a connected component.
	bool isSegSetConnComp;
	//! The initial data of a connected component containing the initial segments.
	CandidateBlock initialConnComp;
	//! Stores the connected component according to their weight.
	std::multimap<double, CandidateBlock> orderedConnComp;
	/*!
		From the parameter block, this method generates as many blocks as different combinations of segments on a block exists. The candidate segments to be added to the parameter block are the ones that are adjacent to any of the segments of the parameter block.
		\return A set of blocks.
	*/
	virtual BlockSet increaseConnComp(const CandidateBlock&);
	/*!
		Check whether the segment set passed as argument to the constructor is a connected component. It also constructs the initial connected component block.
		/return True if the segments in the segment set taken by the constructor form a connected component. False otherwise.
	*/
	virtual bool isSegmentSetConnComp();

  public:
	//! A constructor that sets the isSegSetConnComp variable by calling isSegmentSetConnComp.
	ConnComp_ExhaustiveImp(const MultiSequence&, const GeneralizedSet<OutM>&, PairwiseLocalMatchingSet<InM>&, const SegmentSet&);
	virtual ~ConnComp_ExhaustiveImp() {}
	/*!
		Gets the maximum CandidateBlock generated.
		\return A CandidateBlock.
	*/
	virtual CandidateBlock getNextBlock();
	//! Computes all blocks, and stores only the one with maximum weight between all possible connected components blocks.
	virtual void computeOneBlock();
	//! Computes all blocks, and stores them in the orderedConnComp map.
	virtual void computeAllBlocks();
};

template <typename InM, typename OutM>
ConnComp_ExhaustiveImp<InM, OutM>::ConnComp_ExhaustiveImp(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs)
{
	isSegSetConnComp = this->isSegmentSetConnComp();
}

template <typename InM, typename OutM>
void ConnComp_ExhaustiveImp<InM, OutM>::computeOneBlock()
{
	BlockSet connCompCandidates = this->increaseConnComp(initialConnComp);
	BlockSet lastAddedCandidates = connCompCandidates;

	while(lastAddedCandidates.size() > 0)
	{
		BlockSet lastAdded;
		for(BlockSet::const_iterator it1 = lastAddedCandidates.begin(); it1 != lastAddedCandidates.end(); ++it1)
		{
			const BlockSet& temp = this->increaseConnComp(*it1);
			for(BlockSet::const_iterator it2 = temp.begin(); it2 != temp.end(); ++it2)
				if(connCompCandidates.insert(*it2)) lastAdded.insert(*it2);
		}

		std::swap(lastAddedCandidates, lastAdded);
	}

	CandidateBlock bestConnComp = initialConnComp;

	// get the maximum weight block candidate
	for(BlockSet::const_iterator cit = connCompCandidates.begin(); cit != connCompCandidates.end(); ++cit)
		if((*cit).weight > bestConnComp.weight) bestConnComp = *cit;

	orderedConnComp.insert(std::make_pair(bestConnComp.weight, bestConnComp));
}

template <typename InM, typename OutM>
void ConnComp_ExhaustiveImp<InM, OutM>::computeAllBlocks()
{
	BlockSet connCompCandidates = this->increaseConnComp(initialConnComp);
	BlockSet lastAddedCandidates = connCompCandidates;

	while(lastAddedCandidates.size() > 0)
	{
		BlockSet lastAdded;
		for(BlockSet::const_iterator it1 = lastAddedCandidates.begin(); it1 != lastAddedCandidates.end(); ++it1)
		{
			const BlockSet& temp = this->increaseConnComp(*it1);
			for(BlockSet::const_iterator it2 = temp.begin(); it2 != temp.end(); ++it2)
				if(connCompCandidates.insert(*it2)) lastAdded.insert(*it2);
		}

		std::swap(lastAddedCandidates, lastAdded);
	}

	// order in descending order according to connected component weight
	for(BlockSet::const_iterator cit = connCompCandidates.begin(); cit != connCompCandidates.end(); ++cit)
		orderedConnComp.insert(std::pair<double, CandidateBlock>((*cit).weight, *cit));
}

template <typename InM, typename OutM>
CandidateBlock ConnComp_ExhaustiveImp<InM, OutM>::getNextBlock()
{
	CandidateBlock maxWeightedBlock;

	// check first if the initial segments make a connected component
	if(!isSegSetConnComp || orderedConnComp.size() == 0) return maxWeightedBlock;

	std::multimap<double, CandidateBlock>::reverse_iterator rit = orderedConnComp.rbegin();
	maxWeightedBlock = (*rit).second;
	orderedConnComp.erase(--rit.base());

	return maxWeightedBlock;
}

template <typename InM, typename OutM>
BlockSet ConnComp_ExhaustiveImp<InM, OutM>::increaseConnComp(const CandidateBlock& connComp)
{
	// the set to be returned
	BlockSet connComps;

	// for each candidate, generate a new connected component
	for(SegmentSet::const_iterator cit = connComp.candidates.begin(); cit != connComp.candidates.end(); ++cit)
	{
		// the candidate to add to the connected component
		const Segment& candidate = *cit;

		// updated candidates
		double addedWeight = 0;
		std::set<const PairwiseLocalMatching*> toAdd;
		SegmentSet newCandidates;

		typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
		vit = this->m_pairwiseSegmentGraph.hasVertex(candidate);
		typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
		for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
		{
			const Segment& checkSegment = *ait;
			// if the adjacent of the candidate is already in the connected component, the weight and matching are added
			if(connComp.segments.find(checkSegment) != connComp.segments.end())
			{
				const PairwiseLocalMatching* plm = !ait;
				toAdd.insert(plm);
				addedWeight += plm->segmentsWeight();
			}
			// if the adjacent is not in the connected component, it is a new candidate
			else newCandidates.insert(checkSegment);
		}

		// create a new connected component data
		CandidateBlock newConnComp;

		// set the new connected component data
		newConnComp.segments = connComp.segments;
		newConnComp.segments.insert(candidate);
		newConnComp.matchings = connComp.matchings;
		newConnComp.matchings.insert(toAdd.begin(), toAdd.end());
		newConnComp.weight = connComp.weight + addedWeight;
		newConnComp.candidates = newCandidates;

		// add the generated connected component to the return set
		connComps.insert(newConnComp);
	}

	return connComps;
}

template <typename InM, typename OutM>
bool ConnComp_ExhaustiveImp<InM, OutM>::isSegmentSetConnComp()
{
	// first check that it is a connected component
	for(SegmentSet::const_iterator it1 = this->m_currentSegments.begin(); it1 != this->m_currentSegments.end(); ++it1)
	{
		SegmentSet::const_iterator it2;
		for(it2 = ++it1, it1--; it2 != this->m_currentSegments.end(); ++it2)
			if(!this->m_pairwiseSegmentGraph.existsPath(*it1, *it2)) return false;
	}

	initialConnComp.segments = this->m_currentSegments;

	// get all possible candidates to be on the connected component
	for(SegmentSet::const_iterator it1 = this->m_currentSegments.begin(); it1 != this->m_currentSegments.end(); ++it1)
	{
		typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
		vit = this->m_pairwiseSegmentGraph.hasVertex(*it1);
		typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
		for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
			// if the adjacent is not already in the connected component, it is added
			if(this->m_currentSegments.find(*ait) == this->m_currentSegments.end())
				initialConnComp.candidates.insert(*ait);
			// if it is, add the pointer to the matching and update the weight
			else
			{
				initialConnComp.weight += (!ait)->segmentsWeight();
				initialConnComp.matchings.insert(!ait);
			}
	}

	return true;
}

#endif
