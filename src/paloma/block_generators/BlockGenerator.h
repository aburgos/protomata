
#ifndef BLOCKGENERATOR_H
#define BLOCKGENERATOR_H

#include "CandidateBlock.h"

/*!
Abstract class for block generators.
*/
class BlockGenerator
{
  public:
	BlockGenerator() {}
	virtual ~BlockGenerator() {}
	/*!
		Gets the next CandidateBlock generated.
		\return A CandidateBlock.
	*/
	virtual CandidateBlock getNextBlock() = 0;
	//! Computes one block.
	virtual void computeOneBlock() = 0;
	//! Computes all possible blocks.
	virtual void computeAllBlocks() = 0;
};

#endif
