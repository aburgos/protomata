
#ifndef CLIQUEEXHAUSTIVE_H
#define CLIQUEEXHAUSTIVE_H

#include "Clique.h"
#include "ConnComp_ExhaustiveImp.h"

/*!
Computes all possible cliques starting from a set of segments.
*/
template <typename InM, typename OutM>
class Clique_ExhaustiveImp: public virtual Clique<InM, OutM>, public virtual ConnComp_ExhaustiveImp<InM, OutM>
{
  protected:
	/*!
		From the parameter block, this method generates as many blocks as different combinations of segments on a block exists. The candidate segments to be added to the parameter block are the ones that are adjacent to all of the segments of the parameter block.
		\return A set of blocks.
	*/
	virtual BlockSet increaseConnComp(const CandidateBlock&);
	/*!
		Check whether the segment set passed as argument to the constructor is a clique.  It also constructs the initial clique block.
		/return True if the segments in the segment set taken by the constructor form a clique. False otherwise.
	*/
	virtual bool isSegmentSetConnComp();
	/*!
		Checks if a Segment is a candidate of a set of segments, meaning that it is adjacent in the pairwise segment graph to all the segments in the segmentes set.
		\return True if the Segment is adjacent to all the segments in the segment set. False otherwise.
	*/
	virtual bool isCandidate(const Segment&, const SegmentSet&);

  public:
	//! A constructor that sets the isSegSetConnComp variable by calling isSegmentSetConnComp.
	Clique_ExhaustiveImp(const MultiSequence&, const GeneralizedSet<OutM>&, PairwiseLocalMatchingSet<InM>&, const SegmentSet&);
	virtual ~Clique_ExhaustiveImp() {}
};

template <typename InM, typename OutM>
Clique_ExhaustiveImp<InM, OutM>::Clique_ExhaustiveImp(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs), Clique<InM, OutM>(ms, gs, plms, segs), ConnComp_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs)
{
	this->isSegSetConnComp = this->isSegmentSetConnComp();
}

template <typename InM, typename OutM>
BlockSet Clique_ExhaustiveImp<InM, OutM>::increaseConnComp(const CandidateBlock& clique)
{
	// the set to be returned
	BlockSet cliques;

	// for each candidate, generate a new clique
	for(SegmentSet::const_iterator cit = clique.candidates.begin(); cit != clique.candidates.end(); ++cit)
	{
		// the candidate to add to the clique
		const Segment& candidate = *cit;

		// get needed info
		double addedWeight = 0;
		std::set<const PairwiseLocalMatching*> toAdd;
		SegmentSet newCandidates;

		typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
		vit = this->m_pairwiseSegmentGraph.hasVertex(candidate);
		typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
		for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
		{
			const Segment& checkSegment = *ait;
			// if the adjacent of the candidate is already in the clique, the weight and matching are added
			if(clique.segments.find(checkSegment) != clique.segments.end())
			{
				const PairwiseLocalMatching* plm = !ait;
				toAdd.insert(plm);
				addedWeight += plm->segmentsWeight();
			}
			// if the adjacent is not in the clique
			else if(clique.candidates.find(checkSegment) != clique.candidates.end())
				// if the adjacent is already a candidate
				// then is also adjacent to the rest of the segments of the clique
				newCandidates.insert(checkSegment);
		}

		// create a new clique data
		CandidateBlock newClique;

		// set the new clique data
		newClique.segments = clique.segments;
		newClique.segments.insert(candidate);
		newClique.matchings = clique.matchings;
		newClique.matchings.insert(toAdd.begin(), toAdd.end());
		newClique.weight = clique.weight + addedWeight;
		newClique.candidates = newCandidates;

		// add the generated clique to the return set
		cliques.insert(newClique);
	}

	return cliques;
}

template <typename InM, typename OutM>
bool Clique_ExhaustiveImp<InM, OutM>::isSegmentSetConnComp()
{
	// first check that it is a clique
	for(SegmentSet::const_iterator it1 = this->m_currentSegments.begin(); it1 != this->m_currentSegments.end(); ++it1)
	{
		SegmentSet::const_iterator it2;
		for(it2 = ++it1, it1--; it2 != this->m_currentSegments.end(); ++it2)
		{
			PairwiseSegmentGraph::relation_iterator rit = this->m_pairwiseSegmentGraph.hasEdge(*it1, *it2);
			if(rit == this->m_pairwiseSegmentGraph.relation_end()) return false;
			else
			{
				const SegmentPairwiseRelation& r = *rit;
				const PairwiseLocalMatching* plm = r.value();
				this->initialConnComp.matchings.insert(plm);
				this->initialConnComp.weight += plm->segmentsWeight();
			}
		}
	}

	this->initialConnComp.segments = this->m_currentSegments;

	// get all possible candidates to be on the clique
	const Segment& segment = *(this->m_currentSegments.begin());
	typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
	vit = this->m_pairwiseSegmentGraph.hasVertex(segment);
	typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
	// traverse the adjacents of a segment of the clique, doesn't matter which one
	for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
		// if the adjacent is also adjacent to all the segments in the clique, add it as a candidate
		if(this->isCandidate(*ait, this->m_currentSegments))
			this->initialConnComp.candidates.insert(*ait);

	return true;
}

template <typename InM, typename OutM>
bool Clique_ExhaustiveImp<InM, OutM>::isCandidate(const Segment& candidate, const SegmentSet& checkSet)
{
	// if the candidate is already in the check set, it is not a candidate for adding to the clique
	if(checkSet.find(candidate) != checkSet.end()) return false;
	if(checkSet.size() == 1) return true;

	// get all possible candidates to be on the clique
	typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
	vit = this->m_pairwiseSegmentGraph.hasVertex(candidate);

	for(SegmentSet::const_iterator sit = checkSet.begin(); sit != checkSet.end(); ++sit)
	{
		typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
		for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
			if(*sit == *ait) break;
		if(ait == vit.adjacent_end()) return false;
	}

	return true;
}

#endif
