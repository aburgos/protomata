
#ifndef CONNCOMPGLOCONOPTGREEDYBESTWGTIMP_H
#define CONNCOMPGLOCONOPTGREEDYBESTWGTIMP_H

#include "ConnComp_GloConOpt_GreedyImp.h"

/*!
Tries! to compute the best maximal clique. A possible approach would be to check for compatibility at the end of computing the clique, and in case of incompatibility, discard it and compute on clique again. To be defined.
*/
template <typename InM, typename OutM>
class ConnComp_GloConOpt_GreedyBestWgtImp: public ConnComp_GloConOpt_GreedyImp<InM, OutM>
{
  protected:
	/*!
		Check whether the segment set passed as argument to the constructor is a connected component. It also computes the initial connected component, by choosing for the candidates of it the maximum weight segment of each sequence (being the segment adjacent to all of the current segments).
		/return True if the segments in the segment set taken by the constructor form a connected component. False otherwise.
	*/
	virtual bool isSegmentSetConnComp();

  public:
	//! A constructor that sets the isSegSetConnComp variable by calling isSegmentSetConnComp.
	ConnComp_GloConOpt_GreedyBestWgtImp(const MultiSequence&, const GlobalConsistentGA<OutM>&, PairwiseLocalMatchingSet<InM>&, const SegmentSet&);
	virtual ~ConnComp_GloConOpt_GreedyBestWgtImp() {}
};

template <typename InM, typename OutM>
ConnComp_GloConOpt_GreedyBestWgtImp<InM, OutM>::ConnComp_GloConOpt_GreedyBestWgtImp(const MultiSequence& ms, const GlobalConsistentGA<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs), ConnComp_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), ConnComp_GloConOpt_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), ConnComp_GreedyImp<InM, OutM>(ms, gs, plms, segs), ConnComp_GloConOpt_GreedyImp<InM, OutM>(ms, gs, plms, segs)
{
	this->isSegSetConnComp = this->isSegmentSetConnComp();
}

template <typename InM, typename OutM>
bool ConnComp_GloConOpt_GreedyBestWgtImp<InM, OutM>::isSegmentSetConnComp()
{
	this->initialConnComp.weight = 0;

	// first check that it is a connected component
	for(SegmentSet::const_iterator it1 = this->m_currentSegments.begin(); it1 != this->m_currentSegments.end(); ++it1)
	{
		SegmentSet::const_iterator it2;
		this->initialConnComp.usedSeqs.insert((*it1).sequence());
		for(it2 = ++it1, it1--; it2 != this->m_currentSegments.end(); ++it2)
			if(!this->m_pairwiseSegmentGraph.existsPath(*it1, *it2)) return false;
	}

	this->initialConnComp.segments = this->m_currentSegments;

	std::map<unsigned int, std::pair<double, Segment> > bestWgtCandidates;

	// get all possible candidates to be on the connected component
	for(SegmentSet::const_iterator it1 = this->m_currentSegments.begin(); it1 != this->m_currentSegments.end(); ++it1)
	{
		typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
		vit = this->m_pairwiseSegmentGraph.hasVertex(*it1);
		typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
		for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
		{
			const Segment& seg = *ait;
			if(this->m_currentSegments.find(seg) == this->m_currentSegments.end() && this->initialConnComp.usedSeqs.find(seg.sequence()) == this->initialConnComp.usedSeqs.end())
			{
				const PairwiseLocalMatching* plm = !ait;
				if(bestWgtCandidates.find(seg.sequence()) == bestWgtCandidates.end())
					bestWgtCandidates[seg.sequence()] = std::make_pair(plm->segmentsWeight(), seg);
				else if(plm->segmentsWeight() > bestWgtCandidates[seg.sequence()].first)
					bestWgtCandidates[seg.sequence()] = std::make_pair(plm->segmentsWeight(), seg);
			}
		}
	}

	std::map<unsigned int, std::pair<double, Segment> >::const_iterator mit;
	for(mit = bestWgtCandidates.begin(); mit != bestWgtCandidates.end(); ++mit)
	{
		const std::pair<double, Segment>& sp = (*mit).second;
		this->initialConnComp.candidates.insert(sp.second);
	}

	return true;
}

#endif
