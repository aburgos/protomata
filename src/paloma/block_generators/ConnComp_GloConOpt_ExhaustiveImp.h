
#ifndef CONNCOMPGLOCONOPTEXHAUSTIVE_H
#define CONNCOMPGLOCONOPTEXHAUSTIVE_H

#include "ConnComp_ExhaustiveImp.h"

/*!
Computes all possible connected components starting from a set of segments, but checking for every clique computed (while being constructed) the compatibility with the global consistent generalized alignment that will hold the result.
*/
template <typename InM, typename OutM>
class ConnComp_GloConOpt_ExhaustiveImp: public virtual ConnComp_ExhaustiveImp<InM, OutM>
{
  protected:
	/*!
		From the parameter block, this method generates as many blocks as different combinations of segments on a block exists. The candidate segments to be added to the parameter block are the ones that are adjacent to any of the segments of the parameter block, and also compatible with the global consistent generalized alignment.
		\return A set of blocks.
	*/
	virtual BlockSet increaseConnComp(const CandidateBlock&);
	/*!
		Check whether the segment set passed as argument to the constructor is a connected component. It also constructs the initial connected component block.
		/return True if the segments in the segment set taken by the constructor form a connected component. False otherwise.
	*/
	virtual bool isSegmentSetConnComp();

  public:
	//! A constructor that sets the isSegSetConnComp variable by calling isSegmentSetConnComp.
	ConnComp_GloConOpt_ExhaustiveImp(const MultiSequence&, const GlobalConsistentGA<OutM>&, PairwiseLocalMatchingSet<InM>&, const SegmentSet&);
	virtual ~ConnComp_GloConOpt_ExhaustiveImp() {}
};

template <typename InM, typename OutM>
ConnComp_GloConOpt_ExhaustiveImp<InM, OutM>::ConnComp_GloConOpt_ExhaustiveImp(const MultiSequence& ms, const GlobalConsistentGA<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs), ConnComp_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs)
{
	this->isSegSetConnComp = this->isSegmentSetConnComp();
}

template <typename InM, typename OutM>
BlockSet ConnComp_GloConOpt_ExhaustiveImp<InM, OutM>::increaseConnComp(const CandidateBlock& connComp)
{
	const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(this->m_gs);

	// the set to be returned
	BlockSet connComps;

	// for each candidate, generate a new connected component
	for(SegmentSet::const_iterator cit = connComp.candidates.begin(); cit != connComp.candidates.end(); ++cit)
	{
		// the candidate to add to the connected component
		const Segment& candidate = *cit;
		bool discardCandidate = false;

		// updated candidates
		double addedWeight = 0;
		std::set<const PairwiseLocalMatching*> toAdd;
		SegmentSet newCandidates;

		typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
		vit = this->m_pairwiseSegmentGraph.hasVertex(candidate);
		typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
		for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
		{
			const Segment& checkSegment = *ait;
			// if the adjacent of the candidate is already in the connected component, the weight and matching are added
			if(connComp.segments.find(checkSegment) != connComp.segments.end())
			{
				// if there is an adjacent of the candidate that makes the connected component incompatible
				// it is discarded, the edge is deleted from the pairwise segment graph
				// and the next candidate is evaluated
				if(!gcga.gabios->areAlignable(checkSegment, candidate))
				{
					discardCandidate = 1;
					this->m_pairwiseSegmentGraph.deleteEdge(checkSegment, candidate);
					break;
				}
				const PairwiseLocalMatching* plm = !ait;
				toAdd.insert(plm);
				addedWeight += plm->segmentsWeight();
			}
			// if the adjacent is not in the connected component
			else
			{
				// if there is no segment already in the connected component with the same sequence of
				// the adjacent being checked, it is added as a candidate
				if(connComp.usedSeqs.find(checkSegment.sequence()) == connComp.usedSeqs.end())
					newCandidates.insert(checkSegment);
			}
		}

		if(discardCandidate) continue;

		// create a new connected component data
		CandidateBlock newConnComp;

		// set the new connected component data
		newConnComp.segments = connComp.segments;
		newConnComp.segments.insert(candidate);
		newConnComp.usedSeqs = connComp.usedSeqs;
		newConnComp.usedSeqs.insert(candidate.sequence());
		newConnComp.matchings = connComp.matchings;
		newConnComp.matchings.insert(toAdd.begin(), toAdd.end());
		newConnComp.weight = connComp.weight + addedWeight;
		newConnComp.candidates = newCandidates;

		// add the generated connected component to the return set
		connComps.insert(newConnComp);
	}

	return connComps;
}

template <typename InM, typename OutM>
bool ConnComp_GloConOpt_ExhaustiveImp<InM, OutM>::isSegmentSetConnComp()
{
	// first check that it is a connected component
	for(SegmentSet::const_iterator it1 = this->m_currentSegments.begin(); it1 != this->m_currentSegments.end(); ++it1)
	{
		SegmentSet::const_iterator it2;
		this->initialConnComp.usedSeqs.insert((*it1).sequence());
		for(it2 = ++it1, it1--; it2 != this->m_currentSegments.end(); ++it2)
			if(!this->m_pairwiseSegmentGraph.existsPath(*it1, *it2)) return false;
	}

	this->initialConnComp.segments = this->m_currentSegments;

	// get all possible candidates to be on the connected component
	for(SegmentSet::const_iterator it1 = this->m_currentSegments.begin(); it1 != this->m_currentSegments.end(); ++it1)
	{
		typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
		vit = this->m_pairwiseSegmentGraph.hasVertex(*it1);
		typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
		for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
		{
			const Segment& seg = *ait;
			// if the adjacent is not already in the connected component and the sequence id is not already used
			// by other segment in the connected component, it is added
			if(this->m_currentSegments.find(seg) == this->m_currentSegments.end() && this->initialConnComp.usedSeqs.find(seg.sequence()) == this->initialConnComp.usedSeqs.end())
				this->initialConnComp.candidates.insert(seg);
			// if it is in the connected component, add the pointer to the matching and update the weight
			else if(this->m_currentSegments.find(seg) != this->m_currentSegments.end())
			{
				this->initialConnComp.weight += (!ait)->segmentsWeight();
				this->initialConnComp.matchings.insert(!ait);
			}
		}
	}

	return true;
}

#endif
