
#ifndef CLIQUEGLOCONOPTGREEDYIMP_H
#define CLIQUEGLOCONOPTGREEDYIMP_H

#include "Clique_GreedyImp.h"
#include "Clique_GloConOpt_ExhaustiveImp.h"

/*!
Computes a clique in a greedy way starting from a set of segments, but checking for every clique computed (while being constructed) the compatibility with the global consistent generalized alignment that will hold the result.
*/
template <typename InM, typename OutM>
class Clique_GloConOpt_GreedyImp: public virtual Clique_GloConOpt_ExhaustiveImp<InM, OutM>, public virtual Clique_GreedyImp<InM, OutM>
{
  public:
	Clique_GloConOpt_GreedyImp(const MultiSequence&, const GlobalConsistentGA<OutM>&, PairwiseLocalMatchingSet<InM>&, const SegmentSet&);
	virtual ~Clique_GloConOpt_GreedyImp() {}
	//! Starting from a seed block, it is increased at each step and the best weighted block is stored.
	virtual void computeOneBlock();
	//! Same as computeOneBlock, but it stores every block computed.
	virtual void computeAllBlocks();
};

template <typename InM, typename OutM>
Clique_GloConOpt_GreedyImp<InM, OutM>::Clique_GloConOpt_GreedyImp(const MultiSequence& ms, const GlobalConsistentGA<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs), Clique<InM, OutM>(ms, gs, plms, segs), ConnComp_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), Clique_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), Clique_GloConOpt_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), ConnComp_GreedyImp<InM, OutM>(ms, gs, plms, segs), Clique_GreedyImp<InM, OutM>(ms, gs, plms, segs) {}

template <typename InM, typename OutM>
void Clique_GloConOpt_GreedyImp<InM, OutM>::computeOneBlock()
{
	Clique_GreedyImp<InM, OutM>::computeOneBlock();
}

template <typename InM, typename OutM>
void Clique_GloConOpt_GreedyImp<InM, OutM>::computeAllBlocks()
{
	Clique_GreedyImp<InM, OutM>::computeAllBlocks();
}

#endif
