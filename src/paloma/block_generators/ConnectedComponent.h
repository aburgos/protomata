
#ifndef CONNECTEDCOMPONENT_H
#define CONNECTEDCOMPONENT_H

#include <set>
#include "BlockGenerator.h"
#include "MultiSequence.h"
#include "generalized_sets/GeneralizedSet.h"
#include "lib/BlockSet.h"

/*!
Abstract class for connected component blocks generators.
*/
template <typename InM, typename OutM>
class ConnectedComponent: public BlockGenerator
{
  protected:
	//! The sequences on which the class will work
	const MultiSequence& m_ms;
	//! The generalized set to check for compatibility (if needed) of the generated matchings.
	const GeneralizedSet<OutM>& m_gs;
	//! Input set with all the matchings read somewhere.
	PairwiseLocalMatchingSet<InM>& m_plms;
	//! The pairwise segment graph to be used for generating the candidates.
	PairwiseSegmentGraph& m_pairwiseSegmentGraph;
	//! The segments that will be included in the connected component.
	const SegmentSet& m_currentSegments;

  public:
	ConnectedComponent(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): m_ms(ms), m_gs(gs), m_plms(plms), m_pairwiseSegmentGraph(plms.pairwiseSegmentGraph), m_currentSegments(segs) {}
	virtual ~ConnectedComponent() {}
};

#endif
