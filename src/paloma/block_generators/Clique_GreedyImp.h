
#ifndef CLIQUEGREEDYIMP_H
#define CLIQUEGREEDYIMP_H

#include "Clique_ExhaustiveImp.h"
#include "ConnComp_GreedyImp.h"

/*!
Computes a clique in a greedy way starting from a set of segments.
*/
template <typename InM, typename OutM>
class Clique_GreedyImp: public virtual Clique_ExhaustiveImp<InM, OutM>, public virtual ConnComp_GreedyImp<InM, OutM>
{
  public:
	Clique_GreedyImp(const MultiSequence&, const GeneralizedSet<OutM>&, PairwiseLocalMatchingSet<InM>&, const SegmentSet&);
	virtual ~Clique_GreedyImp() {}
	//! Starting from a seed block, it is increased at each step and the best weighted block is stored.
	virtual void computeOneBlock();
	//! Same as computeOneBlock, but it stores every block computed.
	virtual void computeAllBlocks();
};

template <typename InM, typename OutM>
Clique_GreedyImp<InM, OutM>::Clique_GreedyImp(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs), Clique<InM, OutM>(ms, gs, plms, segs), ConnComp_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), Clique_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), ConnComp_GreedyImp<InM, OutM>(ms, gs, plms, segs) {}

template <typename InM, typename OutM>
void Clique_GreedyImp<InM, OutM>::computeOneBlock()
{
	ConnComp_GreedyImp<InM, OutM>::computeOneBlock();
}

template <typename InM, typename OutM>
void Clique_GreedyImp<InM, OutM>::computeAllBlocks()
{
	ConnComp_GreedyImp<InM, OutM>::computeAllBlocks();
}

#endif
