
#ifndef CONNCOMPGREEDYBESTWGTIMP_H
#define CONNCOMPGREEDYBESTWGTIMP_H

#include "ConnComp_GreedyImp.h"

/*!
Computes a connected component in a greedy way starting from a set of segments, but preprocessing the segment candidates for increassing a block.
*/
template <typename InM, typename OutM>
class ConnComp_GreedyBestWgtImp: public virtual ConnComp_GreedyImp<InM, OutM>
{
  protected:
	/*!
		Check whether the segment set passed as argument to the constructor is a connected component. It also computes the initial connected component, by choosing for the candidates of it the maximum weight segment of each sequence (being the segment adjacent to any of the current segments).
		/return True if the segments in the segment set taken by the constructor form a connected component. False otherwise.
	*/
	bool isSegmentSetConnComp();

  public:
	//! A constructor that sets the isSegSetConnComp variable.
	ConnComp_GreedyBestWgtImp(const MultiSequence&, const GeneralizedSet<OutM>&, PairwiseLocalMatchingSet<InM>&, const SegmentSet&);
	virtual ~ConnComp_GreedyBestWgtImp() {}
};

template <typename InM, typename OutM>
ConnComp_GreedyBestWgtImp<InM, OutM>::ConnComp_GreedyBestWgtImp(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs), ConnComp_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), ConnComp_GreedyImp<InM, OutM>(ms, gs, plms, segs)
{
	this->isSegSetConnComp = this->isSegmentSetConnComp();
}

template <typename InM, typename OutM>
bool ConnComp_GreedyBestWgtImp<InM, OutM>::isSegmentSetConnComp()
{
	// first check that it is a connected component
	for(SegmentSet::const_iterator it1 = this->m_currentSegments.begin(); it1 != this->m_currentSegments.end(); ++it1)
	{
		SegmentSet::const_iterator it2;
		for(it2 = ++it1, it1--; it2 != this->m_currentSegments.end(); ++it2)
			if(!this->m_pairwiseSegmentGraph.existsPath(*it1, *it2)) return false;
	}

	this->initialConnComp.segments = this->m_currentSegments;

	std::map<unsigned int, std::pair<double, Segment> > bestWgtCandidates;

	// get all possible candidates to be on the connected component
	for(SegmentSet::const_iterator it1 = this->m_currentSegments.begin(); it1 != this->m_currentSegments.end(); ++it1)
	{
		typename PairwiseLocalMatchingSet<InM>::segment_iterator vit;
		vit = this->m_pairwiseSegmentGraph.hasVertex(*it1);
		typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator ait;
		for(ait = vit.adjacent_begin(); ait != vit.adjacent_end(); ++ait)
			// if the adjacent is not already in the connected component
			if(this->m_currentSegments.find(*ait) == this->m_currentSegments.end())
			{
				const PairwiseLocalMatching* plm = !ait;
				if(bestWgtCandidates.find((*ait).sequence()) == bestWgtCandidates.end())
					bestWgtCandidates[(*ait).sequence()] = std::make_pair(plm->segmentsWeight(), (*ait));
				else if(plm->segmentsWeight() > bestWgtCandidates[(*ait).sequence()].first)
					bestWgtCandidates[(*ait).sequence()] = std::make_pair(plm->segmentsWeight(), (*ait));
			}
	}

	std::map<unsigned int, std::pair<double, Segment> >::iterator mit;
	for(mit = bestWgtCandidates.begin(); mit != bestWgtCandidates.end(); ++mit)
	{
		const std::pair<double, Segment>& sp = (*mit).second;
		this->initialConnComp.candidates.insert(sp.second);
	}

	return true;
}

#endif
