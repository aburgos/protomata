
#ifndef CONNCOMPGLOCONOPTGREEDYIMP_H
#define CONNCOMPGLOCONOPTGREEDYIMP_H

#include "ConnComp_GreedyImp.h"
#include "ConnComp_ExhaustiveImp.h"
#include "ConnComp_GloConOpt_ExhaustiveImp.h"

/*!
Computes a connected component in a greedy way starting from a set of segments, but checking for every connected component computed (while being constructed) the compatibility with the global consistent generalized alignment that will hold the result.
*/
template <typename InM, typename OutM>
class ConnComp_GloConOpt_GreedyImp: public virtual ConnComp_GloConOpt_ExhaustiveImp<InM, OutM>, public virtual  ConnComp_GreedyImp<InM, OutM>
{
  public:
	ConnComp_GloConOpt_GreedyImp(const MultiSequence&, const GlobalConsistentGA<OutM>&, PairwiseLocalMatchingSet<InM>&, const SegmentSet&);
	virtual ~ConnComp_GloConOpt_GreedyImp() {}
};

template <typename InM, typename OutM>
ConnComp_GloConOpt_GreedyImp<InM, OutM>::ConnComp_GloConOpt_GreedyImp(const MultiSequence& ms, const GlobalConsistentGA<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs): ConnectedComponent<InM, OutM>(ms, gs, plms, segs), ConnComp_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), ConnComp_GloConOpt_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs), ConnComp_GreedyImp<InM, OutM>(ms, gs, plms, segs) {}

#endif
