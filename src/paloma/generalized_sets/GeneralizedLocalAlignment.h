
#ifndef GENLOCALALIGNMENT_H
#define GENLOCALALIGNMENT_H

#include "GeneralizedAlignment.h"

/*!
A GeneralizedAlignment with a SegmentGraph implementation.
*/
template <class OutM>
class GeneralizedLocalAlignment: public virtual GeneralizedAlignment<OutM>
{
  protected:
	//! Keeps the segment relations.
	SegmentGraph segmentGraph;
	/*!
		Adds the segment pair relation to the segment graph.
		\return The result of adding an edge to the segment graph.
	*/
	virtual bool addMatching(const SegmentPairRelation&);
	//!	Definition of the output (cout) operator for a GeneralizedLocalAlignment.
	virtual void print(std::ostream&) const;

  public:
	//! The constructor.
	GeneralizedLocalAlignment(const MultiSequence& ms): MatchingSet<OutM>(ms), Alignment_UFImp(ms),  GeneralizedAlignment<OutM>(ms) {}
	//! The destructor.
	virtual ~GeneralizedLocalAlignment() {}
	/*!
		First it adds all segment relations and after calls GeneralizedAlignment::addMatching.
		\return The result of GeneralizedAlignment::addMatching.
	*/
	virtual bool addMatching(const OutM&);
	/*!
		\return The result of GeneralizedAlignment::isCompatible.
	*/
	virtual bool isCompatible(const OutM&) const;
};

template <typename OutM>
bool GeneralizedLocalAlignment<OutM>::addMatching(const OutM& m)
{
	typename OutM::segment_iterator it1, it2;
	for(it1 = m.segment_begin(); it1 != m.segment_end(); it1++)
		for(it2 = ++it1, --it1; it2 != m.segment_end(); it2++)
		{
			SegmentPairRelation r(std::make_pair(*it1, *it2), 0);
			this->addMatching(r);
		}

	return GeneralizedAlignment<OutM>::addMatching(m);
}

template <typename OutM>
bool GeneralizedLocalAlignment<OutM>::isCompatible(const OutM& m) const
{
	// check this out
	return GeneralizedAlignment<OutM>::isCompatible(m);
}

template <typename OutM>
bool GeneralizedLocalAlignment<OutM>::addMatching(const SegmentPairRelation& r)
{
	return segmentGraph.addEdge(r.first(), r.second(), r.value());
}

template <typename OutM>
void GeneralizedLocalAlignment<OutM>::print(std::ostream& o) const
{
	GeneralizedAlignment<OutM>::print(o);
	o << segmentGraph << std::endl;
}

#endif
