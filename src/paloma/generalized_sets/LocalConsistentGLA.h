
#ifndef LOCALCONSGLA_H
#define LOCALCONSGLA_H

#include "GeneralizedLocalAlignment.h"
#include "lib/Gabios.h"

/*!
A GeneralizedLocalAlignment with the restriction of being locally consistent. For more information
refer to the PDF localConsistency in the documentation.
*/
template <typename OutM>
class LocalConsistentGLA: public virtual GeneralizedLocalAlignment<OutM>
{
  private:
	//! Weather the generalized set will allow repeats or not.
	bool allowRepeats;
	/*
		Constructs a Gabios object to evaluate the global consistency of the parameter matching.
		\param A matching whose consistency will be evaluated by using Gabios.
		\return True if the parameter matching is global consistent, False otherwise.
	*/
	virtual bool isConsistent(const OutM&) const;
	//!	Definition of the output (cout) operator for a LocalConsistentGLA.
	virtual void print(std::ostream&) const;

  public:
	//! The constructor.
	LocalConsistentGLA(const MultiSequence&, bool);
	//! The destructor.
	virtual ~LocalConsistentGLA() {}
	/*!
		It just calls GeneralizedLocalAlignment<OutM>::addMatching.
		\return The result of GeneralizedLocalAlignment<OutM>::addMatching.
	*/
	virtual bool addMatching(const OutM&);
	/*!
		The generalized set will be compatible if it will be an alignment after adding m and if it is also consistent.
		\return The value of (willBeAlignmentAfterAdding && isConsistent).
	*/
	virtual bool isCompatible(const OutM&) const;
};

template <typename OutM>
LocalConsistentGLA<OutM>::LocalConsistentGLA(const MultiSequence& ms, bool rep): MatchingSet<OutM>(ms), Alignment_UFImp(ms), GeneralizedAlignment<OutM>(ms), GeneralizedLocalAlignment<OutM>(ms)
{
	allowRepeats = rep;
}

template <typename OutM>
bool LocalConsistentGLA<OutM>::addMatching(const OutM& m)
{
	return GeneralizedLocalAlignment<OutM>::addMatching(m);
}

template <typename OutM>
bool LocalConsistentGLA<OutM>::isCompatible(const OutM& m) const
{
	return (willBeAlignmentAfterAdding(m) && isConsistent(m));
}

template <typename OutM>
bool LocalConsistentGLA<OutM>::isConsistent(const OutM& m) const
{
	SegmentSet segments;

	typename OutM::segment_iterator it;
	for(it = m.segment_begin(); it != m.segment_end(); it++)
		segments.insert(*it);

	Gabios gabios(segments, allowRepeats);
	relation_visitor v = m.site_relations();
	while(v.valid())
	{
		const SitePairRelation& r = v.get();
		if(!gabios.areAlignable(r.first(), r.second())) return false;
		gabios.addAlignment(r.first(), r.second());
		v.next();
	}

	return true;
}

template <class OutM>
void LocalConsistentGLA<OutM>::print(std::ostream& o) const
{
	GeneralizedLocalAlignment<OutM>::print(o);
}

#endif
