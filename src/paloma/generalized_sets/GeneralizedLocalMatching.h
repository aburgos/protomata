
#ifndef GENLOCALMATCHING_H
#define GENLOCALMATCHING_H

#include "GeneralizedMatching.h"

/*!
A GeneralizedMatching with a SegmentGraph implementation.
*/
template <typename OutM>
class GeneralizedLocalMatching: public virtual GeneralizedMatching<OutM>
{
  protected:
	//! Keeps the segment relations.
	SegmentGraph segmentGraph;
	/*!
		Adds the segment pair relation to the segment graph.
		\return The result of adding an edge to the segment graph.
	*/
	virtual bool addMatching(const SegmentPairRelation&);
	//!	Definition of the output (cout) operator for a GeneralizedLocalMatching.
	virtual void print(std::ostream&) const;

  public:
	//! The constructor.
	GeneralizedLocalMatching(const MultiSequence& ms): MatchingSet<OutM>(ms), Matching_SiGrImp(ms), SiteMatchingSet<OutM>(ms), GeneralizedMatching<OutM>(ms) {}
	//! The destructor.
	virtual ~GeneralizedLocalMatching() {}
	/*!
		First it adds all segment relations and after calls GeneralizedMatching::addMatching.
		\return The result of GeneralizedMatching::addMatching.
	*/
	virtual bool addMatching(const OutM&);
	/*!
		Nothing.
		\return Always True.
	*/
	virtual bool isCompatible(const OutM&) const;
};

template <typename OutM>
bool GeneralizedLocalMatching<OutM>::addMatching(const OutM& m)
{
	typename OutM::segment_iterator it1, it2;
	for(it1 = m.segment_begin(); it1 != m.segment_end(); it1++)
		for(it2 = ++it1, --it1; it2 != m.segment_end(); it2++)
		{
			SegmentPairRelation r(std::make_pair(*it1, *it2), 0);
			this->addMatching(r);
		}

	return GeneralizedMatching<OutM>::addMatching(m);
}

template <typename OutM>
bool GeneralizedLocalMatching<OutM>::isCompatible(const OutM&) const
{
	return true;
}

template <typename OutM>
bool GeneralizedLocalMatching<OutM>::addMatching(const SegmentPairRelation& r)
{
	return segmentGraph.addEdge(r.first(), r.second(), r.value());
}

template <typename OutM>
void GeneralizedLocalMatching<OutM>::print(std::ostream& o) const
{
	GeneralizedMatching<OutM>::print(o);
	o << segmentGraph << std::endl;
}

#endif
