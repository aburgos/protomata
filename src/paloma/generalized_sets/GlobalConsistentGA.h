
#ifndef GLOBALCONSGA_H
#define GLOBALCONSGA_H

#include "GeneralizedAlignment.h"
#include "lib/Gabios.h"

/*!
A GeneralizedAlignment with the restriction of being globally consistent.
*/
template <typename OutM>
class GlobalConsistentGA: public virtual GeneralizedAlignment<OutM>
{
  protected:
	//! A pointer to a GABIOS object.
	Gabios* gabios;
	/*!
		Checks that every site relation of the matching is alignable. The copy alignment is for adding the already checked site relations, because an inconsistency can happend if they are checked but not added.
		\return True if the final set will be consistent after adding m, False otherwise.
	*/
	virtual bool isConsistent(const OutM&) const;
	//!	Definition of the output (cout) operator for a GlobalConsistentGA.
	virtual void print(std::ostream&) const;

  public:
	//! The constructor.
	GlobalConsistentGA(const MultiSequence&);
	//! The destructor deletes the GABIOS object.
	virtual ~GlobalConsistentGA() { delete gabios; }
	/*!
		First adds the site relations to the GABIOS object, then it calls GeneralizedAlignment::addMatching.
		\return The result of GeneralizedAlignment::addMatching.
	*/
	virtual bool addMatching(const OutM&);
	/*!
		The generalized set will be compatible if it will be an alignment after adding m and if it is also consistent.
		\return The value of (willBeAlignmentAfterAdding && isConsistent).
	*/
	virtual bool isCompatible(const OutM&) const;
	//! Needed for optimization pourposes.
	template <typename In, typename Out> friend class SeedApproach;
	//! Needed for optimization pourposes.
	template <typename In, typename Out> friend class MaxWeightPairwiseLocalCandidate_GloConOpt;
	//! Needed for optimization pourposes.
	template <typename In, typename Out> friend class ConnComp_GloConOpt_ExhaustiveImp;
	//! Needed for optimization pourposes.
	template <typename In, typename Out> friend class Clique_GloConOpt_ExhaustiveImp;
};

template <typename OutM>
GlobalConsistentGA<OutM>::GlobalConsistentGA(const MultiSequence& ms): MatchingSet<OutM>(ms), Alignment_UFImp(ms), GeneralizedAlignment<OutM>(ms)
{
	gabios = new Gabios(ms);
}

template <typename OutM>
bool GlobalConsistentGA<OutM>::addMatching(const OutM& m)
{
	relation_visitor v = m.site_relations();
	while(v.valid())
	{
		SitePairRelation r = v.get();
		gabios->addAlignment(r.first(), r.second());
		v.next();
	}

	return GeneralizedAlignment<OutM>::addMatching(m);
}

template <typename OutM>
bool GlobalConsistentGA<OutM>::isCompatible(const OutM& m) const
{
	return (willBeAlignmentAfterAdding(m) && isConsistent(m));
}

template <typename OutM>
bool GlobalConsistentGA<OutM>::isConsistent(const OutM& m) const
{
	relation_visitor v = m.site_relations();
	while(v.valid())
	{
		SitePairRelation r = v.get();
		// check for consistency
		if(!gabios->areAlignable(r.first(), r.second())) return false;
		v.next();
	}
	return true;
}

template <typename OutM>
void GlobalConsistentGA<OutM>::print(std::ostream& o) const
{
	GeneralizedAlignment<OutM>::print(o);
}

#endif
