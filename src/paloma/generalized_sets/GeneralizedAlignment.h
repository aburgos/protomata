
#ifndef GENALIGNMENT_H
#define GENALIGNMENT_H

#include "GeneralizedSet.h"
#include "matching_sets/MatchingSet.h"
#include "alignments/Alignment_UFImp.h"
#include "iterators/AlignmentUFImpRelationIterator.h"

/*!
A GeneralizedSet with a MatchingSet and UnionFind implementation.
*/
template <typename OutM>
class GeneralizedAlignment: public virtual GeneralizedSet<OutM>, private virtual MatchingSet<OutM>, protected virtual Alignment_UFImp
{
  protected:
	//!	Definition of the output (cout) operator for a GeneralizedAlignment.
	virtual void print(std::ostream&) const;

  public:
	//! The constructor.
	GeneralizedAlignment(const MultiSequence& ms): MatchingSet<OutM>(ms), Alignment_UFImp(ms) {}
	//! The destructor.
	virtual ~GeneralizedAlignment() {}
	unsigned int size() const { return MatchingSet<OutM>::size(); }
	/*!
		It adds all relations and after the matching itself.
		\return The result of MatchingSet::storeMatching if the relations were added. False otherwise.
	*/
	virtual bool addMatching(const OutM&);
	/*!
		Checks if, after adding the parameter matching, the resulting generalized set is still an alignment.
		\return True if the result of adding the matching is an alignment. False otherwise.
	*/
	virtual bool isCompatible(const OutM&) const;
	virtual bool hasSiteRelation(const SitePairRelation&) const;
	//! Alias for the iterator through the list of matchings.
	typedef typename std::list<OutM>::const_iterator matching_iterator;
	/*! \return An iterator to the begin of the matchings list. */
	matching_iterator matching_begin() const { return this->matchings.begin(); }
	/*! \return An iterator to the end of the matchings list. */
	matching_iterator matching_end() const { return this->matchings.end(); }
	/*!
		Gets a visitor to access the all the relations of this alignment. The visitor is implemented by a AlignmentUFImpRelationIterator.
		\return A visitor to all the relations of this alignment.
	*/
	relation_visitor site_relations() const;
	//! For the output the private variables are needed.
	template <typename OM> friend class AlignmentOutput;
	//! For the block graph the private variables are needed.
	template <typename A, typename C, template <typename T> class P> friend class ProtoGraphBuilder;
};

template <typename OutM>
bool GeneralizedAlignment<OutM>::addMatching(const OutM& m)
{
	// add the site edges to the union find structure
	if(Alignment_UFImp::addMatching(m)) return MatchingSet<OutM>::storeMatching(m);
	else return false;
}

template <typename OutM>
bool GeneralizedAlignment<OutM>::isCompatible(const OutM& m) const
{
	return Alignment_UFImp::willBeAlignmentAfterAdding(m);
}

template <typename OutM>
bool GeneralizedAlignment<OutM>::hasSiteRelation(const SitePairRelation& r) const
{
	return union_find.hasRelation(site2uint(r.first()), site2uint(r.second()));
}

template <typename OutM>
void GeneralizedAlignment<OutM>::print(std::ostream& o) const
{
	MatchingSet<OutM>::print(o);
	Alignment_UFImp::print(o);
}

template <typename OutM>
relation_visitor GeneralizedAlignment<OutM>::site_relations() const
{
	relation_iterator* it = new AlignmentUFImpRelationIterator(this, false);
	relation_iterator* end = new AlignmentUFImpRelationIterator(this, true);
	return relation_visitor(it, end);
}

#endif
