					
#ifndef GENMATCHING_H
#define GENMATCHING_H

#include "GeneralizedSet.h"
#include "matching_sets/SiteMatchingSet.h"

/*!
A GeneralizedSet with a SiteGraph implementation, obtained by deriving from the SiteMatchingSet class. This way it also keeps a copy of the matching objects added.
*/
template <typename OutM>
class GeneralizedMatching: public virtual GeneralizedSet<OutM>, private virtual SiteMatchingSet<OutM>
{
  protected:
	//!	Definition of the output (cout) operator for a GeneralizedMatching.
	virtual void print(std::ostream&) const;

  public:
	//! The constructor.
	GeneralizedMatching(const MultiSequence& ms): MatchingSet<OutM>(ms), Matching_SiGrImp(ms), SiteMatchingSet<OutM>(ms) {}
	//! The destructor.
	virtual ~GeneralizedMatching() {}
	//! Number of matchings.
	unsigned int size() const { return SiteMatchingSet<OutM>::size(); }
	/*!
		Adds a matching to the generalized matching.
		\return Always True.
	*/
	virtual bool addMatching(const OutM&);
	/*!
		Nothing.
		\return Always True.
	*/
	virtual bool isCompatible(const OutM&) const;
	virtual bool hasSiteRelation(const SitePairRelation&) const;
	//! Alias for the iterator through the list of matchings.
	typedef typename std::list<OutM>::const_iterator matching_iterator;
	/*! \return An iterator to the begin of the matchings list. */
	matching_iterator matching_begin() const { return this->matchings.begin(); }
	/*! \return An iterator to the end of the matchings list. */
	matching_iterator matching_end() const { return this->matchings.end(); }
	/*!
		Gets a visitor to access the all the relations of this generalized matching. The visitor is implemented by a GraphRelationIterator.
		\return A visitor to all the relations of this generalized matching.
	*/
	relation_visitor site_relations() const;
};

template <typename OutM>
bool GeneralizedMatching<OutM>::addMatching(const OutM& m)
{
	return SiteMatchingSet<OutM>::storeMatching(m);
}

template <typename OutM>
bool GeneralizedMatching<OutM>::isCompatible(const OutM&) const
{
	return true;
}

template <typename OutM>
bool GeneralizedMatching<OutM>::hasSiteRelation(const SitePairRelation& r) const
{
	return (SiteMatchingSet<OutM>::siteGraph.hasEdge(r.first(), r.second()) != SiteMatchingSet<OutM>::siteGraph.relation_end());
}

template <typename OutM>
relation_visitor GeneralizedMatching<OutM>::site_relations() const
{
	relation_iterator* it = new GraphRelationIterator(SiteMatchingSet<OutM>::siteGraph.relation_begin());
	relation_iterator* end = new GraphRelationIterator(SiteMatchingSet<OutM>::siteGraph.relation_end());
	return relation_visitor(it, end);
}

template <typename OutM>
void GeneralizedMatching<OutM>::print(std::ostream& o) const
{
	SiteMatchingSet<OutM>::print(o);
}

#endif
