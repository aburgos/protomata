
#ifndef GENERALIZEDSET_H
#define GENERALIZEDSET_H

#include "Typedefs.h"
#include "iterators/Visitor.h"

/*!
Set to store the generated candidates.
*/
template <typename OutM>
class GeneralizedSet
{
  protected:
	//! Virtual method to be implemented in each derived class.
	virtual void print(std::ostream&) const {}

  public:
	//! The constructor.
	GeneralizedSet() {}
	//! The destructor.
	virtual ~GeneralizedSet() {}
	//! Number of matchings.
	virtual unsigned int size() const = 0;
	/*!
		Adds a matching to the generalized set.
		\return True if the matching was added. False otherwise.
	*/
	virtual bool addMatching(const OutM&) = 0;
	/*!
		Checks for compatibility with the parameter matching.
		\return True if the generalized set will keep compatibility constraints after adding the matching passed by parameter.
	*/
	virtual bool isCompatible(const OutM&) const = 0;
	virtual bool hasSiteRelation(const SitePairRelation&) const = 0;
	virtual relation_visitor site_relations() const = 0;
	//!	Definition of the output stream operator for a generalized set. It calls the protected function print defined on each class.
	friend std::ostream& operator<<(std::ostream& o, const GeneralizedSet<OutM>& m)
	{ m.print(o); return o; }
};

#endif
