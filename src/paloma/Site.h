
#ifndef SITE_H
#define SITE_H

#include <iostream>

/*!
A Site represents a char in a Sequence, by indicating the sequence it belongs to and its position in that sequence.
*/
class Site
{
  private:
	//! The Sequence number starting from 0.
	unsigned int m_sequence;
	//! The Sequence position starting from 0.
	unsigned int m_position;

  public:
	//! A default constructor for using the Graph library.
	Site() {}
	//! A constructor that initialize the variables.
	Site(unsigned int seq, unsigned int pos): m_sequence(seq), m_position(pos) {}
	/*!	\return The sequence ID in MultiSequence of the site. */
	unsigned int sequence() const { return m_sequence; }
	/*!	\return The position of the site in the sequence. */
	unsigned int position() const { return m_position; }
	/*!
		The comparison (equal) operator between two sites.
		\return True if the sequence ID and the position of both sites are equal, False otherwise.
	*/
	bool operator==(const Site& s) const
	{ return (sequence() == s.sequence() && position() == s.position()); }
	/*!
		The comparison (not equal) operator between two sites.
		\return The negation of the equal operator.
	*/
	bool operator!=(const Site& s) const { return !(*this == s); }
	/*!
		The order operator for sites.
		\return True if this Site sequence is less than the argument site's sequence or, if they are 
equal, True if this Site position is less than the argument site's position. False otherwise.
	*/
	bool operator<(const Site& s) const
	{ 
		if(sequence() < s.sequence()) return true;
		else if(sequence() == s.sequence()) return (position() < s.position());
		else return false;
	}
	bool operator<=(const Site& s) const
	{ 
		return (operator<(s) || operator==(s));
	}
	//!	Definition of the output (cout) operator for a Site.
	friend std::ostream& operator<<(std::ostream& aStream, const Site& s)
	{
		aStream << "(" << s.sequence() + 1 << "," << s.position() + 1 << ")";
		return aStream;
	}
};

#endif
