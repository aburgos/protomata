
#ifndef SUPPORT_H
#define SUPPORT_H

#include "SegmentSeedGenerator.h"

/*!
Get the support of each segment present in a PairwiseLocalMatchingSet. The support of a segment is given by the total of sequences that contains at least one segment adjacent to the former.
*/
template <typename InM>
class Support: public SegmentSeedGenerator
{
  private:
	typedef typename PairwiseLocalMatchingSet<InM>::segment_iterator segment_it;
	typedef typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator adjacent_it;
	/*!
		Computes the support of the segment iterating through its adjacents and counting how many sequences have a segment adjacent to it.
		\return The total number of sequences that contains at least one adjacent segment to the one passed as parameter.
	*/
	unsigned int getSupport(segment_it);

  public:
	//! The constructor computes the support for each segment and adds it to the seeds map.
	Support(const MultiSequence&, const PairwiseLocalMatchingSet<InM>&);
};

template <typename InM>
Support<InM>::Support(const MultiSequence& ms, const PairwiseLocalMatchingSet<InM>& plms): SegmentSeedGenerator(ms)
{
	for(segment_it it = plms.segment_begin(); it != plms.segment_end(); it++)
	{
		const unsigned int support = getSupport(it);

		// create segment set
		SegmentSet segSet; segSet.insert(*it);

		// add to seeds
		seeds.insert(std::pair<double, SegmentSet>((double)support, segSet));
	}

	//cout << "\033[1;36m" << "Done!" << "\033[m" << endl;
	//cout << endl << "\033[1;32m" << seeds.size() << " seeds obtained." << "\033[m" << endl << endl;
}

template <typename InM>
unsigned int Support<InM>::getSupport(segment_it it)
{
	unsigned int support = 0;

	std::vector<bool> seqSupporter(ms.size(), false);

	// for all the adjacent segments of this segment
	for(adjacent_it ait = it.adjacent_begin(); ait != it.adjacent_end(); ait++)
	{
		const Segment& neighbour = *ait;
		const unsigned int sequence = neighbour.sequence();
		if(!seqSupporter[sequence])
		{
			seqSupporter[sequence] = true;
			support++;
		}
		if(support == ms.size()) return support;
	}

	return support;
}

#endif
