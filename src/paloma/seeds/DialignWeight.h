
#ifndef DIALIGNW_H
#define DIALIGNW_H

#include "SegmentSeedGenerator.h"
#include "matching_sets/PairwiseLocalMatchingSet.h"

/*!
Get the weight of each segment present in a PairwiseLocalMatchingSet. For computing the weight, it adds all the weights of adjacents segments, but considering only one segment for each sequence, the one with the highest weight.
*/
template <typename InM>
class DialignWeight: public SegmentSeedGenerator
{
  private:
	typedef typename PairwiseLocalMatchingSet<InM>::segment_iterator segment_it;
	typedef typename PairwiseLocalMatchingSet<InM>::segment_iterator::adjacent_iterator adjacent_it;
	/*!
		Computes the weight of the segment iterating through its adjacents and summing only the weight with one segment per sequence, the one with the highest value.
		\return The sum of weights.
	*/
	double getWeight(const segment_it&) const;

  public:
	//! The constructor computes the weight for each segment and adds it to the seeds map.
	DialignWeight(const MultiSequence&, const PairwiseLocalMatchingSet<InM>&);
};

template <typename InM>
DialignWeight<InM>::DialignWeight(const MultiSequence& ms, const PairwiseLocalMatchingSet<InM>& plms): SegmentSeedGenerator(ms)
{
	for(segment_it it = plms.segment_begin(); it != plms.segment_end(); it++)
	{
		const double weight = getWeight(it);

		// create segment set
		SegmentSet segSet; segSet.insert(*it);

		// add to seeds
		seeds.insert(std::pair<double, SegmentSet>(weight, segSet));
	}
}

template <typename InM>
double DialignWeight<InM>::getWeight(const segment_it& it) const
{
	double weight = 0;

	std::vector<bool> seqSupporter(ms.size(), false);
	std::vector<double> wgtSupporter(ms.size(), 0.0);

	// for all the adjacent segments of this segment
	for(adjacent_it ait = it.adjacent_begin(); ait != it.adjacent_end(); ait++)
	{
		const Segment& neighbour = *ait;
		const unsigned int sequence = neighbour.sequence();
		const double wgt = (!ait)->segmentsWeight();

		if(seqSupporter[sequence])
		{
			if(wgt > wgtSupporter[sequence])
				wgtSupporter[sequence] = wgt;
		}
		else
		{
			seqSupporter[sequence] = true;
			wgtSupporter[sequence] = wgt;
		}
	}

	for(unsigned int i = 0; i < ms.size(); i++)
		weight += wgtSupporter[i];

	return weight;
}

#endif
