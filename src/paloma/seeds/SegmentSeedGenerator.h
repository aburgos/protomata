
#ifndef SEGSEEDGEN_H
#define SEGSEEDGEN_H

#include <map>
#include "Typedefs.h"
#include "MultiSequence.h"

/*!
Obtains a set of segments somehow.
*/
class SegmentSeedGenerator
{
  protected:
	//! The set of sequences.
	const MultiSequence& ms;
	//! Container of seeds, a mapping from double to a set of segments.
	std::multimap<double, SegmentSet> seeds;

  public:
	//! The constructor.
	SegmentSeedGenerator(const MultiSequence& _ms): ms(_ms) {}
	//! The destructor.
	virtual ~SegmentSeedGenerator() {}
	/*! \return The number of seeds left. */
	unsigned int seeds_left() const { return seeds.size(); }
	/*!
		Gets the next seed and deletes it from the seeds multimap. In case of tie (more than one seed with the same weight), the one with the lowest position in its first segment is chosen. In case of tie the one with the lowest length is chosen.
		\return A pair with a bool value and a set of segments. The bool value is True if there is still at least one seed. False otherwise.
	*/
	std::pair<bool, SegmentSet> getNextSegmentSet()
	{
		SegmentSet ret;
		if(seeds.empty()) return std::pair<bool, SegmentSet>(false, ret);

		typedef std::multimap<double, SegmentSet> SeedsMultiMap;
		std::multimap<double, SegmentSet>::reverse_iterator rit;
		rit = seeds.rbegin();
		double weight = (*rit).first;
		ret = (*rit).second;

		SeedsMultiMap::iterator it_to_delete = --rit.base();

		// untie if there is more than one seed with the same value
		if(seeds.count(weight) > 1)
		{
			std::pair<SeedsMultiMap::iterator, SeedsMultiMap::iterator> ip;
			ip = seeds.equal_range(weight);

			// to avoid comparing once again to the starting seed
			SeedsMultiMap::iterator ibeflast = ip.second;
			--ibeflast;

			// choose one according to position and length
			for(SeedsMultiMap::iterator it = ip.first; it != ibeflast; ++it)
			{
				const SegmentSet& segments = (*it).second;
				const Segment& segment = *segments.begin();
				const Segment& ret_segment = *ret.begin();

				// the shortest segments are favoured
				if(segment.length() < ret_segment.length())
				{
					ret = (*it).second;
					it_to_delete = it;
				}
				else
				{
					// in case of equal length, untie by begin value
					if(segment.length() == ret_segment.length())
					{
						if(segment.begin() < ret_segment.begin())
						{
							ret = (*it).second;
							it_to_delete = it;
						}
						else
						{
							// in case of equal begin, untie by smaller sequence id
							if(segment.begin() == ret_segment.begin())
							{
								if(segment.sequence() < ret_segment.sequence())
								{
									ret = (*it).second;
									it_to_delete = it;
								}
							}
						}
					}
				}
			}
		}

		// delete chosen seed
		seeds.erase(it_to_delete);

		return std::pair<bool, SegmentSet>(true, ret);
	}
};

#endif
