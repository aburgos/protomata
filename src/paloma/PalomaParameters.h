
#ifndef PALOMAPARAMETERS_H
#define PALOMAPARAMETERS_H

#include "EnumTypes.h"
#include "Parameters.h"
#include "PalomaAllowedParameters.h"

class PalomaParameters: public virtual Parameters
{
  protected:
	PalomaAllowedParameters allowed;

	TCLAP::ValueArg<std::string> m_paloma_input;
	TCLAP::ValueArg<std::string> m_paloma_output;
	TCLAP::ValueArg<std::string> m_paloma_tag;
	TCLAP::ValueArg<std::string> m_afc_file;

	TCLAP::ValueArg<unsigned int> m_quorum_plma;
	TCLAP::ValueArg<double> m_threshold;
	TCLAP::ValueArg<unsigned int> m_min_size;
	TCLAP::ValueArg<unsigned int> m_max_size;
	TCLAP::ValueArg<unsigned int> m_test_seeds;

	TCLAP::SwitchArg m_dialign;
	TCLAP::SwitchArg m_smithwaterman;
	TCLAP::SwitchArg m_deactivate_global_opt;
	TCLAP::SwitchArg m_weak_consensus;
	TCLAP::SwitchArg m_strong_consensus;
	TCLAP::SwitchArg m_no_repeats;
	TCLAP::SwitchArg m_local_consistency;
	TCLAP::SwitchArg m_global_consistency;
	TCLAP::SwitchArg m_verbose;

	TCLAP::ValueArg<std::string> m_bm;
	TCLAP::ValueArg<std::string> m_bg;
	TCLAP::ValueArg<std::string> m_sg;
	TCLAP::ValueArg<std::string> m_gs;
	TCLAP::ValueArg<std::string> m_st;
	TCLAP::ValueArg<std::string> m_ot;
	TCLAP::ValueArg<std::string> m_ds;
	TCLAP::ValueArg<std::string> m_df;
	//TCLAP::ValueArg<std::string> m_ms;

	ReaderType m_reader;
	CandGenType m_block_mode;
	BlockType m_block_generator;
	SeedType m_seed_generator;
	GeneralizedSetType m_generalized_set;
	ScoreType m_score_type;
	OutputType m_output_type;
	DiscSegOpt m_discard_segments;
	DiscFragOpt m_discard_fragments;
	MatchingSetType m_matching_set;

	bool m_run_optimized_code;

	virtual void sanity_checks();
	virtual void add_string_parameters();
	virtual std::string compute_output_file(const std::string&) const;
	virtual void initialize_values();

  public:
	PalomaParameters();
	virtual ~PalomaParameters() {}

	virtual void parse(int argc, char **argv);
	virtual void add_arguments(TCLAP::CmdLine&);

	virtual std::string input() const { return m_paloma_input.getValue(); }
	virtual std::string output() const;
	virtual std::string tag() const { return m_paloma_tag.getValue(); }
	virtual std::string afc_file() const;

	unsigned int plma_quorum() const { return m_quorum_plma.getValue(); }
	double threshold() const { return m_threshold.getValue(); }
	unsigned int min_size() const { return m_min_size.getValue(); }
	unsigned int max_size() const { return m_max_size.getValue(); }
	unsigned int test_seeds() const { return m_test_seeds.getValue(); }

	bool run_optimized_code() const { return m_run_optimized_code; }
	bool weak_consensus() const { if(!strong_consensus()) return m_weak_consensus.getValue(); else return false; }
	bool strong_consensus() const { return m_strong_consensus.getValue(); }
	bool no_repeats() const { return m_no_repeats.getValue(); }
	bool local_consistency() const { return m_local_consistency.getValue(); }
	bool global_consistency() const { return m_global_consistency.getValue(); }
	bool verbose() const { return m_verbose.getValue(); }

	MatchingSetType matching_set() const { return m_matching_set; }
	ReaderType reader() const { return m_reader; }
	BlockType block_generator() const { return m_block_generator; }
	CandGenType block_mode() const { return m_block_mode; }
	SeedType seed_generator() const { return m_seed_generator; }
	GeneralizedSetType generalized_set() const { return m_generalized_set; }
	ScoreType score_type() const { return m_score_type; }
	OutputType output_type() const { return m_output_type; }
	DiscSegOpt discard_segments() const { return m_discard_segments; }
	DiscFragOpt discard_fragments() const { return m_discard_fragments; }
};

#endif
