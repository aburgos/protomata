
#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <iostream>
#include <string>

/*!
A set of characters with a tag and an ID.
*/
class Sequence
{
  private:
	std::string m_tag;
	std::string m_long_tag;
	std::string m_data;
	unsigned int m_id;
	double m_weight;

  public:
	/*!
		The constructor creates a copy of the parameters passed and stores them.
		\param t is the truncated tag of the Sequence.
		\param d is the data of the Sequence.
		\param i is the ID of the Sequence.
		\param w is the weight of the Sequence.
	*/
	Sequence(const std::string& t, const std::string& d, unsigned int i, double w): m_tag(t), m_long_tag(t), m_data(d), m_id(i), m_weight(w) {}
	/*!
		The constructor creates a copy of the parameters passed and stores them, assigning weight 1 to each Sequence.
		\param t is the truncated tag of the Sequence.
		\param d is the data of the Sequence.
		\param i is the ID of the Sequence.
	*/
	Sequence(const std::string& t, const std::string& d, unsigned int i): m_tag(t), m_long_tag(t), m_data(d), m_id(i), m_weight(1) {}
	/*!
		The constructor creates a copy of the parameters passed and stores them.
		\param t is the truncated tag of the Sequence.
		\param c is the complete tag of the Sequence.
		\param d is the data of the Sequence.
		\param i is the ID of the Sequence.
		\param w is the weight of the Sequence.
	*/
	Sequence(const std::string& t, const std::string& c, const std::string& d, unsigned int i, double w): m_tag(t), m_long_tag(c), m_data(d), m_id(i), m_weight(w) {}
	/*!
		The constructor creates a copy of the parameters passed and stores them, assigning weight 1 to each Sequence.
		\param t is the truncated tag of the Sequence.
		\param c is the complete tag of the Sequence.
		\param d is the data of the Sequence.
		\param i is the ID of the Sequence.
	*/
	Sequence(const std::string& t, const std::string& c, const std::string& d, unsigned int i): m_tag(t), m_long_tag(c), m_data(d), m_id(i), m_weight(1) {}
	/*!
		The comparison (equal) operator between two sequences.
		\return True if the sequence IDs are equal, False otherwise.
	*/
	bool operator==(const Sequence& s) const { return (tag() == s.tag() && data() == s.data()); }
	/*!
		The comparison (not equal) operator between two sequences.
		\return The negation of the equal operator.
	*/
	bool operator!=(const Sequence& s) const { return !(*this == s); }
	/*!
		The order operator for sequences.
		\return True if both IDs are equal. False otherwise.
	*/
	bool operator<(const Sequence& s) const { return (tag() < s.tag()); }
	/*! \return The i-th position. This operation does not checked the index to be valid. */
	const char& operator[](unsigned int pos) const { return m_data[pos]; }
	/*!	\return The ID of the Sequence.	*/
	unsigned int id() const { return m_id; }
	/*!	\return A string containing the truncated tag of the Sequence. */
	std::string tag() const { return m_tag; }
	/*!	\return A string containing the complete tag of the Sequence. */
	std::string long_tag() const { return m_long_tag; }
	/*!	\return A string containing the data of the Sequence. */
	std::string data() const { return m_data; }
	/*!	\return The length of the Sequence.	*/
	unsigned int length() const { return m_data.length(); }
	/*! \return The weight of the Sequence. */
	double weight() const { return m_weight; }
	//! Alias for an iterator on the sequence.
	typedef std::string::const_iterator const_iterator;
	//! Begin iterator for the sequence data.
	const_iterator begin() const { return m_data.begin(); }
	//! End iterator for the sequence data.
	const_iterator end() const { return m_data.end(); }
	//!	Outputs tag and data of the Sequence in FASTA format.
	friend std::ostream& operator<<(std::ostream& aStream, const Sequence& s)
	{
		aStream << ">" << s.tag() << std::endl;
		aStream << s.data() << std::endl;
		return aStream;
	}
};

#endif

