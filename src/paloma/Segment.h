
#ifndef SEGMENT_H
#define SEGMENT_H

#include "Site.h"
#include "Sequence.h"

/*!
A Segment is a set of consecutive sites.
*/
class Segment
{
  private:
	//! The sequence ID in MultiSequence it belongs to.
	unsigned int m_sequence;
	//! The begin position of the Segment. The first is 0.
	unsigned int m_begin;
	//! The length of the Segment.
	unsigned int m_length;

  public:
	//! A default constructor for using the Graph library.
	Segment() {}
	//!	A constructor that initialized the object.
	Segment(unsigned int seq, unsigned int beg, unsigned int len): m_sequence(seq), m_begin(beg), m_length(len) {}
	/*!	\return The sequence ID in MultiSequence it belongs to. */
	unsigned int sequence() const { return m_sequence; }
	/*!	\return The beginning of the Segment. */
	unsigned int begin() const { return m_begin; }
	/*!	\return The length of the Segment. */
	unsigned int length() const { return m_length; }
	/*!	\return The end of the Segment, i.e. the last position that belongs to it. */
	unsigned int end() const { return (m_begin + m_length - 1); }
	/*!
		The comparison (equal) operator between two segments.
		\return True if the sequence ID, the beginning and the length of both segments are equal, 	False otherwise.
	*/
	bool operator==(const Segment&) const;
	/*!
		The comparison (not equal) operator between two segments.
		\return The negation of the equal operator.
	*/
	bool operator!=(const Segment& s) const { return !(*this == s); }
	/*!
		The order operator for segments.
		\return A Segment is prior to another if its sequence is less than the other's. In case of 	tie, if the beginnign is less than the other's. In case of tie, if the length is less than the other's. Otherwise if False.
	*/
	bool operator<(const Segment&) const;
	bool operator<=(const Segment&) const;
	/*!
		Tells whether this segment includes this site or not.
		\return True if the site belongs to the segment. False otherwise.
	*/
	bool includes(const Site&) const;
	/*!
		Tells whether this segment includes the parameter segment or not.
		\return True if the parameter segment is includedin this segment. False otherwise.
	*/
	bool includes(const Segment&) const;
	/*!
		Tells whether this segment overlapps the parameter segment or not.
		\return True if both segments share at least one site. False otherwise.
	*/
	bool overlapps(const Segment&) const;
	//!	Definition of the output (cout) operator for a Segment
	friend std::ostream& operator<<(std::ostream& aStream, const Segment& s)
	{
		aStream << "(" << s.sequence() + 1 << ", " << s.begin() + 1 << ", " << s.length() << ")";
		return aStream;
	}
};

inline bool Segment::operator==(const Segment& s) const
{
	if(begin() != s.begin()) return false;
	else if(length() != s.length()) return false;
	else if(sequence() != s.sequence()) return false;
	else return true;
}

inline bool Segment::operator<(const Segment& s) const
{
	if(sequence() < s.sequence()) return true;
	else
	{
		if(sequence() == s.sequence())
		{
			if(begin() < s.begin()) return true;
			else
			{
				if(begin() == s.begin())
					if(length() < s.length()) return true;
			}
		}
	}

	return false;
}

inline bool Segment::operator<=(const Segment& s) const
{
	return (operator<(s) || operator==(s));
}

inline bool Segment::includes(const Site& s) const
{
	return (sequence() == s.sequence() && begin() <= s.position() && end() >= s.position());
}

inline bool Segment::includes(const Segment& s) const
{
	return (sequence() == s.sequence() && begin() <= s.begin() && end() >= s.end());
}

inline bool Segment::overlapps(const Segment& s) const
{
	if(sequence() != s.sequence()) return false;
	bool cmp1 = (begin() <= s.begin() && s.begin() <= end());
	bool cmp2 = (s.begin() <= begin() && begin() <= s.end());
	return (cmp1 || cmp2);
}

#endif
