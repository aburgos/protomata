
#include "DialignReader.h"

DialignReader::DialignReader(const MultiSequence& ms, double threshold, unsigned int min_size, unsigned int max_size, const std::string& afc_file)
{
	m_threshold = threshold;
	m_min_size = min_size;
	m_max_size = max_size;
	twoSequences = (ms.size() == 2);

	// check if dialign's maximum fragment length size is passed
	if(m_max_size > 40)
	{
		std::cerr << "Warning! Dialign computes only fragments of length size up to 40. ";
		std::cerr << "This value will be used instead." << std::endl;
		m_max_size = 40;
	}

	// check if dialign's minimum fragment size if bigger or equal than maximum size
	if(m_min_size >= m_max_size)
	{
		std::cerr << "Warning! Minimum fragment's size is bigger or equal to maximum. ";
		std::cerr << "Using 1 for min-size." << std::endl;
		m_min_size = 1;
	}

	// check for afc file existance
	file_handler.open(afc_file.c_str());
	if(!file_handler.good())
	{
		// create a temporary fasta filename
		char tmp_fasta[] = "/tmp/tmpfileXXXXXX";
		if(mkstemp(tmp_fasta) == -1)
			{ std::cerr << "Could not create temporary file for dialign execution." << std:: endl; exit(1); }

		// write current sequences to temporary file
		std::ofstream fasta_handler;
		fasta_handler.open(tmp_fasta);
		fasta_handler << ms;
		fasta_handler.close();

		// can't check dialign exit code since it seems to be random
		execDialign(tmp_fasta);

		// rename the result file for an output filename with its parameters
		std::string cmd = "mv " + std::string(tmp_fasta) + ".afc" + " " + afc_file;
		if(system(cmd.c_str()))
		{
			std::cout << "Error executing: " << cmd << std::endl;
			exit(1);
		}

		file_handler.open(afc_file.c_str(), std::ifstream::in);
		if(!file_handler.good())
		{
			std::cerr << "File " << afc_file << " could not be opened." << std::endl;
			exit(1);
		}
	}
}

DialignReader::~DialignReader()
{
	file_handler.close();
}

int DialignReader::execDialign(const std::string& file)
{
	std::ostringstream s_thr;
	s_thr << m_threshold;
	std::ostringstream s_max;
	s_max << m_max_size;
	std::string cmd = "dialign -nta -thr " + s_thr.str() + " -lmax " + s_max.str() + " -afc " + file;
	return system(cmd.c_str());
}

const DialignPLA_Imp* DialignReader::nextInput()
{
	unsigned int seq[2];
	unsigned int begin[2];
	unsigned int length = 0;
	double weight = 0;
	unsigned int it;

	do
	{
		if(file_handler.eof()) return NULL;

		std::string line;
		getline(file_handler, line);
		if(line.substr(0, 3) != "FRG") continue;

		std::istringstream iss(line, std::ios_base::in);

		iss.ignore(1024, ':');

		if(twoSequences)
		{
			seq[0] = 1;
			seq[1] = 2;
		}
		else
		{
			iss.ignore(1024, ':');
			iss >> seq[0] >> seq[1];
		}

		iss.ignore(1024, ':');
		iss >> begin[0] >> begin[1];

		iss.ignore(1024, ':');
		iss >> length;

		iss.ignore(1024, ':');
		iss >> weight;

		iss.ignore(1024, '=');
		iss >> it;

	} while(weight < m_threshold || length < m_min_size || length > m_max_size || it > 1);

	Segment s1(seq[0] - 1, begin[0] - 1, length);
	Segment s2(seq[1] - 1, begin[1] - 1, length);
	SegmentPairRelation r(std::make_pair(s1, s2), weight);

	const DialignPLA_Imp* dpla = new DialignPLA_Imp(r);
	toDestroy.push_back(dpla);

	return dpla;
}
