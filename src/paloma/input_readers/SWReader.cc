
#include "SWReader.h"

SWReader::SWReader(const MultiSequence& ms, double threshold, unsigned int min_size, unsigned int max_size, ScoreType score_type)
{
	typedef seqan::String<seqan::AminoAcid> TSequence;
	typedef seqan::StringSet<TSequence, seqan::Dependent<> > TStringSet;
	typedef seqan::String<seqan::Fragment<> > TFragmentString;
	typedef seqan::String<int> TScoreValues;
	typedef seqan::Id<TStringSet>::Type TId;
	typedef seqan::Size<TStringSet>::Type TSize;

	unsigned int numSeqs = ms.size();
	for(unsigned int seq_i = 0; seq_i < numSeqs; seq_i++)
		for(unsigned int seq_j = seq_i + 1; seq_j < numSeqs; seq_j++)
		{
			//cout << "seqs " << seq_i << "-" << seq_j << endl;
			TSequence seq1 = ms[seq_i].data();
			TSequence seq2 = ms[seq_j].data();

			TStringSet string_set;
			seqan::appendValue(string_set, seq1);
			seqan::appendValue(string_set, seq2);

			TScoreValues scores;
			TFragmentString matches;

			if(score_type == DNA)
			{
				// here the score corresponds to (match, mismatch, gap, gap_open)
				static seqan::Score<int> score(2, -1, -1, -2);
				seqan::multiLocalAlignment(string_set, matches, scores, score, 10000, seqan::SmithWatermanClump());
			}
			else if(score_type == Blosum30)
			{
				static seqan::Blosum30 score;
				seqan::multiLocalAlignment(string_set, matches, scores, score, 10000, seqan::SmithWatermanClump());
			}
			else if(score_type == Blosum62)
			{
				static seqan::Blosum62 score;
				seqan::multiLocalAlignment(string_set, matches, scores, score, 10000, seqan::SmithWatermanClump());
			}
			else if(score_type == Blosum80)
			{
				static seqan::Blosum80 score;
				seqan::multiLocalAlignment(string_set, matches, scores, score, 10000, seqan::SmithWatermanClump());
			}

			//seqan::_debugMatches(string_set, matches);
			//cout << "Found " << length(scores) << " scores" << endl;
			//cout << "Found " << length(matches) << " matches" << endl;
			unsigned int j;

			for(TSize i = 0; i < length(matches); i += j)
			{
				unsigned int firstMatch = i;
				//cout << "Score for fragment " << i + 1 << ": " << scores[i] << endl;

				TId tmp_id1 = sequenceId(matches[i], 0);
				TId tmp_id2 = sequenceId(matches[i], 1);

				unsigned int segmentBegin[2], segmentLength[2], segmentEnd[2]; j = i + 1;
				segmentBegin[0] = fragmentBegin(matches[firstMatch], tmp_id1);
				segmentBegin[1] = fragmentBegin(matches[firstMatch], tmp_id2);
				segmentLength[0] = fragmentLength(matches[firstMatch], tmp_id1);
				segmentLength[1] = fragmentLength(matches[firstMatch], tmp_id2);
				segmentEnd[0] = segmentBegin[0] + segmentLength[0] - 1;
				segmentEnd[1] = segmentBegin[1] + segmentLength[1] - 1;
/*
	Given a segment (b, b + l - 1), a fragment (bi, bi + li - 1) will be added to the resulting
	segment (b', b' + l' - 1) in this way:

	b' = min{b, bi}
	l' = if(bi < b) (li + (b - (bi + li)) + l = b + l - bi)
		 else 		(l + (bi - (b + l)) + li = bi + li - b)
*/

				// loop first to get the segment's length
				while((j < length(matches)) && (scores[firstMatch] == scores[j]))
				{
					if(fragmentBegin(matches[j], tmp_id1) < segmentBegin[0])
					{
						segmentLength[0] = segmentBegin[0] + segmentLength[0] - fragmentBegin(matches[j], tmp_id1);
						segmentBegin[0] = fragmentBegin(matches[j], tmp_id1);
					}
					else
					{
						segmentLength[0] = fragmentBegin(matches[j], tmp_id1) + fragmentLength(matches[j], tmp_id1) - segmentBegin[0];
					}

					if(fragmentBegin(matches[j], tmp_id2) < segmentBegin[1])
					{
						segmentLength[1] = segmentBegin[1] + segmentLength[1] - fragmentBegin(matches[j], tmp_id2);
						segmentBegin[1] = fragmentBegin(matches[j], tmp_id2);
					}
					else
					{
						segmentLength[1] = fragmentBegin(matches[j], tmp_id2) + fragmentLength(matches[j], tmp_id2) - segmentBegin[1];
					}

					j++;

				}

				//if(segmentLength[0] != segmentLength[1])
				//cout << "(" << seq_i + 1 << ", " << segmentBegin[0] + 1 << ", " << segmentLength[0] << ") -- [" << scores[i] << "] --> (" << seq_j + 1 << ", " << segmentBegin[1] + 1 << ", " << segmentLength[1] << ")" << endl;

				// create the segments to add to the local alignment
				Segment ds1 (seq_i, segmentBegin[0], segmentLength[0]);
				Segment ds2 (seq_j, segmentBegin[1], segmentLength[1]);

				if(scores[firstMatch] < threshold) continue;
				if(ds1.length() < min_size || ds1.length() > max_size) continue;
				if(ds2.length() < min_size || ds2.length() > max_size) continue;

				PairwiseLocalAlignmentImp* pla = new PairwiseLocalAlignmentImp(ms, seq_i, seq_j, scores[firstMatch]);

				pla->addSegment(ds1);
				pla->addSegment(ds2);

				j = i;

				while((j < length(matches)) && (scores[firstMatch] == scores[j]))
				{
					tmp_id1 = sequenceId(matches[j], 0);
					tmp_id2 = sequenceId(matches[j], 1);

					unsigned int begin[2], length[2];
					begin[0] = fragmentBegin(matches[j], tmp_id1);
					begin[1] = fragmentBegin(matches[j], tmp_id2);
					length[0] = fragmentLength(matches[j], tmp_id1);
					length[1] = fragmentLength(matches[j], tmp_id2);

					for(TSize t = begin[0], k = begin[1]; t < begin[0] + length[0] && k < begin[1] + length[1]; ++t, ++k)
					{
						double weight = 0;
						Site s1 (seq_i, t); Site s2 (seq_j, k);
						//cout << s1 << " -- " << s2 << endl;
						SitePairRelation r (std::make_pair(s1, s2), weight);
						pla->addMatching(r);
					}

					j++;
				}

				plas.push_back(pla);
				//cout << plas.size() << " plas added" << endl;
			}
		}
}

const PairwiseLocalAlignmentImp* SWReader::nextInput()
{
	if(plas.empty()) return NULL;

	const PairwiseLocalAlignmentImp* pla = plas.back();
	plas.pop_back();
	toDestroy.push_back(pla);

	return pla;
}
