
#ifndef PLAREADER_H
#define PLAREADER_H

#include "InputReader.h"

/*!
Base class for readers of PairwiseLocalAlignments.
*/
template <typename InM>
class PLAReader: public InputReader<InM>
{
  public:
	//! The constructor.
	PLAReader() {}
	//! The destructor.
	virtual ~PLAReader() {}
};

#endif
