
#ifndef DIALIGNREADER_H
#define DIALIGNREADER_H

#include <cstdlib>
#include <fstream>
#include <sstream>
#include "PLAReader.h"
#include "alignments/DialignPLA_Imp.h"
#include "MultiSequence.h"

/*!
Reads the fragments from an afc file obtained from dialign. It doesn't work when the file was obtained from the comparisson between only two sequences, since the format is different.
*/
class DialignReader: public PLAReader<DialignPLA_Imp>
{
  private:
	//! An input file stream.
	std::ifstream file_handler;
	//! The minimum weight for a pairwise local matching.
	double m_threshold;
	//! The minimum size for a pairwise local matching.
	unsigned int m_min_size;
	//! The maximum size for a pairwise local matching.
	unsigned int m_max_size;
	//! True if the sequences in MultiSequence are only two.
	bool twoSequences;
	//! Executes dialign.
	int execDialign(const std::string& file);

  public:
	DialignReader(const MultiSequence&, double, unsigned int, unsigned int, const std::string&);
	~DialignReader();
	/*!
		This method reads the next line in the afc file, i.e. the next fragment.
		\return A pointer to an DialignPLA object with the data collected from the afc file, or NULL if there
		is no more data.
	*/
	const DialignPLA_Imp* nextInput();
};

#endif
