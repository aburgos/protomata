
#ifndef SWREADER_H
#define SWREADER_H

#include <vector>
#include <seqan/align.h>
#include <seqan/graph_align.h>
#include <seqan/graph_msa.h>

#include "EnumTypes.h"
#include "PLAReader.h"
#include "alignments/PairwiseLocalAlignment_PairUFImp.h"

//! The implementation to be used for a PairwiseLocalAlignment.
typedef PairwiseLocalAlignment_PairUFImp PairwiseLocalAlignmentImp;

/*!
Computes as many PairwiseLocalAlignment as it can from a set of sequences, taken in groups of two.
*/
class SWReader: public PLAReader<PairwiseLocalAlignmentImp>
{
  private:
	//! A vector container for the PairwiseLocalAlignment pointers.
	std::vector<PairwiseLocalAlignmentImp*> plas;

  public:
	/*!
		The constructor. Executes seqan::multiLocalAlignment with each sequence against every other sequence. The maximum number of pairwise local alignments that the seqan function will return is hardcoded in this method, being 1000. If more is needed, this parameter should be changed. Every pairwise local alignment generated is stored in the vector of pointers to plas.
		\param ms is a MultiSequence object.
		\param params are all paloma2 defined parameters.
	*/
	SWReader(const MultiSequence&, double, unsigned int, unsigned int, ScoreType);
	~SWReader() {}
	/*!
		Removes from the vector plas the first generated PairwiseLocalAlignment and returns it.
		\return A pointer to a new PairwiseLocalAlignment object.
	*/
	const PairwiseLocalAlignmentImp* nextInput();
};

#endif
