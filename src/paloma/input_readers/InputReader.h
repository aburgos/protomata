
#ifndef INPUTREADER_H
#define INPUTREADER_H

#include "matchings/Matching.h"

/*!
Pure abstract class to get the input matchings.
*/
template <typename InM>
class InputReader
{
  protected:
	//! Stores the created matchings in order to delete them after.
	std::vector<const InM*> toDestroy;

  public:
	//! The constructor.
	InputReader() {}
	//! The destructor deletes all created objects.
	virtual ~InputReader();
	/*!
		Abstract method to get the next input.
		\return A pointer to a Matching object or derived Matching object.
	*/
	virtual const InM* nextInput() = 0;
};

template <typename InM>
InputReader<InM>::~InputReader()
{
	typename std::vector<const InM*>::iterator it;
	for(it = toDestroy.begin(); it != toDestroy.end(); it++)
		delete *it;
}

#endif
