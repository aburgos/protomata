
#include "PalomaParameters.h"

PalomaParameters::PalomaParameters():
	m_paloma_input(TCLAP::ValueArg<std::string>("i", "input", "Set fasta file name.", true, "", "fasta_filename")),
	m_paloma_output(TCLAP::ValueArg<std::string>("o", "output", "Set output file name.", false, "fasta_basename.plma", "plma_filename")),
	m_paloma_tag(TCLAP::ValueArg<std::string>("", "tag", "Add a tag to the end of the basename.", false, "", "string")),
	m_afc_file(TCLAP::ValueArg<std::string>("", "use-afc", "Use an existing dialign .afc file.", false, "", "afc_filename")),
	m_quorum_plma(TCLAP::ValueArg<unsigned int>("Q", "quorum-plma", "The minimum number of 'segments' in blocks.", false, 2, "value")),
	m_threshold(TCLAP::ValueArg<double>("t", "threshold", "Use only input fragments with weight bigger than <value>.", false, 5, "value")),
	m_min_size(TCLAP::ValueArg<unsigned int>("m", "min-size", "Use only input fragments with size bigger or equal to the value specified.", false, 1, "value")),
	m_max_size(TCLAP::ValueArg<unsigned int>("M", "max-size", "Use only input fragments with size smaller or equal to the value specified.", false, 15, "value")),
	m_test_seeds(TCLAP::ValueArg<unsigned int>("", "test-seeds", "Compute blocks from the first <value> seeds (advanced).", false, 0, "value")),
	m_dialign(TCLAP::SwitchArg("d", "dialign", "Use dialign for generating PLA fragments.", true)),
	m_smithwaterman(TCLAP::SwitchArg("", "smith-waterman", "Use Smith & Waterman algorithm to generate PLA with insertions and deletions (experimental).", false)),
	m_deactivate_global_opt(TCLAP::SwitchArg("", "deactivate-global-opt", "Do not optimize if a global consistent alignment will be computed (experimental).", false)),
	m_weak_consensus(TCLAP::SwitchArg("c", "weak-consensus", "Short option for connCompGreedy.", true)),
	m_strong_consensus(TCLAP::SwitchArg("C", "strong-consensus", "Short option for cliqueGreedy.", false)),
	m_no_repeats(TCLAP::SwitchArg("", "no-repeats", "Do not allow repeats in the resulting alignment (experimental).", false)),
	m_local_consistency(TCLAP::SwitchArg("l", "local-consistency", "The result will be a local consistent alignment (experimental).", false)),
	m_global_consistency(TCLAP::SwitchArg("g", "global-consistency", "The result will be a global consistent alignment.", true)),
	m_verbose(TCLAP::SwitchArg("", "verbose", "Verbose mode.", false)),
	m_bm(TCLAP::ValueArg<std::string>("", "block-mode", "Specifies how blocks will be added to the generalized matching (advanced).", false, "bestConnCompOnly", allowed.block_mode_values_constraints())),
	m_bg(TCLAP::ValueArg<std::string>("", "block-generator", "Specifies how a block will be computed from seeds (advanced).", false, "connCompGreedy", allowed.block_generator_values_constraints())),
	m_sg(TCLAP::ValueArg<std::string>("", "seed-generator", "Specifies which seed generator will be used (advanced).", false, "dialignW", allowed.seed_generator_values_constraints())),
	m_gs(TCLAP::ValueArg<std::string>("", "matching-type", "G:Generalized; M:Matching; A:Alignment; L:Local; GC:Global Consistent; LC:Local Consistent (advanced).", false, "GA-GC", allowed.generalized_set_values_constraints())),
	m_st(TCLAP::ValueArg<std::string>("", "score-type", "Select a score type for smith & waterman (advanced).", false, "Blosum62", allowed.score_type_values_constraints())),
	m_ot(TCLAP::ValueArg<std::string>("", "output-type", "Select the output type (experimental).", false, "PLMA", allowed.output_type_values_constraints())),
	m_ds(TCLAP::ValueArg<std::string>("", "discard-segments", "Discard seeds after adding a matching to a generalized set (advanced).", false, "insideSegments", allowed.discard_segments_values_constraints())),
	m_df(TCLAP::ValueArg<std::string>("", "discard-fragments", "Discard fragments of the pairwise segment graph after adding a matching to a generalized set (advanced).", false, "noFragments", allowed.discard_fragments_values_constraints()))
{}

void PalomaParameters::parse(int argc, char **argv)
{
	TCLAP::CmdLine cmd("Computes Partial Local Multiple Alignment from a set of sequences.", ' ', VERSION);
	add_arguments(cmd);
	//cmd.xorAdd(global_consistency, local_consistency);
	//cmd.xorAdd(dialign, sw);
	cmd.parse(argc, argv);
	initialize_values();
	sanity_checks();
	add_string_parameters();
}

void PalomaParameters::add_arguments(TCLAP::CmdLine& cmd)
{
	// add in reverse appereance order
	cmd.add(m_test_seeds);

	cmd.add(m_deactivate_global_opt);
	cmd.add(m_weak_consensus);
	cmd.add(m_strong_consensus);
	cmd.add(m_no_repeats);
	cmd.add(m_local_consistency);
	cmd.add(m_global_consistency);
	cmd.add(m_verbose);

	cmd.add(m_df);
	cmd.add(m_ds);
	cmd.add(m_ot);
	cmd.add(m_st);
	cmd.add(m_gs);
	cmd.add(m_sg);
	cmd.add(m_bg);
	cmd.add(m_bm);

	cmd.add(m_max_size);
	cmd.add(m_min_size);
	cmd.add(m_threshold);
	cmd.add(m_quorum_plma);

	cmd.add(m_afc_file);

	cmd.add(m_smithwaterman);
	cmd.add(m_dialign);

	cmd.add(m_paloma_tag);
	cmd.add(m_paloma_output);
	cmd.add(m_paloma_input);
}

void PalomaParameters::initialize_values()
{
	if(m_smithwaterman.getValue()) m_reader = SWR;
	else m_reader = DialignR;

	m_matching_set = PLMSet;

	if(m_bm.getValue() == "maxWeightFragments") m_block_mode = maxWeightFragments;
	else if(m_bm.getValue() == "maxSupportFragments") m_block_mode = maxSupportFragments;
	else if(m_bm.getValue() == "bestConnCompOnly") m_block_mode = bestConnCompOnly;
	else if(m_bm.getValue() == "bestCompatibleConnComp") m_block_mode = bestCompatibleConnComp;

	if(m_bg.getValue() == "connCompExhaustive") m_block_generator = connCompExhaustive;
	else if(m_bg.getValue() == "connCompGreedy") m_block_generator = connCompGreedy;
	else if(m_bg.getValue() == "connCompBestWeights") m_block_generator = connCompBestWeights;
	else if(m_bg.getValue() == "cliqueExhaustive") m_block_generator = cliqueExhaustive;
	else if(m_bg.getValue() == "cliqueGreedy") m_block_generator = cliqueGreedy;
	else if(m_bg.getValue() == "cliqueBestWeights") m_block_generator = cliqueBestWeights;

	if(m_sg.getValue() == "dialignW") m_seed_generator = dialignW;
	else if(m_sg.getValue() == "support") m_seed_generator = support;

	if(m_gs.getValue() == "GM") m_generalized_set = GM;
	else if(m_gs.getValue() == "GLM") m_generalized_set = GLM;
	else if(m_gs.getValue() == "GA") m_generalized_set = GA;
	else if(m_gs.getValue() == "GLA") m_generalized_set = GLA;
	else if(m_gs.getValue() == "GA-GC") m_generalized_set = GA_GC;
	else if(m_gs.getValue() == "GLA-LC") m_generalized_set = GLA_LC;

	if(m_st.getValue() == "DNA") m_score_type = DNA;
	else if(m_st.getValue() == "Blosum30") m_score_type = Blosum30;
	else if(m_st.getValue() == "Blosum62") m_score_type = Blosum62;
	else if(m_st.getValue() == "Blosum80") m_score_type = Blosum80;

	if(m_ot.getValue() == "PLMA") m_output_type = PLMAt;
	else if(m_ot.getValue() == "ALN") m_output_type = ALNt;

	if(m_ds.getValue() == "blockSegments") m_discard_segments = blockSegments;
	else if(m_ds.getValue() == "insideSegments") m_discard_segments = insideSegments;
	else if(m_ds.getValue() == "overlappedSegments") m_discard_segments = overlappedSegments;
	else if(m_ds.getValue() == "noSegments") m_discard_segments = noSegments;

	if(m_df.getValue() == "blockFragments") m_discard_fragments = blockFragments;
	else if(m_df.getValue() == "blockAdjFragments") m_discard_fragments = blockAdjFragments;
	else if(m_df.getValue() == "seedsToDiscAdjFragments") m_discard_fragments = seedsToDiscAdjFragments;
	else if(m_df.getValue() == "noFragments") m_discard_fragments = noFragments;

	if(weak_consensus()) m_block_generator = connCompGreedy;
	else if(strong_consensus()) m_block_generator = cliqueGreedy;

	m_run_optimized_code = !m_deactivate_global_opt.getValue();

	if(local_consistency()) { m_generalized_set = GLA_LC; m_run_optimized_code = false; }
	else if(global_consistency()) m_generalized_set = GA_GC;
}

void PalomaParameters::sanity_checks()
{
	if(m_dialign.isSet() && m_smithwaterman.getValue())
	{
		std::cout << "Error: only one pairwise matching generator can be used." << std::endl;
		exit(1);
	}
	if(m_dialign.isSet() && m_st.isSet())
	{
		std::cout << "Warning: --score-type option can be used only with Smith & Waterman input. ";
		std::cout << "It will be ignored." << std::endl;
	}
	if(m_reader != DialignR && m_afc_file.isSet())
	{
		std::cout << "Warning: AFC file will not be use. " << std::endl;
	}
	bool blockIsUseless = (m_block_mode != bestConnCompOnly && m_block_mode != bestCompatibleConnComp);
	if(m_bg.isSet() && blockIsUseless)
	{
		std::cout << "Warning: --block-generator option can be used only with blocks mode ";
		std::cout << "bestConnCompOnly and bestCompatibleConnComp. It will be ignored." << std::endl;
	}
	if(m_quorum_plma.isSet() && blockIsUseless)
	{
		std::cout << "Warning: --quorum-plma option can be used only with blocks mode ";
		std::cout << "bestConnCompOnly and bestCompatibleConnComp. It will be ignored." << std::endl;
	}
	if((m_generalized_set == GM || m_generalized_set == GLM) && m_output_type == PLMAt)
	{
		std::cout << "Error: PLMA output can only be used with generalized alignments." << std::endl;
		exit(1);
	}
	if(m_global_consistency.isSet() && m_local_consistency.isSet())
	{
		std::cout << "Error: flags for global and local consistency are both set." << std::endl;
		exit(1);
	}
	if(m_strong_consensus.isSet() && m_weak_consensus.isSet())
	{
		std::cout << "Error: flags for strong and weak consensus are both set." << std::endl;
		exit(1);
	}
}

void PalomaParameters::add_string_parameters()
{
	// stable parameters
	string_parameters.push_back(std::make_pair("--input", input()));
	string_parameters.push_back(std::make_pair("--output", output()));
	if(!m_afc_file.getValue().empty()) string_parameters.push_back(std::make_pair("use-afc", afc_file()));
	std::ostringstream ss1; ss1 << plma_quorum();
	string_parameters.push_back(std::make_pair("--quorum-plma", ss1.str()));
	std::ostringstream ss2; ss2 << threshold();
	string_parameters.push_back(std::make_pair("--threshold", ss2.str()));
	std::ostringstream ss3; ss3 << min_size();
	string_parameters.push_back(std::make_pair("--min-size", ss3.str()));
	std::ostringstream ss4; ss4 << max_size();
	string_parameters.push_back(std::make_pair("--max-size", ss4.str()));
	if(strong_consensus()) string_parameters.push_back(std::make_pair("--strong-consensus", ""));
	else if(weak_consensus()) string_parameters.push_back(std::make_pair("--weak-consensus", ""));

	// advance parameters
	std::string str;

	switch(m_reader)
	{
		case DialignR: string_parameters.push_back(std::make_pair("--dialign", "")); break;
		case SWR: string_parameters.push_back(std::make_pair("--smith-waterman", "")); break;
	}
	string_parameters.push_back(std::make_pair("--block-mode", m_bm.getValue()));
	string_parameters.push_back(std::make_pair("--block-generator", m_bg.getValue()));
	string_parameters.push_back(std::make_pair("--seed-generator", m_sg.getValue()));
	string_parameters.push_back(std::make_pair("--matching-type", m_gs.getValue()));
	if(m_reader == SWR) string_parameters.push_back(std::make_pair("--score-type", m_st.getValue()));
	string_parameters.push_back(std::make_pair("--discard-segments", m_ds.getValue()));
	string_parameters.push_back(std::make_pair("--discard-fragments", m_df.getValue()));
	if(test_seeds() > 0)
	{
		std::ostringstream ss5; ss5 << test_seeds();
		string_parameters.push_back(std::make_pair("--test-seeds", ss5.str()));
	}

	// experimental parameters
	if(local_consistency()) string_parameters.push_back(std::make_pair("--local-consistency", ""));
	else if(global_consistency()) string_parameters.push_back(std::make_pair("--global-consistency", ""));
	string_parameters.push_back(std::make_pair("--output-type", m_ot.getValue()));
	if(!run_optimized_code()) string_parameters.push_back(std::make_pair("--deactivate-global-opt", ""));
	if(no_repeats()) string_parameters.push_back(std::make_pair("--no-repeats", ""));
}

std::string PalomaParameters::compute_output_file(const std::string& filename) const
{
	std::ostringstream ss;
	ss << compute_basename(filename);
	if(!tag().empty()) ss << "_" << tag();
	ss << "_Q" << plma_quorum() << "_t" << threshold();
	ss << "_m" << min_size() << "_M" << max_size();
	ss << (strong_consensus() ? "_C" : "_c");
	ss << (local_consistency() ? "_l" : "_g");
	const std::string& afc = m_afc_file.getValue();
	if(!afc.empty())
	{
		std::string afc_file_basename_ext = afc.substr(afc.find_last_of('/') + 1, afc.length());
		ss << "_afc:" << afc_file_basename_ext;
	}
	ss << "_plma.xml";
	return ss.str();
}

std::string PalomaParameters::output() const
{
	if(!m_paloma_output.isSet()) return PalomaParameters::compute_output_file(m_paloma_input.getValue());
	else return m_paloma_output.getValue();
}

std::string PalomaParameters::afc_file() const
{
	if(!m_afc_file.getValue().empty()) return m_afc_file.getValue();
	else
	{
		std::ostringstream ss;
		ss << compute_basename(input()) << "_t" << threshold() << "_M" << max_size();
		ss << "_dialign.afc";
		return ss.str();
	}
}
