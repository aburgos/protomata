
#include "MultiSequence.h"

MultiSequence::MultiSequence(const std::string& filename, bool building_mode, bool allow_x, unsigned int lobound, unsigned int upbound, bool verbose)
{
	FastaParser fparser(filename, building_mode, allow_x, lobound, upbound, verbose);
	m_sequences = fparser.sequences();
	add_tags2position();
}

MultiSequence::MultiSequence(const std::vector<Sequence>& sequences): m_sequences(sequences)
{
	add_tags2position();
}

void MultiSequence::add_tags2position()
{
	for(const_iterator it = begin(); it != end(); ++it)
	{
		const Sequence& sequence = *it;
		if(!(m_position.insert(std::make_pair(sequence.tag(), sequence.id()))).second)
		{
			std::cerr << "Truncated tags of at least two sequences are equal." << std::endl;
			std::cerr << "Tags should be all different until the first white space." << std::endl;
			std::cerr << "Repeated (truncated) tag: " << sequence.tag() << std::endl;
			exit(1);
		}
	}
}

const Sequence& MultiSequence::operator[](unsigned int pos) const
{
	assert(pos < m_sequences.size());
	return m_sequences[pos];
}

const Sequence& MultiSequence::operator[](const std::string& tag) const
{
	assert(m_position.find(tag) != m_position.end());
	return operator[]((*m_position.find(tag)).second);
}

std::string MultiSequence::operator[](const Segment& s) const
{
	assert(s.sequence() < m_sequences.size());
	assert(s.begin() + s.length() <= m_sequences[s.sequence()].length());
	return (m_sequences[s.sequence()].data()).substr(s.begin(), s.length());
}

char MultiSequence::operator[](const Site& s) const
{
	assert(s.sequence() < m_sequences.size());
	assert(s.position() <= m_sequences[s.sequence()].length());

	return (m_sequences[s.sequence()].data())[s.position()];
}

void MultiSequence::weight_sequences()
{
	// create a temporary fasta filename
	char tmp_fasta[] = "/tmp/tmpfileXXXXXX";
	if(mkstemp(tmp_fasta) == -1)
		{ std::cerr << "Could not create temporary file for plma parser." << std:: endl; exit(1); }

	// open the temporary file
	std::ofstream fasta_handler;
	fasta_handler.open(tmp_fasta);
	fasta_handler << *this;
	fasta_handler.close();

	ClustalWeightsParser clustal(tmp_fasta);

	// remove the created temporary fasta file
	std::string rm_cmd = "rm -f ";
	rm_cmd.append(tmp_fasta);
	if(std::system(rm_cmd.c_str()))
		std::cerr << "Could not remove temporary created file " << tmp_fasta << std::endl;

	// store the new weighted sequences
	std::vector<Sequence> weighted_sequences;
	for(const_iterator it = begin(); it != end(); ++it)
	{
		const std::string& tag = (*it).tag();
		const Sequence& old_seq = operator[](tag);
		double new_weight = clustal.weight(tag);
		Sequence new_seq(old_seq.tag(), old_seq.data(), old_seq.id(), new_weight);
		weighted_sequences.push_back(new_seq);
	}

	std::swap(m_sequences, weighted_sequences);
}

void MultiSequence::weight_sequences(const std::string& filename)
{
	ManualWeightsParser mwp(filename);

	// store the new weighted sequences
	std::vector<Sequence> weighted_sequences;
	for(const_iterator it = begin(); it != end(); ++it)
	{
		const std::string& tag = (*it).tag();
		const Sequence& old_seq = operator[](tag);
		double new_weight = mwp.weight(tag);
		Sequence new_seq(old_seq.tag(), old_seq.data(), old_seq.id(), new_weight);
		weighted_sequences.push_back(new_seq);
	}

	std::swap(m_sequences, weighted_sequences);
}
