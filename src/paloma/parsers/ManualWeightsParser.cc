
#include "ManualWeightsParser.h"

ManualWeightsParser::ManualWeightsParser(const std::string& weights_file)
{
	std::ifstream file;
	file.open(weights_file.c_str(), std::ifstream::in);
	if(!file.good())
	{
		std::cerr << "File " << weights_file << " could not be opened." << std::endl;
		exit(1);
	}

	std::string line;
	double max_weight = 0;
	while(getline(file, line))
	{
		if(line[0] == '#') continue;

		line = tounix(line);
		std::istringstream iss(line);
		// automatically truncates till first white space
		std::string tag; iss >> tag;
		iss.ignore(2048, '\t');
		double weight; iss >> weight;
		m_weights[tag] = weight;
		if(max_weight < weight) max_weight = weight;
	}
	file.close();

	m_total_weight = 0;
	for(std::map<std::string, double>::iterator it = m_weights.begin(); it != m_weights.end(); ++it)
	{
		const std::string& tag = (*it).first;
		m_weights[tag] = m_weights[tag] / max_weight;
		m_total_weight += m_weights[tag];
	}
}

std::string ManualWeightsParser::tounix(const std::string& str) const
{
	std::string unix_str(str);
	size_t pos = str.find_last_of('\r');
	if(pos != std::string::npos)
		unix_str.erase(pos, 1);
	return unix_str;
}
