
#ifndef WEIGHTSPARSER_H
#define WEIGHTSPARSER_H

#include <iostream>

class WeightsParser
{
  public:
	WeightsParser() {}
	/*! \return The computed weight of the provided truncated tag sequence. */
	virtual double weight(const std::string&) const = 0;
	/*! \return The sum of normalized weights of all sequences. */
	virtual double total_weight() const = 0;
};

#endif
