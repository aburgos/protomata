
#ifndef FASTAPARSER_H
#define FASTAPARSER_H

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <algorithm>
#include <vector>
#include <limits>
#include "Sequence.h"
#include "AminoAcids.h"
#include "EnumTypes.h"

class FastaParser
{
  private:
	std::vector<Sequence> m_sequences;
	//! Function for removing a set of given characters from a string.
	std::string remove_chars(const std::string& str, const std::string& rmv, bool) const;
	//! Resolve ambiguities on sequence data. It assigns the amino acid with higher background probability.
	bool resolve_ambiguities(std::string&, std::string&, bool, bool);
	//! Shows all changes in sequences and if they are ignored and prints a resume.
	bool verbose;
	unsigned int rejected_sequences;
	unsigned int sequences_changed;
	unsigned int changed_aminoacids;
	unsigned int seq_changed_aminoacids;

  public:
	FastaParser();
	/*!
		A constructor from a fasta file.
		\param If TRUE, building mode. If FALSE, scanning mode.
		\param If FALSE, don't allow X's on sequences.
		\param Lower bound for sequence's length filtering.
		\param Upper bound for sequence's length filtering.
		\param If TRUE, prints amino acids changes in sequences and a resume of rejected and accepted sequences.
	*/
	FastaParser(const std::string&, bool = true, bool = true, unsigned int = std::numeric_limits<unsigned int>::min(), unsigned int = std::numeric_limits<unsigned int>::max(), bool = false);
	unsigned int size() const { return m_sequences.size(); }
	std::vector<Sequence> sequences() const { return m_sequences; }
	std::string clean_tag(const std::string&) const;
	std::string clean_data(const std::string&) const;
	std::string truncate(const std::string&) const;
	std::string tounix(const std::string&) const;
	void create_sequence(std::string&, std::string&, bool, bool, unsigned int = std::numeric_limits<unsigned int>::min(), unsigned int = std::numeric_limits<unsigned int>::max());
	typedef std::vector<Sequence>::const_iterator const_iterator;
	const_iterator begin() const { return m_sequences.begin(); }
	const_iterator end() const { return m_sequences.end(); }
	//!	Outputs tag and data of each Sequence in FASTA format.
	friend std::ostream& operator<<(std::ostream& aStream, const FastaParser& ff)
	{
		for(FastaParser::const_iterator it = ff.begin(); it != ff.end(); ++it)
			aStream << *it;
		return aStream;
	}
};

#endif
