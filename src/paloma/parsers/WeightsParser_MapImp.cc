
#include "WeightsParser_MapImp.h"

double WeightsParser_MapImp::weight(const std::string& tag) const
{
	std::map<std::string, double>::const_iterator it = m_weights.find(tag);
	if(it == m_weights.end())
	{
		std::cout << "No weight for sequence " << tag << " found." << std::endl;
		exit(1);
	}
	else return (*it).second;
}
