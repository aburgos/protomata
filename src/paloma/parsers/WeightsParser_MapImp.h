
#ifndef WEIGHTSPARSERMAPIMP_H
#define WEIGHTSPARSERMAPIMP_H

#include <map>
#include <cstdlib>
#include "WeightsParser.h"

class WeightsParser_MapImp: public WeightsParser
{
  protected:
	double m_total_weight;
	//! Stores the obtained by clustal weights.
	std::map<std::string, double> m_weights;

  public:
	WeightsParser_MapImp() {}
	/*! \return The computed weight of the provided truncated tag sequence. */
	double weight(const std::string&) const;
	/*! \return The sum of normalized weights of all sequences. */
	double total_weight() const { return m_total_weight; }
	typedef std::map<std::string, double>::const_iterator const_iterator;
	const_iterator begin() const { return m_weights.begin(); }
	const_iterator end() const { return m_weights.end(); }
};

#endif
