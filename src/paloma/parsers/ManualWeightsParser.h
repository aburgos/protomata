
#ifndef MANUALWEIGHTSPARSER_H
#define MANUALWEIGHTSPARSER_H

#include <iostream>
#include <sstream>
#include <fstream>
#include "WeightsParser_MapImp.h"

class ManualWeightsParser: public WeightsParser_MapImp
{
  public:
	ManualWeightsParser(const std::string&);
	std::string tounix(const std::string&) const;
};

#endif
