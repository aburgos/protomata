
#include "ClustalWeightsParser.h"

ClustalWeightsParser::ClustalWeightsParser(const std::string& fasta_file)
{
	char msf[] = "/tmp/tmpfileXXXXXX";
	char output[] = "/tmp/tmpfileXXXXXX";

	// create unique filenames for temporary files
	if(mkstemp(msf) == -1)
		{ std::cerr << "Could not create temporary file for clustal parser." << std:: endl; exit(1); }
	std::string msf_str = msf;

	if(mkstemp(output) == -1)
		{ std::cerr << "Could not create temporary file for clustal parser." << std:: endl; exit(1); }
	std::string output_str = output;

	std::string cmd = "clustalw2 -INFILE=" + fasta_file + " -OUTFILE=" + msf + " -OUTPUT=GCG -ALIGN > " + output;
	if(system(cmd.c_str())) { std::cerr << "Error executing " << cmd << std::endl; exit(1); }

	std::ifstream file;
	file.open(msf, std::ifstream::in);
	if(!file.good())
	{
		std::cerr << "File " << msf << " could not be opened." << std::endl;
		exit(1);
	}

	std::string line;
	double max_weight = 0;
	while(getline(file, line))
	{
		size_t tag_pos = line.rfind("Name: ");
		size_t len_pos = line.rfind(" oo  Len: ");
		size_t wgt_pos = line.rfind("Weight: ");
		if(wgt_pos != std::string::npos && tag_pos != std::string::npos && len_pos != std::string::npos)
		{
			std::string tag = line.substr(tag_pos + 6, len_pos - 7);
			std::istringstream iss(line.substr(wgt_pos + 9, line.length()));
			double weight; iss >> weight;
			m_weights[tag] = weight;
			if(max_weight < weight) max_weight = weight;
		}
	}
	file.close();

	m_total_weight = 0;
	for(std::map<std::string, double>::iterator it = m_weights.begin(); it != m_weights.end(); ++it)
	{
		const std::string& tag = (*it).first;
		m_weights[tag] = m_weights[tag] / max_weight;
		m_total_weight += m_weights[tag];
	}

	std::string dnd_file = fasta_file.substr(0, fasta_file.find_last_of('.')) + ".dnd";
	std::string rm_cmd = "rm " + msf_str + " " + dnd_file + " " + output_str;
	if(system(rm_cmd.c_str()))
		std::cerr << "Could not remove clustal temporary created files." << std::endl;
}
