
#ifndef CLUSTALWEIGHTSPARSER_H
#define CLUSTALWEIGHTSPARSER_H

#include <iostream>
#include <sstream>
#include <fstream>
#include "WeightsParser_MapImp.h"

/*!
Executes external program clustalw2 for getting sequence's similarity weights. It parses the obtained weights and stores them.
*/
class ClustalWeightsParser: public WeightsParser_MapImp
{
  public:
	//! The constructor executes clustal with the parameter fasta file and parses the weights.
	ClustalWeightsParser(const std::string&);
};

#endif
