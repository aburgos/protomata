
#include "FastaParser.h"

FastaParser::FastaParser()
{
	verbose = false;
	rejected_sequences = 0;
	sequences_changed = 0;
	changed_aminoacids = 0;
	seq_changed_aminoacids = 0;
}

FastaParser::FastaParser(const std::string& filename, bool building_mode, bool allow_x, unsigned int lobound, unsigned int upbound, bool v): verbose(v)
{
	std::ifstream file;
	file.open(filename.c_str(), std::ifstream::in);
	if(!file.good())
	{
		std::cerr << "File " << filename << " could not be opened." << std::endl;
		exit(1);
	}

	if(verbose) std::cout << "Parsing fasta file " << filename << ":" << std::endl;

	rejected_sequences = 0;
	sequences_changed = 0;
	changed_aminoacids = 0;
	seq_changed_aminoacids = 0;

	std::string line, tag, data;
	while(getline(file, line))
	{
		line = tounix(line);
		size_t found = line.rfind(">");
		if(found != std::string::npos)
		{
			if(!data.empty())
			{
				create_sequence(tag, data, building_mode, allow_x, lobound, upbound);

				data.clear();
				tag.clear();
			}
			tag = line;
		}
		else data.append(line);
	}

	if(!tag.empty() && !data.empty()) create_sequence(tag, data, building_mode, allow_x, lobound, upbound);

	if(verbose)
	{
		std::cout << std::endl << "Fasta file parsing resume:" << std::endl;
		std::cout << rejected_sequences << " sequences were rejected" << std::endl;
		std::cout << sequences_changed << " accepted sequences were changed by at least one amino acid" << std::endl;
		std::cout << changed_aminoacids << " amino acids were changed in all considered sequences" << std::endl;
		std::cout << m_sequences.size() << " sequences will be considered" << std::endl;
	}
}

void FastaParser::create_sequence(std::string& tag, std::string& data, bool building_mode, bool allow_x, unsigned int lobound, unsigned int upbound)
{
	tag = clean_tag(tag);
	data = clean_data(data);

	if(!resolve_ambiguities(tag, data, building_mode, allow_x))
	{
		std::cout << "Sequence " << tag << " contains unknown amino acids. ";
		if(building_mode)
		{
			// if we get here it means we want to use these sequences for building, so stop
			std::cerr << std::endl << "Fasta file can't be used for building a model.";
			std::cerr << std::endl;
			exit(1);
		}
		std::cout << "It will be ignored." << std::endl;
		rejected_sequences++;
		seq_changed_aminoacids = 0;
	}
	else
	{
		if(seq_changed_aminoacids > 0)
		{
			changed_aminoacids += seq_changed_aminoacids;
			seq_changed_aminoacids = 0;
			sequences_changed++;
		}
		if(data.length() <= upbound && data.length() >= lobound)
		{
			std::string truncated_tag = truncate(tag);
			const Sequence sequence(truncated_tag, tag, data, m_sequences.size());
			m_sequences.push_back(sequence);
		}
		else if(verbose) std::cout << "Sequence " << tag << " length's out of bounds" << std::endl;
	}
}

std::string FastaParser::clean_tag(const std::string& tag) const
{
	std::string cleaned_tag = remove_chars(tag, ">'#{}/", false);
	// replace some chars with '_' for clustalw compatibility
	// who knows if they are making some other changes
	replace(cleaned_tag.begin(), cleaned_tag.end(), ':', '_');
	replace(cleaned_tag.begin(), cleaned_tag.end(), ',', '_');
	// replace tabs in case user defined weights will be used
	replace(cleaned_tag.begin(), cleaned_tag.end(), '\t', '_');
	return cleaned_tag;
}

std::string FastaParser::clean_data(const std::string& data) const
{
	std::string cleaned_data = remove_chars(data, ">'#{}/", true);
	std::transform(cleaned_data.begin(), cleaned_data.end(), cleaned_data.begin(), ::toupper);
	return cleaned_data;
}

std::string FastaParser::truncate(const std::string& tag) const
{
	// truncate tag to first white space
	size_t stop_pos = tag.find_first_of(' ');
	return tag.substr(0, stop_pos);
}

std::string FastaParser::tounix(const std::string& str) const
{
	std::string unix_str(str);
	size_t pos = str.find_last_of('\r');
	if(pos != std::string::npos)
		unix_str.erase(pos, 1);
	return unix_str;
}

std::string FastaParser::remove_chars(const std::string& str, const std::string& rmv, bool rmv_ws) const
{
	bool* flags = new bool[128];
	for(unsigned int i = 0; i < 128; i++)
		flags[i] = false;

	if(rmv_ws) flags[32] = true;

	for(unsigned int i = 0; i < rmv.length(); i++)
		flags[(int)rmv[i]] = true;

	std::string ret;
	unsigned int pos = 0;
	while(pos < str.length())
	{
		if(!flags[(int)str[pos]]) ret.append(1, str[pos]);
		pos++;
	}

	delete [] flags;

	return ret;
}

bool FastaParser::resolve_ambiguities(std::string& tag, std::string& data, bool building_mode, bool allow_x)
{
	const AminoAcids& amino_acids = AminoAcids::instance();
	for(std::string::iterator it = data.begin(); it != data.end(); ++it)
	{
		char current = *it;
		if(current == 'B')
		{
			*it = 'D';
			if(verbose) std::cout << tag << " > replacement of amino acid B by D" << std::endl;
			seq_changed_aminoacids++;
		}
		else if(current == 'J')
		{
			*it = 'L';
			if(verbose) std::cout << tag << " > replacement of amino acid J by L" << std::endl;
			seq_changed_aminoacids++;
		}
		else if(current == 'Z')
		{
			*it = 'E';
			if(verbose) std::cout << tag << " > replacement of amino acid Z by E" << std::endl;
			seq_changed_aminoacids++;
		}
		else if(current == 'U')
		{
			*it = 'C';
			if(verbose) std::cout << tag << " > replacement of amino acid U by C" << std::endl;
			seq_changed_aminoacids++;
		}
		else if(current == 'O')
		{
			*it = 'K';
			if(verbose) std::cout << tag << " > replacement of amino acid O by K" << std::endl;
			seq_changed_aminoacids++;
		}
		else if(current == 'X')
		{
			if(verbose) std::cout << tag << " > X found!" << std::endl;
			if(building_mode || !allow_x) return false;
		}
		else if(!amino_acids.has(current))
		{
			if(verbose) std::cout << "Unknown amino acid " << current << std::endl;
			return false;
		}
	}

	return true;
}

