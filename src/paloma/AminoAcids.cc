
#include "AminoAcids.h"
#include "MultiSequence.h"

AminoAcids::AminoAcids()
{
	// amino acids table taken from http://www.ebi.ac.uk/2can/biology/molecules_small_aatable.html
	aa.resize(20);

	// standard encoded amino acids
	aa[0] = 'A'; aa[1] = 'C'; aa[2] = 'E'; aa[3] = 'D'; aa[4] = 'G'; aa[5] = 'F'; aa[6] = 'I'; aa[7] = 'H';
	aa[8] = 'K'; aa[9] = 'M'; aa[10] = 'L'; aa[11] = 'N'; aa[12] = 'Q'; aa[13] = 'P'; aa[14] = 'S'; aa[15] = 'R';
	aa[16] = 'T'; aa[17] = 'W'; aa[18] = 'V'; aa[19] = 'Y';

	// amino acid ambiguities
	//aa[20] = 'B'; aa[21] = 'J'; aa[22] = 'X'; aa[23] = 'Z';

	// special encoded amino acids
	//aa[24] = 'U'; aa[25] = 'O';

	// assign background probabilities
	p['A'] = 0.078; p['C'] = 0.016; p['E'] = 0.066; p['D'] = 0.053; p['G'] = 0.069; p['F'] = 0.040; p['I'] = 0.059;
	p['H'] = 0.023; p['K'] = 0.059; p['M'] = 0.024; p['L'] = 0.096; p['N'] = 0.042; p['Q'] = 0.040; p['P'] = 0.048;
	p['S'] = 0.069; p['R'] = 0.053; p['T'] = 0.055; p['W'] = 0.012; p['V'] = 0.067; p['Y'] = 0.031;
}

bool AminoAcids::has(char _aa) const
{
	for(const_iterator it = begin(); it != end(); ++it)
		if(_aa == (*it)) return true;
	return false;
}

double AminoAcids::back_prob(char _aa) const
{
	std::map<char, double>::const_iterator it;
	if((it = p.find(_aa)) != p.end()) return (*it).second;
	else { std::cerr << "Amino acid " << _aa << " has no assigned background probability." << std::endl; exit(1); }
}
