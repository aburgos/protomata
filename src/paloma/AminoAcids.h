
#ifndef AMINOACIDS_H
#define AMINOACIDS_H

#include <map>
#include <vector>

/*!
Lists all amino acids and provides an iterator to access them. Also this class is useful for computing the (double) counts of each amino acid in a site partition element. This represents a column, and each amino acid's count is equal to the sum of the sequence's weight were the amino acid appears.
*/
class AminoAcids
{
  private:
	//! Stores the amino acids.
	std::vector<char> aa;
	//! Stores the background probability for each amino acid.
	std::map<char, double> p;
	//! Private constructor for avoiding instance repetition.
	AminoAcids();

  public:
	//! 
	static AminoAcids& instance() { static AminoAcids aa; return aa; }
	//! Get the number of amino acids currently used
	unsigned int size() const { return aa.size(); }
	//! Check if the amino acid is considered for protomata.
	bool has(char _aa) const;
	/*! \return The background probability of the amino acid passed as parameter. */
	double back_prob(char _aa) const;

	//! Alias for an amino acid iterator.
	typedef std::vector<char>::const_iterator const_iterator;
	//! A begin iterator to the amino acids.
	const_iterator begin() const { return aa.begin(); }
	//! An end iterator to the amino acids.
	const_iterator end() const { return aa.end(); }
};

#endif
