
#include <time.h>
#include <iostream>
#include "factories/PalomaFactory.h"

int main(int argc, char **argv)
{
	//double start, end;
	//start = (double)clock();

	PalomaParameters parameters;
	parameters.parse(argc, argv);

	PalomaFactory pfact(parameters);

	//end = (double)clock();
	//double time_elapsed = double(end - start) / CLOCKS_PER_SEC;
	//std::cout << std::endl << "Time elapsed: " << time_elapsed << " secs" << std::endl;
	return 0;
}
