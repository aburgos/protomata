
#ifndef GENERALIZEDSETFACTORY_H
#define GENERALIZEDSETFACTORY_H

#include "EnumTypes.h"
#include "generalized_sets/GeneralizedLocalMatching.h"
#include "generalized_sets/GlobalConsistentGA.h"
#include "generalized_sets/LocalConsistentGLA.h"
#include "matchings/LocalMatching_SiGrImp.h"
#include "alignments/LocalAlignment_UFImp.h"

class GeneralizedSetFactory
{
  public:
	typedef Matching_SiGrImp M1;
	typedef LocalMatching_SiGrImp M2;
	typedef Alignment_UFImp M3;
	typedef LocalAlignment_UFImp M4;

	GeneralizedSet<M1>* create(const M1*, const MultiSequence& ms, GeneralizedSetType, bool)
	{
		return new GeneralizedMatching<M1>(ms);
	}
	GeneralizedSet<M2>* create(const M2*, const MultiSequence& ms, GeneralizedSetType, bool)
	{
		return new GeneralizedAlignment<M2>(ms);
	}
	GeneralizedSet<M3>* create(const M3*, const MultiSequence& ms, GeneralizedSetType, bool)
	{
		return new GeneralizedAlignment<M3>(ms);
	}
	GeneralizedSet<M4>* create(const M4*, const MultiSequence& ms, GeneralizedSetType type, bool repeats)
	{
		switch(type)
		{
			case GLA: return new GeneralizedLocalAlignment<M4>(ms);
			case GA_GC: return new GlobalConsistentGA<M4>(ms);
			case GLA_LC: return new LocalConsistentGLA<M4>(ms, repeats);
			default: return NULL;
		}
	}
};

#endif
