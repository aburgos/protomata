
#ifndef MATCHINGSETFACTORY_H
#define MATCHINGSETFACTORY_H

#include "EnumTypes.h"
#include "PalomaParameters.h"
#include "matching_sets/SiteMatchingSet.h"
#include "matching_sets/SitePairwiseLocalMatchingSet.h"

/*!
Instantiate the right object to store the input matchings generated, according to the enum SetType passed as parameter.
*/
template <typename InM>
class MatchingSetFactory
{
  public:
	/*!
		Creates a new matching set object.
		\param params are all paloma2 defined parameters.
		\return A new matching set object.
	*/
	MatchingSet<InM>* create(const MultiSequence& ms, MatchingSetType type)
	{
		switch(type)
		{
			case MSet: return new MatchingSet<InM>(ms);
			case PLMSet: return new PairwiseLocalMatchingSet<InM>(ms);
			case SMSet: return new SiteMatchingSet<InM>(ms);
			case SPLMSet: return new SitePairwiseLocalMatchingSet<InM>(ms);
			default: return NULL;
		}
	}
};

#endif
