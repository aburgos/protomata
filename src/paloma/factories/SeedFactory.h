
#ifndef SEEDFACTORY_H
#define SEEDFACTORY_H

#include "EnumTypes.h"
#include "PalomaParameters.h"
#include "seeds/DialignWeight.h"
#include "seeds/Support.h"

/*!
Instantiate the right object to be used as a seed generator, according to the enum SeedType passed as parameter.
*/
class SeedFactory
{
  public:
	/*!
		Creates a new segment seed generator object.
		\param ms is the object that contains the sequences.
		\param mset is a set of matchings read from input.
		\param params are all paloma2 defined parameters.
		\return A new segment seed generator object.
	*/
	template <typename InM> SegmentSeedGenerator* create(const MultiSequence& ms, const PairwiseLocalMatchingSet<InM>& mset, SeedType type)
	{
		switch(type)
		{
			case dialignW: return new DialignWeight<InM>(ms, mset);
			case support: return new Support<InM>(ms, mset);

			default: return NULL;
		}
	}
};

#endif
