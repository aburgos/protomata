
#ifndef OUTPUTFACTORY_H
#define OUTPUTFACTORY_H

#include "EnumTypes.h"
#include "PalomaParameters.h"
#include "outputs/ALN.h"
#include "outputs/PLMA.h"

/*!
Instantiate the right object to output the results. It can be in XML format or ALN format.
*/
template <typename OutM>
class OutputFactory
{
  public:
	/*!
		Creates a new output object.
		\param ms is the object that contains the sequences.
		\param gs is the generalized set from which the result will be taken.
		\param params are all paloma2 defined parameters.
	*/
	Output<OutM>* create(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, const PalomaParameters& params)
	{
		if(params.output_type() == PLMAt)
		{
			const GeneralizedAlignment<OutM>& ga = dynamic_cast<const GeneralizedAlignment<OutM>&>(gs);
			return new PLMA<OutM>(ms, ga, params);
		}
		if(params.output_type() == ALNt)
		{
			const GeneralizedAlignment<OutM>& ga = dynamic_cast<const GeneralizedAlignment<OutM>&>(gs);
			return new ALN<OutM>(ms, ga, params);
		}
		else return NULL;
	}
};

#endif
