
#ifndef PALOMAFACTORY_H
#define PALOMAFACTORY_H

#include "Paloma.h"
#include "EnumTypes.h"

class PalomaFactory
{
  public:
	PalomaFactory(ReaderType reader, GeneralizedSetType generalized_set)
	{
		switch(reader)
		{
			case DialignR:
			{
				typedef DialignPLA_Imp InM;
				switch(generalized_set)
				{
					case GM:
					{
						typedef Matching_SiGrImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
					case GLM:
					{
						typedef LocalMatching_SiGrImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
					case GA:
					{
						typedef Alignment_UFImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
					case GLA:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
					case GA_GC:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
					case GLA_LC:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
				}
				break;
			}
			case SWR:
			{
				typedef PairwiseLocalAlignment_PairUFImp InM;
				switch(generalized_set)
				{
					case GM:
					{
						typedef Matching_SiGrImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
					case GLM:
					{
						typedef LocalMatching_SiGrImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
					case GA:
					{
						typedef Alignment_UFImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
					case GLA:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
					case GA_GC:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
					case GLA_LC:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p();
						break;
					}
				}
				break;
			}
		}
	}

	PalomaFactory(const PalomaParameters& params)
	{
		switch(params.reader())
		{
			case DialignR:
			{
				typedef DialignPLA_Imp InM;
				switch(params.generalized_set())
				{
					case GM:
					{
						typedef Matching_SiGrImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
					case GLM:
					{
						typedef LocalMatching_SiGrImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
					case GA:
					{
						typedef Alignment_UFImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
					case GLA:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
					case GA_GC:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
					case GLA_LC:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
				}
				break;
			}
			case SWR:
			{
				typedef PairwiseLocalAlignment_PairUFImp InM;
				switch(params.generalized_set())
				{
					case GM:
					{
						typedef Matching_SiGrImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
					case GLM:
					{
						typedef LocalMatching_SiGrImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
					case GA:
					{
						typedef Alignment_UFImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
					case GLA:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
					case GA_GC:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
					case GLA_LC:
					{
						typedef LocalAlignment_UFImp OutM;
						Paloma<InM, OutM> p(params);
						break;
					}
				}
				break;
			}
		}
	}
};

#endif
