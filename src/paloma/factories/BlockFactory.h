
#ifndef BLOCKFACTORY_H
#define BLOCKFACTORY_H

#include "EnumTypes.h"
#include "PalomaParameters.h"
#include "block_generators/ConnComp_ExhaustiveImp.h"
#include "block_generators/ConnComp_GreedyImp.h"
#include "block_generators/ConnComp_GreedyBestWgtImp.h"
#include "block_generators/Clique_ExhaustiveImp.h"
#include "block_generators/Clique_GreedyImp.h"
#include "block_generators/Clique_GreedyBestWgtImp.h"
#include "block_generators/ConnComp_GloConOpt_ExhaustiveImp.h"
#include "block_generators/ConnComp_GloConOpt_GreedyImp.h"
#include "block_generators/ConnComp_GloConOpt_GreedyBestWgtImp.h"
#include "block_generators/Clique_GloConOpt_ExhaustiveImp.h"
#include "block_generators/Clique_GloConOpt_GreedyImp.h"
#include "block_generators/Clique_GloConOpt_GreedyBestWgtImp.h"

/*!
Instantiate the right object to be used as a seed generator, according to the enum BlockType passed as parameter.
*/
template <typename InM, typename OutM>
class BlockFactory
{
  public:
	/*!
		Creates a new block generator.
		\param ms is the object that contains the sequences.
		\param gs is the result set were the generated candidates will (or not) be added.
		\param plms is the matching set containing the read matchings from input.
		\param segs is the set of segments that will be included in the generated block (the seed).
		\param params are all paloma2 defined parameters.
		\return A pointer to a new BlockGenerator-derived object.
	*/
	BlockGenerator* create(const MultiSequence& ms, const GeneralizedSet<OutM>& gs, PairwiseLocalMatchingSet<InM>& plms, const SegmentSet& segs, const PalomaParameters& params)
	{
		switch(params.block_generator())
		{
			// connected components
			case connCompExhaustive:
			{
				if(params.run_optimized_code())
				{
					const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(gs);
					return new ConnComp_GloConOpt_ExhaustiveImp<InM, OutM>(ms, gcga, plms, segs);
				}
				else return new ConnComp_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs);
			}
			case connCompGreedy:
			{
				if(params.run_optimized_code())
				{
					const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(gs);
					return new ConnComp_GloConOpt_GreedyImp<InM, OutM>(ms, gcga, plms, segs);
				}
				else return new ConnComp_GreedyImp<InM, OutM>(ms, gs, plms, segs);
			}
			case connCompBestWeights:
			{
				if(params.run_optimized_code())
				{
					const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(gs);
					return new ConnComp_GloConOpt_GreedyBestWgtImp<InM, OutM>(ms, gcga, plms, segs);
				}
				else return new ConnComp_GreedyBestWgtImp<InM, OutM>(ms, gs, plms, segs);
			}
			// cliques
			case cliqueExhaustive:
			{
				if(params.run_optimized_code())
				{
					const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(gs);
					return new Clique_GloConOpt_ExhaustiveImp<InM, OutM>(ms, gcga, plms, segs);
				}
				else return new Clique_ExhaustiveImp<InM, OutM>(ms, gs, plms, segs);
			}
			case cliqueGreedy:
			{
				if(params.run_optimized_code())
				{
					const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(gs);
					return new Clique_GloConOpt_GreedyImp<InM, OutM>(ms, gcga, plms, segs);
				}
				else return new Clique_GreedyImp<InM, OutM>(ms, gs, plms, segs);
			}
			case cliqueBestWeights:
			{
				if(params.run_optimized_code())
				{
					const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(gs);
					return new Clique_GloConOpt_GreedyBestWgtImp<InM, OutM>(ms, gcga, plms, segs);
				}
				else return new Clique_GreedyBestWgtImp<InM, OutM>(ms, gs, plms, segs);
			}
			default: return NULL;
		}
	}
};

#endif
