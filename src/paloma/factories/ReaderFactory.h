
#ifndef READERFACTORY_H
#define READERFACTORY_H

#include "EnumTypes.h"
#include "PalomaParameters.h"
#include "input_readers/DialignReader.h"
#include "input_readers/SWReader.h"
#include "matching_sets/MatchingSet.h"

/*!
Instantiate the right object to read the input set of Matching. The create function is overloaded in order to create the right reader depending on the matching set being use when called.
*/
class ReaderFactory
{
  public:
	typedef DialignPLA_Imp InM1;
	typedef PairwiseLocalAlignment_PairUFImp InM2;
	/*!
		Create a new dialign reader object.
		\param mset is a set of matchings read from input.
		\param ms is the object that contains the sequences.
		\param params are all paloma2 defined parameters.
		\return A pointer to a new dialign reader object.
	*/
	InputReader<InM1>* create(const MatchingSet<InM1>*, const MultiSequence& ms, const PalomaParameters& params)
	{
		return new DialignReader(ms, params.threshold(), params.min_size(), params.max_size(), params.afc_file());
	}
	/*!
		Create a new smith and waterman reader object.
		\param mset is a set of matchings read from input.
		\param ms is the object that contains the sequences.
		\param filename is not used.
		\param threshold is not used.
		\param min_size is not used.
		\param max_size is not used.
		\return A pointer to a new smith and waterman reader object.
	*/
	InputReader<InM2>* create(const MatchingSet<InM2>*, const MultiSequence& ms, const PalomaParameters& params)
	{
		return new SWReader(ms, params.threshold(), params.min_size(), params.max_size(), params.score_type());
	}
};

#endif
