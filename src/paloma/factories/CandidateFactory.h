
#ifndef CANDIDATEFACTORY_H
#define CANDIDATEFACTORY_H

#include "EnumTypes.h"
#include "PalomaParameters.h"
#include "BlockFactory.h"
#include "candidates/MaxWeightPairwiseLocalCandidate.h"
#include "candidates/MaxWeightPairwiseLocalCandidate_GloConOpt.h"
#include "candidates/SupportPairwiseLocalCandidate.h"
#include "candidates/SeedApproach.h"
#include "candidates/BestConnCompOnly.h"
#include "candidates/BestCompatibleConnComp.h"
#include "candidates/BestConnCompOnly_GloConOpt.h"
#include "candidates/BestCompatibleConnComp_GloConOpt.h"
#include "seeds/SegmentSeedGenerator.h"

/*!
Instantiate the right object to generate the candidates, according to the enum CandGenType passed as parameter. Also some candidate generators need to know how to construct or generate a new candidate. This is specified with the BlockType passed as parameter. Classes bestConnCompOnly and bestCompatibleConnComp can generate their candidates using every type of block generator available.
*/
template <typename InM, typename OutM>
class CandidateFactory
{
  public:
	/*!
		Creates a new candidate generator.
		\param ms is the object that contains the sequences.
		\param mset is a set of matchings read from input.
		\param seed is the segment seed generator that the candidate generator will use to compute its candidates.
		\param gs is the result set to check whether a created object is compatible with it or not. If it is, it became a candidate. If it is not, it is discarded.
		\param params are all paloma2 defined parameters.
		\return A pointer to a new candidate generator object.
	*/
	CandidateGenerator<InM, OutM>* create(const MultiSequence& ms, MatchingSet<InM>& mset, SegmentSeedGenerator& seed, const GeneralizedSet<OutM>& gs, const PalomaParameters& params)
	{
		PairwiseLocalMatchingSet<InM>& plmset = dynamic_cast<PairwiseLocalMatchingSet<InM>&>(mset);

		switch(params.block_mode())
		{
			case maxWeightFragments:
			{
				if(params.run_optimized_code())
				{
					const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(gs);
					return new MaxWeightPairwiseLocalCandidate_GloConOpt<InM, OutM>(ms, gcga, plmset, params);
				}
				else return new MaxWeightPairwiseLocalCandidate<InM, OutM>(ms, gs, plmset, params);
			}
			case maxSupportFragments: return new SupportPairwiseLocalCandidate<InM, OutM>(ms, gs, plmset, params);
			case bestConnCompOnly:
			{
				if(params.run_optimized_code())
				{
					const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(gs);
					return new BestConnCompOnly_GloConOpt<InM, OutM>(ms, gcga, plmset, seed, params);
				}
				else return new BestConnCompOnly<InM, OutM>(ms, gs, plmset, seed, params);
			}
			case bestCompatibleConnComp:
			{
				if(params.run_optimized_code())
				{
					const GlobalConsistentGA<OutM>& gcga = dynamic_cast<const GlobalConsistentGA<OutM>&>(gs);
					return new BestCompatibleConnComp_GloConOpt<InM, OutM>(ms, gcga, plmset, seed, params);
				}
				else return new BestCompatibleConnComp<InM, OutM>(ms, gs, plmset, seed, params);
			}
			default: return NULL;
		}
	}
};

#endif
