
#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include "Site.h"
#include "Segment.h"
#include "lib/Graph.h"

class PairwiseLocalMatching;

// graphs
typedef Graph<Site, double> SiteGraph;
typedef Graph<Segment, double> SegmentGraph;
typedef Graph<Segment, const PairwiseLocalMatching*> PairwiseSegmentGraph;
typedef Graph<unsigned int, double> RelationGraph;

// relations
typedef RelationGraph::relation WeightedPairRelation;
typedef SiteGraph::relation SitePairRelation;
typedef SegmentGraph::relation SegmentPairRelation;
typedef PairwiseSegmentGraph::relation SegmentPairwiseRelation;

// sets
typedef std::set<Segment> SegmentSet;
typedef std::set<Site> SitePartitionElement;
typedef std::set<SitePartitionElement> SitePartition;
typedef std::set<Site> SiteSet;

#endif
