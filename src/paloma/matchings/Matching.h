
#ifndef MATCHING_H
#define MATCHING_H

#include "iterators/Visitor.h"

/*!
A pure abstract class. A Matching is a set of pair relations between sites with no restriction.
*/
class Matching
{
  protected:
	//!	Definition of the output (cout) operator for a Matching.
	virtual void print(std::ostream&) const {}

  public:
	//! The constructor.
	Matching() {}
	//! The destructor
	virtual ~Matching() {}
	//! Number of sites.
	virtual unsigned int sites_size() const = 0;
	//! Number of relations.
	virtual unsigned int relations_size() const = 0;
	/*!
		Adds a relation of sites to the matching.
		\param A relation between sites.
		\return True if the site relation was added. False otherwise.
	*/
	virtual bool addMatching(const SitePairRelation&) = 0;
	/*!
		Adds a all site relations of the parameter to the matching.
		\param A matching.
		\return Always True.
	*/
	virtual bool addMatching(const Matching&) = 0;
	//!	Undefined.
	virtual double sitesWeight() const = 0;
	/*!
		Checks if a specific site relation exists in the matching.
		\param A site pair relation.
		\return True if the site relation exists. False otherwise.
	*/
	virtual bool hasSiteRelation(const SitePairRelation&) const = 0;
	/*!
		Gets a visitor to access the all the sites of this matching.
		\return A visitor to all the sites of this matching.
	*/
	virtual site_visitor sites() const = 0;
	/*!
		Gets a visitor to access the all the site relations of this matching.
		\return A visitor to all the relations of this matching.
	*/
	virtual relation_visitor site_relations() const = 0;
	//!	Definition of the output (cout) operator for a matching. It calls the protected function print defined on each class.
	friend std::ostream& operator<<(std::ostream& o, const Matching& m) { m.print(o); return o; }
};

#endif
