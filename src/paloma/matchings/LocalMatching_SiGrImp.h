#ifndef LOCALMATCHINGSGIMP_H
#define LOCALMATCHINGSGIMP_H

#include <set>

#include "LocalMatching.h"
#include "Matching_SiGrImp.h"

/*!
A SiteGraph and SegmentGraph implementation of a LocalMatching.
*/
class LocalMatching_SiGrImp: public virtual LocalMatching, public virtual Matching_SiGrImp
{
  protected:
	//! Holds the undefined weight of the segments.
	double segments_weight;
	//!	First prints all segments relations, after the site relations.
	virtual void print(std::ostream& aStream) const;

  public:
	//! The constructor
	LocalMatching_SiGrImp(const MultiSequence&);
	//! The destructor
	virtual ~LocalMatching_SiGrImp() {}
	/*!
		Adds a segment to the local matching.
		\param A segment.
		\return True if the segment added. False otherwise.
	*/
	virtual bool addSegment(const Segment&);
	/*!
		First it adds the relation to the site graph. Then it adds the borders and finally a relation between the sequences of the sites of the relation.
		\param A relation between sites.
		\return Always True.
	*/
	virtual bool addMatching(const SitePairRelation&);
	/*! \return Always False since its not allowed. */
	virtual bool addMatching(const Matching&);
	/*!
		First it adds all the segment relations and then it calls Matching_SiGrImp::addMatching for adding the site relations. Notice that each call to addMatching(SitePairRelation) made inside Matching_SiGrImp::addMatching calls the method of THIS class, since there is a "this->" preceding the call.
		\param A matching.
		\return Always True.
	*/
	virtual bool addMatching(const LocalMatching&);
	//! Undefined.
	virtual double segmentsWeight() const { return segments_weight; }
};

#endif
