#ifndef PAIRWISELOCALMATCHINGPSSGIMP_H
#define PAIRWISELOCALMATCHINGPSSGIMP_H

#include "PairwiseLocalMatching.h"
#include "LocalMatching_SiGrImp.h"
#include "PairwiseMatching_SiGrImp.h"

/*!
A SiteGraph and SegmentGraph implementation of a PairwiseLocalMatching.
*/
class PairwiseLocalMatching_PairSiGrImp: public PairwiseLocalMatching, public LocalMatching_SiGrImp, public PairwiseMatching_SiGrImp
{
  protected:
	//!	Just calls LocalMatching_PairSiGrImp::print.
	virtual void print(std::ostream& aStream) const;

  public:
	//! The constructor ignores the MultiSequence object and set the sequences' IDs that will be used.
	PairwiseLocalMatching_PairSiGrImp(const MultiSequence&, unsigned int, unsigned int, double);
	//! The destructor.
	virtual ~PairwiseLocalMatching_PairSiGrImp() {}
	/*!
		First checks if it's pairwise. If so, LocalMatching_SiGrImp::addMatching is called.
		\return True if it's pairwise. False otherwise.
	*/
	virtual bool addMatching(const SitePairRelation&);
	/*!
		First checks if it belongs to one of the two sequences. If so, LocalMatching_SiGrImp::addSegment is called.
		\return True if if it belongs to one of the two sequences and was not already in the matching. False otherwise.
	*/
	virtual bool addSegment(const Segment&);
	/*! \return Always False since its not allowed. */
	virtual bool addMatching(const Matching&);
	/*!
		It calls LocalMatching_SiGrImp::addMatching. If the sequences of the site relations in the parameter matching don't correspond to the sequences of this matching, they won't be added.
		\return Always True.
	*/
	virtual bool addMatching(const LocalMatching&);
};

#endif
