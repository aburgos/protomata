
#ifndef LOCALMATCHING_H
#define LOCALMATCHING_H

#include "Matching.h"

/*!
A pure abstract class. A LocalMatching is a set of relations between sites with no restriction. Each sequence involved in the local matching has one and only one segment. This segment is the smaller contiguous set of sites containing all the sites of the sequence in the local matching. There is a relation between two segments if and only if there is a relation between a site of one segment and a site of the other segment.
*/
class LocalMatching: public virtual Matching
{
  protected:
	//! The set of segments of the local matching.
	SegmentSet segments;

  public:
	//! The constructor.
	LocalMatching() {}
	//! The destructor.
	virtual ~LocalMatching() {}
	/*!
		Adds a segment to the local matching.
		\param A segment.
		\return True if the segment added. False otherwise.
	*/
	virtual bool addSegment(const Segment&) = 0;
	/*! \return Always False since its not allowed. */
	virtual bool addMatching(const Matching&) = 0;
	/*!
		Adds all segment pair relations and all site relations of the parameter to the local matching.
		\param A local matching.
		\return True if everything was added, False otherwise.
	*/
	virtual bool addMatching(const LocalMatching&) = 0;
	//!	Undefined.
	virtual double segmentsWeight() const = 0;
	//! Alias for the segment set iterator.
	typedef SegmentSet::iterator segment_iterator;
	/*! \return An iterator to the begin of the segment set. */
	virtual segment_iterator segment_begin() const { return segments.begin(); }
	/*! \return An iterator to the end of the segment set. */
	virtual segment_iterator segment_end() const { return segments.end(); }
};

#endif
