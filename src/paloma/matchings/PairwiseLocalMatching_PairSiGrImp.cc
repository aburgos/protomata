
#include "PairwiseLocalMatching_PairSiGrImp.h"

PairwiseLocalMatching_PairSiGrImp::PairwiseLocalMatching_PairSiGrImp(const MultiSequence& ms, unsigned int s2, unsigned int s1, double wgt): Matching_SiGrImp(ms), LocalMatching_SiGrImp(ms), PairwiseMatching_SiGrImp(ms, s1, s2)
{
	segments_weight = wgt;
}

bool PairwiseLocalMatching_PairSiGrImp::addMatching(const SitePairRelation& r)
{
	if(!PairwiseMatching_SiGrImp::isPairwise(r)) return false;
	return LocalMatching_SiGrImp::addMatching(r);
}

bool PairwiseLocalMatching_PairSiGrImp::addSegment(const Segment& seg)
{
	if(seg.sequence() != seq1 && seg.sequence() != seq2) return false;
	return LocalMatching_SiGrImp::addSegment(seg);
}

bool PairwiseLocalMatching_PairSiGrImp::addMatching(const Matching&)
{
	return false;
}

bool PairwiseLocalMatching_PairSiGrImp::addMatching(const LocalMatching& m)
{
	return LocalMatching_SiGrImp::addMatching(m);
}

void PairwiseLocalMatching_PairSiGrImp::print(std::ostream& aStream) const
{
	LocalMatching_SiGrImp::print(aStream);
}
