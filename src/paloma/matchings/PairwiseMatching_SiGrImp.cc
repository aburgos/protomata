
#include "PairwiseMatching_SiGrImp.h"

PairwiseMatching_SiGrImp::PairwiseMatching_SiGrImp(const MultiSequence& ms, unsigned int s1, unsigned int s2): Matching_SiGrImp(ms)
{
	seq1 = s1;
	seq2 = s2;
}

bool PairwiseMatching_SiGrImp::addMatching(const SitePairRelation& r)
{
	if(!this->isPairwise(r)) return false;
	else return Matching_SiGrImp::addMatching(r);
}

bool PairwiseMatching_SiGrImp::addMatching(const Matching& m)
{
	return Matching_SiGrImp::addMatching(m);
}

bool PairwiseMatching_SiGrImp::isPairwise(const SitePairRelation& r) const
{
	if(r.first().sequence() != seq1 && r.first().sequence() != seq2) return false;
	if(r.second().sequence() != seq1 && r.second().sequence() != seq2) return false;
	return true;
}
