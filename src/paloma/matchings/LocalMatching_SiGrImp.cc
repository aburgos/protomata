#include "LocalMatching_SiGrImp.h"

LocalMatching_SiGrImp::LocalMatching_SiGrImp(const MultiSequence& ms): Matching_SiGrImp(ms)
{
	segments_weight = 0;
}

bool LocalMatching_SiGrImp::addSegment(const Segment& seg)
{
	// should I check that the segment is legal?
	return segments.insert(seg).second;
}

bool LocalMatching_SiGrImp::addMatching(const SitePairRelation& r)
{
	// should I check that each site is in a segment?
	return Matching_SiGrImp::addMatching(r);
}

bool LocalMatching_SiGrImp::addMatching(const Matching&)
{
	return false;
}

bool LocalMatching_SiGrImp::addMatching(const LocalMatching& m)
{
	for(segment_iterator it = m.segment_begin(); it != m.segment_end(); it++)
		this->addSegment(*it);
	return Matching_SiGrImp::addMatching(m);
}

void LocalMatching_SiGrImp::print(std::ostream& aStream) const
{
	for(segment_iterator it = segment_begin(); it != segment_end(); it++)
		aStream << *it << std::endl;
	Matching_SiGrImp::print(aStream);
}
