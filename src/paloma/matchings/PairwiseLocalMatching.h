
#ifndef PAIRWISELOCALMATCHING_H
#define PAIRWISELOCALMATCHING_H

#include "PairwiseMatching.h"
#include "LocalMatching.h"

/*!
A PairwiseLocalMatching is the sum of the restrictions applied by PairwiseMatching and LocalMatching to a Matching.
*/
class PairwiseLocalMatching: public virtual LocalMatching, public virtual PairwiseMatching
{
  public:
	//! The constructor.
	PairwiseLocalMatching() {}
	//! The desctructor.
	virtual ~PairwiseLocalMatching() {}
};

#endif
