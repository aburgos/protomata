
#ifndef MATCHINGSGIMP_H
#define MATCHINGSGIMP_H

#include "Matching.h"
#include "MultiSequence.h"
#include "iterators/GraphSiteIterator.h"
#include "iterators/GraphRelationIterator.h"

/*!
A SiteGraph implementation of a Matching.
*/
class Matching_SiGrImp: public virtual Matching
{
  protected:
	//! Holds the undefined weight of the sites.
	double sites_weight;
	//! A graph representing the site relations.
	SiteGraph siteGraph;
	//!	Definition of the output (cout) operator for a Matching_SiGrImp.
	virtual void print(std::ostream& aStream) const;

  public:
	//! The constructor ignores the MultiSequence, its signature is for homogeneity (except pairwise classes).
	Matching_SiGrImp(const MultiSequence&);
	//! The destructor.
	virtual ~Matching_SiGrImp() {}
	//! Number of sites.
	unsigned int sites_size() const { return siteGraph.vertices(); }
	//! Number of relations.
	unsigned int relations_size() const { return siteGraph.edges(); }
	/*!
		Adds a relation of sites to the matching, adding an edge between the sites in the sites graph.
		\param A relation between sites.
		\return True if the relation was added. False otherwise.
	*/
	virtual bool addMatching(const SitePairRelation&);
	/*!
		Iterates through the matching parameter adding each site pair relation to this matching. If any of these is not added, it continues with the others, since the relation can exist already. It is assumed that every relation between two sites have the same value.
		\param A matching.
		\return Always True.
	*/
	virtual bool addMatching(const Matching&);
	//!	Undefined. It is 0 for now, initialize in the constructor and never changed.
	virtual double sitesWeight() const { return sites_weight; }
	/*!
		Checks if a specific relation exists in the matching. It checks for an edge between the sites of the relation, without caring about the weight, that is supposed to be the same for all relations betweem these sites.
		\param A site pair relation.
		\return True if the relation exists. False otherwise.
	*/
	virtual bool hasSiteRelation(const SitePairRelation&) const;
	/*!
		Gets a visitor to access the all the sites of this matching. Implemented by a GraphSiteIterator.
		\return A visitor to all the sites of this matching.
	*/
	virtual site_visitor sites() const;
	/*!
		Gets a visitor to access the all the relations of this matching. Implemented by a GraphRelationIterator.
		\return A visitor to all the relations of this matching.
	*/
	virtual relation_visitor site_relations() const;
};

#endif
