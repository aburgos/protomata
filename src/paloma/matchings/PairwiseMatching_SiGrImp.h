
#ifndef PAIRWISEMATCHINGSGIMP_H
#define PAIRWISEMATCHINGSGIMP_H

#include "Matching_SiGrImp.h"
#include "PairwiseMatching.h"

/*!
A SiteGraph implementation of a PairwiseMatching.
*/
class PairwiseMatching_SiGrImp: public virtual PairwiseMatching, public virtual Matching_SiGrImp
{
  protected:
	//! Holds the sequence's IDs of the sequences specified when constructed.
	unsigned int seq1, seq2;
	/*!
		Checks that each site's sequence is already a sequence of the pairwise matching.
		\return True if the sequences involved in the site relation are included in the sequences of the pairwise matching. False otherwise.
	*/
	virtual bool isPairwise(const SitePairRelation&) const;

  public:
	//! The constructor ignores the MultiSequence object and set the sequences' IDs that will be used.
	PairwiseMatching_SiGrImp(const MultiSequence&, unsigned int, unsigned int);
	//! The destructor.
	virtual ~PairwiseMatching_SiGrImp() {}
	/*!
		First checks if the site relation being added is pairwise. In that case, an edge to the site graph is added.
		\return True if it's pairwise. False otherwise.
	*/
	virtual bool addMatching(const SitePairRelation&);
	/*!
		It calls Matching_SiGrImp::addMatching.
		\param A matching.
		\return Always True.
	*/
	virtual bool addMatching(const Matching&);
};

#endif
