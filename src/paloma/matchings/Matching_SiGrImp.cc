
#include "Matching_SiGrImp.h"

Matching_SiGrImp::Matching_SiGrImp(const MultiSequence&)
{
	sites_weight = 0;
}

bool Matching_SiGrImp::addMatching(const SitePairRelation& r)
{
	return siteGraph.addEdge(r.first(), r.second(), r.value());
}

bool Matching_SiGrImp::addMatching(const Matching& m)
{
	relation_visitor v = m.site_relations();
	while(v.valid())
	{
		//if(!this->addMatching(v.get())) return false;
		this->addMatching(v.get());
		v.next();
	}
	return true;
}

bool Matching_SiGrImp::hasSiteRelation(const SitePairRelation& r) const
{
	return (siteGraph.hasEdge(r.first(), r.second()) != siteGraph.relation_end());
}

site_visitor Matching_SiGrImp::sites() const
{
	site_iterator* it = new GraphSiteIterator(siteGraph.vertex_begin());
	site_iterator* end = new GraphSiteIterator(siteGraph.vertex_end());
	return site_visitor(it, end);
}

relation_visitor Matching_SiGrImp::site_relations() const
{
	relation_iterator* it = new GraphRelationIterator(siteGraph.relation_begin());
	relation_iterator* end = new GraphRelationIterator(siteGraph.relation_end());
	return relation_visitor(it, end);
}

void Matching_SiGrImp::print(std::ostream& aStream) const
{
	aStream << siteGraph;
}
