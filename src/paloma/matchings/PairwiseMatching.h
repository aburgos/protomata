
#ifndef PAIRWISEMATCHING_H
#define PAIRWISEMATCHING_H

#include "Matching.h"

/*!
A PairwiseMatching is a Matching with the condition that there are only two sequences involved in the sites relations.
*/
class PairwiseMatching: public virtual Matching
{
  protected:
	/*!
		Checks whether a site pair relation can be added to the pairwise matching.
		\return True if the sequences involved in the site relation are included in the sequences of the pairwise matching. False otherwise.
	*/
	virtual bool isPairwise(const SitePairRelation&) const = 0;

  public:
	//! The constructor.
	PairwiseMatching() {}
	//! The destructor.
	virtual ~PairwiseMatching() {}
};

#endif
