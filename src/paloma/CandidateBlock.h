
#ifndef CANDIDATEBLOCK_H
#define CANDIDATEBLOCK_H

#include "Typedefs.h"

/*!
A CandidateBlock is a structure for making more efficient the generation of a candidate. The candidate will be compose from pairwise matchings.
*/
class CandidateBlock
{
  public:
	//! The current segments composing the candidate block.
	SegmentSet segments;
	//! The segments which are candidates for being added to the candidate block.
	SegmentSet candidates;
	//! A set of sequences which already have a segment composing the candidate block, i.e. in the segments variable.
	std::set<unsigned int> usedSeqs;
	//! A set of pointers to the pairwise local matchings composing the candidate block.
	std::set<const PairwiseLocalMatching*> matchings;
	//! The weight of the block is the sum of the weight of each pairwise local matching.
	double weight;
	//! The constructor.
	CandidateBlock(): weight(0) {}
	/*!
		An order operator between blocks. It compares each segment of each candidate block.
		/return True if a block is prior to the other one. False otherwise.
	*/
	bool operator<(const CandidateBlock& s) const
	{
		if(segments.size() < s.segments.size()) return true;
		else if(segments.size() > s.segments.size()) return false;
		else
		{
			SegmentSet::const_iterator fit = segments.begin(), sit = s.segments.begin();
			while(fit != segments.end() && sit != s.segments.end())
			{
				if(*fit < *sit) return true;
				else if(*fit == *sit) { ++fit; ++sit; }
				else return false;
			}
		}

		return false;
	}
	/*!
		The comparison (equal) operator between two candidate blocks.
		/return True if each segment of a candidate block is equal to each segment of the other one. The order of the segments determines which ones are compared. False otherwise.
	*/
	bool operator==(const CandidateBlock& s) const
	{
		if(segments.size() != s.segments.size()) return false;
		SegmentSet::const_iterator fit = segments.begin(), sit = s.segments.begin();
		while(fit != segments.end() && sit != s.segments.end())
		{
			if(*fit != *sit) return false;
			++fit; ++sit;
		}

		return true;
	}
	/*!
		The comparison (not equal) operator between two candidate blocks.
		\return The negation of the equal operator.
	*/
	bool operator!=(const CandidateBlock& s) const
	{
		return !(operator==(s));
	}
	/*!
		Checks if the candidate block is empty.
		\return True if no pairwise matching was added. False otherwise.
	*/
	bool isEmpty()
	{
		return (segments.size() < 2);
	}
	//!	Definition of the output (cout) operator for a Site.
	friend std::ostream& operator<<(std::ostream& aStream, const CandidateBlock& b)
	{
		aStream << "CandidateBlock weight = " << b.weight << std::endl;
		aStream << "Segments:" << std::endl;
		for(SegmentSet::const_iterator it = b.segments.begin(); it != b.segments.end(); ++it)
			aStream << *it << std::endl;
		if(b.candidates.size() > 0)
		{
			aStream << "Candidates:" << std::endl;
			for(SegmentSet::const_iterator it = b.candidates.begin(); it != b.candidates.end(); ++it)
				aStream << *it << std::endl;
		}
		return aStream;
	}
};

#endif
