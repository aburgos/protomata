
#ifndef PALOMAALLOWEDPARAMETERS_H
#define PALOMAALLOWEDPARAMETERS_H

#include "tclap/CmdLine.h"

class PalomaAllowedParameters
{
  private:
	std::vector<std::string> block_mode_values;
	std::vector<std::string> block_generator_values;
	std::vector<std::string> seed_generator_values;
	std::vector<std::string> generalized_set_values;
	std::vector<std::string> score_type_values;
	std::vector<std::string> output_type_values;
	std::vector<std::string> discard_segments_values;
	std::vector<std::string> discard_fragments_values;

  public:
	PalomaAllowedParameters();

	TCLAP::ValuesConstraint<std::string>* block_mode_values_constraints();
	TCLAP::ValuesConstraint<std::string>* block_generator_values_constraints();
	TCLAP::ValuesConstraint<std::string>* seed_generator_values_constraints();
	TCLAP::ValuesConstraint<std::string>* generalized_set_values_constraints();
	TCLAP::ValuesConstraint<std::string>* score_type_values_constraints();
	TCLAP::ValuesConstraint<std::string>* output_type_values_constraints();
	TCLAP::ValuesConstraint<std::string>* discard_segments_values_constraints();
	TCLAP::ValuesConstraint<std::string>* discard_fragments_values_constraints();
};

#endif
