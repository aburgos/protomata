
#ifndef MULTISEQUENCE_H
#define MULTISEQUENCE_H

#include <map>
#include <set>
#include <cassert>
#include "Segment.h"
#include "Site.h"
#include "parsers/FastaParser.h"
#include "parsers/ManualWeightsParser.h"
#include "parsers/ClustalWeightsParser.h"

/*!
A MultiSequence contains all the sequences read from a file. 
*/
class MultiSequence
{
  private:
	std::vector<Sequence> m_sequences;
	//! For providing access to sequences through tags.
	std::map<std::string, unsigned int> m_position;
	//! Function for removing a set of given characters from a string.
	std::string remove_chars(const std::string& str, const std::string& rmv, bool) const;
	//! Resolve ambiguities on sequence data. It assigns the amino acid with higher background probability.
	bool resolve_ambiguities(std::string&, std::string&, bool);
	//! Add truncated tags to mapping to positions.
	void add_tags2position();

  public:
	//! The default constructor.
	MultiSequence() {}
	//! A constructor from a vector of sequences.
	MultiSequence(const std::vector<Sequence>&);
	/*!
		A constructor from a fasta file.
		\param If TRUE, building mode. If FALSE, scanning mode.
		\param If FALSE, don't allow X's on sequences.
		\param Lower bound for sequence's length filtering.
		\param Upper bound for sequence's length filtering.
		\param If TRUE, prints amino acids changes in sequences and a resume of rejected and accepted sequences.
	*/
	MultiSequence(const std::string&, bool = true, bool = true, unsigned int = std::numeric_limits<unsigned int>::min(), unsigned int = std::numeric_limits<unsigned int>::max(), bool = false);

	/*! \return Access to a Sequence from its position on the parsed fasta file. */
	const Sequence& operator[](unsigned int pos) const;
	/*! \return Access to a Sequence from its truncated tag on the parsed fasta file. */
	const Sequence& operator[](const std::string&) const;
	/*! \return The string corresponding to the segment. */
	std::string operator[](const Segment& s) const;
	/*! \return The character corresponding to the site. */
	char operator[](const Site& s) const;

	/*!	\return The number of Sequence stored. */
	unsigned int size() const { return m_sequences.size(); }

	//! Weight sequences using clustal.
	void weight_sequences();
	//! Take user defined sequences' weights.
	void weight_sequences(const std::string&);

	typedef std::vector<Sequence>::const_iterator const_iterator;
	const_iterator begin() const { return m_sequences.begin(); }
	const_iterator end() const { return m_sequences.end(); }

	//!	Outputs tag and data of each Sequence in FASTA format.
	friend std::ostream& operator<<(std::ostream& aStream, const MultiSequence& ms)
	{
		for(MultiSequence::const_iterator it = ms.begin(); it != ms.end(); ++it)
			aStream << *it;
		return aStream;
	}
};

#endif
