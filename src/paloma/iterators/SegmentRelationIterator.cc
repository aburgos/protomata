
#include "SegmentRelationIterator.h"

SegmentRelationIterator::SegmentRelationIterator(SegmentSet::const_iterator begin, SegmentSet::const_iterator end)
{
	for(SegmentSet::const_iterator it1 = begin; it1 != end; ++it1)
	{
		SegmentSet::const_iterator it2 = it1;
		for(++it2; it2 != end; ++it2)
			segmentGraph.addEdge(*it1, *it2, 0);
	}

	it = segmentGraph.relation_begin();
}

std::pair<Segment, Segment> SegmentRelationIterator::operator*() const
{
	const SegmentPairRelation& r = *it;
	return std::make_pair(r.first(), r.second());
}

SegmentRelationIterator& SegmentRelationIterator::operator++()
{
	++it;
	return *this;
}

SegmentRelationIterator SegmentRelationIterator::operator++(int)
{
	SegmentRelationIterator tmp = *this;
	it++;
	return tmp;
}

bool SegmentRelationIterator::operator==(const SegmentRelationIterator& iter) const
{
	return (it == iter.it);
}

bool SegmentRelationIterator::operator!=(const SegmentRelationIterator& iter) const
{
	return !(operator==(iter));
}

