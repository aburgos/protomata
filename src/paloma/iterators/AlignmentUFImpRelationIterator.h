
#ifndef UNIONFINDRELATIONITERATOR_H
#define UNIONFINDRELATIONITERATOR_H

#include <set>
#include <typeinfo>
#include "lib/UnionFind.h"
#include "SiteRelationIterator.h"
#include "alignments/Alignment_UFImp.h"

class Alignment_UFImp;

/*!
Iterates through the Site relations of a Alignment_UFImp object. The implementation of the site relations in the alignment does not store a weight between them, so this iterator would create an arbitrary weight when it is asked for a SitePairRelation.
*/
class AlignmentUFImpRelationIterator: public SiteRelationIterator
{
  private:
	//! A pointer to the Alignment_UFImp object to traverse.
	const Alignment_UFImp* alignment;
	//! Stores every partition of the alignment.
	UnionFind::Partition partition;
	//! An iterator to access the partition.
	UnionFind::Partition::iterator main_it;
	//! Others iterators needed to traverse the partition.
	UnionFind::PartitionElement::iterator sec_it, cpy_it;

  protected:
	/*! \return True if all partition were traversed. False otherwise. */
	bool atEnd() const { return main_it == partition.end(); }

  public:
	//! The constructor initialized the private variables in a complicated way.
	AlignmentUFImpRelationIterator(const Alignment_UFImp* alig, bool end);
	/*! \return The current site pair relation. The weight of the site relation is always 0. */
	SitePairRelation operator*() const;
	//! Increments (pre) the iterator.
	void operator++();
	//AlignmentUFImpRelationIterator operator++(int);
	/*!
		The equal comparisson operator. First it checks if there is one iterator at its end. If non of both are, the iterator variables are compared.
		\return True if both private variables are equal. False otherwise.
	*/
	bool operator==(const SiteRelationIterator* it) const;
	/*!
		The unequal comparisson operator.
		\return The opposite of the equal operator.
	*/
	bool operator!=(const SiteRelationIterator* it) const;
};

#endif
