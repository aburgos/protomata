
#ifndef SEGMENTRELATIONITERATOR_H
#define SEGMENTRELATIONITERATOR_H

#include "Typedefs.h"

/*!
Abstract class for a segment iterator. The increment operator does not return something of the same type since the iterators hierarchy makes it impossible. Because of this, the pos-increment operator is disable, since its not possible to implement, or at least I couldn't find a way to do it.
*/
class SegmentRelationIterator
{
  private:
	SegmentGraph segmentGraph;
	SegmentGraph::relation_iterator it;

  public:
	//! The constructor.
	SegmentRelationIterator(SegmentSet::const_iterator, SegmentSet::const_iterator);
	//! The destructor.
	~SegmentRelationIterator() {}
	//! The dereference operator.
	std::pair<Segment, Segment> operator*() const;
	//! The pre-increment operator.
	SegmentRelationIterator& operator++();
	//! The pos-increment operator.
	SegmentRelationIterator operator++(int);
	//! The equal comparisson operator.
	bool operator==(const SegmentRelationIterator& it) const;
	//! The unequal comparisson operator.
	bool operator!=(const SegmentRelationIterator& it) const;
};

typedef SegmentRelationIterator segment_pair_iterator;

#endif
