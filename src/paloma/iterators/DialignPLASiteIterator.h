
#ifndef DIALIGNSITEITERATOR_H
#define DIALIGNSITEITERATOR_H

#include <typeinfo>
#include "SiteRelationIterator.h"
#include "alignments/DialignPLA_Imp.h"

class DialignPLA_Imp;

/*!
Iterates through the Site relations of a DialignPLA object. The weight of the site relations is undefined.
*/
class DialignPLASiteIterator: public SiteIterator
{
  private:
	//! Keeps the position of the iterator on a fragment.
	unsigned int iterator;
	//! Pointer to a DialignPLA default implementation.
	const DialignPLA_Imp* dpla;

  public:
	//! The constructor takes a pointer of the object to traverse and a flag indicating whether this iterator will be a begin or an end iterator.
	DialignPLASiteIterator(const DialignPLA_Imp* dial, bool end);
	//! The destructor.
	virtual ~DialignPLASiteIterator() {}
	/*! \return The current site. */
	Site operator*() const;
	//! Increments (pre) the iterator.
	void operator++();
	//void operator++(int);
	/*!
		The equal comparisson operator. It compares the private variable called iterator to determine if two iterators are equal. It does not check whether the DialignPLA_Imp object pointed by this class is the same as the one pointed by the parameter object. It does make a dynamic cast to check if the iterators are of the same type.
		\return True if both' private variable iterator is at the same position. False otherwise.
	*/
	bool operator==(const SiteIterator* it) const;
	/*!
		The unequal comparisson operator.
		\return The opposite of the equal operator.
	*/
	bool operator!=(const SiteIterator* it) const;
};

#endif
