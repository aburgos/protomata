
#include "DialignPLASiteIterator.h"

DialignPLASiteIterator::DialignPLASiteIterator(const DialignPLA_Imp *dial, bool end)
{
	dpla = dial;
	if(end) iterator = dpla->seg1.length() + dpla->seg2.length();
	else iterator = 0;
}

Site DialignPLASiteIterator::operator*() const
{
	assert(iterator < dpla->seg1.length() + dpla->seg2.length());

	if(iterator < dpla->seg1.length())
		return Site(dpla->seg1.sequence(), dpla->seg1.begin() + iterator);
	else
		return Site(dpla->seg2.sequence(), dpla->seg2.begin() + iterator - dpla->seg1.length());
}

void DialignPLASiteIterator::operator++()
{
	++iterator;
}

bool DialignPLASiteIterator::operator==(const SiteIterator* it) const
{
	try
	{
		const DialignPLASiteIterator* casted_it = dynamic_cast<const DialignPLASiteIterator*>(it);
		return (iterator == casted_it->iterator);
	}
	catch(const std::bad_cast& e)
	{
		std::cerr << "\033[1;31m" << e.what() << "\033[m";
		std::cerr << " -- trying to compare a DialignPLASiteIterator with a ";
		std::cerr << typeid(it).name() << std::endl;
		return false;
	}
}

bool DialignPLASiteIterator::operator!=(const SiteIterator* it) const
{
	return !(operator==(it));
}
