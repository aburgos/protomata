
#ifndef VISITOR_H
#define VISITOR_H

#include "SiteIterator.h"
#include "SiteRelationIterator.h"

/*!
Makes possible to iterate through the range [begin, end) of a class.
*/
template <typename ItT, typename ObjT>
class Visitor
{
  private:
	//! A pointer to an iterator.
	ItT* it;
	//! A pointer to an end iterator.
	ItT* end;

  public:
	//! The constuctor assigns the beginning and the end iterators. The class owns them.
	Visitor(ItT* _b, ItT* _e): it(_b), end(_e) {}
	//! The destructor deletes the objects took by the constructor.
	~Visitor() { delete it; delete end; }
	//! Gets the current object referenced by the it variable.
	ObjT get() { return it->operator*(); }
	//! Increment the it variable.
	void next() { it->operator++(); }
	//! Check if the it variable is valid or not.
	bool valid() { return it->operator!=(end); }
};

typedef Visitor<site_iterator, Site> site_visitor;
typedef Visitor<relation_iterator, SitePairRelation> relation_visitor;

#endif
