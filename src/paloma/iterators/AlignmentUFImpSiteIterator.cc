
#include "AlignmentUFImpSiteIterator.h"

AlignmentUFImpSiteIterator::AlignmentUFImpSiteIterator(const Alignment_UFImp* alig, bool end)
{
	alignment = alig;

	if(end)
	{
		main_it = partition.end();
	}
	else
	{
		// get partition from union find structure
		partition = alignment->union_find.partition();

		if(partition.size() > 0)
		{
			main_it = partition.begin();
			sec_it = (*main_it).begin();
		}
		else
		{
			main_it = partition.end();
		}
	}
}

Site AlignmentUFImpSiteIterator::operator*() const
{
	assert(main_it != partition.end());
	assert(sec_it != (*main_it).end());

	const Site& s = alignment->uint2site(*sec_it);
	return s;
}

void AlignmentUFImpSiteIterator::operator++()
{
	++sec_it;
	if(sec_it == (*main_it).end())
	{
		++main_it;
		if(main_it != partition.end())
			sec_it = (*main_it).begin();
	}
}

bool AlignmentUFImpSiteIterator::operator==(const SiteIterator* it) const
{
	try
	{
		const AlignmentUFImpSiteIterator* cit = dynamic_cast<const AlignmentUFImpSiteIterator*>(it);

		if(cit->atEnd() && atEnd()) return true;
		else if(cit->atEnd()) return false;
		else if(atEnd()) return false;

		return (main_it == cit->main_it && sec_it == cit->sec_it);
	}
	catch(const std::bad_cast& e)
	{
		std::cerr << "\033[1;31m" << e.what() << "\033[m";
		std::cerr << " -- trying to compare a AlignmentUFImpSiteIterator with a ";
		std::cerr << typeid(it).name() << std::endl;
		return false;
	}
}

bool AlignmentUFImpSiteIterator::operator!=(const SiteIterator* it) const
{
	return !(operator==(it));
}
