
#ifndef GRAPHRELATIONITERATOR_H
#define GRAPHRELATIONITERATOR_H

#include <typeinfo>
#include "SiteRelationIterator.h"

/*!
This class acts as a proxy for a SiteGraph::relation_iterator, and also mantains the hierarchy.
*/
class GraphRelationIterator: public SiteRelationIterator
{
  private:
	//! Internal iterator.
	SiteGraph::relation_iterator siteGraphIter;

  public:
	//! The constructor assigns the passed SiteGraph::relation_iterator.
	GraphRelationIterator(SiteGraph::relation_iterator sg): siteGraphIter(sg) {}
	//! Calls the internal iterator's dereference operator.
	SitePairRelation operator*() const { return siteGraphIter.operator*(); }
	//! Calls the internal iterator's pre-increment operator.
	virtual void operator++() { siteGraphIter.operator++(); }
	//virtual void operator++(int) = 0;
	/*!
		The equal comparisson operator. It compares the internals iterators.
		\return True if both internal iterators are equal. False otherwise.
	*/
	virtual bool operator==(const SiteRelationIterator* it) const
	{
		try
		{
			const GraphRelationIterator* casted_it = dynamic_cast<const GraphRelationIterator*>(it);
			return siteGraphIter.operator==(casted_it->siteGraphIter);
		}
		catch(const std::bad_cast& e)
		{
			std::cerr << "\033[1;31m" << e.what() << "\033[m";
			std::cerr << " -- trying to compare a GraphSiteIterator with a ";
			std::cerr << typeid(it).name() << std::endl;
			return false;
		}
	}
	/*!
		The unequal comparisson operator.
		\return The opposite of the equal operator.
	*/
	virtual bool operator!=(const SiteRelationIterator* it) const { return !(operator==(it)); }
};

#endif
