
#ifndef GRAPHSITEITERATOR_H
#define GRAPHSITEITERATOR_H

#include <typeinfo>
#include "SiteIterator.h"

/*!
This class acts as a proxy for a SiteGraph::site_iterator, and also mantains the hierarchy.
*/
class GraphSiteIterator: public SiteIterator
{
  private:
	//! Internal iterator.
	SiteGraph::vertex_iterator siteGraphIter;

  public:
	//! The constructor assigns the passed SiteGraph::site_iterator.
	GraphSiteIterator(SiteGraph::vertex_iterator sg): siteGraphIter(sg) {}
	//! Calls the internal iterator's dereference operator.
	Site operator*() const { return siteGraphIter.operator*(); }
	//! Calls the internal iterator's pre-increment operator.
	virtual void operator++() { siteGraphIter.operator++(); }
	//virtual void operator++(int) = 0;
	/*!
		The equal comparisson operator. It compares the internals iterators.
		\return True if both internal iterators are equal. False otherwise.
	*/
	virtual bool operator==(const SiteIterator* it) const
	{
		try
		{
			const GraphSiteIterator* casted_it = dynamic_cast<const GraphSiteIterator*>(it);
			return siteGraphIter.operator==(casted_it->siteGraphIter);
		}
		catch(const std::bad_cast& e)
		{
			std::cerr << "\033[1;31m" << e.what() << "\033[m";
			std::cerr << " -- trying to compare a GraphSiteIterator with a ";
			std::cerr << typeid(it).name() << std::endl;
			return false;
		}
	}
	/*!
		The unequal comparisson operator.
		\return The opposite of the equal operator.
	*/
	virtual bool operator!=(const SiteIterator* it) const { return !(operator==(it)); }
};

#endif
