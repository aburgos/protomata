
#include "DialignPLARelationIterator.h"

DialignPLARelationIterator::DialignPLARelationIterator(const DialignPLA_Imp *dial, bool end)
{
	dpla = dial;
	if(end) iterator = dpla->seg1.length();
	else iterator = 0;
}

SitePairRelation DialignPLARelationIterator::operator*() const
{
	assert(iterator < dpla->seg1.length());

	Site s1 (dpla->seg1.sequence(), dpla->seg1.begin() + iterator);
	Site s2 (dpla->seg2.sequence(), dpla->seg2.begin() + iterator);
	double wgt = 0;

	// create relation object
	SitePairRelation r (std::make_pair(s1, s2), wgt);
	return r;
}

void DialignPLARelationIterator::operator++()
{
	++iterator;
}

bool DialignPLARelationIterator::operator==(const SiteRelationIterator* it) const
{
	try
	{
		const DialignPLARelationIterator* casted_it = dynamic_cast<const DialignPLARelationIterator*>(it);
		return (iterator == casted_it->iterator);
	}
	catch(const std::bad_cast& e)
	{
		std::cerr << "\033[1;31m" << e.what() << "\033[m";
		std::cerr << " -- trying to compare a DialignPLARelationIterator with a ";
		std::cerr << typeid(it).name() << std::endl;
		return false;
	}
}

bool DialignPLARelationIterator::operator!=(const SiteRelationIterator* it) const
{
	return !(operator==(it));
}
