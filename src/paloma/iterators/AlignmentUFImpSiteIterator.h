
#ifndef UNIONFINDSITEITERATOR_H
#define UNIONFINDSITEITERATOR_H

#include <set>
#include <typeinfo>
#include "lib/UnionFind.h"
#include "SiteIterator.h"
#include "alignments/Alignment_UFImp.h"

class Alignment_UFImp;

/*!
Iterates through the sites of a Alignment_UFImp object.
*/
class AlignmentUFImpSiteIterator: public SiteIterator
{
  private:
	//! A pointer to the Alignment_UFImp object to traverse.
	const Alignment_UFImp* alignment;
	//! Stores every partition of the alignment.
	UnionFind::Partition partition;
	//! An iterator to access the partition.
	UnionFind::Partition::iterator main_it;
	//! Secondary iterator needed to traverse the partition.
	UnionFind::PartitionElement::iterator sec_it;

  protected:
	/*! \return True if all partition were traversed. False otherwise. */
	bool atEnd() const { return main_it == partition.end(); }

  public:
	//! The constructor initialized the private variables in a complicated way.
	AlignmentUFImpSiteIterator(const Alignment_UFImp* alig, bool end);
	/*! \return The current site. */
	Site operator*() const;
	//! Increments (pre) the iterator.
	void operator++();
	//AlignmentUFImpSiteIterator operator++(int);
	/*!
		The equal comparisson operator. First it checks if there is one iterator at its end. If non of both are, the iterator variables are compared.
		\return True if both private variables are equal. False otherwise.
	*/
	bool operator==(const SiteIterator* it) const;
	/*!
		The unequal comparisson operator.
		\return The opposite of the equal operator.
	*/
	bool operator!=(const SiteIterator* it) const;
};

#endif
