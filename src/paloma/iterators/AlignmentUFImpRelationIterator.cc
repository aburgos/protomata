
#include "AlignmentUFImpRelationIterator.h"

AlignmentUFImpRelationIterator::AlignmentUFImpRelationIterator(const Alignment_UFImp* alig, bool end)
{
	alignment = alig;

	if(end)
	{
		main_it = partition.end();
	}
	else
	{
		// get partition from union find structure
		partition = alignment->union_find.partition();

		if(partition.size() > 0)
		{
			main_it = partition.begin();
			sec_it = (*main_it).begin();
			cpy_it = sec_it;
			if((*main_it).size() > 1) ++cpy_it;
		}
		else
		{
			main_it = partition.end();
		}
	}
}

SitePairRelation AlignmentUFImpRelationIterator::operator*() const
{
	assert(main_it != partition.end());
	assert(sec_it != (*main_it).end());
	assert(cpy_it != (*main_it).end());

	const Site& s1 = alignment->uint2site(*sec_it);
	const Site& s2 = alignment->uint2site(*cpy_it);

	double wgt = 0;

	// create relation object
	SitePairRelation r (std::make_pair(s1, s2), wgt);
	return r;
}

void AlignmentUFImpRelationIterator::operator++()
{
	++cpy_it;
	if(cpy_it == (*main_it).end())
	{
		++sec_it;
		if(sec_it == (*main_it).end())
		{
			++main_it;
			if(main_it != partition.end())
			{
				sec_it = (*main_it).begin();
				cpy_it = sec_it;
				if((*main_it).size() > 1) ++cpy_it;
			}
		}
		else
		{
			cpy_it = sec_it;
			operator++();
		}
	}
}

bool AlignmentUFImpRelationIterator::operator==(const SiteRelationIterator* it) const
{
	try
	{
		const AlignmentUFImpRelationIterator* cit = dynamic_cast<const AlignmentUFImpRelationIterator*>(it);

		if(cit->atEnd() && atEnd()) return true;
		else if(cit->atEnd()) return false;
		else if(atEnd()) return false;

		return (main_it == cit->main_it && sec_it == cit->sec_it && cpy_it == cit->cpy_it);
	}
	catch(const std::bad_cast& e)
	{
		std::cerr << "\033[1;31m" << e.what() << "\033[m";
		std::cerr << " -- trying to compare a AlignmentUFImpRelationIterator with a ";
		std::cerr << typeid(it).name() << std::endl;
		return false;
	}
}

bool AlignmentUFImpRelationIterator::operator!=(const SiteRelationIterator* it) const
{
	return !(operator==(it));
}
