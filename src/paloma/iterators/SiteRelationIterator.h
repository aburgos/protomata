
#ifndef SITERELATIONITERATOR_H
#define SITERELATIONITERATOR_H

#include "Typedefs.h"

/*!
Abstract class for a site relation iterator. The increment operator does not return something of the same type since the iterators hierarchy makes it impossible. Because of this, the pos-increment operator is disable, since its not possible to implement, or at least I couldn't find a way to do it.
*/
class SiteRelationIterator
{
  public:
	//! The constructor.
	SiteRelationIterator() {}
	//! The destructor.
	virtual ~SiteRelationIterator() {};
	//! The dereference operator.
	virtual SitePairRelation operator*() const = 0;
	//! The pre-increment operator.
	virtual void operator++() = 0;
	//virtual void operator++(int) = 0;
	//! The equal comparisson operator.
	virtual bool operator==(const SiteRelationIterator* it) const = 0;
	//! The unequal comparisson operator.
	virtual bool operator!=(const SiteRelationIterator* it) const = 0;
};

typedef SiteRelationIterator relation_iterator;

#endif
